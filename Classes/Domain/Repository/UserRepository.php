<?php
namespace Maagit\Maagitproduct\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Repository
	class:				UserRepository

	description:		Repository for the typo3 frontend users.

	created:			2024-02-14
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2024-02-14	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class UserRepository extends \Maagit\Maagitproduct\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
    /**
     * @var \TYPO3\CMS\Core\Database\Connection
     */
    protected $connection;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function initializeObject()
	{
		parent::initializeObject();
		$this->objectType = 'Maagit\Maagitproduct\Domain\Model\User';
		$this->connection = $this->makeInstance('TYPO3\CMS\Core\Database\ConnectionPool')->getConnectionForTable($this->getUserTable());
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Get typo3 tablename of frontend user's
     *
     * @param	-
	 * @return	string						the table name
     */
	protected function getUserTable()
	{
		$feUser = $this->makeInstance('TYPO3\\CMS\\Frontend\\Authentication\\FrontendUserAuthentication');
		return $feUser->user_table;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
