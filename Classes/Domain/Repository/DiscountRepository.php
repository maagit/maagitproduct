<?php
namespace Maagit\Maagitproduct\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Repository
	class:				DiscountRepository

	description:		Repository for the "discount" model.
						Get the data of tx_maagitproduct_domain_model_discount.

	created:			2022-05-31
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-05-31	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class DiscountRepository extends \Maagit\Maagitproduct\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Initialize the object, set default values
     *
     */
	public function initializeObject() {
		// initialization things
		parent::initializeObject();
		$this->objectType = 'Maagit\Maagitproduct\Domain\Model\Discount';

		// set default query settings
		$querySettings = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($querySettings);
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Create a new object
     *
	 * @param	$arguments		array										variable arguments
	 * @return 					\Maagit\Maagitproduct\Domain\Model\Base		domain model object
     */
	public function create(...$arguments)
	{
		$discountObj = parent::create();
		$record = $arguments[0];
		$propertyService = $this->makeInstance('Maagit\Maagitproduct\Service\Property\PropertyService');
		$transformService = $this->makeInstance('Maagit\Maagitproduct\Service\Content\TransformService');
		$discountObj = $propertyService->setProperties($discountObj, $record);
		$discountObj->setDiscountperarticle($this->getValidArticles($transformService->convertTableToArray($record['discountperarticle'], array('article', 'unit', 'count', 'amount', 'percent', 'base'))));
		if ($discountObj->getDiscountperarticlemode() != 2) {$discountObj->setDiscountperarticleall(0);}
		(!empty($record['fe_groups'])) ? $discountObj->setUsergroups(explode(',', $record['fe_groups'])) : $discountObj->setUsergroups(array());
		(!empty($record['fe_users'])) ? $discountObj->setUsers(explode(',', $record['fe_users'])) : $discountObj->setUsers(array());
		return $discountObj;
	}

	/**
     * Select discount by given coupon code.
     *
     * @param	string		$code      			the coupon code of discount
	 * @param	boolean		$respectHidden      respect hidden flag?
	 * @return 									the discount object
     */
	public function findByCode($code, $respectHidden=true)
	{
		// build query
		$queryBuilder = $this->makeInstance('TYPO3\CMS\Core\Database\ConnectionPool')->getQueryBuilderForTable('tx_maagitproduct_domain_model_discount');	
		if (!$respectHidden) {$queryBuilder->getRestrictions()->removeByType(\TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction::class);}
		$queryBuilder 
			->select('tx_maagitproduct_domain_model_discount.*')
			->from('tx_maagitproduct_domain_model_discount')
			->where(
				$queryBuilder->expr()->eq(
					'tx_maagitproduct_domain_model_discount.couponcode',
					$queryBuilder->createNamedParameter($code, \TYPO3\CMS\Core\Database\Connection::PARAM_STR)
				)
			);

		// execute query and fetch records
		$record = $queryBuilder
		->executeQuery()
		->fetchAssociative();

		// return data
		if ($record === FALSE)
		{
			throw new \RuntimeException('The discount with coupon code "'.$code.'" was not found in tx_maagitproduct_domain_model_discount table.', 1653986740);
		}
		return $this->create($record);
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Get article uids and convert it to product arrays
     *
     * @param	string		$uids      	the uids of articles, separated by coma
	 * @return 							the articles as array with product objects
     */
	protected function convertArticlesToArray($uids)
	{
		$articles = array();
		if (!empty($uids))
		{
			$productRepository = $this->makeInstance('Maagit\Maagitproduct\Domain\Repository\ProductRepository');
			$productIds = explode(',', $uids);
			foreach ($productIds as $productId)
			{
				try
				{
					$articles[count($articles)] = $productRepository->findByUid($productId)->getUid();
				}
				catch (\Exception $ex) { }
			}
		}
		return $articles;
	}

	/**
     * Check of valid articles
     *
     * @param	array		$source			      	the discount array with given definitions, array keys are:
	 *												- article			the product uid
	 *												- unit				the unit id
	 *												- count				the count
	 *												- amount			the amount
	 *												- percent			the percent
	 *												- base				the calculation base (0=article/1=basket)
	 * @return 										the same array with valid articles
     */
	protected function getValidArticles($source)
	{
		$articles = array();
		foreach ($source as $article)
		{
			if (!array_key_exists('article', $article)) {throw new \RuntimeException('Key "article" not found in field "discount per article".', 1653990311);}
			if (!array_key_exists('unit', $article)) {throw new \RuntimeException('Key "unit" not found in field "discount per article".', 1653990311);}
			if (!array_key_exists('count', $article)) {throw new \RuntimeException('Key "count" not found in field "discount per article".', 1653990311);}
			if (!array_key_exists('amount', $article)) {throw new \RuntimeException('Key "amount" not found in field "discount per article".', 1653990311);}
			if (!array_key_exists('percent', $article)) {throw new \RuntimeException('Key "percent" not found in field "discount per article".', 1653990311);}
			if (!array_key_exists('base', $article)) {throw new \RuntimeException('Key "base" not found in field "discount per article".', 1653990311);}
			if (empty($article['count'])) {$article['count'] = 0;}
			if (empty($article['amount'])) {$article['amount'] = 0;}
			if (empty($article['percent'])) {$article['percent'] = 0;}
			if (empty($article['base'])) {$article['base'] = 0;}
			$products = $this->convertArticlesToArray($article['article']);
			if (!empty($products))
			{
				$index = count($articles);
				$articles[$index]['article'] = $products[0];
				$articles[$index]['unit'] = $article['unit'];
				$articles[$index]['count'] = $article['count'];
				$articles[$index]['amount'] = $article['amount'];
				$articles[$index]['percent'] = $article['percent'];
				$articles[$index]['base'] = $article['base'];
			}
		}
		return $articles;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}