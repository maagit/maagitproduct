<?php
namespace Maagit\Maagitproduct\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Repository
	class:				CategoryRepository

	description:		Repository for the "category" model.

	created:			2020-08-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-08-21	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed
													Definition of "ObjectType" added

------------------------------------------------------------------------------------- */


class CategoryRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	use \Maagit\Maagitproduct\General;


	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function initializeObject()
	{
		// instantiate persistenceManager ()
		$this->persistenceManager = $this->makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager');
		$this->objectType = 'Maagit\Maagitproduct\Domain\Model\Category';
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
	 * get category descriptions from product
	 *
	 * @param	array				$uid				the product uid
	 * @return	array									the category descriptions
	 */
	public function getCategoryDescriptions(int $uid)
	{
		$categoryObjects = $this->findByUids($this->findByTtContentUid($uid));
		$result = array();
		foreach ($categoryObjects as $category)
		{
			$result[] = $category->getTitle();
		}
		return $result;
	}

	/**
	 * find categories by multiple uids
	 *
	 * @param	array				$uids				the category uid's
	 * @return	array[Category]							the category objects
	 */
	public function findByUids(array $uids)
	{
		if (empty($uids))
		{
			return array();
		}
		$query = $this->createQuery();
		foreach ($uids as $uid)
		{
			$query->logicalOr($query->equals('uid', $uid));
		}
		return $query->matching(
			$query->logicalAnd(
				$query->equals('hidden', 0),
				$query->equals('deleted', 0)
			)
		)->execute();
	}

	/**
	 * Get category uids by given tt_content uid
	 *
	 * @param	int					$uid				the tt_content uid
	 * @return	array									the category uids
	 */
	public function findByTtContentUid(int $uid)
	{
		$queryBuilder = $this->makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool')->getQueryBuilderForTable('sys_category_record_mm');
		$queryBuilder
			->select('uid_local')
			->from('sys_category_record_mm')
			->where(
				$queryBuilder->expr()->and(
					$queryBuilder->expr()->eq('uid_foreign', $uid),
					$queryBuilder->expr()->eq('tablenames', '\'tt_content\''),
					$queryBuilder->expr()->eq('fieldname', '\'categories\'')
				)
			);
		$records = $queryBuilder->executeQuery()->fetchAllAssociative();
		$result = array();
		foreach($records as $record)
		{
			$result[] = $record['uid_local'];
		}
		return $result;
	}

	/**
	 * Return the categories and all subcategories (recursive)
	 *
	 * @param	array				$categories			the categories
	 * @return	array									expanded categories
	 */
	public function expandCategories(array $categories)
	{
		// get all categories from database
		$queryBuilder = $this->makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool')->getQueryBuilderForTable('sys_category');
		$queryBuilder->getRestrictions()->removeAll();
		$queryBuilder->select('uid', 'parent')->from('sys_category');
		$allCategoriesFromDatabase = $queryBuilder->executeQuery()->fetchAllAssociative();

		// index the categories by parent
		$categoriesByParent = array();
		foreach ($allCategoriesFromDatabase as $categoryFromDatabase)
		{
			$categoriesByParent[(int)$categoryFromDatabase['parent']][] = (int)$categoryFromDatabase['uid'];	
		}

		// expand the categories to all subcategories
		$categoriesToExpand = $categories;
		$expandedCategories = $categories;
		while (count($categoriesToExpand) > 0)
		{
			$currentSubCategories = array();
			foreach ($categoriesToExpand as $category)
			{
				if (!empty($categoriesByParent[$category]))
				{
					foreach ($categoriesByParent[$category] as $subCategory)
					{
						$currentSubCategories[] = $subCategory;
					}
				}
			}
			$categoriesToExpand = array_diff($currentSubCategories, $expandedCategories);
			$expandedCategories = array_unique(array_merge($expandedCategories, $currentSubCategories));
		}
		return $expandedCategories;
	}

	/**
	 * Get tt_content uids with given categories
	 *
	 * @param	array				$categories			the categories
	 * @return	array									the tt_content uids with given categories
	 */
	public function getUidsByCategories(array $categories)
	{
		$result = array();
		if (empty($categories))
		{
			$result[] = -1;
			return $result;
		}
		$queryBuilder = $this->makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool')->getQueryBuilderForTable('sys_category_record_mm');
		$queryBuilder->getRestrictions()->removeAll();
		$queryBuilder
			->select('uid_foreign')
			->from('sys_category_record_mm')
			->where(
				$queryBuilder->expr()->and(
					$queryBuilder->expr()->in('uid_local', $categories),
					$queryBuilder->expr()->eq('tablenames', '\'tt_content\''),
					$queryBuilder->expr()->eq('fieldname', '\'categories\'')
				)
			);
		$records = $queryBuilder->executeQuery()->fetchAllAssociative();
		if (empty($records))
		{
			$result[] = -1;
			return $result;
		}
		foreach($records as $record)
		{
			$result[] = $record['uid_foreign'];
		}
		return $result;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}