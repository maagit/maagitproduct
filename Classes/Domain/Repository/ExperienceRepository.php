<?php
namespace Maagit\Maagitproduct\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Repository
	class:				ExperienceRepository

	description:		Repository for the "experience" model.

	created:			2020-10-31
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-10-31	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed
													Definition of "ObjectType" added
						2022-06-07	Urs Maag		Flexform-Settings changed

------------------------------------------------------------------------------------- */


class ExperienceRepository extends \Maagit\Maagitproduct\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Initialize the object, set default values
     *
     */
	public function initializeObject() {
		// initialization things
		parent::initializeObject();
		$this->objectType = 'Maagit\Maagitproduct\Domain\Model\Experience';

		// make default query settings
		$querySettings = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
		if (empty($this->settings['experience']['storagePid']))
		{
			$querySettings->setRespectStoragePage(false);
		}
		else
		{
			$pageRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\PageRepository');
			$pidList = $pageRepository->getPageTreeUids(
				\TYPO3\CMS\Core\Utility\GeneralUtility::intExplode(',', $this->settings['experience']['storagePid'], true),
				(!empty($this->settings['experience']['recursive'])) ? $this->settings['experience']['recursive'] : 0
			);
			$querySettings->setRespectStoragePage(true);
			$querySettings->setStoragePageIds($pidList);
		}
        $this->setDefaultQuerySettings($querySettings);
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}