<?php
namespace Maagit\Maagitproduct\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Repository
	class:				ProductRepository

	description:		Repository for the "product" model.
						Get the data of tt_content and map it to the product model.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed
													Definition of "ObjectType" added
						2022-06-05	Urs Maag		Update method added

------------------------------------------------------------------------------------- */


class ProductRepository extends \Maagit\Maagitproduct\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Repository\AssetRepository
     */
    protected $assetRepository;

	/**
	 * @var \Maagit\Maagitproduct\Domain\Repository\CategoryRepository
     */
    protected $categoryRepository;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Initialize the object, set default values
     *
     */
	public function initializeObject() {
		// initialization things
		parent::initializeObject();
		$this->objectType = 'Maagit\Maagitproduct\Domain\Model\Product';
		
		// inject repositories
		$this->assetRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\AssetRepository');
		$this->categoryRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\CategoryRepository');

		// set default query settings
		$querySettings = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($querySettings);
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Create a new object
     *
	 * @param	$arguments		array										variable arguments
	 * @return 					\Maagit\Maagitproduct\Domain\Model\Base		domain model object
     */
	public function create(...$arguments)
	{
		$productObj = parent::create();
		$record = $arguments[0];
		$propertyService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Property\\PropertyService');
		$productObj = $propertyService->setProperties($productObj, $record);
		$productObj->setCategoryDescriptions(implode(',', $this->categoryRepository->getCategoryDescriptions($productObj->getUid())));
		$productObj->setPriceSorting((empty($productObj->getUnit()))?$productObj->getPrice():(float)$productObj->getUnit()[0][3]);
		return $productObj;
	}
	
	/**
     * Select product by given uid.
     *
     * @param	int		$uid      	the uid of product
	 * @return 						the product object
     */
	public function findByUid($uid)
	{
		// build query
		$queryBuilder = $this->makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool')->getQueryBuilderForTable('tt_content');	
		$queryBuilder 
			->select('tt_content.*')
			->from('tt_content')
			->where(
				$queryBuilder->expr()->eq(
					'tt_content.CType', 
					$queryBuilder->createNamedParameter('maagitproduct_content', \TYPO3\CMS\Core\Database\Connection::PARAM_STR)
			),
			$queryBuilder->expr()->eq(
				'tt_content.uid',
				$queryBuilder->createNamedParameter($uid, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
			)
		);
			
		// execute query and fetch records
		$record = $queryBuilder
		->executeQuery()
		->fetchAssociative();

		// return data
		if ($record === FALSE)
		{
			throw new \RuntimeException('The product with uid '.$uid.' was not found in tt_content table.', 1593103932);
		}
		return $this->create($record);
	}

	/**
     * Select products by given storagePids.
     *
     * @param	array				$pids       		the pid list of record storage
	 * @return	array[Product]							array of Product Models
     */
	public function findByPids(array $pids)
	{
		return $this->findByPidsAndFilter($pids, array());
	}

	/**
     * find products by type
     *
     * @param	array				$pids       		the pid list of record storage
	 * @param	string				$types				the types, delimited by ','
	 * @return	array[Product]							array of Product Models
     */
	public function findByPidsAndType(array $pids, string $types)
	{
		if (empty($types)) {$types = '-1';}
		$typeArray = array_map('string', explode(',', $types));
		if ($typeArray[0]=='') {$typeArray = array(-1);}
		$uids = $this->getProductUidsByType($typeArray);
		return $this->findByPidsAndFilter($pids, $uids);
	}

	/**
     * find products by category
     *
     * @param	array				$pids       		the pid list of record storage
	 * @param	string				$categories			the categories, delimited by ','
	 * @return	array[Product]							array of Product Models
     */
	public function findByPidsAndCategory(array $pids, string $categories)
	{
		if (empty($categories)) {$categories = '-1';}
		$categoryArray = array_map('intval', explode(',', $categories));
		if ($categoryArray[0]==0) {$categoryArray = array(-1);}
		$uids = $this->getProductUidsByCategory($categoryArray);
		return $this->findByPidsAndFilter($pids, $uids);
	}
	
	/**
     * filter selection by type and category
     *
     * @param	array				$pids       		the pid list of record storage
	 * @param	string				$types				the types, delimited by ','
	 * @param	string				$categories			the categories, delimited by ','
	 * @return	array[Product]							array of Product Models
     */
	public function findByPidsAndTypeAndCategory(array $pids, string $types, string $categories)
	{
		// initialize
		if (empty($types)) {$types = '-1';}
		$typeArray = array_map('intval', explode(',', $types));
		if (empty($categories)) {$categories = '-1';}
		$categoryArray = array_map('intval', explode(',', $categories));

		// get uids, found by given filter settings
		$typeUids = $this->getProductUidsByType($typeArray);
		$categoryUids = $this->getProductUidsByCategory($categoryArray);

		// merge uids and get only those, which are in both filter results
		$uids = array();
		$uids[] = '-1';
		foreach ($typeUids as $typeUid)
		{
			foreach ($categoryUids as $categoryUid)
			{
				if ($typeUid == $categoryUid)
				{
					$uids[] = $typeUid;
					break;
				}
			}
		}

		// select products and return the result
		return $this->findByPidsAndFilter($pids, $uids);
	}

	/**
     * Select products by given order.
     *
     * @param	int					$order       		the uid of order
	 * @return	array[Product]							array of Product Models
     */
	public function findByOrder(int $order)
	{
		// instantiate category repository, if it's null (this method can be called from a backend method and no injections are made)
		if (empty($this->categoryRepository)) {$this->categoryRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\CategoryRepository');}

		// build query
		$queryBuilder = $this->makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool')->getQueryBuilderForTable('tx_maagitproduct_domain_model_article');	
		$queryBuilder 
			->select('tx_maagitproduct_domain_model_article.productuid')
			->from('tx_maagitproduct_domain_model_article')
			->where(
				$queryBuilder->expr()->eq(
					'tx_maagitproduct_domain_model_article.order',
					$queryBuilder->createNamedParameter($order, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
			)
		);

		// execute query and fetch records
		$records = $queryBuilder->executeQuery()->fetchAllAssociative();

		// create domain models
		$objectsArray = array();
		foreach ($records as $record)
		{
			$objectsArray[] = $this->findByUid($record['productuid']);
		}

		return $objectsArray;
	}

	/**
     * filter selection by search sentences
     *
     * @param	array				$products      		the products
	 * @param	string				$search				the search words
	 * @return	array[Product]							array of Product Models
     */
	public function filterBySearch(array $products, string $search)
	{
		// initialize
		$compareService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Content\\CompareService');
		$searchResult = array();

		// define search patterns
		$patterns = array($search, $search.'*', '*'.$search, '*'.$search.'*');
	
		// define search subjects
		$subjects = explode(',', $this->settings['selection']['fields']);
		if (empty($subjects[0]))
		{
			throw new \RuntimeException('A search was executed, but no search fields are defined (see in plugin configuration).', 1598004282);
		}

		// loop search patterns
		foreach ($patterns as $pattern)
		{
			// loop products and make search comparing
			foreach ($products as $product)
			{
				// loop subjects
				foreach ($subjects as $key => $subject)
				{
					$searchString = $product->$subject();
					$searchString = ($searchString==null) ? '' : $searchString;
					$searchString = (is_array($searchString)) ? implode(',', $searchString) : $searchString;
					if ($compareService->compareWithWildcards($pattern, $searchString))
					{
						if (!in_array($product, $searchResult))
						{
							$searchResult[] = $product;
						}
						break;
					}
				}
			}
		}

		// return result
		return $searchResult;
	}

	/**
     * Update product by given fields
     *
     * @param	int		$uid      	the uid of product
	 * @param	string	$field		the update field
	 * @param	mixed	$value		the update value
	 * @return 	void
     */
	public function updateByUid(int $uid, string $field, $value)
	{
		$queryBuilder = $this->makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool')->getQueryBuilderForTable('tt_content');	
		$queryBuilder
			->update('tt_content')
			->where(
				$queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid, \TYPO3\CMS\Core\Database\Connection::PARAM_INT))
			)
			->set($field, $value)
			->executeStatement();
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Select products by given storagePids and filter uids.
     *
     * @param	array				$pids       		the pid list of record storage
	 * @param	array				$filterUids 		the tt_content uids with given filter
	 * @return	array[Product]							array of Product Models
     */
	protected function findByPidsAndFilter(array $pids, array $filterUids=array())
	{
		// build query
		$queryBuilder = $this->makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool')->getQueryBuilderForTable('tt_content');	
		$queryBuilder 
			->select('tt_content.*', 'pages.sorting AS pageSorting')
			->from('tt_content')
			->join(
				'tt_content',
				'pages',
				'pages',
				$queryBuilder->expr()->eq(
					'tt_content.pid', 
					$queryBuilder->quoteIdentifier('pages.uid')
				)
			)
			->where(
				$queryBuilder->expr()->eq(
					'tt_content.CType', 
					$queryBuilder->createNamedParameter('maagitproduct_content', \TYPO3\CMS\Core\Database\Connection::PARAM_STR)
				),
				$queryBuilder->expr()->in(
					'tt_content.pid',
					$queryBuilder->createNamedParameter($pids, \TYPO3\CMS\Core\Database\Connection::PARAM_INT_ARRAY)
				)
			);

		// add $filterUids (if not empty)
		if (!empty($filterUids))
		{
			$queryBuilder->andWhere(
				$queryBuilder->expr()->in(
					'tt_content.uid',
					$queryBuilder->createNamedParameter($filterUids, \TYPO3\CMS\Core\Database\Connection::PARAM_INT_ARRAY)
				)
			);
		}

		// execute query and fetch records
		$records = $queryBuilder
		->executeQuery()
		->fetchAllAssociative();

		// create domain models
		$objectsArray = array();
		foreach ($records as $record)
		{
			$objectsArray[] = $this->create($record);
		}

		return $objectsArray;
	}

	/**
     * get product uids of given types
     *
	 * @param	array				$types				the types
	 * @return	array									array of product uids
     */
	protected function getProductUidsByType(array $types)
	{
		$result = array();
		if (empty($types))
		{
			$result[] = -1;
			return $result;
		}
		$queryBuilder = $this->makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool')->getQueryBuilderForTable('tt_content');
		$queryBuilder->getRestrictions()->removeAll();
		$queryBuilder
			->select('uid')
			->from('tt_content')
			->where(
				$queryBuilder->expr()->in(
					'tt_content.tx_maagitproduct_type',
					$queryBuilder->createNamedParameter($types, \TYPO3\CMS\Core\Database\Connection::PARAM_STR_ARRAY)
				)
			);
		$records = $queryBuilder->executeQuery()->fetchAllAssociative();
		if (empty($records))
		{
			$result[] = -1;
			return $result;
		}
		foreach($records as $record)
		{
			$result[] = $record['uid'];
		}
		return $result;
	}
	
	/**
     * get product uids of given categories
     *
	 * @param	array				$categories			the categories
	 * @return	array									array of uids
     */
	protected function getProductUidsByCategory(array $categories)
	{
		$categories = $this->categoryRepository->expandCategories($categories);
		$uids = $this->categoryRepository->getUidsByCategories($categories);
		return $uids;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}