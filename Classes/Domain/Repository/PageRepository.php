<?php
namespace Maagit\Maagitproduct\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Repository
	class:				PageRepository

	description:		Repository for the typo3 pages.

	created:			2020-08-06
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-08-06	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed
													Definition of "ObjectType" added
						2021-12-25	Urs Maag		Call "getMenu" with last parameter
													"false" (method "_getPagesRecursive")
													to avoid PHP 8 errors (unknown array
													keys)
						2022-06-07	Urs Maag		Method findByTypes added

------------------------------------------------------------------------------------- */


class PageRepository extends \Maagit\Maagitproduct\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function initializeObject()
	{
		parent::initializeObject();
		$this->objectType = 'Maagit\Maagitproduct\Domain\Model\Page';
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * get page id's from page tree
     *
	 * @param	array			$uids					the root page uid(s)
	 * @param	int				$level					the max. tree level
	 * @return	array							the page uid's
     */
	public function getPageTreeUids(array $uids, int $level=0)
	{
		$pages = array();
		foreach ($uids as $uid)
		{
			$pages = array_merge($this->_getPagesRecursive($uid, $level), $pages);
		}
		return $pages;
	}

	/**
     * find pages by given types in tt_content
     *
	 * @param	array			$cTypes					the relevant CTypes
	 * @param	array			$listTypes				the relevant list_types
	 * @return	array									the page uid's
     */
	public function findByTypes(array $cTypes, array $listTypes=array())
	{
		$queryBuilder = $this->makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool')->getQueryBuilderForTable('tt_content');	
		$result = $queryBuilder 
			->select('tt_content.pid')
			->from('tt_content')
			->join(
				'tt_content',
				'pages',
				'pages',
				$queryBuilder->expr()->eq('pages.uid', $queryBuilder->quoteIdentifier('tt_content.pid'))
			)
			->where(
				$queryBuilder->expr()->eq('pages.doktype', $queryBuilder->createNamedParameter(1, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)),
				$queryBuilder->expr()->or(
					$queryBuilder->expr()->in(
						'tt_content.CType', 
						$queryBuilder->createNamedParameter($cTypes, \TYPO3\CMS\Core\Database\Connection::PARAM_STR_ARRAY)
					),
					$queryBuilder->expr()->in(
						'tt_content.CType',
						$queryBuilder->createNamedParameter($listTypes, \TYPO3\CMS\Core\Database\Connection::PARAM_STR_ARRAY)
					)
				)
			)
			->groupBy('tt_content.pid')
			->executeQuery();
		
		$array = array();
		while ($row = $result->fetchAssociative())
		{
		   $array[] = $row['pid'];
		}
		return ($array);
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	/**
     * get page id's, selected recursively from page tree
     *
	 * @param	int			$uid					the current page uid
	 * @param	int			$level					the max. tree level
	 * @param	int			$currentLevel			the current tree level
	 * @return	array							the page uid's
     */
	private function _getPagesRecursive(int $uid, int $level=0, int $currentLevel=0)
	{
		$pages = array($uid);
		if ($currentLevel < $level)
		{
			$currentLevel++;
			$pageRepository = $this->makeInstance('TYPO3\\CMS\\Core\\Domain\\Repository\\PageRepository');
			$menuItems = $pageRepository->getMenu($uid, 'uid', '', 'sorting', false);
			foreach ($menuItems as $key => $value)
			{
				$pages = array_merge($pages, $this->_getPagesRecursive($key, $level, $currentLevel));
			}
		}
		return $pages;
	}
}
