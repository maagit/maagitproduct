<?php
namespace Maagit\Maagitproduct\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Repository
	class:				BasketItemRepository

	description:		Repository for the "basketItem" model.
						Inherits the method "create" for creating fitting models.
						Inherits the method "findByUid" to select a given item of
						the current basket.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed
													Definition of "ObjectType" added

------------------------------------------------------------------------------------- */


class BasketItemRepository extends \Maagit\Maagitproduct\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Repository\ProductRepository
     */
    protected $productRepository;
	
	/**
	 * @var \Maagit\Maagitproduct\Service\Content\TransformService
     */
    protected $transformService;

	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Contructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// initialization things
		parent::initializeObject();
		$this->objectType = 'Maagit\Maagitproduct\Domain\Model\BasketItem';
		
		// inject classes
		$this->productRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\ProductRepository');
		$this->transformService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Content\\TransformService');
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Create a new basketItem object
     *
	 * @param	int													$quantity      		the quantity
	 * @param	int													$productUid    		the product uid
	 * @param	int													$unitUid    		the unit uid
	 * @return 	\Maagit\Maagitproduct\Domain\Model\BasketItem							the basket item object
     */
	//public function create(int $quantity = NULL, int $uid = NULL, $settings = NULL)
	public function create(...$arguments)
	{
		if (isset($arguments))
		{
			$quantity = (isset($arguments[0])) ? $arguments[0] : NULL;
			$uid = (isset($arguments[1])) ? $arguments[1] : NULL;
			$unit = (isset($arguments[2])) ? $arguments[2] : NULL;
		}
		$basketItem = parent::create();
		$basketItem->setProduct($this->productRepository->findByUid($uid));
		$basketItem->setQuantity($quantity);
		$basketItem->setUnit($unit);
		return $basketItem;
	}
	
    /**
     * get a basket item by uid
     *
     * @param	int													$uid		the uid
	 * @param	int													$unit		the unit uid
	 * @param	\Maagit\Maagitproduct\Domain\Model\Basket			$basket		the basket
	 * @return	\Maagit\Maagitproduct\Domain\Model\BasketItem					the basketitem object
     */
	public function findByUid($uid, $unit=null, \Maagit\Maagitproduct\Domain\Model\Basket $basket=null)
	{
		if ($basket == null)
		{
			$basketRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\BasketRepository');
			$basket = $basketRepository->findAll();
		}
		foreach ($basket->getBasketItems() as $basketItem)
		{
			if ($basketItem->getUid() == $uid && $basketItem->getUnit() == $unit)
			{
				return $basketItem;
			}
		}
		return null;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}