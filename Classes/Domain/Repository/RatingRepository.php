<?php
namespace Maagit\Maagitproduct\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Repository
	class:				RatingRepository

	description:		Repository for the "rating" model.
						Inherits the method "create" for creating fitting models.
						Inherits the method "findByUid" to select the ratings of given
						product.

	created:			2020-08-25
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-08-25	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed
													Definition of "ObjectType" added
						2022-05-25	Urs Maag		Using own ObjectStorage to avoid
													problems with "spl_object_hash" in
													various php versions

------------------------------------------------------------------------------------- */


class RatingRepository extends \Maagit\Maagitproduct\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function initializeObject()
	{
		parent::initializeObject();
		$this->objectType = 'Maagit\Maagitproduct\Domain\Model\Rating';
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Create a new object
     *
	 * @param	$arguments		array										variable arguments
	 * @return 					\Maagit\Maagitproduct\Domain\Model\Base		domain model object
     */
	public function create(...$arguments)
	{
		$ratingObj = parent::create();
		$record = $arguments[0];
		$propertyService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Property\\PropertyService');
		$ratingObj = $propertyService->setProperties($ratingObj, $record);
		return $ratingObj;
	}

	/**
     * Select ratings from tt_content by given uid.
     *
     * @param 	int												$uid        the uid of the tt_content element
	 * @return 	Maagit\Maagitproduct\Helper\ObjectStorage					object storage with file references
     */
	public function findByUid($uid)
	{
		// build query
		$queryBuilder = $this->makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool')->getQueryBuilderForTable('tt_content');	
		 	$queryBuilder 
			->select('tx_maagitproduct_domain_model_rating.*')
			->from('tx_maagitproduct_domain_model_rating')
			->where(
				$queryBuilder->expr()->eq(
					'tx_maagitproduct_domain_model_rating.tt_content',
					$queryBuilder->createNamedParameter($uid, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->orderBy('crdate', 'DESC')
		;

		// execute query and fetch records
		$records = $queryBuilder
		->executeQuery()
		->fetchAllAssociative();

		// make rating storage
		$ratings = $this->makeInstance('Maagit\Maagitproduct\Helper\ObjectStorage');
		
		// add ratings
		foreach ($records as $record)
		{
			$rating = $this->create($record);
			$ratings->attach($rating);
		}
		
		// return rating object storage
		return $ratings;
	}

	/**
     * Select hidden ratings from tt_content by given uid.
     *
     * @param 	int												$uid        the uid of the tt_content element
	 * @return 	Maagit\Maagitproduct\Helper\ObjectStorage					object storage with file references
     */
	public function findHiddenByUid(int $uid)
	{
		$query = $this->createQuery();
		$query->getQuerySettings()->setIgnoreEnableFields(true);
		$query->matching($query->equals('uid', $uid));
		return $query->execute()->getFirst();
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}