<?php
namespace Maagit\Maagitproduct\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Repository
	class:				BasketRepository

	description:		Repository for the "basket" model.
						Inherits methods to create, add, change and remove items.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed
													Definition of "ObjectType" added

------------------------------------------------------------------------------------- */


class BasketRepository extends \Maagit\Maagitproduct\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Repository\BasketItemRepository
     */
    protected $basketItemRepository;
	
	/**
     * @var \Maagit\Maagitproduct\Service\Network\SessionService
     */
    protected $sessionService;
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Contructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// initialization things
		parent::initializeObject();
		$this->objectType = 'Maagit\Maagitproduct\Domain\Model\Basket';

		// inject repositories and services
		$this->basketItemRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\BasketItemRepository');
		$this->sessionService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Network\\SessionService');
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * get the basket
     *
	 * @return 			\Maagit\Maagitproduct\Domain\Model\Basket		the basket object
     */
	public function findAll()
	{
		return $this->findBySession();
	}
	
	/**
     * save the basket
     *
	 * @param	\Maagit\Maagitproduct\Domain\Model\Basket		$basket		the basket object
	 * @return 	void
     */
	public function persist(\Maagit\Maagitproduct\Domain\Model\Basket $basket)
	{
		$this->writeToSession($basket);
	}
	
	/**
     * add to basket
     *
	 * @param	\Maagit\Maagitproduct\Domain\Model\BasketItem		$basketItem		the basketitem to add
	 * @return 	\Maagit\Maagitproduct\Domain\Model\Basket							the basket object
     */
	public function add($basketItem)
	{
		$basket = $this->findAll();
		$item = $this->basketItemRepository->findByUid($basketItem->getUid(), $basketItem->getUnit(), $basket);
		if ($item == null)
		{
			$basket->getBasketItems()->attach($basketItem);
		}
		if ($item != null)
		{
			$availableQuantities = $item->getProduct()->getOrderQuantity();
			if (!empty($availableQuantities)) 
			{
				$index = array_search($item->getQuantity(), array_column($availableQuantities, 'value'));
				$nextIndex = $index + 1;
				if (!empty($availableQuantities[$nextIndex]))
				{
					$item->setQuantity($availableQuantities[$nextIndex]['value']);
				}
			}
		}
		$this->persist($basket);
		return $basket;
	}
	
    /**
     * remove from basket
     *
     * @param	\Maagit\Maagitproduct\Domain\Model\BasketItem		$basketItem		the basketitem to remove
	 * @return	\Maagit\Maagitproduct\Domain\Model\Basket							the basket object
     */
    public function remove($basketItem)
    {
		// get basket
		$basket = $this->findAll();
		
		// return, if $basketItem is null
		if ($basketItem == NULL)
		{
			return $basket;
		}
		
		// go through current items and clear given basketItem
		$currentItems = $basket->getBasketItems();
		$basket->setBasketItems($this->makeInstance('Maagit\Maagitproduct\Helper\ObjectStorage'));
		$this->persist($basket);
		
		// loop currentItems and add all, except given parameter
		foreach ($currentItems as $currentItem)
		{
			if ($currentItem->getUid().'_'.$currentItem->getUnit() != $basketItem->getUid().'_'.$basketItem->getUnit())
			{
				$basket = $this->add($currentItem);
			}
		}
		$this->persist($basket);
		return $basket;
    }
	
    /**
     * change basket
     *
     * @param	\Maagit\Maagitproduct\Domain\Model\BasketItem		$basketItem		the basketitem to change
	 * @param	int													$quantity		the new quantity of this item
	 * @return	\Maagit\Maagitproduct\Domain\Model\Basket							the basket object
     */
    public function change(\Maagit\Maagitproduct\Domain\Model\BasketItem $basketItem, int $quantity)
    {
		// get basket
		$basket = $this->findAll();
		
		// return, if $basketItem is null
		if ($basketItem == NULL)
		{
			return $basket;
		}

		// change quantity of given basketItem
		$basketItem->setQuantity($quantity);
		foreach ($basket->getBasketItems() as $item)
		{
			if ($item->getUid().'_'.$item->getUnit() == $basketItem->getUid().'_'.$basketItem->getUnit())
			{
				$item->setQuantity($quantity);
			}
		}
		$this->persist($basket);
		return $basket;
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * read session and get the basket
     *
	 * @return 			\Maagit\Maagitproduct\Domain\Model\Basket		the basket object
     */
	protected function findBySession()
	{
		$basket = $this->sessionService->restore($this->settings['basket']['sessionKey']);
		if (!$basket instanceof \Maagit\Maagitproduct\Domain\Model\Basket)
		{
            $basket = $this->create();
            $this->persist($basket);
        }
		return $basket;
	}
	
	/**
     * write the basket to the session
     *
	 * @param	\Maagit\Maagitproduct\Domain\Model\Basket $basket
	 * @return	void
     */
	protected function writeToSession(\Maagit\Maagitproduct\Domain\Model\Basket $basket)
	{
		$this->sessionService->write($basket, $this->settings['basket']['sessionKey']);
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}