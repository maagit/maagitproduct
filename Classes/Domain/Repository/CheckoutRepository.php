<?php
namespace Maagit\Maagitproduct\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Repository
	class:				CheckoutRepository

	description:		Repository for the "checkout" model.
						Inherits the method "create" for creating fitting models.
						Inherits the method "init" to initailize and refresh all
						relevant datas of current checkout process.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed
													Definition of "ObjectType" added

------------------------------------------------------------------------------------- */


class CheckoutRepository extends \Maagit\Maagitproduct\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var \Maagit\Maagitproduct\Service\Network\SessionService
     */
    protected $sessionService;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Contructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// initialization things
		parent::initializeObject();
		$this->objectType = 'Maagit\Maagitproduct\Domain\Model\Checkout';

		// inject important repositories and services
		$this->sessionService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Network\\SessionService');
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Make initalize things, like: refresh basket, refresh delivery methods, ...
     *
	 * @param	\Maagit\Maagitproduct\Domain\Model\Checkout		$checkout		the checkout object
	 * @return	\Maagit\Maagitproduct\Domain\Model\Checkout						the checkout object
     */
	public function init(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		$checkoutService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Checkout\\CheckoutService', $checkout);
		$checkout = $checkoutService->refresh();
		return $checkout;
	}
	
	/**
     * get the checkout
     *
	 * @return 			\Maagit\Maagitproduct\Domain\Model\Checkout		the checkout object
     */
	public function findAll()
	{
		$checkout = $this->findBySession();
		// @extensionScannerIgnoreLine
		$checkout = $this->init($checkout);
		return $checkout;
	}
	
	/**
     * save the checkout
     *
	 * @param	\Maagit\Maagitproduct\Domain\Model\Checkout		$checkout		the checkout object
	 * @return 	\Maagit\Maagitproduct\Domain\Model\Checkout						the checkout object
     */
	public function persist(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		$checkout = $this->writeToSession($checkout);
		// @extensionScannerIgnoreLine
		return $this->init($checkout);
	}
	
	/**
     * clear the checkout and the basket objects from the session
     *
	 * @return	void
     */
	public function clear()
	{
		$this->sessionService->clear($this->settings['checkout']['sessionKey']);
		$this->sessionService->clear($this->settings['basket']['sessionKey']);
	}

	/**
     * Removes all persistences of session datas
     *
	 * @return	void
     */
	public function removeAll()
	{
		// @extensionScannerIgnoreLine
		$this->sessionService->remove();
	}

	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * read session and get the checkout
     *
	 * @return 			\Maagit\Maagitproduct\Domain\Model\Checkout		the checkout object
     */
	protected function findBySession()
	{
		$checkout = $this->sessionService->restore($this->settings['checkout']['sessionKey']);
		if (!$checkout instanceof \Maagit\Maagitproduct\Domain\Model\Checkout)
		{
			$checkout = $this->create();
            $checkout = $this->persist($checkout);
        }
		return $checkout;
	}
	
	/**
     * write the checkout to the session
     *
	 * @param	\Maagit\Maagitproduct\Domain\Model\Checkout 	$checkout		the checkout object
	 * @return	\Maagit\Maagitproduct\Domain\Model\Checkout						the checkout object
     */
	protected function writeToSession(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		$this->sessionService->write($checkout, $this->settings['checkout']['sessionKey']);
		return $checkout;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}