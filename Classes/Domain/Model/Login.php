<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				Login

	description:		Model for the "login" step of the checkout process.
						Inherits datas for managing the login.

	created:			2022-04-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-04-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Login extends \Maagit\Maagitproduct\Domain\Model\BaseModelStep
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var boolean
     */
    protected $guest;

	/**
     * @var boolean
     */
    protected $loggedIn = false;

	/**
     * @var string
     */
    protected $username;

	/**
     * @var Maagit\Maagitproduct\Domain\Model\User
     */
    protected $user;

	/**
     * @var string
     */
    protected $lastError;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the guest
     *
     * @param boolean $guest
     */
    public function setGuest($guest)
    {
        $this->guest = $guest;
    }
	
	/**
     * Returns the guest
     *
     * @return boolean	$guest
     */
    public function getGuest()
    {
        return $this->guest;
    }

	/**
     * Sets the loggedIn
     *
     * @param boolean $loggedIn
     */
    public function setLoggedIn($loggedIn)
    {
        $this->loggedIn = $loggedIn;
    }

	/**
     * Returns the loggedIn
     *
     * @return boolean	$loggedIn
     */
    public function getLoggedIn()
    {
        $userAspect = $this->makeInstance('TYPO3\CMS\Core\Context\Context')->getAspect('frontend.user');
		$this->loggedIn = $userAspect->isLoggedIn();
		return $this->loggedIn;
    }

	/**
     * Sets the username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

	/**
     * Returns the username
     *
     * @return string	$username
     */
    public function getUsername()
    {
        return $this->username;
    }

	/**
     * Sets the lastError
     *
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

	/**
     * Returns the lastError
     *
     * @return string	$lastError
     */
    public function getLastError()
    {
        return $this->lastError;
    }

	/**
     * Returns the user
     *
     * @return Maagit\Maagitproduct\Domain\Model\User	$user
     */
    public function getUser()
    {
		if (!$this->getLoggedIn()) {return null;}
		$context = $this->makeInstance('TYPO3\CMS\Core\Context\Context');
		$userRepository = $this->makeInstance('Maagit\Maagitproduct\Domain\Repository\UserRepository');
		$this->user = $userRepository->findByUid($context->getPropertyFromAspect('frontend.user', 'id'));
		$this->user->setSettings($this->settings);
		return $this->user;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	
 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}