<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				Discount

	description:		Model for the discount coupons.
						Inherits the datas of tx_maagitproduct_domain_model_discount

	created:			2022-05-31
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-05-31	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Discount extends \Maagit\Maagitproduct\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var int
     */
    protected ?int $uid;

	/**
	 * @var string
     */
    protected $couponcode;
	
	/**
	 * @var string
     */
    protected $coupontext;

	/**
	 * @var int
     */
    protected $coupontype;

	/**
	 * @var double
     */
    protected $discountamount;
	
	/**
	 * @var double
     */
    protected $discountpercent;
	
	/**
	 * @var array
	 */
	protected $discountperarticle;

	/**
	 * @var int
	 */
	protected $discountperarticlemode;

	/**
	 * @var double
	 */
	protected $discountperarticleall;

	/**
	 * @var double
	 */
	protected $minorderamount;

	/**
	 * @var bool
     */
    protected $useonce;

	/**
	 * @var bool
     */
    protected $hidden;

	/**
	 * @var array
	 */
	protected $usergroups;

	/**
	 * @var array
	 */
	protected $users;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
	 * Set the uid
	 *
	 * @param int $uid
	 */
	public function setUid($uid)
	{
	    $this->uid = $uid;
	}

	/**
	 * Get the uid
	 *
	 * @return int $uid
	 */
	public function getUid(): ?int
	{
	    return $this->uid;
	}

	/**
	 * Set the couponcode
	 *
	 * @param string $couponcode
	 */
	public function setCouponcode($couponcode)
	{
	    $this->couponcode = $couponcode;
	}

	/**
	 * Get the couponcode
	 *
	 * @return string
	 */
	public function getCouponcode()
	{
	    return $this->couponcode;
	}

	/**
	 * Set the coupontext
	 *
	 * @param string $coupontext
	 */
	public function setCoupontext($coupontext)
	{
	    $this->coupontext = $coupontext;
	}

	/**
	 * Get the coupontext
	 *
	 * @return string
	 */
	public function getCoupontext()
	{
	    return $this->coupontext;
	}

	/**
	 * Set the coupontype
	 *
	 * @param int $coupontype
	 */
	public function setCoupontype($coupontype)
	{
	    $this->coupontype = $coupontype;
	}

	/**
	 * Get the coupontype
	 *
	 * @return int
	 */
	public function getCoupontype()
	{
	    return $this->coupontype;
	}

	/**
	 * Get the coupontext for html
	 *
	 * @return string
	 */
	public function getCoupontextHtml()
	{
	    return nl2br($this->coupontext);
	}

	/**
	 * Set the discountamount
	 *
	 * @param double $discountamount
	 */
	public function setDiscountamount($discountamount)
	{
	    $this->discountamount = $discountamount;
	}

	/**
	 * Get the discountamount
	 *
	 * @return double
	 */
	public function getDiscountamount()
	{
	    return $this->discountamount;
	}

	/**
	 * Set the discountpercent
	 *
	 * @param double $discountpercent
	 */
	public function setDiscountpercent($discountpercent)
	{
	    $this->discountpercent = $discountpercent;
	}

	/**
	 * Get the discountpercent
	 *
	 * @return double
	 */
	public function getDiscountpercent()
	{
	    return $this->discountpercent;
	}

	/**
	 * Set the discountperarticle
	 *
	 * @param array $discountperarticle
	 */
	public function setDiscountperarticle($discountperarticle)
	{
	    $this->discountperarticle = $discountperarticle;
	}

	/**
	 * Get the discountperarticle
	 *
	 * @return array
	 */
	public function getDiscountperarticle()
	{
	    return $this->discountperarticle;
	}

	/**
	 * Set the discountperarticlemode
	 *
	 * @param int $discountperarticlemode
	 */
	public function setDiscountperarticlemode($discountperarticlemode)
	{
	    $this->discountperarticlemode = $discountperarticlemode;
	}

	/**
	 * Get the discountperarticlemode
	 *
	 * @return int
	 */
	public function getDiscountperarticlemode()
	{
	    return $this->discountperarticlemode;
	}

	/**
	 * Set the discountperarticleall
	 *
	 * @param double $discountperarticleall
	 */
	public function setDiscountperarticleall($discountperarticleall)
	{
	    $this->discountperarticleall = $discountperarticleall;
	}

	/**
	 * Get the discountperarticleall
	 *
	 * @return double
	 */
	public function getDiscountperarticleall()
	{
	    return $this->discountperarticleall;
	}

	/**
	 * Set the minorderamount
	 *
	 * @param double $minorderamount
	 */
	public function setMinorderamount($minorderamount)
	{
	    $this->minorderamount = $minorderamount;
	}

	/**
	 * Get the minorderamount
	 *
	 * @return double
	 */
	public function getMinorderamount()
	{
	    return $this->minorderamount;
	}

	/**
	 * Set the useonce
	 *
	 * @param bool $useonce
	 */
	public function setUseonce($useonce)
	{
	    $this->useonce = $useonce;
	}

	/**
	 * Get the useonce
	 *
	 * @return bool
	 */
	public function getUseonce()
	{
	    return $this->useonce;
	}

	/**
	 * Set the hidden
	 *
	 * @param bool $hidden
	 */
	public function setHidden($hidden)
	{
	    $this->hidden = $hidden;
	}

	/**
	 * Get the hidden
	 *
	 * @return bool
	 */
	public function getHidden()
	{
	    return $this->hidden;
	}

	/**
	 * Set the usergroups
	 *
	 * @param array $usergroups
	 */
	public function setUsergroups($usergroups)
	{
	    $this->usergroups = $usergroups;
	}

	/**
	 * Get the usergroups
	 *
	 * @return array
	 */
	public function getUsergroups()
	{
	    return $this->usergroups;
	}

	/**
	 * Set the users
	 *
	 * @param array $users
	 */
	public function setUsers($users)
	{
	    $this->users = $users;
	}

	/**
	 * Get the users
	 *
	 * @return array
	 */
	public function getUsers()
	{
	    return $this->users;
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}