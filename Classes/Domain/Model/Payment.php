<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				Payment

	description:		Model for the "payment" step of the checkout process.
						Inherits datas for managing the payment aspects.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Payment extends \Maagit\Maagitproduct\Domain\Model\BaseModelStep
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var array
     */
	protected $paymentMethodsWithoutValidationAspect = array(
		'cash',
		'bill',
		'advance',
		'twint'
	);
	
    /**
     * @var array
     */
	protected $paymentMethods;

	/**
     * @var string
     */
    protected $paymentMethod;
	
	/**
     * @var array
     */
    protected $paymentMethodInfo;
	
	/**
     * @var string
     */
    protected $paymentMethodCashInfo;
	
	/**
     * @var string
     */
    protected $paymentMethodBillInfo;
	
	/**
     * @var string
     */
    protected $paymentMethodAdvanceInfo;
	
	/**
     * @var string
     */
    protected $paymentMethodTwintInfo;
	
    /**
     * @var double
     */
	protected $expenses;

	/**
     * @var boolean
     */
    protected $paymentOK;

	/**
     * @var string
     */
    protected $paymentMessage;

	/**
     * @var string
     */
    protected $paymentError;

    /**
     * @var string
     */
	protected $paypalToken;
	
    /**
     * @var string
     */
	protected $saferpayToken;

    /**
     * @var string
     */
	protected $stripeToken;

    /**
     * @var string
     */
	protected $stripePaymentId;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Returns the valid
     *
     * @return bool	$valid
     */
    public function getValid()
    {
        if ($this->valid && $this->getPaymentMethod() == '' && $this->checkout->getTotal()>0)
		{
			$this->valid = false;
		}
        if ($this->visited && $this->checkout->getTotal()==0)
		{
			$this->valid = true;
		}
		return $this->valid;
    }

	/**
     * Sets the paymentMethod
     *
     * @param string $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
		$this->paymentMethod = $paymentMethod;
    }
	
	/**
     * Returns the paymentMethod
     *
     * @return string	the payment method
     */
    public function getPaymentMethod()
    {
		return $this->paymentMethod;
    }
	
	/**
     * Sets the paymentMethods
     *
     * @param array $paymentMethods
     */
    public function setPaymentMethods($paymentMethods)
    {
		$this->paymentMethods = $paymentMethods;
    }
	
	/**
     * Returns the paymentMethods
     *
     * @return array	the payment methods
     */
    public function getPaymentMethods()
    {
		return $this->paymentMethods;
    }
	
	/**
     * Returns the paymentMethodInfo
     *
     * @return array	the selected payment method as array with all it's details
     */
    public function getPaymentMethodInfo()
    {
		$index = array_search($this->getPaymentMethod(), array_column($this->getPaymentMethods(), 'id'));
		if ($index === FALSE)
		{
			return array();
		}
		return $this->getPaymentMethods()[$index];
    }
	
	/**
     * Returns the information of payment method "cash"
     *
     * @return string	the payment cash infos
     */
    public function getPaymentMethodCashInfo()
    {
		$renderHandler = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitproduct\\Service\\Content\\RenderService');
		$info = $this->settings['checkout']['payment']['method']['cash']['information'];
		$info = $renderHandler->renderPlaceholder($info, $this->checkout, $this->settings);
		return $info;
    }
	
	/**
     * Returns the information of payment method "bill"
     *
     * @return string	the payment bill infos
     */
    public function getPaymentMethodBillInfo()
    {
		$renderHandler = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitproduct\\Service\\Content\\RenderService');
		$info = $this->settings['checkout']['payment']['method']['bill']['information'];
		$info = $renderHandler->renderPlaceholder($info, $this->checkout, $this->settings);
		return $info;
    }

	/**
     * Returns the information of payment method "advance"
     *
     * @return string	the payment advance infos
     */
    public function getPaymentMethodAdvanceInfo()
    {
		$renderHandler = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitproduct\\Service\\Content\\RenderService');
		$info = $this->settings['checkout']['payment']['method']['advance']['information'];
		$info = $renderHandler->renderPlaceholder($info, $this->checkout, $this->settings);
		return $info;
    }
	
	/**
     * Returns the information of payment method "twint"
     *
     * @return string	the payment twint infos
     */
    public function getPaymentMethodTwintInfo()
    {
		$renderService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitproduct\\Service\\Content\\RenderService');
		$info = $this->settings['checkout']['payment']['method']['twint']['information'];
		$info = $renderService->renderPlaceholder($info, $this->checkout, $this->settings);
		return $info;
    }
	
	/**
     * Sets the paymentOK
     *
     * @param boolean $paymentOK
     */
    public function setPaymentOK($paymentOK)
    {
		$this->paymentOK = $paymentOK;
    }
	
	/**
     * Sets the expenses
     *
     * @param double $expenses
     */
    public function setExpenses($expenses)
    {
        $this->expenses = $expenses;
    }

	/**
     * Returns the expenses
     *
     * @param string	paymentId
	 * @return double	$expenses
     */
    public function getExpenses(string $paymentId = NULL)
    {
        $checkoutCalculationService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Checkout\\CheckoutCalculationService', $this->checkout);
		return $checkoutCalculationService->calculateExpenses($paymentId);
    }

	/**
     * Returns the paymentOK
     *
     * @return boolean	is the payment ok?
     */
    public function getPaymentOK()
    {
		if (in_array($this->getPaymentMethod(), $this->paymentMethodsWithoutValidationAspect))
		{
			return true;
		}
		return $this->paymentOK;
    }

	/**
     * Sets the paymentMessage
     *
     * @param string $paymentMessage
     */
    public function setPaymentMessage($paymentMessage)
    {
		$this->paymentMessage = $paymentMessage;
    }
	
	/**
     * Returns the paymentMessage
     *
     * @return string	the payment message
     */
    public function getPaymentMessage()
    {
		return $this->paymentMessage;
    }

	/**
     * Sets the paymentError
     *
     * @param string $paymentError
     */
    public function setPaymentError($paymentError)
    {
		$this->paymentError = $paymentError;
    }
	
	/**
     * Returns the paymentError
     *
     * @return string	the payment error
     */
    public function getPaymentError()
    {
		return $this->paymentError;
    }
	
	/**
     * Sets the paypalToken
     *
     * @param string $paypalToken
     */
    public function setPaypalToken($paypalToken)
    {
        $this->paypalToken = $paypalToken;
    }

	/**
     * Returns the paypalToken
     *
     * @return string	$paypalToken
     */
    public function getPaypalToken()
    {
		return $this->paypalToken;
    }
	
	/**
     * Sets the saferpayToken
     *
     * @param string $saferpayToken
     */
    public function setSaferpayToken($saferpayToken)
    {
        $this->saferpayToken = $saferpayToken;
    }

	/**
     * Returns the saferpayToken
     *
     * @return string	$saferpayToken
     */
    public function getSaferpayToken()
    {
		return $this->saferpayToken;
    }
	
	/**
     * Sets the stripeToken
     *
     * @param string $stripeToken
     */
    public function setStripeToken($stripeToken)
    {
        $this->stripeToken = $stripeToken;
    }

	/**
     * Returns the stripeToken
     *
     * @return string	$stripeToken
     */
    public function getStripeToken()
    {
		return $this->stripeToken;
    }

	/**
     * Sets the stripePaymentId
     *
     * @param string $stripePaymentId
     */
    public function setStripePaymentId($stripePaymentId)
    {
        $this->stripePaymentId = $stripePaymentId;
    }

	/**
     * Returns the stripePaymentId
     *
     * @return string	$stripePaymentId
     */
    public function getStripePaymentId()
    {
		return $this->stripePaymentId;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	
 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}