<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				Checkout

	description:		Model for the checkout process.
						Inherits the models of the process steps and other relevant datas.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed
						2022-04-03	Urs Maag		Add login step
						2023-12-18	Urs Maag		Extended conditions handling (AGB's)

------------------------------------------------------------------------------------- */


class Checkout extends \Maagit\Maagitproduct\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var \Maagit\Maagitproduct\Domain\Model\Basket
     */
    protected $basket;
	
	/**
     * @var \Maagit\Maagitproduct\Domain\Model\Login
     */
    protected $login;

	/**
     * @var \Maagit\Maagitproduct\Domain\Model\Address
     */
    protected $address;
	
	/**
     * @var \Maagit\Maagitproduct\Domain\Model\Delivery
     */
    protected $delivery;
	
	/**
     * @var \Maagit\Maagitproduct\Domain\Model\Payment
     */
    protected $payment;

	/**
     * @var \Maagit\Maagitproduct\Domain\Model\Coupon
     */
    protected $coupon;

	/**
     * @var \Maagit\Maagitproduct\Domain\Model\Terms
     */
    protected $terms;

	/**
     * @var \Maagit\Maagitproduct\Domain\Model\Summary
     */
    protected $summary;

	/**
     * @var \Maagit\Maagitproduct\Domain\Model\Experience
     */
    protected $experience;

	/**
     * @var string
     */
    protected $action = 'login';

	/**
     * @var string
     */
    protected $orderId;

    /**
     * @var double
     */
	protected $total;
	
    /**
     * @var string
     */
	protected $agb;
	
	/**
	 * @var string
	 */
	protected $agbUrl;

    /**
     * @var string
     */
	protected $mailToReceiverError;
	
    /**
     * @var string
     */
	protected $mailToSenderError;
	
    /**
     * @var string
     */
	protected $mailDownloadError;
	
    /**
     * @var boolean
     */
	protected $sendingDone = false;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function initializeObject()
	{
		// create order id
		$this->setOrderId(time());
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the basket
     *
     * @param \Maagit\Maagitproduct\Domain\Model\Basket $basket
     */
    public function setBasket($basket)
    {
        $this->basket = $basket;
    }
	
	/**
     * Returns the basket
     *
     * @return \Maagit\Maagitproduct\Domain\Model\Basket	the basket object
     */
    public function getBasket()
    {
        if ($this->basket != null)
		{
			$this->basket->setSettings(array_replace_recursive($this->basket->getSettings(), $this->settings));
		}
		return $this->basket;
    }
	
	/**
     * Sets the login
     *
     * @param \Maagit\Maagitproduct\Domain\Model\Login $login
     */
    public function setLogin($login)
    {
		$this->login = $login;
    }
	
	/**
     * Returns the login
     *
     * @return \Maagit\Maagitproduct\Domain\Model\Login	the login
     */
    public function getLogin()
    {
		return $this->login;
    }

	/**
     * Sets the address
     *
     * @param \Maagit\Maagitproduct\Domain\Model\Address $address
     */
    public function setAddress($address)
    {
		$this->address = $address;
    }
	
	/**
     * Returns the address
     *
     * @return \Maagit\Maagitproduct\Domain\Model\Address	the address
     */
    public function getAddress()
    {
		return $this->address;
    }
	
	/**
     * Sets the delivery
     *
     * @param \Maagit\Maagitproduct\Domain\Model\Delivery $delivery
     */
    public function setDelivery($delivery)
    {
		$this->delivery = $delivery;
    }
	
	/**
     * Returns the delivery
     *
     * @return \Maagit\Maagitproduct\Domain\Model\Delivery	the delivery
     */
    public function getDelivery()
    {
		return $this->delivery;
    }
	
	/**
     * Sets the payment
     *
     * @param \Maagit\Maagitproduct\Domain\Model\Payment $payment
     */
    public function setPayment($payment)
    {
		$this->payment = $payment;
    }
	
	/**
     * Returns the payment
     *
     * @return \Maagit\Maagitproduct\Domain\Model\Payment	the payment
     */
    public function getPayment()
    {
		return $this->payment;
    }

	/**
     * Sets the coupon
     *
     * @param \Maagit\Maagitproduct\Domain\Model\Coupon $coupon
     */
    public function setCoupon($coupon)
    {
		$this->coupon = $coupon;
    }
	
	/**
     * Returns the coupon
     *
     * @return \Maagit\Maagitproduct\Domain\Model\Coupon	the coupon
     */
    public function getCoupon()
    {
		return $this->coupon;
    }

	/**
     * Sets the terms
     *
     * @param \Maagit\Maagitproduct\Domain\Model\Terms $terms
     */
    public function setTerms($terms)
    {
		$this->terms = $terms;
    }
	
	/**
     * Returns the terms
     *
     * @return \Maagit\Maagitproduct\Domain\Model\Terms	the terms
     */
    public function getTerms()
    {
		return $this->terms;
    }

	/**
     * Sets the summary
     *
     * @param \Maagit\Maagitproduct\Domain\Model\Summary $summary
     */
    public function setSummary($summary)
    {
		$this->summary = $summary;
    }
	
	/**
     * Returns the summary
     *
     * @return \Maagit\Maagitproduct\Domain\Model\Summary	the summary
     */
    public function getSummary()
    {
		return $this->summary;
    }

	/**
     * Sets the experience
     *
     * @param \Maagit\Maagitproduct\Domain\Model\Experience $experience
     */
    public function setExperience($experience)
    {
		$this->experience = $experience;
    }
	
	/**
     * Returns the experience
     *
     * @return \Maagit\Maagitproduct\Domain\Model\Experience	the experience
     */
    public function getExperience()
    {
		return $this->experience;
    }

	/**
     * Sets the action
     *
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }
	
	/**
     * Returns the action
     *
     * @return string	$action
     */
    public function getAction()
    {
        return $this->action;
    }
	
	/**
     * Sets the orderId
     *
     * @param string $orderId
     */
    public function setOrderId($orderId)
    {
		$this->orderId = $orderId;
    }
	
	/**
     * Returns the orderId
     *
     * @return string	the order id
     */
    public function getOrderId()
    {
		return $this->orderId;
    }
	
	/**
     * Sets the total
     *
     * @param double $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

	/**
     * Returns the total amount (tax and shipping included)
     *
     * @return double	$total
     */
    public function getTotal()
    {
        $checkoutCalculationService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Checkout\\CheckoutCalculationService', $this);
		return $checkoutCalculationService->calculateTotal();
    }
	
	/**
     * Returns the agb
     *
     * @return string	$agb
     */
    public function getAgb()
    {
		$agb = '';
		if ($this->settings['checkout']['sending']['agb'] != '')
		{
			$divHelper = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitproduct\\Helper\\DivHelper');
			$agb = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.send.agb', 'maagitproduct');
			preg_match('/\|link\|(.*?)\|\/link\|/', $agb, $matches);
		    $targetPos = strrpos($this->settings['checkout']['sending']['agb'], ' ');
			$target = ($targetPos !== FALSE) ? substr($this->settings['checkout']['sending']['agb'], $targetPos) : '';
			// @extensionScannerIgnoreLine
			$link = $divHelper->getExtbaseRequest()->getAttribute('currentContentObject')->typolink($matches[1],	array('parameter' => $this->getAgbUrl().$target));
			$agb = preg_replace('/\|link\|(.*)\|\/link\|/', $link, $agb);
		}
		return $agb;
    }

	/**
     * Sets the agbUrl
     *
     * @param string $agbUrl
     */
    public function setAgbUrl($agbUrl)
    {
        $this->agbUrl = $agbUrl;
    }
	
	/**
     * Returns the agbUrl
     *
     * @return string	$agbUrl
     */
    public function getAgbUrl()
    {
		return $this->agbUrl;
    }
	
	/**
     * Sets the mailToReceiverError
     *
     * @param string $mailToReceiverError
     */
    public function setMailToReceiverError($mailToReceiverError)
    {
        $this->mailToReceiverError = $mailToReceiverError;
    }

	/**
     * Returns the mailToReceiverError
     *
     * @return string	$mailToReceiverError
     */
    public function getMailToReceiverError()
    {
		return $this->mailToReceiverError;
    }
	
	/**
     * Sets the mailToSenderError
     *
     * @param string $mailToSenderError
     */
    public function setMailToSenderError($mailToSenderError)
    {
        $this->mailToSenderError = $mailToSenderError;
    }

	/**
     * Returns the mailToSenderError
     *
     * @return string	$mailToSenderError
     */
    public function getMailToSenderError()
    {
		return $this->mailToSenderError;
    }
	
	/**
     * Sets the mailDownloadError
     *
     * @param string $mailDownloadError
     */
    public function setMailDownloadError($mailDownloadError)
    {
        $this->mailDownloadError = $mailDownloadError;
    }

	/**
     * Returns the mailDownloadError
     *
     * @return string	$mailDownloadError
     */
    public function getMailDownloadError()
    {
		return $this->mailDownloadError;
    }
	
	/**
     * Sets the sendingDone
     *
     * @param boolean $sendingDone
     */
    public function setSendingDone($sendingDone)
    {
        $this->sendingDone = $sendingDone;
    }

	/**
     * Returns the sendingDone
     *
     * @return boolean	$sendingDone
     */
    public function getSendingDone()
    {
		return $this->sendingDone;
    }
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	
 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}