<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				Product

	description:		Model for the article in the shopping cart (basket).
						Inherits the datas of tt_content with type "productelement"

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2022-05-25	Urs Maag		Using own ObjectStorage to avoid
													problems with "spl_object_hash" in
													various php versions

------------------------------------------------------------------------------------- */


class Product extends \Maagit\Maagitproduct\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Service\Content\TsConfigService
     */
    protected $tsConfigService;

	/**
	 * @var int
     */
    protected ?int $uid;
	
	/**
	 * @var int
     */
    protected ?int $pid;
	
	/**
	 * @var string
     */
    protected $rowDescription;
	
	/**
	 * @var string
     */
    protected $type;
	
	/**
	 * @var string
	 */
	protected $typeDescription;
	
	/**
	 * @var bool
	 */
	protected $isNewType;
	
	/**
	 * @var string
     */
    protected $nr;
	
	/**
	 * @var double
     */
    protected $price;

	/**
	 * @var array
     */
    protected $orderquantity;

	/**
	 * @var unit
     */
    protected $unit;

	/**
	 * @var double
     */
    protected $stock;

	/**
	 * @var int
     */
    protected $weight;
	
	/**
	 * @var double
     */
    protected $shipping;
	
	/**
	 * @var bool
	 */
	protected $isDownload;

	/**
	 * @var string
     */
    protected $link;
	
	/**
	 * @var string
	 */
	protected $secureLink;
	
    /**
     * @var int
     */
    protected $basketimage;
	
    /**
     * @var bool
     */
    protected $verkauft;

    /**
	 * @var \Maagit\Maagitproduct\Helper\ObjectStorage<\Maagit\Maagitproduct\Domain\Model\Rating>
     */
    protected $ratings;

	/**
	 * @var string
     */
    protected $CType;
	
	/**
	 * @var int
     */
    protected $colPos;

	/**
     * @var string
     */
    protected $header;
	
	/**
     * @var string
     */
    protected $headerLayout;
	
	/**
     * @var string
     */
    protected $headerPosition;

	/**
	 * @var int
     */
    protected $date;
	
	/**
     * @var string
     */
    protected $headerLink;
	
	/**
     * @var string
     */
    protected $subheader;
	
	/**
     * @var string
     */
    protected $bodytext;

    /**
     * @var \Maagit\Maagitproduct\Helper\ObjectStorage<\Maagit\Maagitproduct\Domain\Model\Asset>
     */
    protected $assets;

	/**
	 * @var int
     */
    protected $imagewidth;
	
	/**
	 * @var int
     */
    protected $imageheight;
	
	/**
	 * @var bool
     */
    protected $imageborder;
	
	/**
	 * @var int
     */
    protected $imageorient;
	
	/**
	 * @var int
     */
    protected $imagecols;
	
	/**
	 * @var bool
     */
    protected $imageZoom;
	
	/**
	 * @var int
     */
    protected $layout;
	
	/**
	 * @var string
     */
    protected $frameClass;
	
	/**
	 * @var string
     */
    protected $spaceBeforeClass;
	
	/**
	 * @var string
     */
    protected $spaceAfterClass;
	
	/**
	 * @var bool
     */
    protected $sectionIndex;
	
	/**
	 * @var bool
     */
    protected $linkToTop;
	
	/**
	 * @var int
     */
    protected $sysLanguageUid;
	
	/**
	 * @var bool
     */
    protected $hidden;
	
	/**
	 * @var int
     */
    protected $starttime;
	
	/**
	 * @var int
     */
    protected $endtime;

	/**
	 * @var string
     */
    protected $feGroup;
	
	/**
	 * @var bool
     */
    protected $editlock;
	
	/**
	 * @var int
     */
    protected $categories;

	/**
	 * @var string
     */
    protected $categoryDescriptions;

	/**
	 * @var int
     */
    protected $sorting;

	/**
	 * @var int
     */
    protected $pageSorting;

	/**
	 * @var double
     */
    protected $priceSorting;

	/**
	 * @var int
     */
    protected $crdate;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function initializeObject()
	{
		// inject services
		$this->tsConfigService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Content\\TsConfigService');
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the tsConfigService
     *
     * @param \Maagit\Maagitproduct\Service\Content\TsConfigService $tsConfigService
     */
    public function setTsConfigService($tsConfigService)
    {
        $this->tsConfigService = $tsConfigService;
    }
	
	/**
     * Returns the tsConfigService
     *
     * @return \Maagit\Maagitproduct\Service\Content\TsConfigService	$tsConfigService
     */
    public function getTsConfigService()
    {
		return $this->tsConfigService;
    }

	/**
	 * Set the uid
	 *
	 * @param int $uid
	 */
	public function setUid($uid)
	{
	    $this->uid = $uid;
	}

	/**
	 * Get the uid
	 *
	 * @return int $uid
	 */
	public function getUid(): ?int
	{
	    return $this->uid;
	}

	/**
	 * Set the rowDescription
	 *
	 * @param string $rowDescription
	 */
	public function setRowDescription($rowDescription)
	{
	    $this->rowDescription = $rowDescription;
	}

	/**
	 * Get the rowDescription
	 *
	 * @return string
	 */
	public function getRowDescription()
	{
	    return $this->rowDescription;
	}

	/**
	 * Set the type
	 *
	 * @param string $type
	 */
	public function setType($type)
	{
	    $this->type = $type;
	}

	/**
	 * Get the type
	 *
	 * @return string
	 */
	public function getType()
	{
	    return $this->type;
	}

	/**
	 * Set the typeDescription
	 *
	 * @param string $typeDescription
	 */
	public function setTypeDescription($typeDescription)
	{
	    $this->typeDescription = $typeDescription;
	}

	/**
	 * Get the typeDescription
	 *
	 * @return string
	 */
	public function getTypeDescription()
	{
	    return $this->tsConfigService->getTsConfigValue(array('tx_maagitproduct_type.', 'addItems.', $this->type), array(), $this->pid);
	}

	/**
	 * Set the isNewType
	 *
	 * @param bool $isNewType
	 */
	public function setIsNewType($isNewType)
	{
	    $this->isNewType = $isNewType;
	}

	/**
	 * Get the isNewType
	 *
	 * @return bool
	 */
	public function getIsNewType()
	{
	    return $this->isNewType;
	}

	/**
	 * Set the nr
	 *
	 * @param string $nr
	 */
	public function setNr($nr)
	{
	    $this->nr = $nr;
	}

	/**
	 * Get the nr
	 *
	 * @return string
	 */
	public function getNr()
	{
	    return $this->nr;
	}

	/**
	 * Set the price
	 *
	 * @param double $price
	 */
	public function setPrice($price)
	{
	    $this->price = $price;
	}

	/**
	 * Get the price
	 *
	 * @return double
	 */
	public function getPrice()
	{
	    return $this->price;
	}

	/**
	 * Set the orderquantity
	 *
	 * @param array $orderquantity
	 */
	public function setOrderquantity($orderquantity)
	{
	    $this->orderquantity = $orderquantity;
	}

	/**
	 * Get the orderquantity
	 *
	 * @return array
	 */
	public function getOrderquantity()
	{
	    return $this->orderquantity;
	}
	
	/**
	 * Set the unit
	 *
	 * @param array $unit
	 */
	public function setUnit($unit)
	{
	    $this->unit = $unit;
	}

	/**
	 * Get the unit
	 *
	 * @return array
	 */
	public function getUnit()
	{
	    return $this->unit;
	}

	/**
	 * Set the stock
	 *
	 * @param double $stock
	 */
	public function setStock($stock)
	{
	    $this->stock = $stock;
	}

	/**
	 * Get the stock
	 *
	 * @return double
	 */
	public function getStock()
	{
	    return $this->stock;
	}

	/**
	 * Set the weight
	 *
	 * @param int $weight
	 */
	public function setWeight($weight)
	{
	    $this->weight = $weight;
	}

	/**
	 * Get the weight
	 *
	 * @return int
	 */
	public function getWeight()
	{
	    return $this->weight;
	}

	/**
	 * Set the shipping
	 *
	 * @param double $shipping
	 */
	public function setShipping($shipping)
	{
	    $this->shipping = $shipping;
	}

	/**
	 * Get the shipping
	 *
	 * @return double
	 */
	public function getShipping()
	{
	    return $this->shipping;
	}

	/**
	 * Set the isDownload
	 *
	 * @param bool $isDownload
	 */
	public function setIsDownload($isDownload)
	{
	    $this->isDownload = $isDownload;
	}

	/**
	 * Get the isDownload
	 *
	 * @return bool
	 */
	public function getIsDownload()
	{
		if ($this->link != '')
		{
			return true;
		}
		return $this->isDownload;
	}

	/**
	 * Set the link
	 *
	 * @param string $link
	 */
	public function setLink($link)
	{
	    $this->link = $link;
	}

	/**
	 * Get the link
	 *
	 * @return string
	 */
	public function getLink()
	{
	    return $this->link;
	}

	/**
	 * Set the secureLink
	 *
	 * @param string $secureLink
	 */
	public function setSecureLink($secureLink)
	{
	    $this->secureLink = $secureLink;
	}

	/**
	 * Get the secureLink
	 *
	 * @return string
	 */
	public function getSecureLink()
	{
		return $this->secureLink;
	}

	/**
	 * Set the basketimage
	 *
	 * @param int $basketimage
	 */
	public function setBasketimage($basketimage)
	{
	    $this->basketimage = $basketimage;
	}

	/**
	 * Get the basketimage
	 *
	 * @return int
	 */
	public function getBasketimage()
	{
	    return $this->basketimage;
	}

	/**
	 * Set the verkauft
	 *
	 * @param bool $verkauft
	 */
	public function setVerkauft($verkauft)
	{
	    $this->verkauft = $verkauft;
	}

	/**
	 * Get the verkauft
	 *
	 * @return bool
	 */
	public function getVerkauft()
	{
	    return $this->verkauft;
	}

    /**
     * Sets the ratings
     *
     * @return \Maagit\Maagitproduct\Helper\ObjectStorage<\Maagit\Maagitproduct\Domain\Model\Rating>
     */
    public function setRatings(\Maagit\Maagitproduct\Helper\ObjectStorage $ratings)
    {
        $this->ratings = $ratings;
    }

    /**
     * Get the ratings
     *
     * @return \Maagit\Maagitproduct\Helper\ObjectStorage<\Maagit\Maagitproduct\Domain\Model\Rating>
     */
    public function getRatings()
    {
        return $this->ratings;
    }

	/**
	 * Get the rating
	 *
	 * @return double
	 */
	public function getRating()
	{
		$result = 0;
		if (count($this->getRatings()) > 0)
		{
			foreach ($this->getRatings() as $rating)
			{
				$result = $result + $rating->getRating();
			}
			$result = $result/count($this->getRatings());
		}
		return $result;
	}

	/**
	 * Set the CType
	 *
	 * @param string $CType
	 */
	public function setCType($CType)
	{
	    $this->CType = $CType;
	}

	/**
	 * Get the CType
	 *
	 * @return string
	 */
	public function getCType()
	{
	    return $this->CType;
	}

	/**
	 * Set the colPos
	 *
	 * @param int $colPos
	 */
	public function setColPos($colPos)
	{
	    $this->colPos = $colPos;
	}

	/**
	 * Get the colPos
	 *
	 * @return int
	 */
	public function getColPos()
	{
	    return $this->colPos;
	}

	/**
	 * Set the header
	 *
	 * @param string $header
	 */
	public function setHeader($header)
	{
	    $this->header = $header;
	}

	/**
	 * Get the header
	 *
	 * @return string
	 */
	public function getHeader()
	{
	    return $this->header;
	}

	/**
	 * Set the headerLayout
	 *
	 * @param string $headerLayout
	 */
	public function setHeaderLayout($headerLayout)
	{
	    $this->headerLayout = $headerLayout;
	}

	/**
	 * Get the headerLayout
	 *
	 * @return string
	 */
	public function getHeaderLayout()
	{
	    return $this->headerLayout;
	}

	/**
	 * Set the headerPosition
	 *
	 * @param string $headerPosition
	 */
	public function setHeaderPosition($headerPosition)
	{
	    $this->headerPosition = $headerPosition;
	}

	/**
	 * Get the headerPosition
	 *
	 * @return string
	 */
	public function getHeaderPosition()
	{
	    return $this->headerPosition;
	}

	/**
	 * Set the date
	 *
	 * @param int $date
	 */
	public function setDate($date)
	{
	    $this->date = $date;
	}

	/**
	 * Get the date
	 *
	 * @return int
	 */
	public function getDate()
	{
	    return $this->date;
	}

	/**
	 * Set the headerLink
	 *
	 * @param string $headerLink
	 */
	public function setHeaderLink($headerLink)
	{
	    $this->headerLink = $headerLink;
	}

	/**
	 * Get the headerLink
	 *
	 * @return string
	 */
	public function getHeaderLink()
	{
	    return $this->headerLink;
	}

	/**
	 * Set the subheader
	 *
	 * @param string $subheader
	 */
	public function setSubheader($subheader)
	{
	    $this->subheader = $subheader;
	}

	/**
	 * Get the subheader
	 *
	 * @return string
	 */
	public function getSubheader()
	{
	    return $this->subheader;
	}

	/**
	 * Set the bodytext
	 *
	 * @param string $bodytext
	 */
	public function setBodytext($bodytext)
	{
	    $this->bodytext = $bodytext;
	}

	/**
	 * Get the bodytext
	 *
	 * @return string
	 */
	public function getBodytext()
	{
	    return $this->bodytext;
	}

	/**
	 * Set the assets
	 *
	 * @param \Maagit\Maagitproduct\Helper\ObjectStorage<\Maagit\Maagitproduct\Domain\Model\Asset> $assets
	 */
	public function setAssets($assets)
	{
	    $this->assets = $assets;
	}

	/**
	 * Get the assets
	 *
	 * @return \Maagit\Maagitproduct\Helper\ObjectStorage<\Maagit\Maagitproduct\Domain\Model\Asset>
	 */
	public function getAssets()
	{
	    return $this->assets;
	}

	/**
	 * Set the imagewidth
	 *
	 * @param int $imagewidth
	 */
	public function setImagewidth($imagewidth)
	{
	    $this->imagewidth = $imagewidth;
	}

	/**
	 * Get the imagewidth
	 *
	 * @return int
	 */
	public function getImagewidth()
	{
	    return $this->imagewidth;
	}

	/**
	 * Set the imageheight
	 *
	 * @param int $imageheight
	 */
	public function setImageheight($imageheight)
	{
	    $this->imageheight = $imageheight;
	}

	/**
	 * Get the imageheight
	 *
	 * @return int
	 */
	public function getImageheight()
	{
	    return $this->imageheight;
	}

	/**
	 * Set the imageborder
	 *
	 * @param bool $imageborder
	 */
	public function setImageborder($imageborder)
	{
	    $this->imageborder = $imageborder;
	}

	/**
	 * Get the imageborder
	 *
	 * @return bool
	 */
	public function getImageborder()
	{
	    return $this->imageborder;
	}

	/**
	 * Set the imageorient
	 *
	 * @param int $imageorient
	 */
	public function setImageorient($imageorient)
	{
	    $this->imageorient = $imageorient;
	}

	/**
	 * Get the imageorient
	 *
	 * @return int
	 */
	public function getImageorient()
	{
	    return $this->imageorient;
	}

	/**
	 * Set the imagecols
	 *
	 * @param int $imagecols
	 */
	public function setImagecols($imagecols)
	{
	    $this->imagecols = $imagecols;
	}

	/**
	 * Get the imagecols
	 *
	 * @return int
	 */
	public function getImagecols()
	{
	    return $this->imagecols;
	}

	/**
	 * Set the imageZoom
	 *
	 * @param bool $imageZoom
	 */
	public function setImageZoom($imageZoom)
	{
	    $this->imageZoom = $imageZoom;
	}

	/**
	 * Get the imageZoom
	 *
	 * @return bool
	 */
	public function getImageZoom()
	{
	    return $this->imageZoom;
	}

	/**
	 * Set the layout
	 *
	 * @param int $layout
	 */
	public function setLayout($layout)
	{
	    $this->layout = $layout;
	}

	/**
	 * Get the layout
	 *
	 * @return int
	 */
	public function getLayout()
	{
	    return $this->layout;
	}

	/**
	 * Set the frameClass
	 *
	 * @param string $frameClass
	 */
	public function setFrameClass($frameClass)
	{
	    $this->frameClass = $frameClass;
	}

	/**
	 * Get the frameClass
	 *
	 * @return string
	 */
	public function getFrameClass()
	{
	    return $this->frameClass;
	}

	/**
	 * Set the spaceBeforeClass
	 *
	 * @param string $spaceBeforeClass
	 */
	public function setSpaceBeforeClass($spaceBeforeClass)
	{
	    $this->spaceBeforeClass = $spaceBeforeClass;
	}

	/**
	 * Get the spaceBeforeClass
	 *
	 * @return string
	 */
	public function getSpaceBeforeClass()
	{
	    return $this->spaceBeforeClass;
	}

	/**
	 * Set the spaceAfterClass
	 *
	 * @param string $spaceAfterClass
	 */
	public function setSpaceAfterClass($spaceAfterClass)
	{
	    $this->spaceAfterClass = $spaceAfterClass;
	}

	/**
	 * Get the spaceAfterClass
	 *
	 * @return string
	 */
	public function getSpaceAfterClass()
	{
	    return $this->spaceAfterClass;
	}

	/**
	 * Set the sectionIndex
	 *
	 * @param bool $sectionIndex
	 */
	public function setSectionIndex($sectionIndex)
	{
	    $this->sectionIndex = $sectionIndex;
	}

	/**
	 * Get the sectionIndex
	 *
	 * @return bool
	 */
	public function getSectionIndex()
	{
	    return $this->sectionIndex;
	}

	/**
	 * Set the linkToTop
	 *
	 * @param bool $linkToTop
	 */
	public function setLinkToTop($linkToTop)
	{
	    $this->linkToTop = $linkToTop;
	}

	/**
	 * Get the linkToTop
	 *
	 * @return bool
	 */
	public function getLinkToTop()
	{
	    return $this->linkToTop;
	}

	/**
	 * Set the sysLanguageUid
	 *
	 * @param int $sysLanguageUid
	 */
	public function setSysLanguageUid($sysLanguageUid)
	{
	    $this->sysLanguageUid = $sysLanguageUid;
	}

	/**
	 * Get the sysLanguageUid
	 *
	 * @return int
	 */
	public function getSysLanguageUid()
	{
	    return $this->sysLanguageUid;
	}

	/**
	 * Set the hidden
	 *
	 * @param bool $hidden
	 */
	public function setHidden($hidden)
	{
	    $this->hidden = $hidden;
	}

	/**
	 * Get the hidden
	 *
	 * @return bool
	 */
	public function getHidden()
	{
	    return $this->hidden;
	}

	/**
	 * Set the starttime
	 *
	 * @param int $starttime
	 */
	public function setStarttime($starttime)
	{
	    $this->starttime = $starttime;
	}

	/**
	 * Get the starttime
	 *
	 * @return int
	 */
	public function getStarttime()
	{
	    return $this->starttime;
	}

	/**
	 * Set the endtime
	 *
	 * @param int $endtime
	 */
	public function setEndtime($endtime)
	{
	    $this->endtime = $endtime;
	}

	/**
	 * Get the endtime
	 *
	 * @return int
	 */
	public function getEndtime()
	{
	    return $this->endtime;
	}

	/**
	 * Set the feGroup
	 *
	 * @param string $feGroup
	 */
	public function setFeGroup($feGroup)
	{
	    $this->feGroup = $feGroup;
	}

	/**
	 * Get the feGroup
	 *
	 * @return string
	 */
	public function getFeGroup()
	{
	    return $this->feGroup;
	}

	/**
	 * Set the editlock
	 *
	 * @param bool $editlock
	 */
	public function setEditlock($editlock)
	{
	    $this->editlock = $editlock;
	}

	/**
	 * Get the editlock
	 *
	 * @return bool
	 */
	public function getEditlock()
	{
	    return $this->editlock;
	}

	/**
	 * Set the categories
	 *
	 * @param int $categories
	 */
	public function setCategories($categories)
	{
	    $this->categories = $categories;
	}

	/**
	 * Get the categories
	 *
	 * @return int
	 */
	public function getCategories()
	{
	    return $this->categories;
	}

	/**
	 * Set the categoryDescriptions
	 *
	 * @param string $categoryDescriptions
	 */
	public function setCategoryDescriptions($categoryDescriptions)
	{
	    $this->categoryDescriptions = $categoryDescriptions;
	}

	/**
	 * Get the categoryDescriptions
	 *
	 * @return string
	 */
	public function getCategoryDescriptions()
	{
	    return $this->categoryDescriptions;
	}

	/**
	 * Set the sorting
	 *
	 * @param int $sorting
	 */
	public function setSorting($sorting)
	{
	    $this->sorting = $sorting;
	}

	/**
	 * Get the sorting
	 *
	 * @return int
	 */
	public function getSorting()
	{
	    return $this->sorting;
	}

	/**
	 * Set the pageSorting
	 *
	 * @param int $pageSorting
	 */
	public function setPageSorting($pageSorting)
	{
	    $this->pageSorting = $pageSorting;
	}

	/**
	 * Get the pageSorting
	 *
	 * @return int
	 */
	public function getPageSorting()
	{
	    return $this->pageSorting;
	}

	/**
	 * Set the priceSorting
	 *
	 * @param double $priceSorting
	 */
	public function setPriceSorting($priceSorting)
	{
	    $this->priceSorting = $priceSorting;
	}

	/**
	 * Get the priceSorting
	 *
	 * @return double
	 */
	public function getPriceSorting()
	{
	    return $this->priceSorting;
	}

	/**
	 * Set the crdate
	 *
	 * @param int $crdate
	 */
	public function setCrdate($crdate)
	{
	    $this->crdate = $crdate;
	}

	/**
	 * Get the crdate
	 *
	 * @return int
	 */
	public function getCrdate()
	{
	    return $this->crdate;
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}