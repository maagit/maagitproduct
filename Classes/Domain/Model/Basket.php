<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				Basket

	description:		Model for the shopping cart (basket).
						Inherits the basket relevant datas and its items.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-12-25	Urs Maag		PHP 8, non existing array keys
						2022-05-25	Urs Maag		Using own ObjectStorage to avoid
													problems with "spl_object_hash" in
													various php versions
						2022-06-03	Urs Maag		Use setting "round" to calculate amounts

------------------------------------------------------------------------------------- */


class Basket extends \Maagit\Maagitproduct\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var \Maagit\Maagitproduct\Helper\ObjectStorage<\Maagit\Maagitproduct\Domain\Model\BasketItem>
     */
    protected $basketItems;
	
	/**
     * @var double
     */
	protected $amount;
	
    /**
     * @var double
     */
	protected $weight;
	
    /**
     * @var boolean
     */
	protected $hasDownloads;
	
    /**
     * @var boolean
     */
	protected $hasOnlyDownloads;
	
    /**
     * @var string
     */
    protected $tsBasket;
	
    /**
     * @var string
     */
    protected $tsDownload;
	
    /**
     * @var double
     */
	protected $tax;

    /**
     * @var double
     */
	protected $minOrderValue;
	

	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Initialization.
     *
	 * @param	-
	 * @return	void
     */
	public function initializeObject()
	{
		$this->basketItems = $this->makeInstance('Maagit\Maagitproduct\Helper\ObjectStorage');
		$this->minOrderValue = isset($this->settings['basket']['shipping']['minordervalue']) ? $this->settings['basket']['shipping']['minordervalue'] : 0;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the basketitems
     *
     * @param \Maagit\Maagitproduct\Helper\ObjectStorage $basketItems
     */
    public function setBasketItems($basketItems)
    {
        $this->basketItems = $basketItems;
    }
	
	/**
     * Returns the basketItems
     *
     * @return \Maagit\Maagitproduct\Helper\ObjectStorage holding instances of basket items
     */
    public function getBasketItems()
    {
        return $this->basketItems;
    }
	
	/**
     * Returns the amount
     *
     * @return double	$amount
     */
    public function getAmount()
    {
		$amount = 0;
		foreach ($this->basketItems as $item)
		{
			$amount += $item->getTotal();
		}
		return $amount;
    }
	
	/**
     * Returns the total weight
     *
     * @return double	$weight
     */
    public function getWeight()
    {
		$weight = 0;
		foreach ($this->basketItems as $item)
		{
			if (!empty($item->getUnitWeight()))
			{
				$temp = $item->getUnitWeight();
			}
			else
			{
				$temp = $item->getProduct()->getWeight();
			}
			$temp = ($temp == NULL) ? 0 : $temp;
			$temp = $item->getQuantity() * $temp;
			$weight = $weight + $temp;
		}
		return $weight;
    }
	
	/**
     * Returns the downlaods information (has it downloads in this basket?)
     *
     * @return boolean	$hasDownloads
     */
    public function getHasDownloads()
    {
		$downloads = 0;
		foreach ($this->basketItems as $item)
		{
			if ($item->getProduct()->getIsDownload())
			{
				$downloads++;
			}
		}
		return ($downloads > 0) ? true : false;
    }
	
	/**
     * Returns the only downlaods information (has it only downloads in this basket?)
     *
     * @return boolean	$hasOnlyDownloads
     */
    public function getHasOnlyDownloads()
    {
		$downloads = 0;
		$others = 0;
		foreach ($this->basketItems as $item)
		{
			if ($item->getProduct()->getIsDownload())
			{
				$downloads++;
			}
			else
			{
				$others++;
			}
		}
		return ($others==0 && $downloads>0) ? true : false;
    }
	
	/**
     * Sets the tsBasket
     *
     * @param string $tsBasket
     */
    public function setTsBasket($tsBasket)
    {
        $this->tsBasket = $tsBasket;
    }
	
	/**
     * Returns the tsBasket
     *
     * @return string	the tsBasket
     */
    public function getTsBasket()
    {
        return $this->tsBasket;
    }
	
	/**
     * Sets the tsDownload
     *
     * @param string $tsDownload
     */
    public function setTsDownload($tsDownload)
    {
        $this->tsDownload = $tsDownload;
    }
	
	/**
     * Returns the tsDownload
     *
     * @return string	the tsDownload
     */
    public function getTsDownload()
    {
        return $this->tsDownload;
    }
	
	/**
     * Sets the tax
     *
     * @param double $tax
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    }

	/**
     * Returns the tax
     *
     * @return double	$tax
     */
    public function getTax()
    {
		$amount = 0;
		$tax = is_numeric($this->settings['checkout']['tax']) ? (double)$this->settings['checkout']['tax'] : 0;
		if ($tax > 0)
		{
			$amount = $this->getAmount() / 100 * $tax;
		}
		$amount = \Maagit\Maagitproduct\Service\Content\TransformService::roundCurrency($amount, $this->settings['checkout']['currency']['round']);
		return $amount;
    }

	/**
     * Sets the minordervalue
     *
     * @param double $minOrderValue
     */
    public function setMinOrderValue($minOrderValue)
    {
        $this->minOrderValue = $minOrderValue;
    }

	/**
     * Returns the minOrderValue
     *
     * @return double	$minOrderValue
     */
    public function getMinOrderValue()
    {
		if (empty($this->minOrderValue)) {$this->minOrderValue = 0;}
		if (!is_numeric($this->minOrderValue)) {$this->minOrderValue = 0;}
		return $this->minOrderValue;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}