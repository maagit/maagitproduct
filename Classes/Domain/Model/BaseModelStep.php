<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				BaseModelStep

	description:		Base class for Model classes, which represent a step from the
						checkout process. Inherits properties "visited" and "valid" to
						mark the steps, if they are visited and valid.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class BaseModelStep extends \Maagit\Maagitproduct\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
    /**
     * @var \Maagit\Maagitproduct\Domain\Model\Checkout
     */
	protected $checkout;

	/**
     * @var bool
     */
    protected $visited;
	
    /**
     * @var bool
     */
    protected $valid;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the checkout
     *
     * @param \Maagit\Maagitproduct\Domain\Model\Checkout $checkout
     */
    public function setCheckout($checkout)
    {
		$this->checkout = $checkout;
    }
	
	/**
     * Returns the checkout
     *
     * @return \Maagit\Maagitproduct\Domain\Model\Checkout	the checkout object
     */
    public function getCheckout()
    {
		return $this->checkout;
    }

	/**
     * Sets the visited
     *
     * @param bool $visited
     */
    public function setVisited($visited)
    {
        $this->visited = $visited;
    }
	
	/**
     * Returns the visited
     *
     * @return bool	$visited
     */
    public function getVisited()
    {
        return $this->visited;
    }
	
	/**
     * Sets the valid
     *
     * @param bool $valid
     */
    public function setValid($valid)
    {
        $this->valid = $valid;
    }
	
	/**
     * Returns the valid
     *
     * @return bool	$valid
     */
    public function getValid()
    {
        return $this->valid;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}