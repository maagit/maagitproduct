<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				Delivery

	description:		Model for the "delivery" step of the checkout process.
						Inherits datas for managing the delivery aspects.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Delivery extends \Maagit\Maagitproduct\Domain\Model\BaseModelStep
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var array
     */
	protected $deliveryMethods;

	/**
     * @var string
     */
    protected $deliveryMethod;
	
	/**
     * @var array
     */
    protected $deliveryMethodInfo;
	
    /**
     * @var double
     */
	protected $shipping;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Returns the valid
     *
     * @return bool	$valid
     */
    public function getValid()
    {
        if ($this->valid && $this->getDeliveryMethod() == '' && !$this->checkout->getBasket()->getHasOnlyDownloads())
		{
			$this->valid = false;
		}
        if ($this->visited && $this->checkout->getBasket()->getHasOnlyDownloads())
		{
			$this->valid = true;
		}
		return $this->valid;
    }

	/**
     * Sets the deliveryMethods
     *
     * @param array $deliveryMethods
     */
    public function setDeliveryMethods($deliveryMethods)
    {
		$this->deliveryMethods = $deliveryMethods;
    }
	
	/**
     * Returns the deliveryMethods
     *
     * @return array	the delivery methods
     */
    public function getDeliveryMethods()
    {
		return $this->deliveryMethods;
    }
	
	/**
     * Sets the deliveryMethod
     *
     * @param string $deliveryMethod
     */
    public function setDeliveryMethod($deliveryMethod)
    {
		$this->deliveryMethod = $deliveryMethod;
    }
	
	/**
     * Returns the deliveryMethod
     *
     * @return string	the delivery method
     */
    public function getDeliveryMethod()
    {
		return $this->deliveryMethod;
    }
	
	/**
     * Returns the deliveryMethodInfo
     *
     * @return array	the selected delivery method as array with all it's details
     */
    public function getDeliveryMethodInfo()
    {
		$index = array_search($this->getDeliveryMethod(), array_column($this->getDeliveryMethods(), 'id'));
		if ($index === FALSE)
		{
			return array();
		}
		return $this->getDeliveryMethods()[$index];
    }
	
	/**
     * Sets the shipping
     *
     * @param double $shipping
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    }

	/**
     * Returns the shipping
     *
     * @param string	deliveryId
	 * @return double	$shipping
     */
    public function getShipping(string $deliveryId = NULL)
    {
		$checkoutCalculationService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Checkout\\CheckoutCalculationService', $this->checkout);
		return $checkoutCalculationService->calculateShipping($deliveryId);
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	
 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}