<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				BillingAddress

	description:		Model for the "billing address".
						Inherits datas for managing the customer addresses.

	created:			2022-04-18
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-04-18	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class BillingAddress extends \Maagit\Maagitproduct\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var \Maagit\Maagitproduct\Service\Content\TsConfigService
     */
    protected $tsConfigService;

	/**
     * @var array
     */
	protected $addressFields;
	
    /**
     * @var array
     */
	protected $requiredFields;

	/**
     * @var string
     */
    protected $salutation;
	
	/**
     * @var array
     */
    protected $salutationOptions;

	/**
     * @var string
     */
    protected $firma;

	/**
     * @var string
     */
    protected $prename;

	/**
     * @var string
     */
    protected $lastname;
	
	/**
     * @var string
     */
    protected $address;
	
	/**
     * @var string
     */
    protected $street;
	
	/**
     * @var int
     */
    protected $houseNumber;
	
	/**
     * @var string
     */
    protected $houseAdd;
	
	/**
     * @var string
     */
    protected $po;
	
	/**
     * @var string
     */
    protected $city;
	
	/**
     * @var string
     */
    protected $poCity;
	
	/**
     * @var string
     */
    protected $country;
	
	/**
     * @var array
     */
    protected $countryOptions;
	
	/**
     * @var string
     */
    protected $telephone;
	
	/**
     * @var string
     */
    protected $mobile;
	
	/**
     * @var string
     */
    protected $email;
	
	/**
     * @var string
     */
    protected $sex;
	
	/**
     * @var array
     */
    protected $sexOptions;
	
	/**
     * @var string
     */
    protected $birthdate;
	
	/**
     * @var string
     */
    protected $message;
	

	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function initializeObject()
	{
		// inject services
		$this->tsConfigService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Content\\TsConfigService');
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the tsConfigService
     *
     * @param \Maagit\Maagitproduct\Service\Content\TsConfigService $tsConfigService
     */
    public function setTsConfigService($tsConfigService)
    {
        $this->tsConfigService = $tsConfigService;
    }
	
	/**
     * Returns the tsConfigService
     *
     * @return \Maagit\Maagitproduct\Service\Content\TsConfigService	$tsConfigService
     */
    public function getTsConfigService()
    {
		return $this->tsConfigService;
    }

	/**
     * Returns the selected address fields
     *
     * @return array	$addressFields
     */
    public function getAddressFields()
    {
		return explode(',', $this->settings['checkout']['address']['billSelected']);
    }

	/**
     * Returns the required fields
     *
     * @return array	$requiredFields
     */
    public function getRequiredFields()
    {
		$fields = array();
		foreach (explode(',', $this->settings['checkout']['address']['billRequired']) as $key => $value)
		{
			$fields[$value] = 1;
		}
        return $fields;
    }

	/**
     * Sets the salutation
     *
     * @param string $salutation
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;
    }
	
	/**
     * Returns the salutation
     *
     * @return string	$salutation
     */
    public function getSalutation()
    {
        return $this->salutation;
    }
	
	/**
     * Returns the salutation options
     *
     * @return array	$salutationOptions
     */
    public function getSalutationOptions()
    {
		return $this->tsConfigService->getTsConfigKeyValuePairs(array('tx_maagitproduct_salutation.', 'addItems.'));
    }

	/**
     * Sets the firma
     *
     * @param string $firma
     */
    public function setFirma($firma)
    {
        $this->firma = $firma;
    }
	
	/**
     * Returns the firma
     *
     * @return string	$firma
     */
    public function getFirma()
    {
        return $this->firma;
    }

	/**
     * Sets the prename
     *
     * @param string $prename
     */
    public function setPrename($prename)
    {
        $this->prename = $prename;
    }
	
	/**
     * Returns the prename
     *
     * @return string	$prename
     */
    public function getPrename()
    {
        return $this->prename;
    }
	
	/**
     * Sets the lastname
     *
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }
	
	/**
     * Returns the lastname
     *
     * @return string	$lastname
     */
    public function getLastname()
    {
        return $this->lastname;
    }
	
	/**
     * Sets the street
     *
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }
	
	/**
     * Returns the street
     *
     * @return string	$street
     */
    public function getStreet()
    {
        return $this->street;
    }
	
	/**
     * Sets the houseNumber
     *
     * @param string $houseNumber
     */
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;
    }
	
	/**
     * Returns the houseNumber
     *
     * @return string	$houseNumber
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }
	
	/**
     * Sets the houseAdd
     *
     * @param string $houseAdd
     */
    public function setHouseAdd($houseAdd)
    {
        $this->houseAdd = $houseAdd;
    }
	
	/**
     * Returns the houseAdd
     *
     * @return string	$houseAdd
     */
    public function getHouseAdd()
    {
        return $this->houseAdd;
    }
	
	/**
     * Sets the address
     *
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
	
	/**
     * Returns the address
     *
     * @return string	$address
     */
    public function getAddress()
    {
        return $this->address;
    }
	
	/**
     * Sets the po
     *
     * @param string $po
     */
    public function setPo($po)
    {
        $this->po = $po;
    }
	
	/**
     * Returns the po
     *
     * @return string	$po
     */
    public function getPo()
    {
        return $this->po;
    }
	
	/**
     * Sets the city
     *
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }
	
	/**
     * Returns the city
     *
     * @return string	$city
     */
    public function getCity()
    {
        return $this->city;
    }
	
	/**
     * Sets the poCity
     *
     * @param string $poCity
     */
    public function setPoCity($poCity)
    {
        $this->poCity = $poCity;
    }
	
	/**
     * Returns the poCity
     *
     * @return string	$poCity
     */
    public function getPoCity()
    {
        return $this->poCity;
    }
	
	/**
     * Sets the country
     *
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }
	
	/**
     * Returns the country
     *
     * @return string	$country
     */
    public function getCountry()
    {
        return $this->country;
    }
	
	/**
     * Returns the country options
     *
     * @return array	$countryOptions
     */
    public function getCountryOptions()
    {
		return $this->tsConfigService->getTsConfigKeyValuePairs(array('tx_maagitproduct_country.', 'addItems.'));
    }
	
	/**
     * Sets the telephone
     *
     * @param string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }
	
	/**
     * Returns the telephone
     *
     * @return string	$telephone
     */
    public function getTelephone()
    {
        return $this->telephone;
    }
	
	/**
     * Sets the mobile
     *
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }
	
	/**
     * Returns the mobile
     *
     * @return string	$mobile
     */
    public function getMobile()
    {
        return $this->mobile;
    }
	
	/**
     * Sets the email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
	
	/**
     * Returns the email
     *
     * @return string	$email
     */
    public function getEmail()
    {
        return $this->email;
    }
	
	/**
     * Sets the sex
     *
     * @param string $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }
	
	/**
     * Returns the sex
     *
     * @return string	$sex
     */
    public function getSex()
    {
        return $this->sex;
    }
	
	/**
     * Returns the sex options
     *
     * @return array	$sexOptions
     */
    public function getSexOptions()
    {
		return $this->tsConfigService->getTsConfigKeyValuePairs(array('tx_maagitproduct_sex.', 'addItems.'));
    }
	
	/**
     * Sets the birthdate
     *
     * @param string $birthdate
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    }
	
	/**
     * Returns the birthdate
     *
     * @return string	$birthdate
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }
	
	/**
     * Sets the message
     *
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
	
	/**
     * Returns the message
     *
     * @return string	$message
     */
    public function getMessage()
    {
        return $this->message;
    }
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	
 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}