<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				Rating

	description:		Model for the rating of products.
						Model fits to the database table tx_maagitproduct_rating.

	created:			2020-08-25
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-08-25	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Rating extends \Maagit\Maagitproduct\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var int
     */
    protected ?int $uid = null;

	/**
	 * @var int
     */
    protected $ttContent;
	
	/**
	 * @var string
     */
    protected $name;

	/**
     * @var string
     */
    protected $email;

	/**
     * @var string
     */
    protected $comment;

	/**
     * @var double
     */
    protected $rating;
	
	/**
     * @var \DateTime
     */
    protected $crdate;

	/**
     * @var \DateTime
     */
    protected $tstamp;

	/**
     * @var bool
     */
    protected $hidden;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
	 * Set the uid
	 *
	 * @param int $uid
	 */
	public function setUid($uid)
	{
	    $this->uid = $uid;
	}

	/**
     * Sets the ttContent
     *
     * @param int $ttContent
     */
    public function setTtContent($ttContent)
    {
        $this->ttContent = $ttContent;
    }
	
    /**
     * Get the ttContent
     *
     * @return int
     */
    public function getTtContent()
    {
        return $this->ttContent;
    }
	
	/**
     * Sets the name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
	
    /**
     * Get the name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
	
	/**
     * Sets the email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get the email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
	
	/**
     * Sets the comment
     *
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }
	
    /**
     * Get the comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

	/**
     * Sets the rating
     *
     * @param double $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }
	
    /**
     * Get the rating
     *
     * @return double
     */
    public function getRating()
    {
        return $this->rating;
    }

	/**
     * Sets the crdate
     *
     * @param \DateTime $crdate
     */
    public function setCrdate($crdate)
    {
        $this->crdate = $crdate;
    }
	
    /**
     * Get the crdate
     *
     * @return \DateTime
     */
    public function getCrdate()
    {
        return $this->crdate;
    }
	
	/**
     * Sets the tstamp
     *
     * @param \DateTime $tstamp
     */
    public function setTstamp($tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * Get the tstamp
     *
     * @return \DateTime
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

	/**
     * Sets the hidden
     *
     * @param bool $hidden
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }
	
    /**
     * Get the hidden
     *
     * @return bool
     */
    public function getHidden()
    {
        return $this->hidden;
    }	


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}