<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				Order

	description:		Model for the order history.
						Model fits to the database table tx_maagitproduct_order.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Order extends \Maagit\Maagitproduct\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \DateTime
     */
    protected $crdate;

	/**
	 * @var \DateTime
     */
    protected $paymentdone;

	/**
	 * @var \DateTime
     */
    protected $shippingdone;	

	/**
	 * @var string
     */
    protected $orderid;

	/**
	 * @var string
     */
    protected $userid;

	/**
	 * @var string
     */
    protected $salutation;
	
	/**
	 * @var string
     */
    protected $firma;

	/**
	 * @var string
     */
    protected $prename;
	
	/**
     * @var string
     */
    protected $lastname;
	
	/**
	 * @var string
     */
    protected $street;
	
	/**
	 * @var string
     */
    protected $housenumber;
	
	/**
	 * @var string
     */
    protected $houseadd;
	
	/**
	 * @var string
     */
    protected $address;
	
	/**
	 * @var string
     */
    protected $po;
	
	/**
	 * @var string
     */
    protected $city;
	
	/**
	 * @var string
     */
    protected $pocity;
	
	/**
	 * @var string
     */
    protected $country;
	
	/**
	 * @var string
     */
    protected $telephone;
	
	/**
	 * @var string
     */
    protected $mobile;

	/**
	 * @var string
     */
    protected $email;
	
	/**
	 * @var string
     */
    protected $sex;
	
	/**
	 * @var string
     */
    protected $birthdate;
	
	/**
	 * @var string
     */
    protected $message;
	
	/**
	 * @var string
     */
    protected $bill;

	/**
	 * @var string
     */
    protected $billingsalutation;
	
	/**
	 * @var string
     */
    protected $billingfirma;

	/**
	 * @var string
     */
    protected $billingprename;
	
	/**
     * @var string
     */
    protected $billinglastname;
	
	/**
	 * @var string
     */
    protected $billingstreet;
	
	/**
	 * @var string
     */
    protected $billinghousenumber;
	
	/**
	 * @var string
     */
    protected $billinghouseadd;
	
	/**
	 * @var string
     */
    protected $billingaddress;
	
	/**
	 * @var string
     */
    protected $billingpo;
	
	/**
	 * @var string
     */
    protected $billingcity;
	
	/**
	 * @var string
     */
    protected $billingpocity;
	
	/**
	 * @var string
     */
    protected $billingcountry;
	
	/**
	 * @var string
     */
    protected $billingtelephone;
	
	/**
	 * @var string
     */
    protected $billingmobile;

	/**
	 * @var string
     */
    protected $billingemail;
	
	/**
	 * @var string
     */
    protected $billingsex;
	
	/**
	 * @var string
     */
    protected $billingbirthdate;
	
	/**
	 * @var string
     */
    protected $billingmessage;

	/**
	 * @var string
     */
    protected $taxpercent;
	
	/**
	 * @var string
     */
    protected $delivery;
	
	/**
	 * @var string
     */
    protected $payment;
	
	/**
	 * @var string
     */
    protected $paymenttransactionid;
	
	/**
	 * @var string
     */
    protected $paymenttext;
	
	/**
	 * @var string
     */
    protected $basketamount;

	/**
	 * @var string
     */
    protected $taxamount;
	
	/**
	 * @var string
     */
    protected $shipping;
	
	/**
	 * @var string
     */
    protected $expenses;
	
	/**
	 * @var string
     */
    protected $totalamount;

	/**
	 * @var string
     */
    protected $couponcode;

	/**
	 * @var string
     */
    protected $coupondiscount;

	/**
	 * @var string
     */
    protected $coupontext;

	/**
	 * @var string
     */
    protected $terms;

	/**
	 * @var string
     */
    protected $receiver;
	
	/**
	 * @var string
     */
    protected $senderconfirmation;
	
	/**
	 * @var string
     */
    protected $senderconfirmationfrom;
	
	/**
	 * @var string
     */
    protected $downloadsecure;
	
	/**
	 * @var string
     */
    protected $downloadfrom;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitproduct\Domain\Model\Article>
     */
    protected $articles;

    
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Initialization.
     *
	 * @param	-
	 * @return	void
     */
	public function initializeObject()
	{
		$this->articles = $this->makeInstance('TYPO3\CMS\Extbase\Persistence\ObjectStorage');
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
    /**
     * Get the crdate
     *
     * @return \DateTime
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * Get the paymentdone
     *
     * @return \DateTime
     */
    public function getPaymentdone()
    {
        return $this->paymentdone;
    }

    /**
     * Get the shippingdone
     *
     * @return \DateTime
     */
    public function getShippingdone()
    {
        return $this->shippingdone;
    }

	/**
     * Sets the orderid
     *
     * @param string $orderid
     */
    public function setOrderid($orderid)
    {
        $this->orderid = $orderid;
    }
	
    /**
     * Get the orderid
     *
     * @return string
     */
    public function getOrderid()
    {
        return $this->orderid;
    }

	/**
     * Sets the userid
     *
     * @param string $userid
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
    }
	
    /**
     * Get the userid
     *
     * @return string
     */
    public function getUserid()
    {
        return $this->userid;
    }

	/**
     * Sets the salutation
     *
     * @param string $salutation
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;
    }
	
    /**
     * Get the salutation
     *
     * @return string
     */
    public function getSalutation()
    {
        return $this->salutation;
    }
	
	/**
     * Sets the firma
     *
     * @param string $firma
     */
    public function setFirma($firma)
    {
        $this->firma = $firma;
    }
	
    /**
     * Get the firma
     *
     * @return string
     */
    public function getFirma()
    {
        return $this->firma;
    }
	
	/**
     * Sets the prename
     *
     * @param string $prename
     */
    public function setPrename($prename)
    {
        $this->prename = $prename;
    }
	
    /**
     * Get the prename
     *
     * @return string
     */
    public function getPrename()
    {
        return $this->prename;
    }
	
	/**
     * Sets the lastname
     *
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }
	
    /**
     * Get the lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }
	
	/**
     * Sets the street
     *
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }
	
    /**
     * Get the street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }
	
	/**
     * Sets the housenumber
     *
     * @param string $housenumber
     */
    public function setHousenumber($housenumber)
    {
        $this->housenumber = $housenumber;
    }
	
    /**
     * Get the housenumber
     *
     * @return string
     */
    public function getHousenumber()
    {
        return $this->housenumber;
    }
	
	/**
     * Sets the houseadd
     *
     * @param string $houseadd
     */
    public function setHouseadd($houseadd)
    {
        $this->houseadd = $houseadd;
    }
	
    /**
     * Get the houseadd
     *
     * @return string
     */
    public function getHouseadd()
    {
        return $this->houseadd;
    }
	
	/**
     * Sets the address
     *
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
	
    /**
     * Get the address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }
	
	/**
     * Sets the po
     *
     * @param string $po
     */
    public function setPo($po)
    {
        $this->po = $po;
    }
	
    /**
     * Get the po
     *
     * @return string
     */
    public function getPo()
    {
        return $this->po;
    }
	
	/**
     * Sets the city
     *
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }
	
    /**
     * Get the city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }
	
	/**
     * Sets the pocity
     *
     * @param string $pocity
     */
    public function setPocity($pocity)
    {
        $this->pocity = $pocity;
    }
	
    /**
     * Get the pocity
     *
     * @return string
     */
    public function getPocity()
    {
        return $this->pocity;
    }
	
	/**
     * Sets the country
     *
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }
	
    /**
     * Get the country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }
	
	/**
     * Sets the telephone
     *
     * @param string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }
	
    /**
     * Get the telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }
	
	/**
     * Sets the mobile
     *
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }
	
    /**
     * Get the mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }
	
	/**
     * Sets the email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
	
    /**
     * Get the email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
	
	/**
     * Sets the sex
     *
     * @param string $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }
	
    /**
     * Get the sex
     *
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }
	
	/**
     * Sets the birthdate
     *
     * @param string $birthdate
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    }
	
    /**
     * Get the birthdate
     *
     * @return string
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }
	
	/**
     * Sets the message
     *
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
	
    /**
     * Get the message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

	/**
     * Sets the bill
     *
     * @param string $bill
     */
    public function setBill($bill)
    {
        $this->bill = $bill;
    }
	
    /**
     * Get the bill
     *
     * @return string
     */
    public function getBill()
    {
        return $this->bill;
    }

	/**
     * Sets the billingsalutation
     *
     * @param string $billingsalutation
     */
    public function setBillingsalutation($billingsalutation)
    {
        $this->billingsalutation = $billingsalutation;
    }
	
    /**
     * Get the billingsalutation
     *
     * @return string
     */
    public function getBillingsalutation()
    {
        return $this->billingsalutation;
    }
	
	/**
     * Sets the billingfirma
     *
     * @param string $billingfirma
     */
    public function setBillingfirma($billingfirma)
    {
        $this->billingfirma = $billingfirma;
    }
	
    /**
     * Get the billingfirma
     *
     * @return string
     */
    public function getBillingfirma()
    {
        return $this->billingfirma;
    }
	
	/**
     * Sets the billingprename
     *
     * @param string $billingprename
     */
    public function setBillingprename($billingprename)
    {
        $this->billingprename = $billingprename;
    }
	
    /**
     * Get the billingprename
     *
     * @return string
     */
    public function getBillingprename()
    {
        return $this->billingprename;
    }
	
	/**
     * Sets the billinglastname
     *
     * @param string $billinglastname
     */
    public function setBillinglastname($billinglastname)
    {
        $this->billinglastname = $billinglastname;
    }
	
    /**
     * Get the billinglastname
     *
     * @return string
     */
    public function getBillinglastname()
    {
        return $this->billinglastname;
    }
	
	/**
     * Sets the billingstreet
     *
     * @param string $billingstreet
     */
    public function setBillingstreet($billingstreet)
    {
        $this->billingstreet = $billingstreet;
    }
	
    /**
     * Get the billingstreet
     *
     * @return string
     */
    public function getBillingstreet()
    {
        return $this->billingstreet;
    }
	
	/**
     * Sets the billinghousenumber
     *
     * @param string $billinghousenumber
     */
    public function setBillinghousenumber($billinghousenumber)
    {
        $this->billinghousenumber = $billinghousenumber;
    }
	
    /**
     * Get the billinghousenumber
     *
     * @return string
     */
    public function getBillinghousenumber()
    {
        return $this->billinghousenumber;
    }
	
	/**
     * Sets the billinghouseadd
     *
     * @param string $billinghouseadd
     */
    public function setBillinghouseadd($billinghouseadd)
    {
        $this->billinghouseadd = $billinghouseadd;
    }
	
    /**
     * Get the billinghouseadd
     *
     * @return string
     */
    public function getBillinghouseadd()
    {
        return $this->billinghouseadd;
    }
	
	/**
     * Sets the billingaddress
     *
     * @param string $billingaddress
     */
    public function setBillingaddress($billingaddress)
    {
        $this->billingaddress = $billingaddress;
    }
	
    /**
     * Get the billingaddress
     *
     * @return string
     */
    public function getBillingaddress()
    {
        return $this->billingaddress;
    }
	
	/**
     * Sets the billingpo
     *
     * @param string $billingpo
     */
    public function setBillingpo($billingpo)
    {
        $this->billingpo = $billingpo;
    }
	
    /**
     * Get the billingpo
     *
     * @return string
     */
    public function getBillingpo()
    {
        return $this->billingpo;
    }
	
	/**
     * Sets the billingcity
     *
     * @param string $billingcity
     */
    public function setBillingcity($billingcity)
    {
        $this->billingcity = $billingcity;
    }
	
    /**
     * Get the billingcity
     *
     * @return string
     */
    public function getBillingcity()
    {
        return $this->billingcity;
    }
	
	/**
     * Sets the billingpocity
     *
     * @param string $billingpocity
     */
    public function setBillingpocity($billingpocity)
    {
        $this->billingpocity = $billingpocity;
    }
	
    /**
     * Get the billingpocity
     *
     * @return string
     */
    public function getBillingpocity()
    {
        return $this->billingpocity;
    }
	
	/**
     * Sets the billingcountry
     *
     * @param string $billingcountry
     */
    public function setBillingcountry($billingcountry)
    {
        $this->billingcountry = $billingcountry;
    }
	
    /**
     * Get the billingcountry
     *
     * @return string
     */
    public function getBillingcountry()
    {
        return $this->billingcountry;
    }
	
	/**
     * Sets the billingtelephone
     *
     * @param string $billingtelephone
     */
    public function setBillingtelephone($billingtelephone)
    {
        $this->billingtelephone = $billingtelephone;
    }
	
    /**
     * Get the billingtelephone
     *
     * @return string
     */
    public function getBillingtelephone()
    {
        return $this->billingtelephone;
    }
	
	/**
     * Sets the billingmobile
     *
     * @param string $billingmobile
     */
    public function setBillingmobile($billingmobile)
    {
        $this->billingmobile = $billingmobile;
    }
	
    /**
     * Get the billingmobile
     *
     * @return string
     */
    public function getBillingmobile()
    {
        return $this->billingmobile;
    }
	
	/**
     * Sets the billingemail
     *
     * @param string $billingemail
     */
    public function setBillingemail($billingemail)
    {
        $this->billingemail = $billingemail;
    }
	
    /**
     * Get the billingemail
     *
     * @return string
     */
    public function getBillingemail()
    {
        return $this->billingemail;
    }
	
	/**
     * Sets the billingsex
     *
     * @param string $billingsex
     */
    public function setBillingsex($billingsex)
    {
        $this->billingsex = $billingsex;
    }
	
    /**
     * Get the billingsex
     *
     * @return string
     */
    public function getBillingsex()
    {
        return $this->billingsex;
    }
	
	/**
     * Sets the billingbirthdate
     *
     * @param string $billingbirthdate
     */
    public function setBillingbirthdate($billingbirthdate)
    {
        $this->billingbirthdate = $billingbirthdate;
    }
	
    /**
     * Get the billingbirthdate
     *
     * @return string
     */
    public function getBillingbirthdate()
    {
        return $this->billingbirthdate;
    }
	
	/**
     * Sets the billingmessage
     *
     * @param string $billingmessage
     */
    public function setBillingmessage($billingmessage)
    {
        $this->billingmessage = $billingmessage;
    }
	
    /**
     * Get the billingmessage
     *
     * @return string
     */
    public function getBillingmessage()
    {
        return $this->billingmessage;
    }

	/**
     * Sets the taxpercent
     *
     * @param string $taxpercent
     */
    public function setTaxpercent($taxpercent)
    {
        $this->taxpercent = $taxpercent;
    }
	
    /**
     * Get the taxpercent
     *
     * @return string
     */
    public function getTaxpercent()
    {
        return $this->taxpercent;
    }
	
	/**
     * Sets the delivery
     *
     * @param string $delivery
     */
    public function setDelivery($delivery)
    {
        $this->delivery = $delivery;
    }
	
    /**
     * Get the delivery
     *
     * @return string
     */
    public function getDelivery()
    {
        return $this->delivery;
    }
	
	/**
     * Sets the payment
     *
     * @param string $payment
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;
    }
	
    /**
     * Get the payment
     *
     * @return string
     */
    public function getPayment()
    {
        return $this->payment;
    }
	
	/**
     * Sets the paymenttransactionid
     *
     * @param string $paymenttransactionid
     */
    public function setPaymenttransactionId($paymenttransactionid)
    {
        $this->paymenttransactionid = $paymenttransactionid;
    }
	
    /**
     * Get the paymenttransactionid
     *
     * @return string
     */
    public function getPaymenttransactionid()
    {
        return $this->paymenttransactionid;
    }
	
	/**
     * Sets the paymenttext
     *
     * @param string $paymenttext
     */
    public function setPaymenttext($paymenttext)
    {
		$this->paymenttext = $paymenttext;
    }
	
    /**
     * Get the paymenttext
     *
     * @return string
     */
    public function getPaymenttext()
    {
        return $this->paymenttext;
    }
	
	/**
     * Sets the basketamount
     *
     * @param string $basketamount
     */
    public function setBasketamount($basketamount)
    {
		$this->basketamount = $basketamount;
    }
	
    /**
     * Get the basketamount
     *
     * @return string
     */
    public function getBasketamount()
    {
        return $this->basketamount;
    }
	
	/**
     * Sets the taxamount
     *
     * @param string $taxamount
     */
    public function setTaxamount($taxamount)
    {
		$this->taxamount = $taxamount;
    }
	
    /**
     * Get the taxamount
     *
     * @return string
     */
    public function getTaxamount()
    {
        return $this->taxamount;
    }
	
	/**
     * Sets the shipping
     *
     * @param string $shipping
     */
    public function setShipping($shipping)
    {
		$this->shipping = $shipping;
    }
	
    /**
     * Get the shipping
     *
     * @return string
     */
    public function getShipping()
    {
        return $this->shipping;
    }
	
	/**
     * Sets the expenses
     *
     * @param string $expenses
     */
    public function setExpenses($expenses)
    {
		$this->expenses = $expenses;
    }
	
    /**
     * Get the expenses
     *
     * @return string
     */
    public function getExpenses()
    {
        return $this->expenses;
    }
	
	/**
     * Sets the totalamount
     *
     * @param string $totalamount
     */
    public function setTotalamount($totalamount)
    {
		$this->totalamount = $totalamount;
    }
	
    /**
     * Get the totalamount
     *
     * @return string
     */
    public function getTotalamount()
    {
        return $this->totalamount;
    }

	/**
     * Sets the couponcode
     *
     * @param string $couponcode
     */
    public function setCouponcode($couponcode)
    {
        $this->couponcode = $couponcode;
    }
	
    /**
     * Get the couponcode
     *
     * @return string
     */
    public function getCouponcode()
    {
        return $this->couponcode;
    }

	/**
     * Sets the coupondiscount
     *
     * @param string $coupondiscount
     */
    public function setCoupondiscount($coupondiscount)
    {
        $this->coupondiscount = $coupondiscount;
    }
	
    /**
     * Get the coupondiscount
     *
     * @return string
     */
    public function getCoupondiscount()
    {
        return $this->coupondiscount;
    }

	/**
     * Sets the coupontext
     *
     * @param string $coupontext
     */
    public function setCoupontext($coupontext)
    {
        $this->coupontext = $coupontext;
    }
	
    /**
     * Get the coupontext
     *
     * @return string
     */
    public function getCoupontext()
    {
        return $this->coupontext;
    }

	/**
     * Sets the terms
     *
     * @param string $terms
     */
    public function setTerms($terms)
    {
		$this->terms = $terms;
    }
	
    /**
     * Get the terms
     *
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }

	/**
     * Sets the receiver
     *
     * @param string $receiver
     */
    public function setReceiver($receiver)
    {
		$this->receiver = $receiver;
    }
	
    /**
     * Get the receiver
     *
     * @return string
     */
    public function getReceiver()
    {
        return $this->receiver;
    }
	
	/**
     * Sets the senderconfirmation
     *
     * @param string $senderconfirmation
     */
    public function setSenderconfirmation($senderconfirmation)
    {
		$this->senderconfirmation = $senderconfirmation;
    }
	
    /**
     * Get the senderconfirmation
     *
     * @return string
     */
    public function getSenderconfirmation()
    {
        return $this->senderconfirmation;
    }
	
	/**
     * Sets the senderconfirmationfrom
     *
     * @param string $senderconfirmationfrom
     */
    public function setSenderconfirmationfrom($senderconfirmationfrom)
    {
		$this->senderconfirmationfrom = $senderconfirmationfrom;
    }
	
    /**
     * Get the senderconfirmationfrom
     *
     * @return string
     */
    public function getSenderconfirmationfrom()
    {
        return $this->senderconfirmationfrom;
    }
	
	/**
     * Sets the downloadsecure
     *
     * @param string $downloadsecure
     */
    public function setDownloadsecure($downloadsecure)
    {
		$this->downloadsecure = $downloadsecure;
    }
	
    /**
     * Get the downloadsecure
     *
     * @return string
     */
    public function getDownloadsecure()
    {
        return $this->downloadsecure;
    }

	/**
     * Sets the downloadfrom
     *
     * @param string $downloadfrom
     */
    public function setDownloadfrom($downloadfrom)
    {
		$this->downloadfrom = $downloadfrom;
    }
	
    /**
     * Get the downloadfrom
     *
     * @return string
     */
    public function getDownloadfrom()
    {
        return $this->downloadfrom;
    }

    /**
     * Sets the articles
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitproduct\Domain\Model\Article>
     */
    public function setArticles(\Maagit\Maagitproduct\Helper\ObjectStorage $articles)
    {
        $this->articles = $articles;
    }

    /**
     * Get the articles
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitproduct\Domain\Model\Article>
     */
    public function getArticles()
    {
        return $this->articles;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Adds a article
     *
     * @param \Maagit\Maagitproduct\Domain\Model\Article $article
     */
	public function addArticle(\Maagit\Maagitproduct\Domain\Model\Article $article)
	{
		$this->articles->attach($article);
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}