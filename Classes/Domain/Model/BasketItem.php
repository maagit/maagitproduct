<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				BasketItem

	description:		Model for the items in the shopping cart (basket).
						Inherits the relevant datas of given articles.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-12-24	Urs Maag		PHP 8, fix non existing array keys

------------------------------------------------------------------------------------- */


class BasketItem extends \Maagit\Maagitproduct\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var int
     */
	protected ?int $uid;

	/**
     * @var int
     */
	protected $quantity;

	/**
     * @var int
     */
	protected $unit;

    /**
     * @var \Maagit\Maagitproduct\Domain\Model\Product
     */
	protected $product;
	
	/**
     * @var double
     */
	protected $total;
	

	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
    /**
     * Returns the uid
     *
     * @return int	$uid
     */
    public function getUid() : ?int
    {
        return $this->uid;
    }

	/**
     * Sets the quantity
     *
     * @param int $quantity
     */
    public function setQuantity($quantity)
	{
		if ($quantity == null)
		{
			if (!isset($this->product->getOrderQuantity()[0]['value']))
			{
				$quantityFromSelection = null;
			}
			else
			{
				$quantityFromSelection = $this->product->getOrderQuantity()[0]['value'];	
			}
			$quantity = ($quantityFromSelection == null) ? 1 : $quantityFromSelection;
		}
		$this->quantity = $quantity;
	}

    /**
     * Returns the quantity
     *
     * @return int	$quantity
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

	/**
     * Sets the unit
     *
     * @param int $unit
     */
    public function setUnit($unit)
	{
		$this->unit = $unit;
	}

    /**
     * Returns the unit
     *
     * @return int	$unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Returns the unit description
     *
     * @return string
     */
    public function getUnitDescription()
    {
        $units = $this->product->getUnit();
		foreach ($units as $unit)
		{
			if ($unit[0] == $this->unit)
			{
				return $unit[1];
			}
		}
		return null;
    }

    /**
     * Returns the unit weight
     *
     * @return int
     */
    public function getUnitWeight()
    {
        $units = $this->product->getUnit();
		foreach ($units as $unit)
		{
			if ($unit[0] == $this->unit)
			{
				return $unit[2];
			}
		}
		return null;
    }

    /**
     * Returns the unit price
     *
     * @return double
     */
    public function getUnitPrice()
    {
        $units = $this->product->getUnit();
		foreach ($units as $unit)
		{
			if ($unit[0] == $this->unit)
			{
				return $unit[3];
			}
		}
		return null;
    }

    /**
     * Returns the unit stock
     *
     * @return double
     */
    public function getUnitStock()
    {
        $units = $this->product->getUnit();
		foreach ($units as $unit)
		{
			if ($unit[0] == $this->unit)
			{
				return $unit[4];
			}
		}
		return null;
    }

	/**
     * Sets the product
     *
     * @param \Maagit\Maagitproduct\Domain\Model\Product $product
     */
    public function setProduct($product)
    {
        if ($product != NULL)
		{
			$this->uid = $product->getUid();	
		}
		$this->product = $product;
    }

    /**
     * Returns the product
     *
     * @return \Maagit\Maagitproduct\Domain\Model\Product	$product
     */
    public function getProduct()
    {
        return $this->product;
    }
	
    /**
     * Returns the total
     *
     * @return double	$total
     */
    public function getTotal()
    {
		if (!empty($this->getUnitPrice()))
		{
			$total = $this->getQuantity() * $this->getUnitPrice();
		}
		else
		{
			$total = $this->getQuantity() * $this->product->getPrice();
		}
		return $total;
    }

	
	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}