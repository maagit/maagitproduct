<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				Article

	description:		Model for the products ("article") in order history.
						Model fits to the database table tx_maagitproduct_article.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2022-06-12	Urs Maag		Downloadmail added

------------------------------------------------------------------------------------- */


class Article extends \Maagit\Maagitproduct\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var int
     */
    protected $order;
	
	/**
	 * @var int
     */
    protected $productuid;

	/**
	 * @var string
     */
    protected $product;

	/**
     * @var string
     */
    protected $unit;

	/**
     * @var string
     */
    protected $description;

	/**
     * @var string
     */
    protected $image;
	
	/**
     * @var string
     */
    protected $link;
	
	/**
     * @var string
     */
    protected $quantity;
	
	/**
     * @var string
     */
    protected $stock;

	/**
     * @var string
     */
    protected $weight;
	
	/**
     * @var string
     */
    protected $price;
	
	/**
     * @var string
     */
    protected $totalamount;

	/**
     * @var \DateTime
     */
    protected $ratingmail;

	/**
     * @var \DateTime
     */
    protected $downloadmail;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the order
     *
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }
	
    /**
     * Get the order
     *
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

	/**
     * Sets the productuid
     *
     * @param int $productuid
     */
    public function setProductuid($productuid)
    {
        $this->productuid = $productuid;
    }

    /**
     * Get the productuid
     *
     * @return int
     */
    public function getProductuid()
    {
        return $this->productuid;
    }

	/**
     * Sets the product
     *
     * @param string $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }
	
    /**
     * Get the product
     *
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }
	
	/**
     * Sets the unit
     *
     * @param string $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    /**
     * Get the unit
     *
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }
	
	/**
     * Sets the description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
	
    /**
     * Get the description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

	/**
     * Sets the image
     *
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }
	
    /**
     * Get the image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
	
	/**
     * Sets the link
     *
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }
	
    /**
     * Get the link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }
	
	/**
     * Sets the quantity
     *
     * @param string $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
	
    /**
     * Get the quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

	/**
     * Sets the stock
     *
     * @param string $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }
	
    /**
     * Get the stock
     *
     * @return string
     */
    public function getStock()
    {
        return $this->stock;
    }

	/**
     * Sets the weight
     *
     * @param string $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }
	
    /**
     * Get the weight
     *
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }
	
	/**
     * Sets the price
     *
     * @param string $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }
	
    /**
     * Get the price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }
	
	/**
     * Sets the totalamount
     *
     * @param string $totalamount
     */
    public function setTotalamount($totalamount)
    {
        $this->totalamount = $totalamount;
    }
	
    /**
     * Get the totalamount
     *
     * @return string
     */
    public function getTotalamount()
    {
        return $this->totalamount;
    }

	/**
     * Sets the ratingmail
     *
     * @param \DateTime $ratingmail
     */
    public function setRatingmail($ratingmail)
    {
        $this->ratingmail = $ratingmail;
    }
	
    /**
     * Get the ratingmail
     *
     * @return \DateTime
     */
    public function getRatingmail()
    {
        return $this->ratingmail;
    }

	/**
     * Sets the downloadmail
     *
     * @param \DateTime $downloadmail
     */
    public function setDownloadmail($downloadmail)
    {
        $this->downloadmail = $downloadmail;
    }
	
    /**
     * Get the downloadmail
     *
     * @return \DateTime
     */
    public function getDownloadmail()
    {
        return $this->downloadmail;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}