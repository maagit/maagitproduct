<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				User

	description:		Model for the user
						Inherits datas for managing the user records.

	created:			2024-02-14
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2024-02-14	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class User extends \Maagit\Maagitproduct\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var string
	 */
	protected $username;

	/**
	 * @var string
	 */
	protected $company;

	/**
	 * @var string
	 */
	protected $title;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $firstName;

	/**
	 * @var string
	 */
	protected $first_name;

	/**
	 * @var string
	 */
	protected $middleName;

	/**
	 * @var string
	 */
	protected $middle_name;

	/**
	 * @var string
	 */
	protected $lastName;

	/**
	 * @var string
	 */
	protected $last_name;

	/**
	 * @var string
	 */
	protected $address;

	/**
	 * @var string
	 */
	protected $zip;

	/**
	 * @var string
	 */
	protected $city;

	/**
	 * @var string
	 */
	protected $country;

	/**
	 * @var string
	 */
	protected $telephone;

	/**
	 * @var string
	 */
	protected $fax;

	/**
	 * @var string
	 */
	protected $email;

	/**
	 * @var string
	 */
	protected $www;

	/**
	 * @var string
	 */
	protected $usergroup;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

	/**
     * Returns the username
     *
     * @return string	$username
     */
    public function getUsername()
    {
		return $this->username;
    }

	/**
     * Sets the company
     *
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

	/**
     * Returns the company
     *
     * @return string	$company
     */
    public function getCompany()
    {
        return $this->company;
    }

	/**
     * Sets the title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

	/**
     * Returns the title
     *
     * @return string	$title
     */
    public function getTitle()
    {
        return $this->title;
    }

	/**
     * Sets the name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

	/**
     * Returns the name
     *
     * @return string	$name
     */
    public function getName()
    {
        return $this->name;
    }

	/**
     * Sets the firstName
     *
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
		$this->first_name = $firstName;
    }

	/**
     * Returns the firstName
     *
     * @return string	$firstName
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
	
	/**
     * Sets the middleName
     *
     * @param string $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
		$this->middle_name = $middleName;
    }

	/**
     * Returns the middleName
     *
     * @return string	$middleName
     */
    public function getMiddleName()
    {
        return $this->middleName;
	}

	/**
     * Sets the lastName
     *
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
		$this->last_name = $lastName;
    }

	/**
     * Returns the lastName
     *
     * @return string	$lastName
     */
    public function getLastName()
    {
        return $this->lastName;
    }

	/**
     * Sets the address
     *
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

	/**
     * Returns the address
     *
     * @return string	$address
     */
    public function getAddress()
    {
        return $this->address;
    }

	/**
     * Sets the zip
     *
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

	/**
     * Returns the zip
     *
     * @return string	$zip
     */
    public function getZip()
    {
        return $this->zip;
    }

	/**
     * Sets the city
     *
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

	/**
     * Returns the city
     *
     * @return string	$city
     */
    public function getCity()
    {
        return $this->city;
    }

	/**
     * Sets the country
     *
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

	/**
     * Returns the country
     *
     * @return string	$country
     */
    public function getCountry()
    {
        return $this->country;
    }

	/**
     * Sets the telephone
     *
     * @param string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

	/**
     * Returns the telephone
     *
     * @return string	$telephone
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

	/**
     * Sets the fax
     *
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

	/**
     * Returns the fax
     *
     * @return string	$fax
     */
    public function getFax()
    {
        return $this->fax;
    }

	/**
     * Sets the email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

	/**
     * Returns the email
     *
     * @return string	$email
     */
    public function getEmail()
    {
        return $this->email;
    }

	/**
     * Sets the www
     *
     * @param string $www
     */
    public function setWww($www)
    {
        $this->www = $www;
    }

	/**
     * Returns the www
     *
     * @return string	$www
     */
    public function getWww()
    {
        return $this->www;
    }

	/**
     * Sets the usergroup
     *
     * @param string $usergroup
     */
    public function setUsergroup($usergroup)
    {
        $this->usergroup = $usergroup;
    }

	/**
     * Returns the usergroup
     *
     * @return string	$usergroup
     */
    public function getUsergroup()
    {
        return $this->usergroup;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	
 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}