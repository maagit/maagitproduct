<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				Asset

	description:		Model for the article image.
						Model data fits to the media fields in tt_content.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2020-08-13	Urs Maag		Add property type
													- "basketImage": The Basket Icon
													- "contentObject": The Content object

------------------------------------------------------------------------------------- */


class Asset extends \Maagit\Maagitproduct\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var int
     */
	protected ?int $uid;

	/**
     * @var string
     */
	protected $type;

	/**
     * @var string
     */
	protected $pathAndFilename;

	/**
     * @var string
     */
	protected $path;
	
    /**
     * @var string
     */
	protected $filename;
	
    /**
     * @var string
     */
	protected $alternative;
	
    /**
     * @var string
     */
	protected $description;
	
    /**
     * @var string
     */
	protected $link;
	
    /**
     * @var string
     */
	protected $title;
	
    /**
     * @var string
     */
	protected $crop;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the uid
     *
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

	/**
     * Sets the type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
	
	/**
     * Returns the type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

	/**
     * Sets the pathAndFilename
     *
     * @param string $pathAndFilename
     */
    public function setPathAndFilename($pathAndFilename)
    {
        $this->pathAndFilename = $pathAndFilename;
    }
	
	/**
     * Returns the pathAndFilename
     *
     * @return string $pathAndFilename
     */
    public function getPathAndFilename()
    {
        return $this->pathAndFilename;
    }
	
	/**
     * Sets the path
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }
	
	/**
     * Returns the path
     *
     * @return string $path
     */
    public function getPath()
    {
        return $this->path;
    }

	/**
     * Sets the filename
     *
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }
	
	/**
     * Returns the filename
     *
     * @return string $filename
     */
    public function getFilename()
    {
        return $this->filename;
    }
	
	/**
     * Sets the alternative
     *
     * @param string $alternative
     */
    public function setAlternative($alternative)
    {
        $this->alternative = $alternative;
    }
	
	/**
     * Returns the alternative
     *
     * @return string $alternative
     */
    public function getAlternative()
    {
        return $this->alternative;
    }
	
	/**
     * Sets the description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
	
	/**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }
	
	/**
     * Sets the link
     *
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }
	
	/**
     * Returns the link
     *
     * @return string $link
     */
    public function getLink()
    {
        return $this->link;
    }
	
	/**
     * Sets the title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
	
	/**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }
	
	/**
     * Sets the crop
     *
     * @param string $crop
     */
    public function setCrop($crop)
    {
        $this->crop = $crop;
    }
	
	/**
     * Returns the crop
     *
     * @return string $crop
     */
    public function getCrop()
    {
        return $this->crop;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}