<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				Coupon

	description:		Model for the "coupon" step of the checkout process.
						Inherits datas for managing the coupon aspects.

	created:			2022-05-30
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-05-30	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Coupon extends \Maagit\Maagitproduct\Domain\Model\BaseModelStep
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var string
     */
    protected $code;

	/**
     * @var string
     */
    protected $lastError;

	/**
     * @var double
     */
    protected $amount;

	/**
     * @var string
     */
    protected $text;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the code
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }
	
	/**
     * Returns the code
     *
     * @return string	$code
     */
    public function getCode()
    {
        return $this->code;
    }

	/**
     * Sets the lastError
     *
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

	/**
     * Returns the lastError
     *
     * @return string	$lastError
     */
    public function getLastError()
    {
        return $this->lastError;
    }

	/**
     * Sets the amount
     *
     * @param string $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

	/**
     * Returns the amount
     *
     * @return double	$amount
     */
    public function getAmount()
    {
		return $this->amount;
    }

	/**
     * Sets the text
     *
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

	/**
     * Returns the text
     *
     * @return string	$text
     */
    public function getText()
    {
        return $this->text;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * get discount object
     *
     * @param	boolean											$respectHidden		respect hidden flag?
	 * @return	\Maagit\Maagitproduct\Domain\Model\Discount							the discount object|null
     */
	public function getDiscount($respectHidden=true)
	{
		try
		{
			$discountRepository = $this->makeInstance('Maagit\Maagitproduct\Domain\Repository\DiscountRepository');
			$discountObject = $discountRepository->findByCode($this->code, $respectHidden);
			return $discountObject;
		}
		catch (\Exception $ex)
		{
			if ($ex->getCode() != 1653986740)
			{
				$this->setLastError('PHP error: '.$ex->getCode().' - '.$ex->getMessage());
			}
		}
		return null;
	}
 

	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}