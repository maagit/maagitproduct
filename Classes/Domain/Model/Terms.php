<?php
namespace Maagit\Maagitproduct\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Model
	class:				Terms

	description:		Model for the "terms" step of the checkout process.
						Inherits datas for managing the terms & conditions aspects.

	created:			2023-12-18
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-12-18	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Terms extends \Maagit\Maagitproduct\Domain\Model\BaseModelStep
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var boolean
     */
    protected $accepted;

	/**
     * @var string
     */
    protected $lastError;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the accepted
     *
     * @param boolean $accepted
     */
    public function setAccepted($accepted)
    {
        $this->accepted = $accepted;
    }
	
	/**
     * Returns the accepted
     *
     * @return boolean	$accepted
     */
    public function getAccepted()
    {
        return $this->accepted;
    }

	/**
     * Sets the lastError
     *
     * @param string $lastError
     */
    public function setLastError($lastError)
    {
        $this->lastError = $lastError;
    }

	/**
     * Returns the lastError
     *
     * @return string	$lastError
     */
    public function getLastError()
    {
        return $this->lastError;
    }

	/**
     * Returns the text
     *
     * @return string	$text
     */
    public function getText()
    {
		$text = '';
		if ($this->settings['checkout']['sending']['agb'] != '')
		{
			$divHelper = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitproduct\\Helper\\DivHelper');
			$text = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.terms.label', 'maagitproduct');
			preg_match('/\|link\|(.*?)\|\/link\|/', $text, $matches);
		    $targetPos = strrpos($this->settings['checkout']['sending']['agb'], ' ');
			$target = ($targetPos !== FALSE) ? substr($this->settings['checkout']['sending']['agb'], $targetPos) : '';
			// @extensionScannerIgnoreLine
			$link = $divHelper->getExtbaseRequest()->getAttribute('currentContentObject')->typolink($matches[1], array('parameter' => $this->checkout->getAgbUrl().$target));
			$text = preg_replace('/\|link\|(.*)\|\/link\|/', $link, $text);
		}
		return $text;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
 

	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}