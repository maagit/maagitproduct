<?php
namespace Maagit\Maagitproduct\Validator\CouponValidator;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Validator
	class:				CouponValidator

	description:		Validate coupon availability and set given discount or error.

	created:			2022-05-31
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-05-31	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class CouponValidator extends \Maagit\Maagitproduct\Validator\BaseValidator
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Model\Checkout
     */
	protected $checkout;

	/**
	 * @var \Maagit\Maagitproduct\Domain\Model\Coupon
     */
	protected $coupon;

	/**
	 * @var \Maagit\Maagitproduct\Domain\Model\Discount
     */
	protected $discount;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout, \Maagit\Maagitproduct\Domain\Model\Discount $discount=null)
	{
		$this->checkout = $checkout;
		$this->coupon = $this->checkout->getCoupon();
		$this->discount = $discount;
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Validate and return the object
     *
	 * @param	-
	 * @return	\Maagit\Maagitproduct\Domain\Model\Coupon		the validated coupon object
     */
	public function validate()
	{
		$this->coupon->setLastError('');
		$this->discount = $this->coupon->getDiscount();
		$this->makeInstance('Maagit\Maagitproduct\Validator\CouponValidator\CouponValidatorExists', $this->checkout, $this->discount)->validate();
		if (empty($this->coupon->getLastError())) {$this->makeInstance('Maagit\Maagitproduct\Validator\CouponValidator\CouponValidatorLogin', $this->checkout, $this->discount)->validate();}
		if (empty($this->coupon->getLastError())) {$this->makeInstance('Maagit\Maagitproduct\Validator\CouponValidator\CouponValidatorUser', $this->checkout, $this->discount)->validate();}
		if (empty($this->coupon->getLastError())) {$this->makeInstance('Maagit\Maagitproduct\Validator\CouponValidator\CouponValidatorUseonce', $this->checkout, $this->discount)->validate();}
		if (empty($this->coupon->getLastError())) {$this->makeInstance('Maagit\Maagitproduct\Validator\CouponValidator\CouponValidatorMinordervalue', $this->checkout, $this->discount)->validate();}
		if (empty($this->coupon->getLastError())) {$this->makeInstance('Maagit\Maagitproduct\Validator\CouponValidator\CouponValidatorArticle', $this->checkout, $this->discount)->validate();}
		return $this->coupon;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}