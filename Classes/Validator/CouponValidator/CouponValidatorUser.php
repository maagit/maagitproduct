<?php
namespace Maagit\Maagitproduct\Validator\CouponValidator;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Validator
	class:				CouponValidatorUser

	description:		Validate, if given user is valid for this coupon.

	created:			2022-06-02
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-06-02	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class CouponValidatorUser extends \Maagit\Maagitproduct\Validator\CouponValidator\CouponValidator
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Validate
     *
	 * @param	-
	 * @return	void
     */
	public function validate()
	{
		if (empty($this->discount->getUsers()) && empty($this->discount->getUsergroups()))
		{
			return;
		}

		$userFound = false;
		$usergroupFound = false;

		if (!empty($this->discount->getUsers()))
		{
			$login_userid = $this->checkout->getLogin()->getUser()->getUid();
			foreach ($this->discount->getUsers() as $discount_userid)
			{
				if ($login_userid == $discount_userid)
				{
					$userFound = true;
					break;
				}
			}
		}
		
		if (!$userFound && !empty($this->discount->getUsergroups()))
		{
			$context = $this->makeInstance('TYPO3\CMS\Core\Context\Context');
			$login_usergroups = $context->getPropertyFromAspect('frontend.user', 'groupIds');
			foreach ($login_usergroups as $login_usergroup)
			{
				foreach ($this->discount->getUsergroups() as $discount_usergroup)
				{
					if ($login_usergroup == $discount_usergroup)
					{
						$usergroupFound = true;
						break 2;
					}
				}
			}
		}

		if (!$userFound && !$usergroupFound)
		{
			$this->coupon->setLastError(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.coupon.error.notuserfound', 'maagitproduct'));
		}
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}