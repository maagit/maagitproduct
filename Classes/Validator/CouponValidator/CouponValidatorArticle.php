<?php
namespace Maagit\Maagitproduct\Validator\CouponValidator;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Validator
	class:				CouponValidatorArticle

	description:		Validate given settings in "discount per article".

	created:			2022-06-02
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-06-02	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class CouponValidatorArticle extends \Maagit\Maagitproduct\Validator\CouponValidator\CouponValidator
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Validate
     *
	 * @param	-
	 * @return	void
     */
	public function validate()
	{
		// don't validate, if coupontype is not 3 (discountperarticle)
		if ($this->discount->getCoupontype() != 3)
		{
			return;
		}
		
		// system error, if invalid definitions ends in empty articles
		if (empty($this->discount->getDiscountperarticle()))
		{
			$this->coupon->setLastError(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.coupon.error.emptydefinitions', 'maagitproduct'));
			return;
		}

		// initialization
		$definitionCounter = 0;
		$foundErrorCounter = 0;
		$quantityErrorCounter = 0;

		// get definitions and validate it
		$basketItemRepository = $this->makeInstance('Maagit\Maagitproduct\Domain\Repository\BasketItemRepository');
		foreach ($this->discount->getDiscountperarticle() as $article)
		{
			$definitionCounter++;
			$unit = (empty($article['unit'])) ? null : $article['unit'];
			$basketItem = $basketItemRepository->findByUid($article['article'], $unit, $this->checkout->getBasket());

			if (empty($basketItem))
			{
				$foundErrorCounter++;
				
			}
			if (!empty($basketItem) && $basketItem->getQuantity() < $article['count'])
			{
				$quantityErrorCounter++;
			}
		}

		// all definitions must be valid
		if ($this->discount->getDiscountperarticlemode() == 1)
		{
			if ($foundErrorCounter > 0)
			{
				if ($definitionCounter == 1)
				{
					$this->coupon->setLastError(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.coupon.error.articlenotfound.and.1', 'maagitproduct'));
				}
				else
				{
					$this->coupon->setLastError(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.coupon.error.articlenotfound.and', 'maagitproduct'));
				}
				return;
			}
			if ($quantityErrorCounter > 0)
			{
				if ($definitionCounter == 1)
				{
					$this->coupon->setLastError(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.coupon.error.invalidarticlequantity.and.1', 'maagitproduct'));
				}
				else
				{
					$this->coupon->setLastError(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.coupon.error.invalidarticlequantity.and', 'maagitproduct'));
				}
				return;
			}
		}

		// at minimum 1 definition must be valid
		if ($this->discount->getDiscountperarticlemode() == 2)
		{
			if ($foundErrorCounter >= $definitionCounter)
			{
				$this->coupon->setLastError(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.coupon.error.articlenotfound.or', 'maagitproduct'));
				return;
			}
			if ($quantityErrorCounter >= $definitionCounter)
			{
				$this->coupon->setLastError(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.coupon.error.invalidarticlequantity.or', 'maagitproduct'));
				return;
			}
			if (($foundErrorCounter+$quantityErrorCounter) >= $definitionCounter)
			{
				$this->coupon->setLastError(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.coupon.error.articlenotfound.or', 'maagitproduct'));
				return;
			}
		}
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}