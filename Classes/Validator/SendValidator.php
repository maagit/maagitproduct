<?php
namespace Maagit\Maagitproduct\Validator;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Validator
	class:				SendValidator

	description:		Execute various send methods and handle errors, if they
						occures.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class SendValidator extends \Maagit\Maagitproduct\Validator\BaseValidator
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Model\Checkout
     */
	protected $checkout;

	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		$this->checkout = $checkout;
	}
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Validate and return the object
     *
	 * @param	-
	 * @return	\Maagit\Maagitproduct\Domain\Model\Checkout		the validated checkout object
     */
	public function validate()
	{
		// check requirements
		if (($this->checkout->getTotal()>0 && !$this->checkout->getPayment()->getPaymentOk()) || ($this->settings['checkout']['login']['step'] && !$this->checkout->getLogin()->getValid()) || !$this->checkout->getAddress()->getValid() || !$this->checkout->getDelivery()->getValid() || !$this->checkout->getPayment()->getValid() || ($this->settings['checkout']['coupon']['step'] && !$this->checkout->getCoupon()->getValid()) || ($this->settings['checkout']['sending']['condition'] == 2 && !$this->checkout->getTerms()->getValid()))
		{
			$this->checkout->setSendingDone(false);
			return $this->checkout;
		}

		// send order mail to receiver and return, if there is a error occured
		if (!$this->sendMailToReceiver()) {$this->checkout->setSendingDone(false); return $this->checkout;}

		// send order confirmation to customer and return, if there is a error occured
		if (!$this->sendMailToCustomer()) {$this->checkout->setSendingDone(false); return $this->checkout;}
	
		// send download links to customer and return, if there is a error occured
		if (!$this->sendDownloadsToCustomer()) {$this->checkout->setSendingDone(false); return $this->checkout;}
		
		// write log entry
		if (!$this->writeLog()) {$this->checkout->setSendingDone(false); return $this->checkout;}

		// recalculate stock
		if (!$this->stockRecalculate()) {$this->checkout->setSendingDone(false); return $this->checkout;}

		// deactivate used coupons
		if (!$this->deactivateCoupon()) {$this->checkout->setSendingDone(false); return $this->checkout;}

		// sending is done, all ok
		$this->checkout->setSendingDone(true);
		return $this->checkout;
	}
		

	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Send order mail to receiver
     *
	 * @param	-
	 * @return	\Maagit\Maagitproduct\Domain\Model\Checkout		the validated checkout object
     */
	protected function sendMailToReceiver()
	{
		// return, if there are no products in basket
		if (count($this->checkout->getBasket()->getBasketItems()) < 1)
		{
			return true;
		}
		
		// send mail to receiver
		$sendMailToReceiver = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Send\\SendMailToReceiverService', $this->checkout);
		$result = $sendMailToReceiver->process();
		
		// refresh checkout data
		$this->checkout = $sendMailToReceiver->getCheckout();
		
		// return result
		return $result;
	}
	
	/**
     * Send order confirmation to customer
     *
	 * @param	-
	 * @return	\Maagit\Maagitproduct\Domain\Model\Checkout		the validated checkout object
     */
	protected function sendMailToCustomer()
	{
		// return, if there are no confirmation mails wished
		if (!$this->settings['checkout']['sender']['confirmation'])
		{
			return true;
		}
		
		// return, if there are no products in basket or customer's email is empty
		if (count($this->checkout->getBasket()->getBasketItems()) < 1 || $this->checkout->getAddress()->getEmail() == '')
		{
			return true;
		}

		// send confirmation mail to customer
		$sendMailToCustomer = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Send\\SendMailToCustomerService', $this->checkout);
		$result = $sendMailToCustomer->process();
		
		// refresh checkout data
		$this->checkout = $sendMailToCustomer->getCheckout();
		
		// return result
		return $result;
	}

	/**
     * Send download links to customer
     *
	 * @param	-
	 * @return	\Maagit\Maagitproduct\Domain\Model\Checkout		the validated checkout object
     */
	protected function sendDownloadsToCustomer()
	{
		// return, if there are no separate download link mails wished
		if (!$this->settings['checkout']['download']['separate'])
		{
			return true;
		}
		
		// return, if there are no products in basket or customer's email is empty
		if (count($this->checkout->getBasket()->getBasketItems()) < 1 || $this->checkout->getAddress()->getEmail() == '')
		{
			return true;
		}

		// send download mails to customer
		$sendDownloadsToCustomer = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Send\\SendDownloadsToCustomerService', $this->checkout);
		$result = $sendDownloadsToCustomer->process();
		
		// refresh checkout data
		$this->checkout = $sendDownloadsToCustomer->getCheckout();
		
		// return result
		return $result;
	}
	
	/**
     * Write log to database
     *
	 * @param	-
	 * @return	\Maagit\Maagitproduct\Domain\Model\Checkout		the validated checkout object
     */
	protected function writeLog()
	{
		// return, if there are no products in basket
		if (count($this->checkout->getBasket()->getBasketItems()) < 1)
		{
			return true;
		}
		
		// write log to database
		$sendLog = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Send\\SendLogService', $this->checkout);
		$result = $sendLog->process();
		
		// return result
		return $result;
	}
	
	/**
     * Recalculate stock data
     *
	 * @param	-
	 * @return	\Maagit\Maagitproduct\Domain\Model\Checkout		the validated checkout object
     */
	protected function stockRecalculate()
	{
		// return, if there are no stock calculation wished
		if (!$this->settings['checkout']['stock']['calc'] || count($this->checkout->getBasket()->getBasketItems()) < 1)
		{
			return true;
		}
		
		// refresh stock
		$sendStock = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Send\\SendStockService', $this->checkout);
		$result = $sendStock->process();

		// return result
		return $result;
	}

	/**
     * Deactivate used coupons
     *
	 * @param	-
	 * @return	\Maagit\Maagitproduct\Domain\Model\Checkout		the validated checkout object
     */
	protected function deactivateCoupon()
	{
		// return, if no coupon step is activated
		if (empty($this->checkout->getCoupon()) || empty($this->checkout->getCoupon()->getCode()) || !$this->settings['checkout']['coupon']['step'])
		{
			return true;
		}

		// get discount object
		$discountRepository = $this->makeInstance('Maagit\Maagitproduct\Domain\Repository\DiscountRepository');
		$discount = $discountRepository->findByCode($this->checkout->getCoupon()->getCode());

		// return, if users or usergroups defined
		if (!empty($discount->getUsers()) || !empty($discount->getUsergroups()))
		{
			return true;
		}

		// modify discount object and save it
		if ($discount->getUseonce())
		{
			$modifyObject = $discountRepository->findByUid($discount->getUid());
			$modifyObject->setHidden(true);
			$discountRepository->update($modifyObject);
		}
		
		// return result
		return true;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}