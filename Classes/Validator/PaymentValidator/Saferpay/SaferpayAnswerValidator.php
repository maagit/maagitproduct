<?php
namespace Maagit\Maagitproduct\Validator\PaymentValidator\Saferpay;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Validator
	class:				SaferpayAnswerValidator

	description:		Validation of callback from saferpay.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class SaferpayAnswerValidator extends \Maagit\Maagitproduct\Validator\PaymentValidator\PaymentAnswerValidator
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */

	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Validate and return the object
     *
	 * @param	-
	 * @return	\Maagit\Maagitproduct\Domain\Model\Checkout		the validated checkout object
     */
	public function validate()
	{
		// return, if there is already a payment error
		if ($this->checkout->getPayment()->getPaymentError() != '')
		{
			return $this->checkout;
		}

		// check referer (if not empty in settings)
		$needle = ($this->settings['checkout']['payment']['method']['saferpay']['useSandbox']) ? trim($this->settings['checkout']['payment']['method']['saferpay']['sandboxReferer']) : trim($this->settings['checkout']['payment']['method']['saferpay']['referer']);
		if ($needle != '')
		{
			$referer = trim(substr(strtolower($_SERVER['HTTP_REFERER']), 0, strlen($needle)));
			if ($referer != $needle)
			{
				$this->checkout->setAction('summary');
				$this->checkout->getPayment()->setPaymentError(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.payment.saferpay.error.invalidReferer', 'maagitproduct').': «'.$referer.'»');
				return $this->checkout;
			}
		}

		// check, if token from payment transaction is available
		if (!isset($_GET['tx_maagitproduct_checkout']['token']) || $_GET['tx_maagitproduct_checkout']['token'] == '')
		{
			$this->checkout->setAction('summary');
			$this->checkout->getPayment()->setPaymentError(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.payment.saferpay.error.invalidToken', 'maagitproduct'));
			return $this->checkout;
		}

		// check, compare token
		if ($this->checkout->getPayment()->getSaferpayToken() != $_GET['tx_maagitproduct_checkout']['token'])
		{
			$this->checkout->setAction('summary');
			$this->checkout->getPayment()->setPaymentError(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.payment.saferpay.error.invalidToken', 'maagitproduct'));
			return $this->checkout;
		}
		
		// clear compare token
		$this->checkout->getPayment()->setSaferpayToken('');
		
		// check, if payment is cancelled
		if (isset($_GET['tx_maagitproduct_checkout']['cancel']) && $_GET['tx_maagitproduct_checkout']['cancel'] == 'true')
		{
			$this->checkout->setAction('summary');
			$this->checkout->getPayment()->setPaymentError(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.payment.saferpay.error.cancelled', 'maagitproduct'));
			return $this->checkout;
		}

		// payment is approved, all is ok
		if (isset($_GET['tx_maagitproduct_checkout']['approve']) && $_GET['tx_maagitproduct_checkout']['approve'] == 'true')
		{
			$this->checkout->setAction('send');
			$this->checkout->getPayment()->setPaymentOK(true);
			$this->checkout->getPayment()->setPaymentError('');
			$this->checkout->getPayment()->setPaymentMessage(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.payment.saferpay.approved', 'maagitproduct').' (ID: '.$_GET['tx_maagitproduct_checkout']['token'].')');
			return $this->checkout;
		}
	}
		

	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}