<?php
namespace Maagit\Maagitproduct\Validator;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Validator
	class:				CheckoutValidator

	description:		Validate available delivery and payment methods and set
						necessary flags for showing the correct datas in the checkout
						process.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2022-04-06	Urs Maag		Added validation of login step

------------------------------------------------------------------------------------- */


class CheckoutValidator extends \Maagit\Maagitproduct\Validator\BaseValidator
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Model\Checkout
     */
	protected $checkout;

	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		$this->checkout = $checkout;
	}
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Validate and return the object
     *
	 * @param	-
	 * @return	\Maagit\Maagitproduct\Domain\Model\Checkout		the validated checkout object
     */
	public function validate()
	{
		if ($this->settings['checkout']['login']['step']) {$this->validateLogin();}
		$this->validateDelivery();
		$this->validatePayment();
		if ($this->settings['checkout']['coupon']['step']) {$this->validateCoupon();}
		if ($this->settings['checkout']['sending']['condition'] == 2) {$this->validateTerms();}
		$this->validateAction();
		$this->setFlags();
		return $this->checkout;
	}
		

	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Validate login things
     *
	 * @param	-
	 * @return	void
     */
	protected function validateLogin()
	{
		$userAspect = $this->makeInstance('TYPO3\CMS\Core\Context\Context')->getAspect('frontend.user');
		$user = (isset($_POST['user'])) ? $_POST['user'] : '';
		$this->checkout->getLogin()->setUsername($user);
		$this->checkout->getLogin()->setLoggedIn($userAspect->isLoggedIn());
		if ($this->checkout->getLogin()->getLoggedIn()) {
			$this->checkout->getLogin()->setLastError('');
		}
		if ($user != '' && !$this->checkout->getLogin()->getGuest() && !$this->checkout->getLogin()->getLoggedIn()) {
			$this->checkout->getLogin()->setLastError(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.login.error.failed', 'maagitproduct'));
		}
		else
		{
			$this->checkout->getLogin()->setLastError('');
		}
	}

	/**
     * Validate delivery things
     *
	 * @param	-
	 * @return	void
     */
	protected function validateDelivery()
	{
		// clear delivery method, if there are only downloads in the basket
		if ($this->checkout->getBasket()->getHasOnlyDownloads())
		{
			$this->checkout->getDelivery()->setDeliveryMethod(null);
			return;
		}

		// clear delivery method, if the selected method isn't available now
		$found = false;
		if ($this->checkout->getDelivery()->getDeliveryMethods() != null)
		{
			foreach ($this->checkout->getDelivery()->getDeliveryMethods() as $method)
			{
				if ($this->checkout->getDelivery()->getDeliveryMethod() == $method['id'])
				{
					$found = true;
					break;
				}
			}
		}
		if (!$found)
		{
			$this->checkout->getDelivery()->setDeliveryMethod(null);
		}
	}
	
	/**
     * Validate payment things
     *
	 * @param	-
	 * @return	void
     */
	protected function validatePayment()
	{
		// clear payment method, if the basket amount is 0
		if ($this->checkout->getTotal()==0)
		{
			$this->checkout->getPayment()->setPaymentMethod(null);
			return;
		}
		
		// clear payment method, if the selected method isn't available now
		$found = false;
		foreach ($this->checkout->getPayment()->getPaymentMethods() as $method)
		{
			if ($this->checkout->getPayment()->getPaymentMethod() == $method['id'])
			{
				$found = true;
				break;
			}
		}
		if (!$found)
		{
			$this->checkout->getPayment()->setPaymentMethod(null);
		}
	}
	
	/**
     * Validate coupon things
     *
	 * @param	-
	 * @return	void
     */
	protected function validateCoupon()
	{
		$this->checkout->getCoupon()->setLastError('');
		if (empty($this->checkout->getCoupon()->getCode())) {return;}
		$couponValidator = $this->makeInstance('Maagit\Maagitproduct\Validator\CouponValidator\CouponValidator', $this->checkout);
		$this->checkout->setCoupon($couponValidator->validate());
	}
	
	/**
     * Validate terms things
     *
	 * @param	-
	 * @return	void
     */
	protected function validateTerms()
	{
		$this->checkout->getTerms()->setLastError('');
		if ($this->checkout->getTerms()->getVisited() && !$this->checkout->getTerms()->getAccepted())
		{
			$this->checkout->getTerms()->setLastError(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.terms.error.notaccepted', 'maagitproduct'));
		}
	}

	/**
     * Validate redirection actions
     *
	 * @param	-
	 * @return	void
     */
	protected function validateAction()
	{
		// set action "login", if there is a login failed
		if ($this->settings['checkout']['login']['step']) {
			$user = (isset($_POST['user'])) ? $_POST['user'] : '';
			if ($user != '' && !$this->checkout->getLogin()->getGuest() && !$this->checkout->getLogin()->getLoggedIn())
			{
				$this->checkout->setAction('login');
			}
		}

		// set action "coupon", if there is a validation failed
		if ($this->settings['checkout']['coupon']['step']) {
			$changed = (isset($_POST['couponChanged'])) ? $_POST['couponChanged'] : '';
			if ($changed != '' && $this->checkout->getCoupon()->getLastError() != '')
			{
				$this->checkout->setAction('coupon');
			}
		}

		// set action "terms", if there is a validation failed
		if ($this->settings['checkout']['sending']['condition'] == 2) {
			$changed = (isset($_POST['termsChanged'])) ? $_POST['termsChanged'] : '';
			if ($changed != '' && $this->checkout->getTerms()->getLastError() != '')
			{
				$this->checkout->setAction('terms');
			}
		}

		// set action "summary", if there are no items in basket
		if (count($this->checkout->getBasket()->getBasketItems()) == 0)
		{
			$this->checkout->setAction('summary');
		}
	}

	/**
     * Validate delivery things
     *
	 * @param	-
	 * @return	void
     */
	protected function setFlags()
	{
		// handle "isValid" flag for login section
		if ($this->settings['checkout']['login']['step']) {
			if (!$this->checkout->getLogin()->getGuest() && !$this->checkout->getLogin()->getLoggedIn())
			{
				$this->checkout->getLogin()->setValid(false);
			}
		}

		// handle "isValid" flag for delivery section
		if ($this->checkout->getDelivery()->getDeliveryMethod() == null && $this->checkout->getDelivery()->getVisited())
		{
			$this->checkout->getDelivery()->setValid(false);
		}

		// handle "isValid" flag for payment section
		if ($this->checkout->getPayment()->getPaymentMethod() == null && $this->checkout->getPayment()->getVisited())
		{
			$this->checkout->getPayment()->setValid(false);
		}

		// handle "isValid" flag for coupon section
		if ($this->settings['checkout']['coupon']['step']) {
			if (!empty($this->checkout->getCoupon()->getLastError()) && $this->checkout->getCoupon()->getVisited())
			{
				$this->checkout->getCoupon()->setValid(false);	
			}
		}

		// handle "isValid" flag for terms section
		if ($this->settings['checkout']['sending']['condition'] == 2) {
			if (!empty($this->checkout->getTerms()->getLastError()) && $this->checkout->getTerms()->getVisited())
			{
				$this->checkout->getTerms()->setValid(false);	
			}
		}
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}