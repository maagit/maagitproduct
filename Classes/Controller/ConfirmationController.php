<?php
namespace Maagit\Maagitproduct\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Controller
	class:				ConfirmationController

	description:		Main class for the checkout confirmation page.
						Show the confirmation data with this plugin (redirect to this
						plugin can be configured by the flexform settings in the
						checkout plugin).

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed

------------------------------------------------------------------------------------- */


class ConfirmationController extends \Maagit\Maagitproduct\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Repository\CheckoutRepository
	 */
	protected $checkoutRepository;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Contructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// parent initalization things
		parent::initializeObject();

		// add settings from session
		$this->settings = array_replace_recursive($this->settings, $this->getSessionSettings());
		
		// inject repositories
		$this->checkoutRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\CheckoutRepository');
	}

	
	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Action for this controller.
     *
     * @return void
     */
	public function showAction()
	{
		$this->baseAction();
		if (empty($this->getSessionSettings())) {return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));}
		$checkout = $this->checkoutRepository->findAll();
		$this->view->assign('settings', $this->settings);
		$this->view->assign('checkout', $checkout);
		if ($this->settings['checkout']['experience']['show'])
		{
			$this->checkoutRepository->clear();	
		}
		else
		{
			$this->checkoutRepository->removeAll();	
		}
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}
	
	/**
     * Initialize Action for this controller.
	 * Experience action initialization.
     *
     * @return void
     */
	protected function initializeExperienceAction()
	{
		$propertyMappingConfiguration = $this->arguments['experience']->getPropertyMappingConfiguration();
		$propertyMappingConfiguration->allowAllProperties();
		$propertyMappingConfiguration->setTypeConverterOption('TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE);
	}

	/**
     * Action for this controller.
	 * Experience return call.
     *
     * @return void
     */
	public function experienceAction(\Maagit\Maagitproduct\Domain\Model\Experience $experience)
	{
		$experienceService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Experience\\ExperienceService', $experience);
		$experience = $experienceService->sendRating();
		$experienceService->saveRating();
		$this->view->assign('experience', $experience);
		$this->checkoutRepository->removeAll();
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}
	

	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}