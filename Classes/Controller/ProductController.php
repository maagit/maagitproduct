<?php
namespace Maagit\Maagitproduct\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Controller
	class:				ProductController

	description:		List the articles of a given backend-page.
						The storage page and the orderings can be configured by
						flexform settings of the plugin.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed

------------------------------------------------------------------------------------- */


class ProductController extends \Maagit\Maagitproduct\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Repository\ProductRepository
	 */
	protected $productRepository;

	/**
	 * @var \Maagit\Maagitproduct\Service\Property\SortingService
	 */
	protected $sortingService;

	/**
	 * @var array
	 */
	protected $pids = array();

	/**
	 * @var array
	 */
	protected $orderDefinitions = array(
		0 => [
			'getPageSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
		],
		1 => [
			'getPageSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getCrdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
		],
		2 => [
			'getPageSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getCrdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
		],
		3 => [
			'getPageSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getHeader' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
		],
		4 => [
			'getPageSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getPriceSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
		],
		5 => [
			'getPageSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getPriceSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
			'getSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
		],
		6 => [
			'getCrdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
			'getSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
		],
		7 => [
			'getCrdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
		],
		8 => [
			'getHeader' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
		],
		9 => [
			'getPriceSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
		],
		10 => [
			'getPriceSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
			'getSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
		],
		11 => [
			'getType' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getCrdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
			'getSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
		],
		12 => [
			'getType' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getCrdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
		],
		13 => [
			'getType' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getHeader' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
		],
		14 => [
			'getType' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getPriceSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
		],
		15 => [
			'getType' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'getPriceSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
			'getSorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
		]
	);
	

	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Contructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// parent initalization things
		parent::initializeObject();
		
		// inject repositories and init data
		$this->productRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\ProductRepository');
		$this->sortingService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Property\\SortingService');
		// @extensionScannerIgnoreLine
		$this->init();
	}
	
	
	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Index action for this controller. Displays a list of products.
     *
     * @return void
     */
	public function listAction()
	{
		$urlParameters = $this->getUrlParameters();
		if ($urlParameters !== FALSE)
		{
			$products = $this->getFilteredResult($urlParameters['types'], $urlParameters['categories'], $urlParameters['search']);
		}
		else
		{
			$products = $this->productRepository->findByPids($this->pids);
		}
		$this->prepareOutput($products, ($this->request->hasArgument('sort')&&is_numeric($this->request->getArgument('sort')))?(int)$this->request->getArgument('sort'):null);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}
	
	/**
     * Index action for this controller. Filters a list of products by given filter settings.
     *
     * @return void
     */
	public function filterAction(string $types='not_submitted', string $categories='not_submitted', string $search='not_submitted', int $order=null)
	{
		$types = htmlspecialchars($types);
		$categories = htmlspecialchars($categories);
		$search = htmlspecialchars($search);
		$products = $this->getFilteredResult($types, $categories, $search);
		$this->prepareOutput($products, $order);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}

	/**
     * Index action for this controller. Show detail of given product
     *
     * @return void
     */
	public function detailAction(int $uid)
	{
		$product = $this->productRepository->findByUid($uid);
		$this->view->assign('product', $product);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}

	/**
     * Index action for this controller. Show ratings of given product
     *
     * @return void
     */
	public function ratingAction(int $uid)
	{
		$product = $this->productRepository->findByUid($uid);
		$this->view->assign('product', $product);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * initialize things.
     *
	 * @param	-
     * @return	void
     */
	protected function init()
	{
		$pidListString = $this->configuration['persistence']['storagePid'];
		$pidList = \TYPO3\CMS\Core\Utility\GeneralUtility::intExplode(',', $pidListString, true);
		$pageRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\PageRepository');
		foreach ($pidList as $pid)
		{
			$this->pids = array_merge($this->pids, $pageRepository->getPageTreeUids(
				\TYPO3\CMS\Core\Utility\GeneralUtility::intExplode(',', $this->configuration['persistence']['storagePid'], true),
				(!empty($this->configuration['persistence']['recursive'])) ? $this->configuration['persistence']['recursive'] : 0
			));
		}
	}
	
	/**
     * get filtered results
	 *
	 * IMPORTANT:	the parameter values must have the value "not_submitted", if they are not submitted from outside
     *
	 * @param	string					$types			coma separated list of selected types
	 * @param	string					$categories		coma separated list of selected categories
	 * @param	string					$search 		search word
     * @return	void
     */
	protected function getFilteredResult(string $types, string $categories, string $search)
	{
		if ($types!='not_submitted' && $categories=='not_submitted')
		{
			$products = $this->productRepository->findByPidsAndType($this->pids, $types);
		}
		elseif ($types=='not_submitted' && $categories!='not_submitted')
		{
			$products = $this->productRepository->findByPidsAndCategory($this->pids, $categories);
		}
		elseif ($types!='not_submitted' && $categories!='not_submitted')
		{
			$products = $this->productRepository->findByPidsAndTypeAndCategory($this->pids, $types, $categories);
		}
		else
		{
			$products = $this->productRepository->findByPids($this->pids);
		}
		if ($search!='not_submitted')
		{
			$products = $this->productRepository->filterBySearch($products, $search);
		}
		return $products;
	}

	/**
     * prepare products for output.
     *
	 * @param	array						$products				the products
     * @return	array												the prepared products
     */
	protected function prepareOutput(array $products, $order=null)
	{
		$paginatingArguments = array();
		$ordering = ($order==null) ? $this->orderDefinitions[$this->settings['order']] : $this->orderDefinitions[$order];
		$products = $this->sortingService->sort($products, $ordering);
		$oldType = 'init';
		foreach ($products as $product)
		{
			// set type descriptions
			$product->setTypeDescription($product->getTypeDescription());
			// set isNewType
			$product->setIsNewType(($product->getType() != $oldType));
			$oldType = $product->getType();
		}
		$this->view->assign('products', $products);
		if ($this->settings['paginate']['doit'])
		{
			$paginationService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Pagination\\PaginationService', $products, $this->getPageFromUrl(), $this->settings['paginate']['productsPerPage'], $this->settings['paginate']['maxLinks']);
			$paginatingArguments = $paginationService->getPaginationArguments();
		}
		else
		{
			$paginatingArguments['paginatedItems'] = $products;
		}
		$this->view->assign('pagination', $paginatingArguments);
	}

	/**
     * get page number from url
     *
     * @return int
     */
	private function getPageFromUrl()
	{
		// get from paginate parameter, if available
		if (!empty($this->request->getArguments()['currentPage']))
		{
			return (int)$this->request->getArguments()['currentPage'];
		}
		return 0;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	/**
     * get and validate relevant url arguments
     *
	 * @param	-
     * @return	array|FALSE										the prepared url parameters or FALSE
     */
	private function getUrlParameters()
	{
		$urlParameters = array('types'=>'not_submitted', 'categories'=>'not_submitted', 'search'=>'not_submitted');
		$doFilter = false;
		if ($this->request->hasArgument('types'))
		{
			$doFilter = true;
			$urlParameters['types'] = htmlspecialchars($this->request->getArgument('types'));
		}
		if ($this->request->hasArgument('categories'))
		{
			$doFilter = true;
			$urlParameters['categories'] = htmlspecialchars($this->request->getArgument('categories'));
		}
		if ($this->request->hasArgument('search'))
		{
			$doFilter = true;
			$urlParameters['search'] = htmlspecialchars($this->request->getArgument('search'));
		}
		if ($doFilter)
		{
			return $urlParameters;
		}
		return FALSE;
	}
}