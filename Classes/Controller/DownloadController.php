<?php
namespace Maagit\Maagitproduct\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Controller
	class:				DownloadController

	description:		Controller to send outstandig download emails (after paying).

	created:			2022-06-10
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-06-10	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class DownloadController extends \Maagit\Maagitproduct\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Repository\ProductRepository
	 */
	protected $productRepository;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Contructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// parent initalization things
		parent::initializeObject();
		
		// inject repositories and init data
		$this->productRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\ProductRepository');
	}
	
	
	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Send action for this controller. Send emails for downloads.
     *
     * @return void
     */
	public function sendAction(int $order)
	{
		$orderRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\OrderRepository');
		$order = $orderRepository->findByUid($order);
		$this->view->assign('order', $order);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}

	/**
     * Send action for this controller. Send emails for downloads.
     *
     * @return void
     */
	public function sendmailAction(int $orderUid, int $productUid=null)
	{
		// initialization
		$orderRepository = $this->makeInstance('Maagit\Maagitproduct\Domain\Repository\OrderRepository');
		$basketRepository = $this->makeInstance('Maagit\Maagitproduct\Domain\Repository\BasketRepository');
		$basketItemRepository = $this->makeInstance('Maagit\Maagitproduct\Domain\Repository\BasketItemRepository');
		$renderService = $this->makeInstance('Maagit\Maagitproduct\Service\Content\RenderService');
		$emailService = $this->makeInstance('Maagit\Maagitproduct\Service\Network\EmailService');
		$error = array();

		// get settings
		$this->getSpecificSettings();

		// get objects
		$order = $this->getOrder($orderUid, $error);
		if (empty($order->getEmail())) {$error['message'] = 'No email address found';}
		$article = $this->getArticle($order, $productUid, $error);
		$basket = $basketRepository->create();
		$product = $this->getProduct($productUid, $error);
		
		// render email and send it
		if (empty($error['message']))
		{
			try
			{
				// create basketItem
				$basketItem = $basketItemRepository->create(1, $product->getUid(), (empty($article->getUnit()))?null:$article->getUnit());
				$basketItem->getProduct()->setSecureLink($this->getSecureLink($basketItem->getProduct()));

				// get body text
				if ($this->settings['checkout']['download']['custom'])
				{
					$bodyText = $renderService->renderCObject($this->settings['checkout']['download']['cobj']);
					$bodyText = $renderService->renderPlaceholder($bodyText, $basketItem);
				}
				else
				{
					$bodyText = $renderService->renderTemplate('Email/Download', array('basketItem' => $basketItem, 'settings' => $this->settings));
				}
				
				// get subject
				if ($this->settings['checkout']['download']['custom'])
				{
					$subject = $this->settings['checkout']['download']['subject'];
				}
				else
				{
					$subject = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.download', 'maagitproduct');
				}
				$subject .= ' ('.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.orderId', 'maagitproduct');
				$subject .= ' '.$order->getOrderId().')';

				// send mail
				try
				{
					$sent = $emailService->sendMail(
						array($this->settings['checkout']['download']['email']),
						array($order->getEmail()),
						$subject,
						$bodyText,
						$bodyText
					);
				}
				catch (\Exception $ex)
				{
					$details = $ex->getMessage();
					$msg = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.error.sender', 'maagitproduct').'<br /><br />';
					$msg .= \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.error.details', 'maagitproduct').':<br />';
					$msg .= $details;
					$error['message'] = $msg;
				}
			}
			catch (\Exception $ex)
			{
				$error['message'] = $ex;
			}
		}

		// update sending date
		if (empty($error['message']))
		{
			$article->setDownloadmail(time());
			$orderRepository->update($order);
			$persistenceManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager');
			$persistenceManager->persistAll();
		}

		$this->view->assign('error', $error);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Get settings
     *
     * @param	-
	 * @return	void
     */
	protected function getSpecificSettings()
	{
		// get settings
		if (!empty($this->settings['download']['settings']))
		{
			$this->getFlexformSettings($this->settings['download']['settings']);
			if (!isset($this->settings['checkout']['download']['email']) || empty($this->settings['checkout']['download']['email']))
			{
				$this->settings['checkout']['download']['email'] = $this->settings['checkout']['sender']['email'];
			}
		}
		else
		{
			$this->settings['checkout'] = array('download' => $this->settings['download']);
		}
	}

	/**
     * Get order object
     *
     * @param	int											$uid			the order uid
	 * @param	by reference: array							$error			the error message
	 * @return	Maagit\Maagitproduct\Domain\Model\Order						the order
     */
	protected function getOrder(int $uid, array &$error)
	{
		$orderRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\OrderRepository');
		$order = $orderRepository->findByUid($uid);
		if (empty($order))
		{
			$error['message'] = 'Order with uid "'.$orderUid.'" not found';
		}
		return $order;
	}

	/**
     * Get article object
     *
     * @param	Maagit\Maagitproduct\Domain\Model\Order		$order			the order object
	 * @param	int											$uid			the article uid
	 * @param	by reference: array							$error			the error message
	 * @return	Maagit\Maagitproduct\Domain\Model\Article					the article
     */
	protected function getArticle(\Maagit\Maagitproduct\Domain\Model\Order $order, int $uid, array &$error)
	{
		$article = null;
		foreach ($order->getArticles() as $art)
		{
			if ($art->getProductuid() == $uid)
			{
				$article = $art;
				break;
			}
		}
		if (empty($article))
		{
			$error['message'] = 'Article with uid "'.$uid.'" not found in order with uid "'.$order->getUid().'"';
		}
		return $article;
	}

	/**
     * Get product object
     *
	 * @param	int											$uid			the product uid
	 * @param	by reference: array							$error			the error message
	 * @return	Maagit\Maagitproduct\Domain\Model\Product					the product object
     */
	protected function getProduct(int $uid, array &$error)
	{
		try
		{
			$productRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\ProductRepository');
			$product = $productRepository->findByUid($uid);
			if (empty($product))
			{
				$error['message'] = 'Product with uid "'.$uid.'" not found';
			}
		}
		catch (\Exception $ex)
		{
			$error['message'] = 'Product not found';
		}
		return $product;
	}

	/**
     * Get secure link
     *
	 * @param	Maagit\Maagitproduct\Domain\Model\Product	$product		the product
	 * @return	string														the secure link|null
     */
	protected function getSecureLink(\Maagit\Maagitproduct\Domain\Model\Product $product)
	{
		$secureLink = null;
		if ($this->settings['checkout']['download']['secure'] == true)
		{
			$secureLink = $this->divHelper->getSecureLink(
				$product->getLink(),
				\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.download.link', 'maagitproduct'),
				$this->settings['checkout']['download']['secureTimeout']
			);
		}
		return $secureLink;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}