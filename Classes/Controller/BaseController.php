<?php
namespace Maagit\Maagitproduct\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Controller
	class:				BaseController

	description:		Base class for Controller classes.
						Check requirements for this shop.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2022-10-11	Urs Maag		Typo3 12.0.0 compatibility
													- add "initializeAction" to set
													  current request on all over
													  "$this->makeInstance" generated
													  Maagit classes

------------------------------------------------------------------------------------- */


class BaseController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	use \Maagit\Maagitproduct\General;


	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var int
     */
    protected $checkResult;
	

	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Initialization.
	 * Check requirements
     *
	 * @param	-
	 * @return	void
     */
	public function initializeObject()
	{
		// check requirements
		$requirementsService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitproduct\\Service\\Requirements\\RequirementsService');
		$this->checkResult = $requirementsService->check();

		// get flexform settings, if there is a ajax request
		if (isset($_GET['ajax']) || isset($_POST['ajax']))
		{
			$this->getFlexformSettings();
		}
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Base Action things
     *
	 * @return	void
     */
	public function baseAction()
	{
		$this->view->assign('cookiesCheckUri', $this->getCookiesCheckUri());
		$this->view->assign('jqueryCheckUri', $this->getJQueryCheckUri());
		$this->view->assign('cookiesDisabledUri', $this->getCookiesDisabledUri());
		$this->view->assign('javascriptDisabledUri', $this->getJavascriptDisabledUri());
		$this->view->assign('jqueryFailedUri', $this->getJQueryFailedUri());
		$this->view->assign('controller', $this->divHelper->getControllerName($this));
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}
	
	/**
     * Action for this controller.
	 * Requirements error.
     *
	 * @return	void
     */
	public function requirementsAction()
	{
		$this->view->assign('cookies', (($this->checkResult & \Maagit\Maagitproduct\Service\Requirements\RequirementsService::COOKIES)==0));
		$this->view->assign('javascript', (($this->checkResult & \Maagit\Maagitproduct\Service\Requirements\RequirementsService::JAVASCRIPT)==0));
		$this->view->assign('jquery', (($this->checkResult & \Maagit\Maagitproduct\Service\Requirements\RequirementsService::JQUERY)==0));
		$this->view->assign('jqueryUnknown', (($this->checkResult & \Maagit\Maagitproduct\Service\Requirements\RequirementsService::JQUERY_UNKNOWN)==0));
		$this->view->assign('jqueryVersion', (($this->checkResult & \Maagit\Maagitproduct\Service\Requirements\RequirementsService::JQUERY_VERSION)==0));
		$this->view->assign('jqueryVersionNumber', $_GET['jqueryVersion']??'');
		$this->view->assign('jqueryMinVersionNumber', \Maagit\Maagitproduct\Service\Requirements\RequirementsService::MIN_JQUERY_VERSION);
		// @extensionScannerIgnoreLine
		$this->view->assign('refresh', $this->divHelper->getUri($GLOBALS['TSFE']->id, array(), true, true));
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}

	/**
     * Action for this controller.
	 * Answer to ajax "cookie enabled" request.
     *
	 * @return	void
     */
	public function cookieAction()
	{
		if (isset($_COOKIE['maagitproduct']))
		{
			echo 'true';
		}
		else
		{
			echo 'false';
		}
		exit();
	}

	/**
     * Action for this controller.
	 * Answer to ajax "jquery validation" request.
     *
	 * @return	void
     */
	public function jqueryAction()
	{
		$jqueryVersion = $_GET['jqueryVersion'];
		if ($jqueryVersion == 'false')
		{
			echo 'false';
			exit();
		}
		if (version_compare($_GET['jqueryVersion'], \Maagit\Maagitproduct\Service\Requirements\RequirementsService::MIN_JQUERY_VERSION) == -1)
		{
			echo 'false';
			exit();
		}
		echo 'true';
		exit();
	}

	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
	 * Create uri for calling "cookies check"
     *
	 * @return	string		uri
     */
	protected function getCookiesCheckUri()
	{
		$params = array(
			'tx_maagitproduct_'.$this->divHelper->getControllerName($this).'[action]' => 'cookie'
		);
		// @extensionScannerIgnoreLine
		$uri = $this->divHelper->getUri($GLOBALS['TSFE']->id, $params, true, true);
		return $uri;
	}
	
	/**
	 * Create uri for calling "jquery check"
     *
	 * @return	string		uri
     */
	protected function getJQueryCheckUri()
	{
		$params = array(
			'tx_maagitproduct_'.$this->divHelper->getControllerName($this).'[action]' => 'jquery'
		);
		// @extensionScannerIgnoreLine
		$uri = $this->divHelper->getUri($GLOBALS['TSFE']->id, $params, true, true);
		return $uri;
	}

	/**
	 * Create uri for calling "cookies disabled" requirements site
     *
	 * @return	string		uri
     */
	protected function getCookiesDisabledUri()
	{
		$params = array(
			'tx_maagitproduct_'.$this->divHelper->getControllerName($this).'[action]' => 'requirements',
			'requirementsCheck' => 'true'
		);
		// @extensionScannerIgnoreLine
		$uri = $this->divHelper->getUri($GLOBALS['TSFE']->id, $params, true, true);
		return $uri;
	}

	/**
	 * Create uri for calling "javascript disabled" requirements site
     *
	 * @return	string		uri
     */
	protected function getJavascriptDisabledUri()
	{
		$params = array(
			'tx_maagitproduct_'.$this->divHelper->getControllerName($this).'[action]' => 'requirements',
			'requirementsCheck' => 'true'
		);
		// @extensionScannerIgnoreLine
		$uri = $this->divHelper->getUri($GLOBALS['TSFE']->id, $params, true, true).'&js=false';
		return $uri;
	}
	
	/**
	 * Create uri for calling "no jquery available" requirements site
     *
	 * @return	string		uri
     */
	protected function getJQueryFailedUri()
	{
		$params = array(
			'tx_maagitproduct_'.$this->divHelper->getControllerName($this).'[action]' => 'requirements',
			'requirementsCheck' => 'true'
		);
		// @extensionScannerIgnoreLine
		$uri = $this->divHelper->getUri($GLOBALS['TSFE']->id, $params, true, true);
		return $uri;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}