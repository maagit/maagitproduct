<?php
namespace Maagit\Maagitproduct\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Controller
	class:				CheckoutController

	description:		Main class for the checkout process.
						Handle the steps "address" -> "delivery" -> "payment" -> "send".

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2020-06-30	Urs Maag		validate and persist checkout on
													every step, because there can be
													changes from outside (e.g. shop
													page).
						2021-09-09	Urs Maag		ObjectManager removed
						2022-04-03	Urs Maag		Added step "login"

------------------------------------------------------------------------------------- */


class CheckoutController extends \Maagit\Maagitproduct\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Repository\CheckoutRepository
     */
    protected $checkoutRepository;

	/**
	 * @var \Maagit\Maagitproduct\Domain\Repository\BasketRepository
     */
    protected $basketRepository;
	
	/**
	 * @var \Maagit\Maagitproduct\Domain\Repository\BasketItemRepository
     */
    protected $basketItemRepository;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Contructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// parent initalization things
		parent::initializeObject();
		
		// inject repositories
		$this->checkoutRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\CheckoutRepository');
		$this->basketRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\BasketRepository');
		$this->basketItemRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\BasketItemRepository');

		// get pagetree, if necessary
		if ($this->settings['checkout']['login']['step'])
		{
			$pageRepository = $this->makeInstance('Maagit\Maagitproduct\Domain\Repository\PageRepository');
			$this->settings['checkout']['login']['pages'] = implode(',', $pageRepository->getPageTreeUids(\TYPO3\CMS\Core\Utility\GeneralUtility::TrimExplode(',', $this->settings['checkout']['login']['pages']), (int)$this->settings['checkout']['login']['recursive']));
		}
	}
	
	
	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Action for this controller.
	 * Login section.
     *
     * @return void
     */
	public function loginAction(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout=NULL)
	{
		if (!$this->settings['checkout']['login']['step']) {return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('address'))->withControllerName('Checkout');}
		if ($checkout == NULL)
		{
			$checkout = $this->checkoutRepository->findAll();
		}
		else
		{
			$checkout = $this->checkoutRepository->persist($checkout);
		}
		$checkoutValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\CheckoutValidator', $checkout);
		$checkout = $this->checkoutRepository->persist($checkoutValidator->validate());
		$this->renderCheckout($checkout);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}

	/**
     * Action for this controller.
	 * Address section.
     *
     * @return void
     */
	public function addressAction(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout=NULL)
	{
		if ($checkout == NULL)
		{
			$checkout = $this->checkoutRepository->findAll();
		}
		else
		{
			$checkout = $this->checkoutRepository->persist($checkout);
		}
		$checkout->setAction('address');
		$checkoutValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\CheckoutValidator', $checkout);
		$checkout = $this->checkoutRepository->persist($checkoutValidator->validate());
		$this->renderCheckout($checkout);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}
	
	/**
     * Action for this controller.
	 * Delivery section.
     *
     * @return void
     */
	public function deliveryAction(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		// validate checkout and render it
		$checkout = $this->checkoutRepository->persist($checkout);
		$checkoutValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\CheckoutValidator', $checkout);
		$checkout = $this->checkoutRepository->persist($checkoutValidator->validate());
		$this->renderCheckout($checkout);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}
	
	/**
     * Action for this controller.
	 * Payment section.
     *
     * @return void
     */
	public function paymentAction(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		// validate checkout and render it
		$checkout = $this->checkoutRepository->persist($checkout);
		$checkoutValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\CheckoutValidator', $checkout);
		$checkout = $this->checkoutRepository->persist($checkoutValidator->validate());
		$this->renderCheckout($checkout);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}

	/**
     * Action for this controller.
	 * Coupon section.
     *
     * @return void
     */
	public function couponAction(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		// validate checkout and render it
		$checkout = $this->checkoutRepository->persist($checkout);
		$checkoutValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\CheckoutValidator', $checkout);
		$checkout = $this->checkoutRepository->persist($checkoutValidator->validate());
		$this->renderCheckout($checkout);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}

	/**
     * Action for this controller.
	 * Terms section.
     *
     * @return void
     */
	public function termsAction(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		// validate checkout and render it
		$checkout = $this->checkoutRepository->persist($checkout);
		$checkoutValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\CheckoutValidator', $checkout);
		$checkout = $this->checkoutRepository->persist($checkoutValidator->validate());
		$this->renderCheckout($checkout);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}

	/**
     * Action for this controller.
	 * Quantity change of a basket item.
     *
     * @return void
     */
	public function quantityAction(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout, int $productUid, int $productUnit=null, int $productQuantity)
	{
		// save client changes
		$checkout = $this->checkoutRepository->persist($checkout);

		// change quantity of basket item
		$checkout->setBasket($this->basketRepository->change($this->basketItemRepository->findByUid($productUid, $productUnit), $productQuantity));
		$checkout = $this->checkoutRepository->persist($checkout);

		// validate checkout
		$checkoutValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\CheckoutValidator', $checkout);
		$checkout = $this->checkoutRepository->persist($checkoutValidator->validate());

		// redirect
		return (new \TYPO3\CMS\Extbase\Http\ForwardResponse($checkout->getAction()))->withArguments(array('checkout' => $checkout));
	}

	/**
     * Action for this controller.
	 * Remove a basket item.
     *
     * @return void
     */
	public function removeAction(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout, int $productUid, int $productUnit=null)
	{
		// save client changes
		$checkout = $this->checkoutRepository->persist($checkout);

		// remove basket item
		$checkout->setBasket($this->basketRepository->remove($this->basketItemRepository->findByUid($productUid, $productUnit)));		
		$checkout = $this->checkoutRepository->persist($checkout);

		// validate checkout
		$checkoutValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\CheckoutValidator', $checkout);
		$checkout = $this->checkoutRepository->persist($checkoutValidator->validate());

		// redirect
		return (new \TYPO3\CMS\Extbase\Http\ForwardResponse($checkout->getAction()))->withArguments(array('checkout' => $checkout));
	}
	
	/**
     * Action for this controller.
	 * Summary section.
     *
     * @return void
     */
	public function summaryAction(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		// validate checkout and render it
		$checkout = $this->checkoutRepository->persist($checkout);
		$checkoutValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\CheckoutValidator', $checkout);
		$checkout = $this->checkoutRepository->persist($checkoutValidator->validate());
		$this->renderCheckout($checkout);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}
	
	/**
     * Action for this controller.
	 * Ajax call.
     *
     * @return void
     */
	public function ajaxAction(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		// save client changes
		$checkout = $this->checkoutRepository->persist($checkout);

		// validate checkout
		$checkoutValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\CheckoutValidator', $checkout);
		$checkout = $this->checkoutRepository->persist($checkoutValidator->validate());
		
		// create json array for processing changed properties on client
		$changes = array();
		$changes['deliveryMethod'] = $checkout->getDelivery()->getDeliveryMethod();
		$changes['paymentMethod'] = $checkout->getPayment()->getPaymentMethod();

		// assign values to view
		$this->view->assign('changes', json_encode($changes));
		$this->view->assign('settings', $this->settings);
		$this->renderCheckout($checkout);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}

	/**
     * Action for this controller.
	 * Paypal call.
     *
     * @return void
     */
	public function paypalAction()
	{
		// get checkout
		$checkout = $this->checkoutRepository->findAll();

		// initialize payment
		$paypal = $this->makeInstance('Maagit\\Maagitproduct\\Payment\\Paypal\\Paypal', $checkout);
		// @extensionScannerIgnoreLine
		$paypal->init();

		// validate payment
		$paymentValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\PaymentValidator\Paypal\PaypalValidator', $paypal, $checkout);
		$checkout = $this->checkoutRepository->persist($paymentValidator->validate());

		// redirect
		if ($checkout->getPayment()->getPaymentError() == '')
		{
			return $this->responseFactory->createResponse()->withAddedHeader('location', $paypal->call());
		}
		else
		{
			return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('summary'))->withArguments(array('checkout' => $checkout));	
		}
	}

	/**
     * Action for this controller.
	 * Paypal return call.
     *
     * @return void
     */
	public function paypalanswerAction()
	{
		// get checkout
		$checkout = $this->checkoutRepository->findAll();

		// validate payment answer
		$paymentAnswerValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\PaymentValidator\Paypal\PaypalAnswerValidator', $checkout);
		$checkout = $this->checkoutRepository->persist($paymentAnswerValidator->validate());

		// redirect
		if ($checkout->getPayment()->getPaymentError() == '')
		{
			return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('send'))->withArguments(array('checkout' => $checkout));
		}
		else
		{
			return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('summary'))->withArguments(array('checkout' => $checkout));	
		}
	}
	
	/**
     * Action for this controller.
	 * Saferpay call.
     *
     * @return void
     */
	public function saferpayAction()
	{
		// get checkout
		$checkout = $this->checkoutRepository->findAll();

		// initialize payment
		$saferpay = $this->makeInstance('Maagit\\Maagitproduct\\Payment\\Saferpay\\Saferpay', $checkout);
		// @extensionScannerIgnoreLine
		$saferpay->init();
		
		// validate payment
		$paymentValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\PaymentValidator\Saferpay\SaferpayValidator', $saferpay, $checkout);
		$checkout = $this->checkoutRepository->persist($paymentValidator->validate());

		// redirect
		if ($checkout->getPayment()->getPaymentError() == '')
		{
			return $this->responseFactory->createResponse()->withAddedHeader('location', $saferpay->call());
		}
		else
		{
			return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('summary'))->withArguments(array('checkout' => $checkout));	
		}
	}
	
	/**
     * Action for this controller.
	 * Saferpay return call.
     *
     * @return void
     */
	public function saferpayanswerAction()
	{
		// get checkout
		$checkout = $this->checkoutRepository->findAll();

		// validate payment answer
		$paymentAnswerValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\PaymentValidator\Saferpay\SaferpayAnswerValidator', $checkout);
		$checkout = $this->checkoutRepository->persist($paymentAnswerValidator->validate());

		// redirect
		if ($checkout->getPayment()->getPaymentError() == '')
		{
			return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('send'))->withArguments(array('checkout' => $checkout));
		}
		else
		{
			return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('summary'))->withArguments(array('checkout' => $checkout));	
		}
	}
	
	/**
     * Action for this controller.
	 * Stripe call.
     *
     * @return void
     */
	public function stripeAction(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		// get checkout
		$checkout = $this->checkoutRepository->findAll();

		// initialize payment
		$stripe = $this->makeInstance('Maagit\\Maagitproduct\\Payment\\Stripe\\Stripe', $checkout);
		// @extensionScannerIgnoreLine
		$stripe->init();
		
		// validate payment
		$paymentValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\PaymentValidator\Stripe\StripeValidator', $stripe, $checkout);
		$checkout = $this->checkoutRepository->persist($paymentValidator->validate());

		// redirect
		if ($checkout->getPayment()->getPaymentError() == '')
		{
			$arguments = $this->divHelper->getUrlParameter($stripe->call());
			$this->view->assign('sessionId', $arguments['sessionId']);
			$this->view->assign('pk', $arguments['pk']);
			return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
		}
		else
		{
			return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('summary'))->withArguments(array('checkout' => $checkout));	
		}
	}

	/**
     * Action for this controller.
	 * Saferpay return call.
     *
     * @return void
     */
	public function stripeanswerAction()
	{
		// get checkout
		$checkout = $this->checkoutRepository->findAll();

		// validate payment answer
		$paymentAnswerValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\PaymentValidator\Stripe\StripeAnswerValidator', $checkout);
		$checkout = $this->checkoutRepository->persist($paymentAnswerValidator->validate());

		// redirect
		if ($checkout->getPayment()->getPaymentError() == '')
		{
			return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('send'))->withArguments(array('checkout' => $checkout));
		}
		else
		{
			return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('summary'))->withArguments(array('checkout' => $checkout));	
		}
	}

	/**
     * Action for this controller.
	 * Send section.
     *
     * @return void
     */
	public function sendAction(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		// save client changes
		$checkout = $this->checkoutRepository->persist($checkout);

		// validate sending this order
		$sendValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\SendValidator', $checkout);
		$checkout = $this->checkoutRepository->persist($sendValidator->validate());

		// add experience rating
		$experienceValidator = $this->makeInstance('Maagit\\Maagitproduct\\Validator\\ExperienceValidator', $checkout);
		$checkout = $this->checkoutRepository->persist($experienceValidator->validate());

		// cancel, if validation fails, else --> redirect or render confirmation
		if (!$checkout->getSendingDone())
		{
			$checkout->setAction('summary');
			return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('summary'))->withArguments(array('checkout' => $checkout));
		}
		else
		{
			$redirectService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Send\\RedirectService', $this, $this->checkoutRepository, $checkout);
			$continue = $redirectService->process();
			if ($continue !== FALSE)
			{
				return $continue;
			}
		}

		// show confirmation and clear checkout and basket objects
		$this->renderCheckout($checkout);
		if ($this->settings['checkout']['experience']['show']) {$this->checkoutRepository->clear();} else {$this->checkoutRepository->removeAll();}
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}

	/**
     * Initialize Action for this controller.
	 * Experience action initialization.
     *
     * @return void
     */
	protected function initializeExperienceAction()
	{
		$propertyMappingConfiguration = $this->arguments['experience']->getPropertyMappingConfiguration();
		$propertyMappingConfiguration->allowAllProperties();
		$propertyMappingConfiguration->setTypeConverterOption('TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE);
	}

	/**
     * Action for this controller.
	 * Experience return call.
     *
     * @return void
     */
	public function experienceAction(\Maagit\Maagitproduct\Domain\Model\Experience $experience)
	{
		$experienceService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Experience\\ExperienceService', $experience);
		$experience = $experienceService->sendRating();
		$experienceService->saveRating();
		$experienceService->clearCache();
		$this->view->assign('experience', $experience);
		$this->checkoutRepository->removeAll();
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * render checkout content.
     *
	 * @param	\Maagit\Maagitproduct\Domain\Model\Checkout	$checkout	the checkout object
	 * @return	void
     */
	protected function renderCheckout(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		// make base action things (e.g. javascript enabled check)
		if (!isset($_GET['ajax']) && !isset($_POST['ajax'])) {$this->baseAction();}

		// check, if all steps are valid, otherwise go to invalid step
		if (($checkout->getAction() == '' || $checkout->getAction() == 'summary' || $checkout->getAction() == 'send') && (($this->settings['checkout']['login']['step'] && !$checkout->getLogin()->getVisited()) || !$checkout->getAddress()->getVisited() || !$checkout->getDelivery()->getVisited() || !$checkout->getPayment()->getVisited() || ($this->settings['checkout']['coupon']['step'] && !$checkout->getCoupon()->getVisited()) || ($this->settings['checkout']['sending']['condition'] == 2 && !$checkout->getTerms()->getVisited())))
		{
			switch(TRUE)
			{
				case $checkout->getPayment()->getVisited():
					$checkout->setAction('summary');
					break;
				case ($this->settings['checkout']['sending']['condition'] == 2 && $checkout->getTerms()->getVisited()):
					$checkout->setAction('terms');
					break;
				case ($this->settings['checkout']['coupon']['step'] && $checkout->getCoupon()->getVisited()):
					$checkout->setAction('coupon');
					break;
				case $checkout->getDelivery()->getVisited():
					$checkout->setAction('payment');
					break;
				case $checkout->getAddress()->getVisited():
					$checkout->setAction('delivery');
					break;
				case ($this->settings['checkout']['login']['step'] && $checkout->getLogin()->getVisited()):
					$checkout->setAction('address');
					break;
				default:
			       	if ($this->settings['checkout']['login']['step'])
					{
						$checkout->setAction('login');
					}
					else
					{
						$checkout->setAction('address');
					}
			}
		}

		// assign datas to views
		$this->view->assign('checkout', $checkout);
		if ($this->settings['checkout']['login']['step'])
		{
			$this->view->assign('requestToken', \TYPO3\CMS\Core\Security\RequestToken::create('core/user-auth/fe')->withMergedParams(['pid' => $this->settings['checkout']['login']['pages']]));
		}
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}