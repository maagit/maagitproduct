<?php
namespace Maagit\Maagitproduct\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Controller
	class:				BasketController

	description:		Main class for the shopping cart (basket).
						Process the actions "show", "add", "remove" and "change".

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed

------------------------------------------------------------------------------------- */


class BasketController extends \Maagit\Maagitproduct\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Repository\BasketRepository
     */
    protected $basketRepository;
	
	/**
	 * @var \Maagit\Maagitproduct\Domain\Repository\BasketItemRepository
     */
    protected $basketItemRepository;

	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Constructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// parent initalization things
		parent::initializeObject();

		// inject repositories
		$this->basketRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\BasketRepository');
		$this->basketItemRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\BasketItemRepository');
	}

	
	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Action for this controller.
	 * Shows the whole page with current basket.
     *
     * @return void
     */
	public function showAction()
	{
		$this->baseAction();
		$this->view->assign('basket', $this->basketRepository->findAll());
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}
	
	/**
     * Action for this controller.
	 * Add a given article to the basket.
     *
 	 * @param int		$uid      	The uid of product
	 * @param int		$unit      	The uid of product unit
     * @return void
     */
	public function addAction(int $uid, int $unit=null)
	{
		$basket = $this->basketRepository->add($this->basketItemRepository->create(null, $uid, $unit));
		$this->renderAjax($basket);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}
	
	/**
     * Action for this controller.
	 * Add a given article to the basket.
     *
     * @param int		$uid      	The uid of product
	 * @param int		$unit      	The uid of product unit
	 * @return void
     */
	public function removeAction(int $uid, int $unit=null)
	{
		$basket = $this->basketRepository->remove($this->basketItemRepository->findByUid($uid, $unit));
		$this->renderAjax($basket);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}
	
	/**
     * Action for this controller.
	 * Change given quantity of given article
     *
     * @param int		$uid      	The uid of product
	 * @param int		$unit      	The uid of product unit
	 * @param int		$quantity	The quantity
	 * @return void
     */
	public function changeAction(int $uid, int $unit=null, int $quantity)
	{
		$basket = $this->basketRepository->change($this->basketItemRepository->findByUid($uid, $unit), $quantity);
		$this->renderAjax($basket);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}
	
	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * render basket content as response from a ajax call.
     *
	 * @param	\Maagit\Maagitproduct\Domain\Model\Basket		$basket  	The basket.
	 * @return void
     */
	protected function renderAjax(\Maagit\Maagitproduct\Domain\Model\Basket $basket)
	{
		$this->view->assign('settings', $this->settings);
		$this->view->assign('basket', $basket);
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}