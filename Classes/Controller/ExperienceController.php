<?php
namespace Maagit\Maagitproduct\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Controller
	class:				ExperienceController

	description:		Controller to display the shop experience ratings.

	created:			2020-11-01
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-11-01	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed

------------------------------------------------------------------------------------- */


class ExperienceController extends \Maagit\Maagitproduct\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Repository\ExperienceRepository
	 */
	protected $experienceRepository;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Contructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// parent initalization things
		parent::initializeObject();
		
		// inject repositories
		$this->experienceRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\ExperienceRepository');
	}

	
	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Show action for this controller. Displays the rating of experiences.
     *
     * @return void
     */
	public function showAction()
	{
		$experiences = $this->experienceRepository->findAll();
		$this->view->assign('experiences', $experiences);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}