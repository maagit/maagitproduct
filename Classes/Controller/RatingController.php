<?php
namespace Maagit\Maagitproduct\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Controller
	class:				RatingController

	description:		Controller to list outstanding ratings and create new ratings.

	created:			2020-08-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-08-29	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed

------------------------------------------------------------------------------------- */


class RatingController extends \Maagit\Maagitproduct\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Repository\ProductRepository
	 */
	protected $productRepository;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Contructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// parent initalization things
		parent::initializeObject();
		
		// inject repositories and init data
		$this->productRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\ProductRepository');
	}
	
	
	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Show action for this controller. Displays form for creating a rating of a given product.
     *
     * @return void
     */
	public function showAction(int $productUid=null)
	{
		$product = null;
		if (!empty($productUid))
		{
			try
			{
				$product = $this->productRepository->findByUid($productUid);
			}
			catch (\Exception $ex) {	}	
		}
		$rating = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Model\\Rating');
		if ($this->request->hasArgument('name'))
		{
			$rating->setName($this->request->getArgument('name'));	
		}
		if ($this->request->hasArgument('email'))
		{
			$rating->setEmail($this->request->getArgument('email'));	
		}
		$this->view->assign('rating', $rating);
		$this->view->assign('product', $product);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}

	/**
     * Create action for this controller. Create a new rating.
     *
     * @return void
     */
	public function createAction(\Maagit\Maagitproduct\Domain\Model\Rating $rating)
	{
		// add new rating
		$ratingRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\RatingRepository');
		$persistenceManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager');
		$ratingRepository->add($rating);
		$persistenceManager->persistAll();

		// send mail for check and accept the rating
		if ($this->settings['new']['check'] || $this->settings['new']['notify'])
		{
			$product = $this->productRepository->findByUid($rating->getTtContent());
			$emailService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Network\\EmailService');
			$renderService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Content\\RenderService');
			$bodyText = $renderService->renderTemplate('Rating/EmailNotification', array('rating' => $rating, 'product' => $product, 'settings' => $this->settings), 'maagitproduct_rating');
			$subject = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('rating.emailnotification.subject', 'maagitproduct');
			$sent = $emailService->sendMail(array($this->settings['from']['email']), array($this->settings['new']['email']), $subject, $bodyText, $bodyText);	
		}

		// show confirmation message
		return $this->redirect('confirm', null, null, array('productUid' => $rating->getTtContent()));
	}

	/**
     * Confirm action for this controller. Confirm the new rating.
     *
     * @return void
     */
	public function confirmAction(int $productUid)
	{
		try
		{
			$product = $this->productRepository->findByUid($productUid);
		}
		catch (\Exception $ex) {	}	
		$this->view->assign('product', $product);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}

	/**
     * Accept action for this controller. Accept the new rating.
     *
     * @return void
     */
	public function acceptAction(int $ratingUid)
	{
		$ratingRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\RatingRepository');
		$rating = $ratingRepository->findHiddenByUid($ratingUid);
		if (!empty($rating))
		{
			$rating->setHidden(false);
			$ratingRepository->update($rating);
			$persistenceManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager');
			$persistenceManager->persistAll();

			try
			{
				$product = $this->productRepository->findByUid($rating->getTtContent());
			}
			catch (\Exception $ex) {	}	
			$this->view->assign('rating', $rating);
			$this->view->assign('product', $product);
		}
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}

	/**
     * Send action for this controller. Send emails for ratings.
     *
     * @return void
     */
	public function sendAction(int $order)
	{
		$orderRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\OrderRepository');
		$order = $orderRepository->findByUid($order);
		$this->view->assign('order', $order);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}

	/**
     * Send action for this controller. Send emails for ratings.
     *
     * @return void
     */
	public function sendmailAction(int $orderUid, int $productUid=null)
	{
		// initialization
		$error = array();

		// initialization - get data from order
		$orderRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\OrderRepository');
		$order = $orderRepository->findByUid($orderUid);
		if (empty($order))
		{
			$error['message'] = 'Order with uid "'.$orderUid.'" not found';
		}
		else
		{
			$email = $order->getEmail();
			if (empty($email))
			{
				$error['message'] = 'No email address found';
			}
		}

		// initializtation - get data from product
		try
		{
			$productRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\ProductRepository');
			$product = $productRepository->findByUid($productUid);
			if (empty($product))
			{
				$error['message'] = 'Product with uid "'.$productUid.'" not found';
			}
		}
		catch (\Exception $ex)
		{
			$error['message'] = 'Product not found';
		}

		// render email and send it
		if (empty($error['message']))
		{
			try
			{
				$emailService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Network\\EmailService');
				$renderService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Content\\RenderService');
				$bodyText = $renderService->renderTemplate('Rating/EmailRating', array('order' => $order, 'product' => $product, 'settings' => $this->settings), 'maagitproduct_rating');
				$subject = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('rating.emailrating.subject', 'maagitproduct');
				$emailService->sendMail(array($this->settings['from']['email']), array($email), $subject, $bodyText, $bodyText);	
			}
			catch (\Exception $ex)
			{
				$error['message'] = $ex;
			}
		}

		// update sending date
		if (empty($error['message']))
		{
			foreach ($order->getArticles() as $article)
			{
				if ($article->getProductuid() == $productUid)
				{
					$article->setRatingmail(time());
					$orderRepository->update($order);
					$persistenceManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager');
					$persistenceManager->persistAll();
					break;
				}
			}
		}

		$this->view->assign('error', $error);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}