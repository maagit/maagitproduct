<?php
namespace Maagit\Maagitproduct\Payment\Paypal;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Payment
	class:				PaypalHelper

	description:		Helper class for external payment service "paypal".

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2024-11-17	Urs Maag		Bugfix null location to empty string

------------------------------------------------------------------------------------- */


class PaypalHelper extends \Maagit\Maagitproduct\Payment\PaymentHelper
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
		* 	Initalize payment process by given payment provider
		*
        *	@param array	$data	order data
		* 	@return array			formatted JSON response
	*/
	public function init($data)
	{
		if ($this->token === null) {$this->getToken();}
		$this->httpService->resetHelper();
		$this->httpService->addHeader('Content-Type: application/json');
		$this->httpService->addHeader('Authorization: Bearer '.$this->token);
		$this->httpService->setUrl($this->createApiUrl('checkout/orders'));
		$this->httpService->setBody($data);
		$returnData = $this->httpService->sendRequest();

		try
		{
			$approveUrl = $this->getApproveUrl($returnData);
			return array(
				'ack' => true,
				'data' => array(
					'id' => $returnData['id'],
					'status' => $returnData['status'],
					'approveUrl' => $approveUrl,
				)
			);
		}
		catch (\Exception $ex)
		{
			// send debug mail, if $debug is set
			if ($this->settings['checkout']['payment']['method']['paypal']['debug'] != '')
			{
				$this->sendDebugInformation(
					$this->settings['checkout']['payment']['method']['paypal']['debug'],
					(array)json_decode($data), 
					$returnData
				);
			}
			// return result
			return array(
				'ack' => false,
				'message' => $returnData['message'],
				'data' => array(
					'id' => $returnData['debug_id'],
					'status' => $returnData['name'],
					'details' => array(
						'issue' => $returnData['details'][0]['issue'],
						'description' => $returnData['details'][0]['description'],
						'field' => $returnData['details'][0]['field'],
						'location' => $returnData['details'][0]['location']??'',
					)
				)
			);
		}
	}

	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
		* 	Get approve url from given paypal answer array
		*
		* 	@return string	paypal approve url
	*/
	protected function getApproveUrl(array $data)
	{
		if (!array_key_exists('links', $data))
		{
			throw new \Exception('There is no "link" element in the paypal "init" answer.', time());
		}
		$approveLink = null;
		$links = $data['links'];
		foreach ($links as $link)
		{
			if ($link['rel'] == 'approve' && $link['href'] != '')
			{
				$approveLink = $link['href'];
			}
		}
		if ($approveLink == null)
		{
			throw new \Exception('There is no approve link found in the paypal "init" answer.', time());
		}
		return $approveLink;
	}
	

	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	/**
		* 	Request for PayPal REST oath bearer token.
		*	
		* 	Reset curl helper. 
		*	Set default PayPal headers.
		*	Set curl url.
		*	Set curl credentials.
		*	Set curl body.
		*	Set class token attribute with bearer token.
		*
		* 	@return void
	*/
	private function getToken()
	{
		$this->httpService->resetHelper();
		$this->httpService->setUrl($this->createApiUrl('oauth2/token'));
		$this->httpService->setAuthentication($this->payment->getUsername().':'.$this->payment->getPassword());
		$this->httpService->setBody('grant_type=client_credentials');
		$returnData = $this->httpService->sendRequest();
		$this->token = $returnData['access_token'];
		$this->payment->setToken($this->token);
	}

	/**
		* 	Create the PayPal REST endpoint url.
		*
		*	Use the configurations and combine resources to create the endpoint.
		*
        *	@param string	$resource	Url to be called using curl
		* 	@return string				REST API url depending on environment.
	*/
	private function createApiUrl($resource)
	{
		if ($resource == 'oauth2/token')
		{
			return $this->payment->getEndpoint().'/v1/'.$resource;
		}
		else
		{
			return $this->payment->getEndpoint().'/'.$this->payment->getPaypalRESTVersion().'/'.$resource;	
		}
	}
}
