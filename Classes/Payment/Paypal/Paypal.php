<?php
namespace Maagit\Maagitproduct\Payment\Paypal;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Payment
	class:				Paypal

	description:		External payment service "paypal".

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-09-09	Urs Maag		if shipping < 0, take 0
						2024-09-22	Urs Maag		Add discount to purchase details
						2024-11-17	Urs Maag		Round amounts to given decimals

------------------------------------------------------------------------------------- */


class Paypal extends \Maagit\Maagitproduct\Payment\Payment
{
	/* ======================================================================================= */
	/* C O N S T A N T S                                                                       */
	/* ======================================================================================= */
	const ENVIRONMENT_TEST = 'sandbox';
	const ENVIRONMENT_PRODUCTION = 'production';
	const ENDPOINTS = array(
		'sandbox' => 'https://api.sandbox.paypal.com',
		'production' => 'https://api.paypal.com'
	);
	const REST_VERSION = 'v2';


	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var string
     */
	protected $answerAction = 'paypalanswer';

	/**
     * @var string
     */
	protected $paypalRESTVersion;
	

	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		parent::initializeObject($checkout);
		$this->paypalRESTVersion = self::REST_VERSION;
		$this->paymentHelper = $this->makeInstance('Maagit\\Maagitproduct\\Payment\\Paypal\\PaypalHelper', $this);
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Returns the environment
     *
     * @return string	environment
     */
    public function getEnvironment()
    {
        return ($this->settings['checkout']['payment']['method']['paypal']['useSandbox']) ? self::ENVIRONMENT_TEST : self::ENVIRONMENT_PRODUCTION;
    }

	/**
     * Returns the endpoint
     *
     * @return string	endpoint
     */
    public function getEndpoint()
    {
        return self::ENDPOINTS[$this->getEnvironment()];
    }
	
	/**
     * Returns the username
     *
     * @return string	username
     */
    public function getUsername()
    {
        return ($this->getEnvironment()==self::ENVIRONMENT_TEST) ? $this->settings['checkout']['payment']['method']['paypal']['sandboxClientId'] : $this->settings['checkout']['payment']['method']['paypal']['clientId'];
    }
	
	/**
     * Returns the password
     *
     * @return string	password
     */
    public function getPassword()
    {
         return ($this->getEnvironment()==self::ENVIRONMENT_TEST) ? $this->settings['checkout']['payment']['method']['paypal']['sandboxSecret'] : $this->settings['checkout']['payment']['method']['paypal']['secret'];
    }
	
	/**
     * Sets the paypalRESTVersion
     *
     * @param string $paypalRESTVersion
     */
    public function setPaypalRESTVersion($paypalRESTVersion)
    {
        $this->paypalRESTVersion = $paypalRESTVersion;
    }
	
	/**
     * Returns the paypalRESTVersion
     *
     * @return string	paypalRESTVersion
     */
    public function getPaypalRESTVersion()
    {
        return $this->paypalRESTVersion;
    }
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * initialize the payment process
     *
     * @param	-
	 * @return	boolean		order created|order creation has errors 
     */
	public function init()
	{
		$orderDataArr = array();
		$orderDataArr['intent'] = 'CAPTURE';
		$orderDataArr['application_context']['return_url'] = $this->getReturnUrl();
		$orderDataArr['application_context']['cancel_url'] = $this->getCancelUrl();
		$orderDataArr['application_context']['brand_name'] = $this->settings['checkout']['payment']['method']['paypal']['description'];
		$orderDataArr['application_context']['locale'] = 'ch-DE';
		$orderDataArr['application_context']['landing_page'] = 'NO_PREFERENCE';
		$orderDataArr['application_context']['shipping_preference'] = 'NO_SHIPPING';
		$orderDataArr['application_context']['user_action'] = 'PAY_NOW';
			$purchaseUnits = array();
			$purchaseUnits['reference_id'] = $this->settings['checkout']['payment']['method']['paypal']['referenceId'];
			$purchaseUnits['description'] = $this->settings['checkout']['payment']['method']['paypal']['description'];
			$purchaseUnits['amount']['currency_code'] = $this->settings['checkout']['currency']['sign'];
			$purchaseUnits['amount']['value'] = $this->checkout->getTotal();
			$purchaseUnits['amount']['breakdown']['item_total']['currency_code'] = $this->settings['checkout']['currency']['sign'];
			$purchaseUnits['amount']['breakdown']['item_total']['value'] = round($this->checkout->getBasket()->getAmount(), (int)$this->settings['checkout']['currency']['decimal']);
			if (!$this->settings['checkout']['taxincluded'])
			{
				$purchaseUnits['amount']['breakdown']['tax_total']['currency_code'] = $this->settings['checkout']['currency']['sign'];
				$purchaseUnits['amount']['breakdown']['tax_total']['value'] = $this->checkout->getBasket()->getTax();			
			}
			$purchaseUnits['amount']['breakdown']['shipping']['currency_code'] = $this->settings['checkout']['currency']['sign'];
			$purchaseUnits['amount']['breakdown']['shipping']['value'] = round((($this->checkout->getDelivery()->getShipping()>0) ? $this->checkout->getDelivery()->getShipping() : 0), (int)$this->settings['checkout']['currency']['decimal']);
			$purchaseUnits['amount']['breakdown']['handling']['currency_code'] = $this->settings['checkout']['currency']['sign'];
			$purchaseUnits['amount']['breakdown']['handling']['value'] = round($this->checkout->getPayment()->getExpenses(), (int)$this->settings['checkout']['currency']['decimal']);
			if ($this->settings['checkout']['coupon']['step'] && !empty($this->checkout->getCoupon()->getCode()))
			{
				$purchaseUnits['amount']['breakdown']['discount']['currency_code'] = $this->settings['checkout']['currency']['sign'];
				$purchaseUnits['amount']['breakdown']['discount']['value'] = round(abs($this->checkout->getCoupon()->getAmount()), (int)$this->settings['checkout']['currency']['decimal']);
			}
				$items = array();
				foreach ($this->checkout->getBasket()->getBasketItems() as $basketItem)
				{
					if ($basketItem->getTotal() > 0)
					{
						$item = array();
						$item['name'] = $basketItem->getProduct()->getHeader();
						$item['decription'] = strip_tags($basketItem->getProduct()->getBodytext());
						$item['unit_amount']['currency_code'] = $this->settings['checkout']['currency']['sign'];
						$item['unit_amount']['value'] = round((!empty($basketItem->getUnitPrice())) ? $basketItem->getUnitPrice() : $basketItem->getProduct()->getPrice(), (int)$this->settings['checkout']['currency']['decimal']);
						$item['quantity'] = $basketItem->getQuantity();
						$items[] = $item;
					}
				}
			$purchaseUnits['items'] = $items;
		$orderDataArr['purchase_units'][] = $purchaseUnits;

		// make json and save it
		$orderData = json_encode($orderDataArr);
		$this->setOrder($orderData);

		// create order and set approveLnk|errorMessage
		// @extensionScannerIgnoreLine
		$result = $this->paymentHelper->init($orderData);
		if (!$result['ack'])
		{
			$this->setError($result['message']);
			$this->setErrorDetails($result['data']['details']);	
		}
		else
		{
			$this->setToken($result['data']['id']);
			$this->setApproveLink($result['data']['approveUrl']);
		}
		$this->setInit($result['ack']);
		return $result['ack'];
	}
	
	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
