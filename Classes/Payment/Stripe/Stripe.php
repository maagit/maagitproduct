<?php
namespace Maagit\Maagitproduct\Payment\Stripe;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Payment
	class:				Stripe

	description:		External payment service "stripe".

	created:			2020-12-19
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-12-19	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Stripe extends \Maagit\Maagitproduct\Payment\Payment
{
	/* ======================================================================================= */
	/* C O N S T A N T S                                                                       */
	/* ======================================================================================= */
	const ENVIRONMENT_TEST = 'test';
	const ENVIRONMENT_PRODUCTION = 'production';
	const ENDPOINTS = array(
		'test' => '',
		'production' => ''
	);


	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var string
     */
	protected $answerAction = 'stripeanswer';

	/**
     * @var string
     */
	protected $paymentId = '';


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		parent::initializeObject($checkout);
		$this->paymentHelper = $this->makeInstance('Maagit\\Maagitproduct\\Payment\\Stripe\\StripeHelper', $this);
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Returns the environment
     *
     * @return string	environment
     */
    public function getEnvironment()
    {
        return ($this->settings['checkout']['payment']['method']['stripe']['useTestEnvironment']) ? self::ENVIRONMENT_TEST : self::ENVIRONMENT_PRODUCTION;
    }
	
	/**
     * Returns the endpoint
     *
     * @return string	endpoint
     */
    public function getEndpoint()
    {
        return self::ENDPOINTS[$this->getEnvironment()];
    }

	/**
     * Returns the username
     *
     * @return string	username
     */
    public function getUsername()
    {
        return null;
    }

	/**
     * Returns the password
     *
     * @return string	password
     */
    public function getPassword()
    {
         return null;
    }

	/**
     * Returns the public key
     *
     * @return string	publicKey
     */
    public function getPublicKey()
    {
        return ($this->getEnvironment()==self::ENVIRONMENT_TEST) ? $this->settings['checkout']['payment']['method']['stripe']['testPublicKey'] : $this->settings['checkout']['payment']['method']['stripe']['publicKey'];
    }
	
	/**
     * Returns the privateKey
     *
     * @return string	privateKey
     */
    public function getPrivateKey()
    {
         return ($this->getEnvironment()==self::ENVIRONMENT_TEST) ? $this->settings['checkout']['payment']['method']['stripe']['testPrivateKey'] : $this->settings['checkout']['payment']['method']['stripe']['privateKey'];
    }

	/**
     * Sets the sessionId
     *
     * @param string	paymentId
     */
    public function setPaymentId(string $paymentId)
    {
         $this->paymentId = $paymentId;
    }

	/**
     * Returns the paymentId
     *
     * @return string	paymentId
     */
    public function getPaymentId()
    {
         return $this->paymentId;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * initialize the payment process
     *
     * @param	-
	 * @return	\Maagit\Maagitproduct\Domain\Model\Checkout		the checkout object
     */
	public function init()
	{
		// generate token
		$this->setToken(bin2hex(openssl_random_pseudo_bytes(16)));
		$this->setReturnUrl($this->getReturnUrl().'&tx_maagitproduct_checkout[token]='.$this->getToken());
		$this->setCancelUrl($this->getCancelUrl().'&tx_maagitproduct_checkout[token]='.$this->getToken());

		// create payload array
		$orderDataArr = array();
		$orderDataArr['customer_email'] = $this->checkout->getAddress()->getEmail();
		$orderDataArr['mode'] = 'payment';
		$orderDataArr['payment_method_types'] = array('card');
		$orderDataArr['success_url'] = $this->getReturnUrl();
		$orderDataArr['cancel_url'] = $this->getCancelUrl();
			$items = array();
			foreach ($this->checkout->getBasket()->getBasketItems() as $basketItem)
			{
				if ($basketItem->getTotal() > 0)
				{
					$item = array();
					$item['quantity'] = $basketItem->getQuantity();
					$item['price_data'] = array();
					$item['price_data']['currency'] = strtolower($this->settings['checkout']['currency']['sign']);
					$item['price_data']['unit_amount'] = (!empty($basketItem->getUnitPrice())) ? ($basketItem->getUnitPrice() * 100) : ($basketItem->getProduct()->getPrice() * 100);
					$item['price_data']['product_data'] = array();
					$item['price_data']['product_data']['name'] = $basketItem->getProduct()->getHeader();
					$item['price_data']['product_data']['images'] = $this->getImages($basketItem);
					$items[] = $item;
				}
			}
			if (!$this->settings['checkout']['taxincluded'] && $this->checkout->getBasket()->getTax() > 0)
			{
				$tax = array();
				$tax['quantity'] = 1;
				$tax['price_data'] = array();
				$tax['price_data']['currency'] = strtolower($this->settings['checkout']['currency']['sign']);
				$tax['price_data']['unit_amount'] = ($this->checkout->getBasket()->getTax() * 100);
				$tax['price_data']['product_data'] = array();
				$tax['price_data']['product_data']['name'] = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.payment.stripe.item.tax', 'maagitproduct');
				$items[] = $tax;
			}
			if ($this->checkout->getDelivery()->getShipping() > 0)
			{
				$shipping = array();
				$shipping['quantity'] = 1;
				$shipping['price_data'] = array();
				$shipping['price_data']['currency'] = strtolower($this->settings['checkout']['currency']['sign']);
				$shipping['price_data']['unit_amount'] = ($this->checkout->getDelivery()->getShipping() * 100);
				$shipping['price_data']['product_data'] = array();
				$shipping['price_data']['product_data']['name'] = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.payment.stripe.item.shipping', 'maagitproduct');
				$items[] = $shipping;
			}
			if ($this->checkout->getPayment()->getExpenses() > 0)
			{
				$expenses = array();
				$expenses['quantity'] = 1;
				$expenses['price_data'] = array();
				$expenses['price_data']['currency'] = strtolower($this->settings['checkout']['currency']['sign']);
				$expenses['price_data']['unit_amount'] = ($this->checkout->getPayment()->getExpenses() * 100);
				$expenses['price_data']['product_data'] = array();
				$expenses['price_data']['product_data']['name'] = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.payment.stripe.item.expenses', 'maagitproduct');
				$items[] = $expenses;
			}
		$orderDataArr['line_items'][] = $items;

		// make stripe array and save it
		$orderData = $orderDataArr;
		$this->setOrder($orderData);

		// create transaction and set approveLnk|errorMessage
		// @extensionScannerIgnoreLine
		$result = $this->paymentHelper->init($orderData);
		if (!$result['ack'])
		{
			$this->setError($result['message']);
			$this->setErrorDetails($result['data']['details']);	
		}
		else
		{
			$this->setApproveLink($result['data']['approveUrl']);
		}
		$this->setInit($result['ack']);
		return $result['ack'];
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * get product images
     *
     * @param	\Maagit\Maagitproduct\Domain\Model\BasketItem		the basket item
	 * @return	array												the product images
     */
	public function getImages(\Maagit\Maagitproduct\Domain\Model\BasketItem $basketItem)
	{
		// initialization
		$host = $_SERVER['HTTP_HOST'];
		$protocol = (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS'])) ? 'https://' : 'http://';
		$images = array();

		// loop assets and add images
		foreach ($basketItem->getProduct()->getAssets() as $asset)
		{
			if ($asset->getType() == 'basketImage')
			{
				$image = $protocol.$host.'/'.$asset->getPathAndFileName();
				$images[] = $image;
			}
		}

		// return images as array
		return $images;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
