<?php
namespace Maagit\Maagitproduct\Payment\Stripe;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Payment
	class:				Stripe

	description:		Helper class for external payment service "stripe".

	created:			2020-12-19
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-12-19	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class StripeHelper extends \Maagit\Maagitproduct\Payment\PaymentHelper
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
		* 	Initalize payment process by given payment provider
		*
        *	@param array	$data	order data
		* 	@return array			formatted JSON response
	*/
	public function init($data)
	{
		// make stripe call
		require_once(dirname(__FILE__).'/Server/init.php');
		\Stripe\Stripe::setApiKey($this->payment->getPrivateKey());

		// get session id
		try {
			$session = \Stripe\Checkout\Session::create($data);
			$this->payment->setPaymentId($session->payment_intent);
			$sessionArray = json_decode(json_encode($session), true);
			$approveUrl = $this->getApproveUrl($sessionArray);
			return array(
				'ack' => true,
				'data' => array(
					// @extensionScannerIgnoreLine
					'id' => $session->id,
					'approveUrl' => $approveUrl,
				)
			);
		}
		catch (\Exception $ex)
		{
			// send debug mail, if $debug is set
			if ($this->settings['checkout']['payment']['method']['stripe']['debug'] != '')
			{
				$this->sendDebugInformation(
					$this->settings['checkout']['payment']['method']['stripe']['debug'],
					$data, 
					array('message' => $ex->getMessage())
				);
			}
			// return result
			return array(
				'ack' => false,
				'message' => $ex->getMessage(),
				'data' => array(
					'id' => time(),
					'status' => 'failed',
					'details' => array(
						'issue' => 'Stripe Create Session failed',
						'description' => $ex->getMessage(),
					)
				)
			);
		}
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
		* 	Get approve url for stripe call
		*
		* 	@return string	stripe approve url
	*/
	protected function getApproveUrl(array $data)
	{
		// get absolute uri of gateway script "StripeGateway.php"
		$host = $_SERVER['HTTP_HOST'];
		$protocol = (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS'])) ? 'https://' : 'http://';
		$path = \TYPO3\CMS\Core\Utility\PathUtility::getAbsoluteWebPath(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('maagitproduct'));
		$uri = $protocol.$host.$path.'Classes/Payment/Stripe/StripeGateway.php';

		// make approveLink with parameters and return it
		$approveLink = $uri.'?sessionId='.$data['id'].'&pk='.$this->payment->getPublicKey();
		return $approveLink;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
