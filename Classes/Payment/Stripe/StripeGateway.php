<html>
	<head>
		<title>Stripe payment - redirect to checkout</title>
	</head>
	<body>
		<script src="https://js.stripe.com/v3/"></script>
		<script>
			var stripe = Stripe('<?php echo($_GET['pk']); ?>');
			stripe.redirectToCheckout({sessionId: '<?php echo($_GET['sessionId']); ?>'});
		</script>
	</body>
</html>