<?php
namespace Maagit\Maagitproduct\Payment;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Payment
	class:				Payment

	description:		Abstract class for implementing external payment services.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


abstract class Payment extends \Maagit\Maagitproduct\Payment\BasePayment
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Payment\PaymentHelper
     */
    protected $paymentHelper;
	
	/**
	 * @var \Maagit\Maagitproduct\Domain\Model\Checkout
     */
    protected $checkout;

	/**
     * @var string
     */
	protected $answerAction = 'paymentanswer';

	/**
     * @var string
     */
	protected $environment;
	
	/**
     * @var string
     */
	protected $endpoint;
	
	/**
     * @var string
     */
	protected $username;
	
	/**
     * @var string
     */
	protected $password;
	
	/**
     * @var string
     */
	protected $order;
	
	/**
     * @var string
     */
	protected $returnUrl;
	
	/**
     * @var string
     */
	protected $cancelUrl;
	
	/**
     * @var boolean
     */
	protected $init;

	/**
     * @var string
     */
	protected $token;

	/**
     * @var string
     */
	protected $approveLink;

	/**
     * @var string
     */
	protected $error;
	
	/**
     * @var array
     */
	protected $errorDetails;
	

	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		$this->checkout = $checkout;
		$this->createUri();
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the environment
     *
     * @param string $environment
     */
    public function setEnvironment($environment)
    {
        $this->environment = $environment;
    }
	
	/**
     * Returns the environment
     *
     * @return string	environment
     */
    abstract public function getEnvironment();

	/**
     * Sets the endpoint
     *
     * @param string $endpoint
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }
	
	/**
     * Returns the endpoint
     *
     * @return string	endpoint
     */
    abstract public function getEndpoint();
	
	/**
     * Sets the username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }
	
	/**
     * Returns the username
     *
     * @return string	username
     */
    abstract public function getUsername();
	
	/**
     * Sets the password
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
	
	/**
     * Returns the password
     *
     * @return string	password
     */
    abstract public function getPassword();
	
	/**
     * Sets the order
     *
     * @param string $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }
	
	/**
     * Returns the order
     *
     * @return string	order
     */
    public function getOrder()
    {
        return $this->order;
    }
	
	/**
     * Sets the returnUrl
     *
     * @param string $returnUrl
     */
    public function setReturnUrl($returnUrl)
    {
        $this->returnUrl = $returnUrl;
    }
	
	/**
     * Returns the returnUrl
     *
     * @return string	returnUrl
     */
    public function getReturnUrl()
    {
        return $this->returnUrl;
    }
	
	/**
     * Sets the cancelUrl
     *
     * @param string $cancelUrl
     */
    public function setCancelUrl($cancelUrl)
    {
        $this->cancelUrl = $cancelUrl;
    }
	
	/**
     * Returns the cancelUrl
     *
     * @return string	cancelUrl
     */
    public function getCancelUrl()
    {
        return $this->cancelUrl;
    }
	
	/**
     * Sets the init flag
     *
     * @param boolean $init
     */
    public function setInit($init)
    {
        $this->init = $init;
    }
	
	/**
     * Returns the init flag
     *
     * @return boolean	init
     */
    public function getInit()
    {
        return $this->init;
    }

	/**
     * Sets the token
     *
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }
	
	/**
     * Returns the token
     *
     * @return string	token
     */
    public function getToken()
    {
        return $this->token;
    }

	/**
     * Sets the approveLink
     *
     * @param string $approveLink
     */
    public function setApproveLink($approveLink)
    {
        $this->approveLink = $approveLink;
    }
	
	/**
     * Returns the approveLink
     *
     * @return string	approveLink
     */
    public function getApproveLink()
    {
        return $this->approveLink;
    }

	/**
     * Sets the error
     *
     * @param string $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }
	
	/**
     * Returns the error
     *
     * @return string	error
     */
    public function getError()
    {
        return $this->error;
    }
	
	/**
     * Sets the errorDetails
     *
     * @param array $errorDetails
     */
    public function setErrorDetails($errorDetails)
    {
        $this->errorDetails = $errorDetails;
    }
	
	/**
     * Returns the errorDetails
     *
     * @return array	errorDetails
     */
    public function getErrorDetails()
    {
        return $this->errorDetails;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * initialize the payment process
     *
     * @param	-
	 * @return	\Maagit\Maagitproduct\Domain\Model\Checkout		the checkout object
     */
	abstract public function init();
	
	/**
     * call the payment process
     *
	 * @return	string								the link
     */
	public function call()
	{
		return $this->getApproveLink();
	}
	
	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
		* 	Create return and cancel url's
		*
		* 	@return void
	*/
	protected function createUri()
	{
		
		
		$uriBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Mvc\\Web\\Routing\\UriBuilder');
		$uriBuilder->setRequest($this->divHelper->getExtbaseRequest());
		// @extensionScannerIgnoreLine
		$page = $uriBuilder->setTargetPageUid($GLOBALS['TSFE']->id)->setArguments(array('tx_maagitproduct_checkout' => array('action' => $this->answerAction)))->build();
		$page = (strpos($page, '&cHash=')===FALSE) ? $page : substr($page, 0, strpos($page, '&cHash='));
		$host = $_SERVER['HTTP_HOST'];
		$protocol = (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS'])) ? 'https://' : 'http://';
		$uri = $protocol.$host.$page;
		$this->setReturnUrl($uri.'&tx_maagitproduct_checkout[approve]=true');
		$this->setCancelUrl($uri.'&tx_maagitproduct_checkout[cancel]=true');
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}