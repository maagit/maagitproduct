<?php
namespace Maagit\Maagitproduct\Payment\Saferpay;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Payment
	class:				Saferpay

	description:		External payment service "saferpay".

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Saferpay extends \Maagit\Maagitproduct\Payment\Payment
{
	/* ======================================================================================= */
	/* C O N S T A N T S                                                                       */
	/* ======================================================================================= */
	const ENVIRONMENT_TEST = 'test';
	const ENVIRONMENT_PRODUCTION = 'production';
	const ENDPOINTS = array(
		'test' => 'https://test.saferpay.com/api/',
		'production' => 'https://www.saferpay.com/api'
	);
	const VERSION = '1.17';


	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var string
     */
	protected $answerAction = 'saferpayanswer';

	/**
     * @var string
     */
	protected $customerId;
	
	/**
     * @var string
     */
	protected $terminalId;

	/**
     * @var string
     */
	protected $version;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		parent::initializeObject($checkout);
		$this->version = self::VERSION;
		$this->paymentHelper = $this->makeInstance('Maagit\\Maagitproduct\\Payment\\Saferpay\\SaferpayHelper', $this);
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Returns the environment
     *
     * @return string	environment
     */
    public function getEnvironment()
    {
        return ($this->settings['checkout']['payment']['method']['saferpay']['useSandbox']) ? self::ENVIRONMENT_TEST : self::ENVIRONMENT_PRODUCTION;
    }

	/**
     * Returns the endpoint
     *
     * @return string	endpoint
     */
    public function getEndpoint()
    {
        return self::ENDPOINTS[$this->getEnvironment()];
    }

	/**
     * Sets the customerId
     *
     * @param string $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }
	
	/**
     * Returns the customerId
     *
     * @return string	customerId
     */
    public function getCustomerId()
    {
        return ($this->getEnvironment()==self::ENVIRONMENT_TEST) ? $this->settings['checkout']['payment']['method']['saferpay']['sandboxCustomerId'] : $this->settings['checkout']['payment']['method']['saferpay']['customerId'];
    }
	
	/**
     * Returns the username
     *
     * @return string	username
     */
    public function getUsername()
    {
        return ($this->getEnvironment()==self::ENVIRONMENT_TEST) ? $this->settings['checkout']['payment']['method']['saferpay']['sandboxUsername'] : $this->settings['checkout']['payment']['method']['saferpay']['username'];
    }
	
	/**
     * Returns the password
     *
     * @return string	password
     */
    public function getPassword()
    {
         return ($this->getEnvironment()==self::ENVIRONMENT_TEST) ? $this->settings['checkout']['payment']['method']['saferpay']['sandboxPassword'] : $this->settings['checkout']['payment']['method']['saferpay']['password'];
    }
	
	/**
     * Sets the terminalId
     *
     * @param string $terminalId
     */
    public function setTerminalId($terminalId)
    {
        $this->terminalId = $terminalId;
    }
	
	/**
     * Returns the terminalId
     *
     * @return string	terminalId
     */
    public function getTerminalId()
    {
        return ($this->getEnvironment()==self::ENVIRONMENT_TEST) ? $this->settings['checkout']['payment']['method']['saferpay']['sandboxTerminalId'] : $this->settings['checkout']['payment']['method']['saferpay']['terminalId'];
    }

	/**
     * Sets the version
     *
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }
	
	/**
     * Returns the version
     *
     * @return string	version
     */
    public function getVersion()
    {
        return $this->version;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * initialize the payment process
     *
     * @param	-
	 * @return	\Maagit\Maagitproduct\Domain\Model\Checkout		the checkout object
     */
	public function init()
	{
		// get credit cards array
		$cards = array();
		if ($this->checkout->getPayment()->getPaymentMethod() == 'saferpayCredit')
		{
			$cards = explode(',', $this->settings['checkout']['payment']['method']['saferpay']['cards']);
			if ($cards[0]=='') {$cards = array();}
		}

		// get wallets array
		$wallets = array();
		if ($this->checkout->getPayment()->getPaymentMethod() == 'saferpayWallet')
		{
			$wallets = explode(',', $this->settings['checkout']['payment']['method']['saferpay']['wallets']);
			if ($wallets[0]=='') {$wallets = array();}
		}
		
		// get requestId (and take it as token)
		$this->setToken(bin2hex(openssl_random_pseudo_bytes(16)));
		$this->setReturnUrl($this->getReturnUrl().'&tx_maagitproduct_checkout[token]='.$this->getToken());
		$this->setCancelUrl($this->getCancelUrl().'&tx_maagitproduct_checkout[token]='.$this->getToken());

		// create payload array
		$payload = array(
			'RequestHeader' => array(
				'SpecVersion' => $this->getVersion(),
				'CustomerId' => $this->getCustomerId(),
				'RequestId' => $this->getToken(),
				'RetryIndicator' => 0,
				'ClientInfo' => array(
					'ShopInfo' => $this->settings['checkout']['payment']['method']['saferpay']['description']
				)
			),
			'TerminalId' => $this->getTerminalId(),
			'PaymentMethods' => $cards,
			'Wallets' => $wallets,
			'Payment' => array(
				'Amount' => array(
					'Value' => (string)($this->checkout->getTotal() * 100),
					'CurrencyCode' => $this->settings['checkout']['currency']['sign']
				),
			        'Description' => $this->settings['checkout']['payment']['method']['saferpay']['summary']
			),
		    'Payer' => array(
				'IpAddress' => $_SERVER['REMOTE_ADDR'],
				'LanguageCode' => "de"
			),
			'ReturnUrls' => array(
		        'Success' => $this->getReturnUrl(),
				'Fail' => $this->getCancelUrl()
			),
			'DeliveryAddressForm' => array(
				'Display' => false,
			)
		);
		
		// make json and save it
		$payloadData = json_encode($payload);
		$this->setOrder($payloadData);

		// create transaction and set approveLnk|errorMessage
		// @extensionScannerIgnoreLine
		$result = $this->paymentHelper->init($payloadData);
		if (!$result['ack'])
		{
			$this->setError($result['message']);
			$this->setErrorDetails($result['data']['details']);	
		}
		else
		{
			$this->setApproveLink($result['data']['approveUrl']);
		}
		$this->setInit($result['ack']);
		return $result['ack'];
	}
	
	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
