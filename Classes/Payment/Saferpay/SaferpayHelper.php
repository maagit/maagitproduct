<?php
namespace Maagit\Maagitproduct\Payment\Saferpay;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Payment
	class:				Paypal

	description:		Helper class for external payment service "saferpay".

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class SaferpayHelper extends \Maagit\Maagitproduct\Payment\PaymentHelper
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
		* 	Initalize payment process by given payment provider
		*
        *	@param array	$data	order data
		* 	@return array			formatted JSON response
	*/
	public function init($data)
	{
		$this->httpService->resetHelper();
		$this->httpService->addHeader('Content-Type: application/json; charset=utf-8');
		$this->httpService->addHeader('Accept: application/json; charset=utf-8');
		$this->httpService->setAuthentication($this->payment->getUsername().':'.$this->payment->getPassword());
		$this->httpService->setUrl($this->createApiUrl('Payment/v1/PaymentPage/Initialize'));
		$this->httpService->setBody($data);
		$returnData = $this->httpService->sendRequest();

		try
		{
			$approveUrl = $this->getApproveUrl($returnData);
			return array(
				'ack' => true,
				'data' => array(
					'id' => $returnData['Token'],
					'expiration' => $returnData['Expiration'],
					'approveUrl' => $approveUrl,
				)
			);
		}
		catch (\Exception $ex)
		{
			// send debug mail, if $debug is set
			if ($this->settings['checkout']['payment']['method']['saferpay']['debug'] != '')
			{
				$this->sendDebugInformation(
					$this->settings['checkout']['payment']['method']['saferpay']['debug'],
					(array)json_decode($data), 
					$returnData
				);
			}
			// return result
			return array(
				'ack' => false,
				'message' => $returnData['ErrorMessage'],
				'data' => array(
					'id' => time(),
					'status' => $returnData['Behavior'],
					'details' => array(
						'issue' => $returnData['ErrorName'],
						'description' => $returnData['ErrorDetail'][0],
					)
				)
			);
		}
	}

	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
		* 	Get approve url from given paypal answer array
		*
		* 	@return string	paypal approve url
	*/
	protected function getApproveUrl(array $data)
	{
		if (!array_key_exists('RedirectUrl', $data))
		{
			throw new \Exception('There is no "RedirectUrl" element in the saferpay "init" answer.', time());
		}
		$approveLink = $data['RedirectUrl'];
		if ($approveLink == null || $approveLink == '')
		{
			throw new \Exception('There is no approve link found in the saferpay "init" answer.', time());
		}
		return $approveLink;
	}
	

	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	/**
		* 	Create the Saferpay endpoint url.
		*
		*	Use the configurations and combine resources to create the endpoint.
		*
        *	@param string	$resource	Url to be called using curl
		* 	@return string				API url depending on environment.
	*/
	private function createApiUrl($resource)
	{
		return $this->payment->getEndpoint().'/'.$resource;	
	}

}
