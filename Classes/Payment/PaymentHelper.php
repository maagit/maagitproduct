<?php
namespace Maagit\Maagitproduct\Payment;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Payment
	class:				PaymentHelper

	description:		Abstract helper class for implementing external payment services.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


abstract class PaymentHelper extends \Maagit\Maagitproduct\Payment\BasePayment
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var \Maagit\Maagitproduct\Service\Network\HttpService
     */
	protected $httpService;

	/**
     * @var \Maagit\Maagitproduct\Payment\Payment
     */
	protected $payment;

	/**
     * @var string
     */
	protected $token;
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(\Maagit\Maagitproduct\Payment\Payment $payment)
	{
		$this->httpService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Network\\HttpService');
		$this->payment = $payment;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
		* 	Initalize payment process by given payment provider
		*
        *	@param array	$data	order data
		* 	@return array			formatted JSON response
	*/
	abstract public function init($data);

	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
		* 	Get approve url from given answer array
		*
		* 	@return string	approve url
	*/
	abstract protected function getApproveUrl(array $data);
	
	/**
		* 	Send debug information to given email address
		*
		* 	@return void
	*/
	protected function sendDebugInformation(string $email, array $data, array $response)
	{
		// get objects
		$renderService= $this->makeInstance('Maagit\\Maagitproduct\\Service\\Content\\RenderService');
		$emailService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Network\\EmailService');

		// render email body text
		$bodyText = $renderService->renderTemplate(
			'Email/Debug',
			array('data' => $data, 'response' => $response, 'settings' => $this->settings)
		);
		
		// send mail
		$emailService->sendMail(
			array('debug@maagit.ch'),
			array($email),
			'Payment Transaction Failure',
			$bodyText,
			$bodyText
		);
	}
	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}