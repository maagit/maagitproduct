<?php
namespace Maagit\Maagitproduct\Userfuncs;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Userfuncs
	class:				Tca

	description:		Backend label formatting

	created:			2023-12-17
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-12-17	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Tca
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * TCA label for "order"
     *
	 * @param array 										$parameters		The tca parameters "table" and "row"
     * @return string														The tca title
     */
	public function order(&$parameters)
	{
		$record = $this->getRecord($parameters);
		$date = new \DateTime();
		$date->setTimestamp($record['crdate']);
		$content = !empty($record['shippingdone']) ? $this->getLabel('LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.done') : $this->getLabel('LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.open');
		$content = $content.' - '.date_format($date, 'd.m.Y H:i:s').' ('.$record['orderid'].')';
		!empty($record['firma']) ? $content = $content.' '.$record['firma'] : '';
		!empty($record['lastname']) ? $content = $content.', '.$record['lastname'] : '';
		!empty($record['prename']) ? $content = $content.' '.$record['prename'] : '';
		!empty($record['paymentdone']) ? $content = $content.', '.$this->getLabel('LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.payedat').' '.date_format($date->setTimestamp($record['paymentdone']), 'd.m.Y H:i:s') : '';
		!empty($record['shippingdone']) ? $content = $content.', '.$this->getLabel('LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.shippedat').' '.date_format($date->setTimestamp($record['shippingdone']), 'd.m.Y H:i:s') : '';
		$parameters['title'] = $content;
	}

	/**
     * TCA label for "experience rating"
     *
	 * @param array 										$parameters		The tca parameters "table" and "row"
     * @return string														The tca title
     */
	public function experienceRating(&$parameters)
	{
		$record = $this->getRecord($parameters);
		$date = new \DateTime();
		$date->setTimestamp($record['crdate']);
		$content = date_format($date, 'd.m.Y H:i:s');
		$content = $content.', '.$record['name'];
		$content = $content.', '.$this->getLabel('LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.rating.stars.'.$record['rating']);
		$parameters['title'] = $content;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Get record from database
     *
	 * @param array 									$parameters		The tca parameters "table" and "row"
     * @return array													The record data as array
     */
	protected function getRecord(array $parameters)
	{
		$record = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord($parameters['table'], $parameters['row']['uid']);
		return $record;
	}

	/**
     * Get language label in given backend language
     *
	 * @param string 									$label		The language label
     * @return string												The content from given language
     */
	protected function getLabel(string $label)
	{
		$languageService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Localization\LanguageServiceFactory::class)->createFromUserPreferences($GLOBALS['BE_USER']);
		return $languageService->sL($label);
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}