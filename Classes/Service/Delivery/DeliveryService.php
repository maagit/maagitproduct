<?php
namespace Maagit\Maagitproduct\Service\Delivery;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				DeliveryService

	description:		Various methods, used by checkout process step "delivery".
						Get available delivery methods.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class DeliveryService extends \Maagit\Maagitproduct\Service\BaseService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Service\Content\TransformService
     */
	protected $transformService;

	/**
	 * @var \Maagit\Maagitproduct\Domain\Model\Checkout
     */
	protected $checkout;
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		$this->checkout = $checkout;
		$this->transformService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Content\\TransformService');
	}
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * get delivery methods, based on settings and items in basket
     *
     * @return array				the available delivery methods
     */
    public function getDeliveryMethods()
	{
		$deliveryMethodsFromSettings = $this->transformService->convertTableToArray($this->settings['checkout']['delivery']['methods'], array('id', 'text', 'products', 'minamount'));
		$deliveryMethods = $this->addTarifToDeliveryMethod($deliveryMethodsFromSettings);
		$deliveryMethods = $this->checkConditions($deliveryMethods);
		return $deliveryMethods;
	}
	
	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Add tarif to delivery methods
     *
	 * @param	array		$deliveryMethods	the delivery methods
	 * @return 	array							the delivery methods with tarif
     */
	protected function addTarifToDeliveryMethod(array $deliveryMethods)
	{
		$array = array();
		foreach ($deliveryMethods as $method)
		{
			$method['tarif'] = $this->checkout->getDelivery()->getShipping($method['id']);
			$array[] = $method;
		}
		return $array;
	}
	
	/**
     * Get the available delivery methods based on defined conditions in the settings
     *
	 * @param	array					$deliveryMethods		the available delivery methods
	 * @return 	array											the validated delivery methods
     */
	protected function checkConditions(array $deliveryMethods)
	{
		// check, if there are delivery methods for given articles
		$limited = array(); $uids = array(); $count = 0;
		foreach ($deliveryMethods as $method)
		{
			if (array_key_exists('products', $method))
			{
				$uids = array_map('trim', explode(',', $method['products']));
				if ($this->onlyThisInBasket($uids))
				{
					$limited[] = $method;
					$count++;
				}
			}
			if (array_key_exists('products', $method) && strtolower($method['products']) == 'all')
			{
				$limited[] = $method;
			}
			
			if (!array_key_exists('products', $method) || strtolower($method['products']) == 'all')
			{
				$all[] = $method;
			}
		}
		// if only given products in basket -> return given delivery methods, otherwise return all (except specials)
		return ($count==0) ? $all : $limited;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
    /**
     * Check, if there are only products of given uids in basket
     *
     * @param 	array		$uids	the product uids to search
	 * @return 	bool				true if only given uid's are in basket, otherwise false
     */
	public function onlyThisInBasket(array $uids)
	{
		if (empty($uids))
		{
			return false;
		}

		foreach ($this->checkout->getBasket()->getBasketItems() as $item)
		{
			if (!$item->getProduct()->getIsDownload())
			{
				if (!in_array($item->getUid(), $uids))
				{
					return false;
				}
			}
		}
		return true;
    }
}