<?php
namespace Maagit\Maagitproduct\Service\Delivery;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				ShippingService

	description:		Abstract class for calucating the shipping costs.
						Demands the method "calculate" for extendes classes

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


abstract class ShippingService extends \Maagit\Maagitproduct\Service\BaseService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Service\Content\TransformService
     */
	protected $transformService;

	/**
	 * @var \Maagit\Maagitproduct\Domain\Model\Basket
     */
	protected $basket;
	
	/**
	 * @var string
     */
	protected $deliveryId;

	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(string $deliveryId = NULL, \Maagit\Maagitproduct\Domain\Model\Basket $basket)
	{
		$this->deliveryId = $deliveryId;
		$this->basket = $basket;
		$this->transformService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Content\\TransformService');
	}
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * calculate shipping
     *
     * @return double	calculated result
     */
    abstract public function calculate();
	
	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
    /**
     * get relevant record from shipping tables
     *
	 * @param	string	$tarifTable		The tarif table
	 * @return	array|FALSE
     */
	protected function getRecordsFromTarifTable(string $tarifTable) {
		$shippingTable = $this->transformService->convertTableToArray($tarifTable);
		if (empty($shippingTable))
		{
			// shipping table for flat tarifs is empty.
			return FALSE;
		}
		if ($this->deliveryId == null)
		{
			// no delivery method is given.
			return FALSE;
		}
		$array = array();
		foreach ($shippingTable as $entry)
		{
			if ($this->deliveryId == $entry[0])
			{
				$array[] = $entry;
			}
		}
		// entries with given deliveryId was found
		if (!empty($array))
		{
			return $array;
		}
		// the given deliveryId was not found in shipping table.
		return FALSE;
	}
	
    /**
     * get count of articles in basket
     *
	 * @return	int
     */
	protected function getBasketCount()
	{
		$count = 0;
		foreach ($this->basket->getBasketItems() as $item)
		{
			$count += $item->getQuantity();
		}
		return $count;
	}

	/**
     * evaluate shipping costs, respecting the min order value
     *
	 * @param	float				$shipping		The shipping to evaluate
	 * @return	float								The evaluated shipping
     */
    protected function respectMinOrderValue(float $shipping)
	{
		// if there are various limits defined (per delivery) ...
		if (!empty($this->deliveryId))
		{
			$deliveryMethodsFromSettings = $this->transformService->convertTableToArray($this->settings['checkout']['delivery']['methods'], array('id', 'text', 'products', 'minamount'));
			$index = array_search($this->deliveryId, array_column($deliveryMethodsFromSettings, 'id'));
			$method = $deliveryMethodsFromSettings[$index];
			$min = (isset($method['minamount']) && is_numeric($method['minamount'])) ? (double)$method['minamount'] : FALSE;
			if ($min && $this->basket->getAmount() >= $min)
			{
				return ($shipping * -1);
			}
		}

		// if there is only a limit defined in basket settings ...
		if (!$this->getIndividualMinOrderValues() && $this->basket->getMinOrderValue() > 0)
		{
			if ($this->basket->getAmount() >= $this->basket->getMinOrderValue())
			{
				return ($shipping * -1);
			}
		}

		// if there is not 'min order value' defined
		return $shipping;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	/**
     * get, if there are delivery 'min order value' settings
     *
	 * @return	boolean								Are there individual settings?
     */
    protected function getIndividualMinOrderValues()
	{
		$deliveryMethodsFromSettings = $this->transformService->convertTableToArray($this->settings['checkout']['delivery']['methods'], array('id', 'text', 'products', 'minamount'));
		foreach ($deliveryMethodsFromSettings as $method)
		{
			if (isset($method['minamount']) && is_numeric($method['minamount']))
			{
				return true;
			}
		}
		return false;
	}
}