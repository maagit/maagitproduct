<?php
namespace Maagit\Maagitproduct\Service\Experience;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				ExperienceService

	description:		Experience handling methods.

	created:			2020-11-01
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-11-01	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed

------------------------------------------------------------------------------------- */


class ExperienceService extends \Maagit\Maagitproduct\Service\BaseService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Model\Experience
     */
	protected $experience;
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(\Maagit\Maagitproduct\Domain\Model\Experience $experience)
	{
		$this->experience = $experience;
	}
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * sendRating
     *
     * @return \Maagit\Maagitproduct\Domain\Model\Experience		the refreshed experience object
     */
    public function sendRating()
	{
		// render email data
		$renderService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Content\\RenderService');
		$emailService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Network\\EmailService');
		$bodyText = $renderService->renderTemplate('Email/Experience', array('experience' => $this->experience, 'settings' => $this->settings));
		$subject = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.experience', 'maagitproduct');

		// send mail
		try
		{
			$sent = $emailService->sendMail(
				array(($this->experience->getEmail() != '') ? $this->experience->getEmail() : $this->settings['checkout']['receiver']['email']),
				array($this->settings['checkout']['receiver']['email']),
				$subject,
				$bodyText,
				$bodyText
			);
		}
		catch (\Exception $ex)
		{
			$details = $ex->getMessage();
			$error = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.error.experience', 'maagitproduct').'<br /><br />';
			$error .= \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.error.details', 'maagitproduct').':<br />';
			$error .= $details;
			$this->experience->setMailError($error);
		}
		
		// return experience object
		return $this->experience;
	}
	
	/**
     * saveRating
     *
     * @return void
     */
    public function saveRating()
	{
		if ($this->settings['checkout']['experience']['save'] != null && $this->settings['checkout']['experience']['save'] != '' && $this->settings['checkout']['experience']['save'] > 0)
		{
			$experienceRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\ExperienceRepository');
			$this->experience->setPid($this->settings['checkout']['experience']['save']);
			$experienceRepository->add($this->experience);
		}
	}

	/**
     * clear cache of pages, which inherits the experience plugin
     *
     * @return void
     */
    public function clearCache()
	{
		$cacheService = $this->makeInstance('Maagit\Maagitproduct\Service\Network\CacheService');
		$pageRepository = $this->makeInstance('Maagit\Maagitproduct\Domain\Repository\PageRepository');
		$pids = $pageRepository->findByTypes(array(), array('maagitproduct_experience'));
		$cacheService->clear($pids);
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}