<?php
namespace Maagit\Maagitproduct\Service\Content;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				CompareService

	description:		Various methods, used for comparing strings.

	created:			2020-08-20
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-08-20	Urs Maag		Initial version
						2021-12-25	Urs Maag		PHP 8, suppress error messages on
													non existing array keys

------------------------------------------------------------------------------------- */


class CompareService extends \Maagit\Maagitproduct\Service\BaseService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
	 * compare given search string with given text and return, if found or not
     *
	 * @param	string 						$search			the search definition
	 * @param	string 						$text			the subject
     * @return	boolean										true: search is found
	 *														false: search is not found
	 */
	public function compareWithWildcards(string $search, string $text)
	{
		// initialization
		$searchIndex = 0;
		$textIndex = 0;

		// settings
		$search = strtolower($search);
		$text = strtolower($text);

		// replace special characters
		$replaceArray = array('À'=>'A', 'à'=>'a', 'È'=>'E', 'è'=>'e', 'É'=>'E', 'é'=>'e', 'Ê'=>'E', 'ê'=>'e', 'Ñ'=>'N', 'ñ'=>'n');
		foreach ($replaceArray as $key => $replace)
		{
			$search = str_replace($key, $replace, $search);
			$text = str_replace($key, $replace, $text);
		}
		
		// define search and text lens
		$searchLen = strlen($search);
		$textLen = strlen($text);

		// process comparing
		while ($textIndex < $textLen && @$search[$searchIndex] != '*')
		{
			if ((@$search[$searchIndex] != $text[$textIndex]) && (@$search[$searchIndex] != '?'))
			{
				return false;
			}
			$searchIndex++;
			$textIndex++;
		}

		$mp = 0;
		$cp = 0;

		while ($textIndex < $textLen)
		{
			if (@$search[$searchIndex] == '*')
			{
				if (++$searchIndex == $searchLen)
				{
					return 1;
				}
				$mp = $searchIndex;
				$cp = $textIndex + 1;
			}
			else
			{
				if ((@$search[$searchIndex] == $text[$textIndex]) || (@$search[$searchIndex] == '?'))
				{
					$searchIndex++;
					$textIndex++;
				}
				else
				{
					$searchIndex = $mp;
					$textIndex = $cp++;
				}	
			}
		}

		while (@$search[$searchIndex] == '*')
		{
			$searchIndex++;
		}

		// return result
		return $searchIndex == $searchLen ? true : false;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}