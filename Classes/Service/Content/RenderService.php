<?php
namespace Maagit\Maagitproduct\Service\Content;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				RenderService

	description:		Various methods, used for rendering with fluid.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed

------------------------------------------------------------------------------------- */


class RenderService extends \Maagit\Maagitproduct\Service\BaseService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * render a template
     *
	 * @param string 										$template		The fluid template
	 * @param array 										$assigns		Key/Value pairs of data's to assign
	 * @param string 										$plugin 		The plugin of given contentObject
     * @return string
     */
	 public function renderTemplate(string $template, array $assigns=array(), string $plugin='maagitproduct_checkout')
	 {
		 $view = $this->makeInstance('TYPO3\\CMS\\Fluid\\View\\StandaloneView', $this->divHelper->getRenderingContext($plugin));
		 $template .= (end(explode('.', $template))) ? '' : '.html';
		 $view->setTemplate($template);
		 $view->setTemplateRootPaths($this->settings['view']['templateRootPaths']);
		 $view->setPartialRootPaths($this->settings['view']['partialRootPaths']);
		 foreach ($assigns as $key => $data)
		 {
			 $view->assign($key, $data);
		 }
		 return $view->render();
	 }
	 
 	/**
      * render a custom cobject from tt_content
      *
 	  * @param int	 										$uid			The uid of tt_content record
      * @return string
      */
	 public function renderCObject(int $uid)
	 {
		$conf = array(
	 	    'tables' => 'tt_content',
	 		'source' => $uid,
	 	    'dontCheckPid' => true
	 	);
	 	// @extensionScannerIgnoreLine
		$text = $this->getConfigurationManager()->getContentObject()->cObjGetSingle('RECORDS', $conf);
		return $text;
	 }
	 
 	/**
      * render and replace placeholders
      *
 	  * @param string 											$text			The text to render
 	  * @param \Maagit\Maagitproduct\Domain\Model\BasketItem	$basketItem		The basketItem object
      * @return string
      */
	 public function renderPlaceholder(string $text, \Maagit\Maagitproduct\Domain\Model\BaseModel $object)
	 {
		 preg_match_all('/###(.*?)###/', $text, $placeholders);
		 foreach ($placeholders[0] as $placeholder)
		 {
			 $renderResult = FALSE;
			 $member = str_replace('###', '', $placeholder);
			 $key = (strpos($member, '.')!== FALSE) ? substr($member, strpos($member, '.')+1) : '';
			 $member = ($key != '') ? substr($member, 0, strpos($member, '.')) : $member;
			 $functionSegment = str_replace('_', '', ucwords(strtolower($member), '_'));
			 $function = 'render'.$functionSegment;
			 $property = 'get'.$functionSegment;

			 // first prio: call method in this class, if exists
			 if (method_exists($this, $function))
			 {
			 	$renderResult = $this->$function($object, $settings);
			 }
			 // second prio: call object property
			 else if (method_exists($object, $property))
			 {
				 $renderResult = $object->$property();
			 }
			 else
			 {
				 $renderResult = FALSE;
			 }
			 
			 // get array value, if result is a array
			 if (is_array($renderResult))
			 {
				 $renderResult = $this->getArrayValue($renderResult, $key);
			 }
			 
			 // format as currency, if it's a currency field
			 $currencyFields = array('amount', 'total');
			 if (in_array(strtolower($member), $currencyFields))
			 {
				 $renderResult = $this->renderTemplate('Div/Currency', array('amount' => $renderResult, 'settings' => $this->settings));
		 	 }

			 // replace placeholder with given result
			 if ($renderResult !== FALSE)
			 {
				 $text = str_replace($placeholder, $renderResult, $text);
			 }
		 }
		 return $text;
	 }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
 	/**
      * render the basket
      *
      * @param \Maagit\Maagitproduct\Domain\Model\Checkout				$checkout		The checkout object
	  * @return string
      */
	protected function renderBasket(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		$text = $this->renderTemplate('Email/CustomBasket', array('checkout' => $checkout, 'settings' => $this->settings));
		return $text;
	}

	/**
      * render the address block
      *
      * @param \Maagit\Maagitproduct\Domain\Model\Checkout				$checkout		The checkout object
	  * @return string
      */
	protected function renderAdditional(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		$text = $this->renderTemplate('Email/CustomAdditional', array('checkout' => $checkout, 'settings' => $this->settings));
		return $text;
	}

	/**
      * render the download link
      *
      * @param \Maagit\Maagitproduct\Domain\Model\BasketItem			$basketItem		The basketItem object
	  * @return string
      */
	protected function renderDownloadLink(\Maagit\Maagitproduct\Domain\Model\BasketItem $basketItem)
	{
		$conf = array(
		    'parameter' => ($basketItem->getProduct()->getSecureLink()) ? $basketItem->getProduct()->getSecureLink() : $basketItem->getProduct()->getData()['header_link'],
		    'useCashHash' => false,
		    'returnLast' => 'url',
			'forceAbsoluteUrl' => true
		);
		// @extensionScannerIgnoreLine
		$linkUrl = $this->getConfigurationManager()->getContentObject()->typolink_URL($conf); 
	    // @extensionScannerIgnoreLine
		$link = $this->getConfigurationManager()->getContentObject()->typolink(
			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.download.link', 'maagitproduct'),
			array('parameter' => $linkUrl,
				'target' => '_blank'
			)
		);
		return $link;
	}
	
 	/**
      * render the download text
      *
      * @param \Maagit\Maagitproduct\Domain\Model\BasketItem			$basketItem		The basketItem object
	  * @return string
      */
	protected function renderDownloadText(\Maagit\Maagitproduct\Domain\Model\BasketItem $basketItem)
	{
		if ($this->settings['checkout']['download']['secure'])
		{
			return $this->settings['checkout']['download']['secureTimeoutText'];	
		}
		else
		{
			return '';
		}
		
	}
	
	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
 	/**
      * make a configuration manager object
      *
      * @return \TYPO3\CMS\Extbase\Configuration\ConfigurationManager	$uid	The configuration manager object
      */
	private function getConfigurationManager()
	{
		$configurationManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');
		return $configurationManager;
	}
	
 	/**
      * get value from a array
      *
      * @param array					$array		The array to search in
	  * @param string					$key		The key(s), delimited by given delimiter
	  * @param string					$delimiter	The delimiter of keys, default is '.'
	  * @return string
      */
	private function getArrayValue(array $array, string $key, string $delimiter='.')
	{
	 	$temp = $array;
		$segments = explode($delimiter, $key);
		foreach ($segments as $segment)
		{
			$value = $temp[$segment];
			$value = ($value == NULL) ? $temp[strtoupper($segment)] : $value;
			$value = ($value == NULL) ? $temp[strtolower($segment)] : $value;
			$value = ($value == NULL) ? $temp[ucwords(strtolower($member))] : $value;
			$temp = (is_array($value)) ? $value : array();
		}
		return $value;
	}
}
