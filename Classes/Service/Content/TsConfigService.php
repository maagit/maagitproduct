<?php
namespace Maagit\Maagitproduct\Service\Content;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				TsConfigService

	description:		Access to tsConfig settings.
						Get values from tsConfig (e.g. list of countries, defined
						in tsconfig).

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-12-25	Urs Maag		PHP 8, non existing array keys

------------------------------------------------------------------------------------- */


class TsConfigService extends \Maagit\Maagitproduct\Service\BaseService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * get a value of current tsconfig on given page
     *
     * @param	array			$keys			the tsconfig keys (in right order)
	 * @param	array			$prefix			the whole tsconf path prefix
	 * @param	int				$pid			the page uid, with tsconfig settings
	 * @return	string							the tsconfig value
     */
	public function getTsConfigValue(array $keys, array $prefix=array(), int $pid=null)
	{
		$value = $this->_getTsConfigValue($keys, $prefix, $pid);
		if ($value == null)
		{
			// @extensionScannerIgnoreLine
			$value = $this->_getTsConfigValue($keys, $prefix, $GLOBALS['TSFE']->id);
		}
		return $value;
	}
	
	/**
     * get key/value pairs of current tsconfig on given page
     *
     * @param	array			$keys			the tsconfig keys (in right order)
	 * @param	array			$prefix			the whole tsconf path prefix
	 * @param	int				$pid			the page uid, with tsconfig settings
	 * @return	string							the tsconfig value
     */
	public function getTsConfigKeyValuePairs(array $keys, array $prefix=array(), int $pid=null)
	{
		// get tsconfig array
		$entries = $this->_getTsConfigValue($keys, $prefix, $pid);
		if (empty($entries))
		{
			// @extensionScannerIgnoreLine
			$entries = $this->_getTsConfigValue($keys, $prefix, $GLOBALS['TSFE']->id);
		}
		
		// make key/value pair array and return it
		$keyValuePairs = array();
		if (!empty($entries))
		{
			foreach ($entries as $key => $value)
			{
				$keyValuePairs[] = array('key' => $key, 'value' => $value);
			}
		}
		return $keyValuePairs;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	/**
     * get a value of current tsconfig on given page
     *
     * @param	array			$keys			the tsconfig keys (in right order)
	 * @param	array			$prefix			the whole tsconf path prefix
	 * @param	int				$pid			the page uid, with tsconfig settings
	 * @return	string							the tsconfig value
     */
	private function _getTsConfigValue(array $keys, array $prefix=array(), int $pid=null)
	{
		// initialization
		// @extensionScannerIgnoreLine
		$pid = ($pid==null) ? $GLOBALS['TSFE']->id : $pid;
    	$prefix = (empty($prefix)) ? array('TCEFORM.', 'tt_content.') : $prefix;
		$keyArray = array_merge($prefix, $keys);

		// get root tsconfig settings
		$tsConfig = \TYPO3\CMS\Backend\Utility\BackendUtility::getPagesTSconfig($pid);
		$entry = $tsConfig[$keyArray[0]];
		$keyArray = array_slice($keyArray, 1);

		// loop keys in given order, to get last key
		foreach ($keyArray as $key)
		{
			if (!isset($entry[$key])) {return null;}
			$entry = $entry[$key];
			if ($entry == null) {return null;}
		}

		// return selected entry
		return $entry;
	}
}
