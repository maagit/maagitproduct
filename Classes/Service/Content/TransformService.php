<?php
namespace Maagit\Maagitproduct\Service\Content;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				TransformService

	description:		Various conversion methods.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class TransformService extends \Maagit\Maagitproduct\Service\BaseService implements \TYPO3\CMS\Core\SingletonInterface
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
     * converts given string to array
     *
     * @param string		$string     The string to convert
	 * @param string		$delimiter  The Delimiter
	 * @return array
     */
    public function convertStringToArray(string $string = null, string $delimiter = '|')
    {
		$array = array();
		$entries = explode($delimiter, $string);
		foreach ($entries as $entry)
		{
			if ($entry != '')
			{
				$array = array_merge($array, $this->convert(trim($entry)));
			}
		}
		return $array;
    }
	
    /**
     * converts given table-definition to array
     *
     * @param string		$table	    	The table definition as string
	 * @param array 		$keys	    	The keys of columns
	 * @param string		$delimiterRow  	The Delimiter for a row
	 * @param string		$delimiterCol  	The Delimiter for a column
	 * @return array
     */
	public function convertTableToArray(string $table = null, array $keys = NULL, string $delimiterRow = "\n", string $delimiterCol = '|')
	{
		if ($table == null) {
			return array();
		}
		$array = array();
		$rows = explode($delimiterRow, $table);
		foreach ($rows as $row)
		{
			if ($row != '')
			{
				$cols = explode($delimiterCol, $row);
				$temp = array();
				for ($i=0; $i<count($cols); $i++)
				{
					if (isset($keys[$i]))
					{
						$temp[$keys[$i]] = trim($cols[$i]);
					}
					else
					{
						$temp[$i] = trim($cols[$i]);
					}
				}
				$array[] = $temp;
			}
		}
		return $array;
	}
		
    /**
     * round given amount of +/- 5
     *
     * @param double		$amount		the amount to round
	 * @param boolean		$round5		do round of 5 centimes?
	 * @return double
     */
	public static function roundCurrency(float $amount, bool $round5)
	{
		if ($round5)
		{
			return round(($amount + 0.000001) * 20) / 20;
		}
		else
		{
			return round($amount, 2);
		}
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
    /**
     * analyse given string and call given prozess function
     *
     * @param string		$string     The string to analyse
	 * @return array
     */
	protected function convert(string $string)
	{
		// there is a "-" in it, convert to range (e.g. [1-9] or [10-50])
		if (preg_match('/\d{1,3}\s*-\s*\d{1,3}/', $string) == 1)
		{
			return $this->convertRange($string);
		};
		
		// there is a "," in it, convert to "key/value pair" (e.g [5,5er Pack])
		if (preg_match('/\d{1,3}\s*,\s*.*/', $string) == 1)
		{
			return $this->convertKeyValue($string);
		};
		
		// there is a separate number (e.g [1])
		if (preg_match('/\s*\d{1,3}\s*/', $string) == 1)
		{
			return $this->convertSeparateNumber($string);
		};
		
		// no conversion reached, return empty array
		return array();
	}
	
    /**
     * convert a given range (as string) to array
     *
     * @param string		$range     The range as string
	 * @return array
     */
	protected function convertRange($range)
	{
		$array = array();
		if (preg_match('/(\d{1,3})\s*-\s*(\d{1,3})/', $range, $matches) == 1)
		{
			for ($i=$matches[1]; $i<=$matches[2]; $i++)
			{
				$array[] = array('value' => (int)$i, 'text' => (string)$i);
			}
		}
		return $array;
	}
	
    /**
     * convert a given key/value pair (as string) to array
     *
     * @param string		$pair     The key/value pair as string
	 * @return array
     */
	protected function convertKeyValue($pair)
	{
		$array = array();
		if (preg_match('/(\d{1,3})\s*,\s*(.*)/', $pair, $matches) == 1)
		{
			$array[] = array('value' => (int)trim($matches[1]), 'text' => trim((string)$matches[2]));
		}
		return $array;
	}

    /**
     * convert a given number (as string) to array
     *
     * @param string		$number     The number as string
	 * @return array
     */
	protected function convertSeparateNumber($number)
	{
		$array = array();
		if (preg_match('/\s*(\d{1,3})\s*/', $number, $matches) == 1)
		{
			$array[] = array('value' => (int)trim($matches[1]), 'text' => trim((string)$matches[1]));
		}
		return $array;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}