<?php
namespace Maagit\Maagitproduct\Service\Checkout;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				CheckoutService

	description:		Initialization and refresh methods using by the checkout process.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed
						2022-04-18	Urs Maag		Fill address fields, when logged in
						2023-12-18	Urs Maag		Extend conditions handling (AGB's)

------------------------------------------------------------------------------------- */


class CheckoutService extends \Maagit\Maagitproduct\Service\BaseService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Model\Checkout
     */
	protected $checkout;
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		$this->checkout = $checkout;
	}
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * refresh various properties, based on actual settings and items in basket
     *
     * @return \Maagit\Maagitproduct\Domain\Model\Checkout		the refreshed checkout object
     */
    public function refresh()
	{
		$this->createObjects();
		$this->refreshBasket();
		$this->refreshAddress();
		$this->refreshDelivery();
		$this->refreshPayment();
		$this->refreshCoupon();
		$this->refreshSecureLinks();
		$this->refreshAgbUrl();
		$this->refreshTsVariables();
		return $this->checkout;
	}

	/**
     * create objects, if necessary
     *
     * @return void
     */
	public function createObjects()
	{
		// login
		if ($this->settings['checkout']['login']['step'] && $this->checkout->getLogin() == null)
		{
			$loginRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\LoginRepository');
			$this->checkout->setLogin($loginRepository->create());	
		}
		if ($this->settings['checkout']['login']['step'])
		{
			$this->checkout->getLogin()->setCheckout($this->checkout);	
		}

		// address
		if ($this->checkout->getAddress() == null)
		{
			$addressRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\AddressRepository');
			$this->checkout->setAddress($addressRepository->create());
		}
		$this->checkout->getAddress()->setCheckout($this->checkout);

		// billing address
		if ($this->settings['checkout']['address']['bill'] && $this->checkout->getAddress()->getBillingAddress() == null)
		{
			$billingAddressRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\BillingAddressRepository');
			$this->checkout->getAddress()->setBillingAddress($billingAddressRepository->create());
		}

		// delivery
		if ($this->checkout->getDelivery() == null)
		{
			$deliveryRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\DeliveryRepository');
			$this->checkout->setDelivery($deliveryRepository->create());	
		}
		$this->checkout->getDelivery()->setCheckout($this->checkout);
		
		// payment
		if ($this->checkout->getPayment() == null)
		{
			$paymentRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\PaymentRepository');
			$this->checkout->setPayment($paymentRepository->create());
		}
		$this->checkout->getPayment()->setCheckout($this->checkout);

		// coupon
		if ($this->settings['checkout']['coupon']['step'] && $this->checkout->getCoupon() == null)
		{
			$couponRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\CouponRepository');
			$this->checkout->setCoupon($couponRepository->create());	
		}
		if ($this->settings['checkout']['coupon']['step'])
		{
			$this->checkout->getCoupon()->setCheckout($this->checkout);	
		}
		
		// terms
		if ($this->settings['checkout']['sending']['condition'] == 2 && $this->checkout->getTerms() == null)
		{
			$termsRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\TermsRepository');
			$this->checkout->setTerms($termsRepository->create());	
		}
		if ($this->settings['checkout']['sending']['condition'] == 2)
		{
			$this->checkout->getTerms()->setCheckout($this->checkout);	
		}

		// summary
		if ($this->checkout->getSummary() == null)
		{
			$summaryRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\SummaryRepository');
			$this->checkout->setSummary($summaryRepository->create());	
		}
		$this->checkout->getSummary()->setCheckout($this->checkout);
	}
	
	/**
     * refresh basket (perhaps it has changed in the meantime)
     *
     * @return void
     */
    public function refreshBasket()
	{
		$basketRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\BasketRepository');
		$this->checkout->setBasket($basketRepository->findAll());
	}
	
	/**
     * set address fields, when logged in
     *
     * @return void
     */
    public function refreshAddress()
	{
		$context = $this->makeInstance('TYPO3\CMS\Core\Context\Context');
		if (!$context->getPropertyFromAspect('frontend.user', 'isLoggedIn')) {return;}
		$user = (isset($_POST['user'])) ? $_POST['user'] : '';
		if (in_array('lastname', $this->checkout->getAddress()->getAddressFields()) && !empty($this->checkout->getAddress()->getLastname())) {return;}
		if (!in_array('lastname', $this->checkout->getAddress()->getAddressFields()) && empty($user)) {return;}
		$userRepository = $this->makeInstance('Maagit\Maagitproduct\Domain\Repository\UserRepository');
		$user = $userRepository->findByUid($context->getPropertyFromAspect('frontend.user', 'id'));
		$this->checkout->getAddress()->setSalutation($user->getTitle());
		$this->checkout->getAddress()->setFirma($user->getCompany());
		$this->checkout->getAddress()->setPrename($user->getFirstName());
		$this->checkout->getAddress()->setLastname($user->getLastName());
		$this->checkout->getAddress()->setStreet($this->getAddressSegments($user->getAddress())['street']);
		$this->checkout->getAddress()->setHouseNumber($this->getAddressSegments($user->getAddress())['number']);
		$this->checkout->getAddress()->setHouseAdd($this->getAddressSegments($user->getAddress())['add']);
		$this->checkout->getAddress()->setAddress($user->getAddress());
		$this->checkout->getAddress()->setPo($user->getZip());
		$this->checkout->getAddress()->setCity($user->getCity());
		$this->checkout->getAddress()->setPoCity($user->getZip().' '.$user->getCity());
		$this->checkout->getAddress()->setCountry($user->getCountry());
		$this->checkout->getAddress()->setTelephone($user->getTelephone());
		$this->checkout->getAddress()->setEmail($user->getEmail());
	}

	/**
     * get delivery methods, based on settings and items in basket
     *
     * @return void
     */
    public function refreshDelivery()
	{
		$deliveryService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Delivery\\DeliveryService', $this->checkout);
		$this->checkout->getDelivery()->setDeliveryMethods($deliveryService->getDeliveryMethods());
	}
	
	/**
     * get payment methods, based on settings and items in basket
     *
     * @return void
     */
    public function refreshPayment()
	{
		$paymentService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Payment\\PaymentService', $this->checkout);
		$this->checkout->getPayment()->setPaymentMethods($paymentService->getPaymentMethods());
	}

	/**
     * get coupon discount
     *
     * @return void
     */
    public function refreshCoupon()
	{
		if (!$this->settings['checkout']['coupon']['step'])
		{
			return;
		}
		$couponService = $this->makeInstance('Maagit\Maagitproduct\Service\Coupon\CouponService', $this->checkout);
		$this->checkout->getCoupon()->setAmount($couponService->calculateDiscount());
	}
	
	/**
     * create secure links on downloads with actaul typoscript settings
     *
     * @return void
     */
    public function refreshSecureLinks()
	{
		foreach ($this->checkout->getBasket()->getBasketItems() as $item)
		{
			$secureLink = '';
			if ($item->getProduct()->getLink() != '' && $this->settings['checkout']['download']['secure'] == true)
			{
				$secureLink = $this->divHelper->getSecureLink(
					$item->getProduct()->getLink(),
					\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.download.link', 'maagitproduct'),
					$this->settings['checkout']['download']['secureTimeout']
				);
			}
			$item->getProduct()->setSecureLink($secureLink);
		}
	}
	
	/**
     * create the agb url, based on link defined in settings
     *
     * @return void
     */
    public function refreshAgbUrl()
	{
		$agbUrl = '';
		if ($this->settings['checkout']['sending']['agb'] != '')
		{
			// @extensionScannerIgnoreLine
			$url = $this->divHelper->getExtbaseRequest()->getAttribute('currentContentObject')->typolink_URL(array(
				'parameter' => $this->settings['checkout']['sending']['agb'],
				'useCashHash' => false
			));
			$host = $_SERVER['HTTP_HOST'];
			$protocol = (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS'])) ? 'https://' : 'http://';
			$agbUrl = $protocol.$host.$url;
		}
		$this->checkout->setAgbUrl($agbUrl);
	}
	
	/**
     * create variables for using in typoscropt by custom content elements
     *
     * @return void
     */
    public function refreshTsVariables()
	{
		$renderService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Content\\RenderService');
		$this->checkout->getBasket()->setTsBasket($renderService->renderTemplate('Redirect/Basket', array('checkout' => $this->checkout, 'settings' => $this->settings)));
		$this->checkout->getBasket()->setTsDownload($renderService->renderTemplate('Redirect/Download', array('checkout' => $this->checkout, 'settings' => $this->settings)));
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	/**
     * get address segments from given address string
     *
     * @param	$address			string			the address
	 * @return	array								the segments 'street', 'number' and 'add'
     */
    private function getAddressSegments(string $address)
	{
		$houseNumber = '';
		$numberFound = false;
		for ($i=strlen($address); $i>-1; $i--)
		{
			$char = mb_substr($address, $i, 1);
			if (is_numeric($char)) {$numberFound = true;}
			if ($numberFound && $char == ' ')
			{
				break;
			}
			if (is_numeric($char))
			{
				$houseNumber = $char.$houseNumber;	
			}
		}
		$houseAdd = trim(substr($address, strpos($address, $houseNumber) + strlen($houseNumber)));
		$street = trim(substr($address, 0, strpos($address, $houseNumber)));
		return (array('street' => $street, 'number' => $houseNumber, 'add' => $houseAdd));
	}
}