<?php
namespace Maagit\Maagitproduct\Service\Checkout;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				CheckoutService

	description:		Calculation of various amounts for the checkout process.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-12-24	Urs Maag		Set expenses to 0 on empty paymentId
						2022-06-03	Urs Maag		Use setting "round" to calculate amounts

------------------------------------------------------------------------------------- */


class CheckoutCalculationService extends \Maagit\Maagitproduct\Service\BaseService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Model\Checkout
     */
	protected $checkout;
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		$this->checkout = $checkout;
	}
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * calculate shipping
     *
     * @param	string				$deliveryId		the deliveryId to calculate
	 * @return	double								the calculated shipping
     */
    public function calculateShipping(string $deliveryId = NULL)
	{
		if ($deliveryId == null) {$deliveryId = $this->checkout->getDelivery()->getDeliveryMethod();}
		$calculationClass = null;
		switch ($this->settings['checkout']['shipping']['type'])
		{
			case 'article':
		        $calculationClass = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Delivery\\ShippingServiceArticle', $deliveryId, $this->checkout->getBasket());
		        break;
			case 'flat':
	        	$calculationClass = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Delivery\\ShippingServiceFlat', $deliveryId, $this->checkout->getBasket());
	        	break;
			case 'weight':
		       	$calculationClass = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Delivery\\ShippingServiceWeight', $deliveryId, $this->checkout->getBasket());
		       	break;
			case 'count':
		       	$calculationClass = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Delivery\\ShippingServiceCount', $deliveryId, $this->checkout->getBasket());
		       	break;
		}
		$shipping = ($calculationClass != null) ? $calculationClass->calculate($deliveryId) : 0;
		$shipping = \Maagit\Maagitproduct\Service\Content\TransformService::roundCurrency($shipping, $this->settings['checkout']['currency']['round']);
		return $shipping;
	}
	
	/**
     * calculate expenses
     *
     * @param	string				$paymentId		the paymentId to calculate
	 * @return	double								the calculated expenses
     */
    public function calculateExpenses(string $paymentId = NULL)
	{
		if ($paymentId == null) {$paymentId = $this->checkout->getPayment()->getPaymentMethod();}
		if ($paymentId == 'saferpayCredit' || $paymentId == 'saferpayWallet') {$paymentId = 'saferpay';}
		$expenses = 0;
		if (!empty($paymentId))
		{
			$expenses = $this->settings['checkout']['payment']['method'][$paymentId]['expenses'];
			$expenses = $expenses + $this->calculateCommission($paymentId);
			$expenses = \Maagit\Maagitproduct\Service\Content\TransformService::roundCurrency($expenses, $this->settings['checkout']['currency']['round']);	
		}
		return $expenses;
	}

	/**
     * calculate total
     *
     * @return double								the calculated total
     */
    public function calculateTotal()
	{
		$tax = ($this->settings['checkout']['taxincluded']) ? 0 : $this->checkout->getBasket()->getTax();
		$shipping = ($this->checkout->getDelivery()->getShipping() > 0) ? $this->checkout->getDelivery()->getShipping() : 0;
		$discount = ($this->checkout->getCoupon() != null) ? $this->checkout->getCoupon()->getAmount() : 0;
		$total = (float)($this->checkout->getBasket()->getAmount() + $tax + $shipping + $this->checkout->getPayment()->getExpenses() + $discount);
		$total = \Maagit\Maagitproduct\Service\Content\TransformService::roundCurrency($total, $this->settings['checkout']['currency']['round']);
		return $total;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * calculate commission
     *
     * @param	string				$paymentId		the paymentId to calculate
	 * @return	double								the calculated commission
     */
    protected function calculateCommission(string $paymentId = NULL)
	{
		$commission = 0;
		if ($paymentId == null) {$paymentId = $this->checkout->getPayment()->getPaymentMethod();}
		if ($paymentId == 'saferpayCredit' || $paymentId == 'saferpayWallet') {$paymentId = 'saferpay';}
		$discount = ($this->checkout->getCoupon() != null) ? $this->checkout->getCoupon()->getAmount() : 0;
		$baseAmount = $this->checkout->getBasket()->getAmount() + $this->checkout->getBasket()->getTax() + $this->checkout->getDelivery()->getShipping() + $this->settings['checkout']['payment']['method'][$paymentId]['expenses'] + $discount;
		if (isset($this->settings['checkout']['payment']['method'][$paymentId]['commission']))
		{
			$percent = $this->settings['checkout']['payment']['method'][$paymentId]['commission'];
			$commission = $baseAmount / 100 * $percent;
			$commission = \Maagit\Maagitproduct\Service\Content\TransformService::roundCurrency($commission, $this->settings['checkout']['currency']['round']);
		}
		return $commission;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}