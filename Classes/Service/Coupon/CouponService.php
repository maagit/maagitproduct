<?php
namespace Maagit\Maagitproduct\Service\Coupon;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				CouponService

	description:		Various methods, used by checkout process step "coupon".
						Calculate coupon discount.

	created:			2022-06-01
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-06-01	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class CouponService extends \Maagit\Maagitproduct\Service\BaseService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Model\Checkout
     */
	protected $checkout;
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		$this->checkout = $checkout;
	}
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * calculate discount
     *
     * @param	-
	 * @return	double				the calculated coupon discount
     */
    public function calculateDiscount()
	{
		$discount = 0;
		if (!empty($this->checkout->getCoupon()->getCode()) && empty($this->checkout->getCoupon()->getLastError()))
		{
			$discountObject = $this->checkout->getCoupon()->getDiscount(false);
			if ($discountObject != null)
			{
				switch ($discountObject->getCoupontype())
				{
					case 1:
						$discount = \Maagit\Maagitproduct\Service\Content\TransformService::roundCurrency($discountObject->getDiscountamount(), $this->settings['checkout']['currency']['round']);
						break;
					case 2:
						$baseAmount = $this->checkout->getBasket()->getAmount();
						$percent = $discountObject->getDiscountpercent();
						$discount = \Maagit\Maagitproduct\Service\Content\TransformService::roundCurrency(($baseAmount / 100 * $percent), $this->settings['checkout']['currency']['round']);
						break;
					case 3:
						$articles = $discountObject->getDiscountperarticle();
						$discount = \Maagit\Maagitproduct\Service\Content\TransformService::roundCurrency($this->getDiscountPerArticle($articles, $discountObject->getDiscountperarticleall()), $this->settings['checkout']['currency']['round']);
						break;
					default:
						$discount = 0;
				} 
			}
		}
		$discount = $discount * -1;
		$this->checkout->getCoupon()->setAmount($discount);
		return $discount;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * loop discount per article definitions and calculate total discount
     *
     * @param	array			$articles		the discount per article definitions
	 * @param	double			$overAll		the overall discount for all articles
	 * @return	double							the discount
     */
	protected function getDiscountPerArticle(array $articles, $overall=null)
	{
		if (empty($overall)) {$overall = 0;}
		if (!is_numeric($overall)) {$overall = 0;}
		$discount = 0;
		foreach ($articles as $article)
		{
			$amount = $this->getBasketItemAmount($article['article'], $article['unit'], $article['count'], $article['base']);
			if ($amount > 0)
			{
				if ($overall > 0)
				{
					$discount = $discount + $overall;
					break;
				}
				elseif (!empty($article['amount']))
				{
					$discount = $discount + $article['amount'];
				}
				elseif (!empty($article['percent']))
				{
					$discount = $discount + ($amount / 100 * $article['percent']);
				}
			}
		}
		return $discount;
	}

	/**
     * check, if product with given count is in basket
     *
     * @param	int				$uid			the product uid
	 * @param	string			$unit			the unit id
	 * @param	int				$count			the count
	 * @param	int				$base			the base (0=article/1=basket)
	 * @return	double							the amount
     */
	protected function getBasketItemAmount(int $uid, string $unit, int $count, int $base)
	{
		$basketItemRepository = $this->makeInstance('Maagit\Maagitproduct\Domain\Repository\BasketItemRepository');
		$unit = ($unit == '') ? null : $unit;
		$unit = (!is_numeric($unit)) ? null : (int)$unit;
		$basketItem = $basketItemRepository->findByUid($uid, $unit, $this->checkout->getBasket());
		if ($basketItem == null) {return 0;}
		if ($basketItem->getQuantity() < $count) {return 0;}
		if ($base == 1) {return $this->checkout->getBasket()->getAmount();}
		$amount = (!empty($basketItem->getUnitPrice())) ? $count * $basketItem->getUnitPrice() : $count * $basketItem->getProduct()->getPrice();
		return $amount;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}