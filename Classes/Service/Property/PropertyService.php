<?php
namespace Maagit\Maagitproduct\Service\Property;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				PropertyService

	description:		Various methods for property mapping between a database record
						and a model (e.g. tt_content and "product").

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2020-08-25	Urs Maag		Added process of type "ObjectStorage"
						2021-09-09	Urs Maag		ObjectManager removed
						2022-05-25	Urs Maag		Using own ObjectStorage to avoid
													problems with "spl_object_hash" in
													various php versions

------------------------------------------------------------------------------------- */


class PropertyService extends \Maagit\Maagitproduct\Service\BaseService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var int
     */
    protected $uid;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * loop given array and set the properties in given domain model
     *
     * @param	\Maagit\Maagitproduct\Domain\Model\BaseModel		$model				the model object
	 * @param	array												$values				the values as array
	 * @return	\Maagit\Maagitproduct\Domain\Model\BaseModel							the model with properties set
     */
	public function setProperties(\Maagit\Maagitproduct\Domain\Model\BaseModel $model, array $values)
	{
		if (!empty($values['uid'])) {$this->uid = $values['uid'];}
		$reflect = new \ReflectionClass(get_class($model));
		foreach ($values as $key => $value)
		{
			try {
				$propertyType = $this->getPropertyTypeFromComment($reflect->getProperty(lcfirst($this->getPropertyName($key))));
			} catch (\Exception $ex)
			{
				$propertyType = '';
			}

			$method = $this->getPropertyMethodName($key);
			if (method_exists($model, $method))
			{
				$value = $this->convertPropertyValueToType($value, $propertyType);
				$model->$method($value);
			}
		}
		return $model;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * create the property "set" method from given member variable
     *
     * @param	string			$key			the name of member variable
	 * @return	string							the property method name
     */
	protected function getPropertyMethodName($key)
	{
		$method = 'set'.$this->getPropertyName($key);
		return $method;
	}
	
	/**
     * get the property name of a given member variable name
     *
     * @param	string			$member			the name of the member variable
	 * @return	string							the property name
     */
	protected function getPropertyName($member)
	{
		if (in_array($member, $this->getExtensionFields()))
		{
			$member = str_replace('tx_maagitproduct_', '', $member);
		}
		$propertyName = str_replace('_', '', ucwords($member, '_'));
		return $propertyName;
	}

	/**
     * get the relevant extension columns
     *
     * @param	-
	 * @return	array							the extension columns
     */
	protected function getExtensionFields()
	{
		$extensionFields = array();
		$tcaColumns = $GLOBALS['TCA']['tt_content']['columns'];
		foreach ($tcaColumns as $tcaColumn => $settings)
		{
			if (substr($tcaColumn, 0, 17) == 'tx_maagitproduct_')
			{
				$extensionFields[] = $tcaColumn;
			}
		}
		return $extensionFields;
	}
	
	/**
     * get property type from property comment
     *
     * @param	ReflectionProperty		$property			the property
	 * @return	string										the property type
     */
	protected function getPropertyTypeFromComment(\ReflectionProperty $property)
	{
		if (preg_match('/@var\s+([^\s]+)/', $property->getDocComment(), $matches)) {
			return $matches[1];
		}
		return null;
	}
	
	/**
     * converts the DB-value to given property type
     *
     * @param	mixed			$value					the value to convert
	 * @param	string			$type					the destination type
	 * @return	mixed									the converted value
     */
	protected function convertPropertyValueToType($value, string $type)
	{
		// convert to array
		if ($type == 'array')
		{
			if ($value == null) {$value = '';}
			$transformService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Content\\TransformService');
			if (!strpos($value, "\n"))
			{
				return $transformService->convertStringToArray($value);
			}
			else
			{
				return $transformService->convertTableToArray($value);
			}
		}

		// convert to unit
		if ($type == 'unit')
		{
			$transformService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Content\\TransformService');
			return $transformService->convertTableToArray($value);
		}
		
		// convert to \TYPO3\CMS\Extbase\Persistence\ObjectStorage
		if (substr($type, 0, 44) == '\TYPO3\CMS\Extbase\Persistence\ObjectStorage')
		{
			$modelName = str_replace('>', '', substr($type, 46));
			$repositoryName = \TYPO3\CMS\Core\Utility\ClassNamingUtility::translateModelNameToRepositoryName($modelName);
			if (class_exists($repositoryName) && !empty($this->uid))
			{
				$repository = $this->makeInstance($repositoryName);
				if (method_exists($repository, 'findByUid'))
				{
					return $repository->findByUid($this->uid);	
				}
			}
		}

		// convert to \Maagit\Maagitproduct\Helper\ObjectStorage
		if (substr($type, 0, 42) == '\Maagit\Maagitproduct\Helper\ObjectStorage')
		{
			$modelName = str_replace('>', '', substr($type, 44));
			$repositoryName = \TYPO3\CMS\Core\Utility\ClassNamingUtility::translateModelNameToRepositoryName($modelName);
			if (class_exists($repositoryName) && !empty($this->uid))
			{
				$repository = $this->makeInstance($repositoryName);
				if (method_exists($repository, 'findByUid'))
				{
					return $repository->findByUid($this->uid);	
				}
			}
		}

		// convert to boolean
		if ($type == 'bool')
		{
			if ($value == 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		
		// return un-converted value
		return $value;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
