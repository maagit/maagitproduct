<?php
namespace Maagit\Maagitproduct\Service\Network;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				EmailService

	description:		Send mail methods.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class EmailService extends \Maagit\Maagitproduct\Service\BaseService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * send a mail
     *
     * @param array 	$from		The email of sender
	 * @param array		$to			The email of receiver
     * @param string	$subject	The subject
	 * @param string	$text		The mail body as plain text
	 * @param string	$html		The mail body as html
     * @return void
     */
    public function sendMail(array $from, array $to, string $subject, string $text, string $html = null)
    {
		$from = (!empty($from) ? $from : \TYPO3\CMS\Core\Utility\MailUtility::getSystemFrom());
		$returnPath = \TYPO3\CMS\Core\Utility\MailUtility::getSystemFromAddress();
		$returnPath = ($returnPath!="no-reply@example.com")?$returnPath:'';
		$to = $to;
		$serverError = '';
		
		try
		{
			$mail = $this->makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
			$mail->setFrom($from);
			if ($returnPath != '') {$mail->setReturnPath($returnPath);}
			$mail->setTo($to);
			$mail->subject($subject);
			$mail->text($text);
			$mail->html((is_null($html) ? $text : $html));
			$sent = $mail->send();
		}
		catch (\Exception $ex)
		{
			$serverError = $ex->getMessage();
		}
		
		// throw error, if mail was not sent
		if (!$sent)
		{
			$error = '- '.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.error.from', 'maagitproduct').': '.implode($from).'<br />';
			$error .= '- '.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.error.to', 'maagitproduct').': '.implode($to).'<br />';
			$error .= '- '.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.error.returnpath', 'maagitproduct').': '.$returnPath.'<br />';
			if ($serverError != '')
			{
				$error .= '<br />'.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.error.server', 'maagitproduct').':<br />'.$serverError;
			}
			throw new \Exception($error);
		}
		
		// return, if sent
		return $sent;
	}

	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
