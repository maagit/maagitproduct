<?php
namespace Maagit\Maagitproduct\Service\Network;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				HttpService

	description:		Various methods for make http requests over CURL.
						Used by payment classes for external payment services.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class HttpService extends \Maagit\Maagitproduct\Service\BaseService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var resource
     */
	protected $curl;

	/**
     * @var array
     */
	protected $headers;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct()
	{
		$this->initCurl();
	}

	public function __destruct()
	{
		curl_close($this->curl);
	}
	

	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
		* 	Reset the helper.
		*	
		*	Re-initialize the curl instance.
		*	Reset class header array property.
		*
		* 	@return void
	*/
	public function resetHelper()
	{
		$this->curl = null;
		$this->initCurl();
		$this->headers = array();
	}
	
	/**
		* 	Push header values into class header array property.
		*	
		*	@param string	$header	Header key-value as string
		* 	@return void
	*/
	public function addHeader($header)
	{
		$this->headers[] = $header;
	}	
	
	/**
		* 	Set curl url.
		*	
		*	@param string	$url	Url to be called using curl
		* 	@return void
	*/
	public function setUrl($url)
	{
		curl_setopt($this->curl, CURLOPT_URL, $url);
	}
	
	/**
		* 	Set authentication attributes of the curl request.
		*	
		*	@param string	$authData	Authentication credentials
		* 	@return void
	*/
	public function setAuthentication($authData)
	{
		curl_setopt($this->curl, CURLOPT_USERPWD, $authData);
	}

	/**
		* 	Set body of the curl request.
		*	
		*	URL encode the request body.
		*	Check if data is array and url encode.
		*	Set the curl body.
		*	Set curl post options
		*	
		*	@param array|string	$postData	Curl request body
		* 	@return void
	*/
	public function setBody($data)
	{
		if (is_array($data))
		{
			$data = json_encode($data);
		}
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($this->curl, CURLOPT_POST, true);
	}
	
	/**
		* 	Public function to start execution of curl request.
		*	
		*	Check if post type request and setup curl options.
		*	If post request, set curl body with required data.
		*	Call internal function to execute curl instance.
		*
		* 	@return array Curl response
	*/
	public function sendRequest()
	{
		$this->setHeaders();
		$result = curl_exec($this->curl);
		if (curl_errno($this->curl))
		{
			trigger_error("Request Error:" . curl_error($this->curl), E_USER_WARNING);
		}
		$headerSize = curl_getinfo($this->curl, CURLINFO_HEADER_SIZE);
		$body = substr($result, $headerSize);
		return json_decode($body, true);
	}

	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	/**
		* 	Initialize curl.
		*
		*	Check if curl is enabled in the PHP environment.
		*	Trigger error if curl is not available.
		*	Initialize curl and set defaults.
		*
		* 	@return void
	*/
	private function initCurl()
	{
		if (!function_exists('curl_version'))
		{
			trigger_error("Curl not available", E_USER_ERROR);
		}
		else
		{
			$this->curl = curl_init();
			$this->setDefaults();
		}
	}
	
	/**
		* 	Set curl defaults.
		*	
		* 	@return void
	*/
	private function setDefaults()
	{  
		curl_setopt($this->curl, CURLOPT_VERBOSE, 1);
		curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($this->curl, CURLOPT_SSLVERSION , 'CURL_SSLVERSION_TLSv1_2');
		curl_setopt($this->curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->curl, CURLOPT_MAXREDIRS, 10);
		curl_setopt($this->curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($this->curl, CURLOPT_HEADER, 1);
		curl_setopt($this->curl, CURLINFO_HEADER_OUT, 1);
	}
	
	/**
		* 	Set curl headers using the class header array property.
		*
		* 	@return void
	*/
	private function setHeaders()
	{
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->headers); 
	}
}
