<?php
namespace Maagit\Maagitproduct\Service\Network;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				SessionService

	description:		Session administration methods.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-09-09	Urs Maag		ObjectManager removed
						2021-12-23	Urs Maag		Return empty string, if no session
													is available
						2022-05-25	Urs Maag		Using own ObjectStorage to avoid
													problems with "spl_object_hash" in
													various php versions

------------------------------------------------------------------------------------- */


class SessionService extends \Maagit\Maagitproduct\Service\BaseService implements \TYPO3\CMS\Core\SingletonInterface
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
 	 * @var \TYPO3\CMS\Core\Session\UserSessionManager
 	 */
	protected $userSessionManager;

	/**
	 * @var \TYPO3\CMS\Core\Session\UserSession
     */
	protected $userSession;

	/**
	 * @var string
     */
	protected $cookieName = 'maagitproduct';

	/**
	 * @var string
     */
	protected $prefixKey = 'tx-maagitproduct_';
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct()
	{
		$this->userSessionManager = \TYPO3\CMS\Core\Session\UserSessionManager::create('FE');
		$this->userSession = $this->getUserSession((empty($_COOKIE[$this->cookieName]))?'':$_COOKIE[$this->cookieName]);
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
     * Restores an object from the PHP session
     *
     * @param	string 											$key 		session key
     * @return	\Maagit\Maagitproduct\Domain\Model\BaseModel				the object
     */
	public function restore($key)
    {
		$sessionData = $this->userSession->get($this->prefixKey.$key);
		if ($sessionData == null)
		{
			$sessionData = '';
		}
		else
		{
			$sessionData = unserialize($sessionData);
		}
		return $sessionData;
    }
	
    /**
     * Writes an object into the PHP session
     *
     * @param	\Maagit\Maagitproduct\Domain\Model\BaseModel	$object
     * @param	string 											$key 		session key
     * @return	void
     */
    public function write(\Maagit\Maagitproduct\Domain\Model\BaseModel $object, $key)
    {
		$this->clearUnnecessaryMembers($object);
		$sessionData = serialize($object);
		$this->userSession->set($this->prefixKey.$key, $sessionData);
		$this->userSessionManager->updateSession($this->userSession);
    }

    /**
     * Clear an object from the PHP session
     *
     * @param	string 											$key 		session key
     * @return	void
     */
    public function clear($key)
    {
		$this->userSession->set($this->prefixKey.$key, null);
		$this->userSessionManager->updateSession($this->userSession);
    }

    /**
     * Remove session
     *
     * @param	-
     * @return	void
     */
    public function remove()
    {
		$this->userSessionManager->removeSession($this->userSession);
		$this->setCookie('');
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
    /**
     * Get or create a (new) user session object
     *
     * @param	string									$sessionId		the session identifier
     * @return	\TYPO3\CMS\Core\Session\UserSession						the user session object
     */
	protected function getUserSession(string $sessionId)
    {
		try
		{
			return $this->userSessionManager->createSessionFromStorage($sessionId);
		}
		catch (\Exception $ex) { }
		$userSession = $this->userSessionManager->createAnonymousSession();
		$this->userSessionManager->fixateAnonymousSession($userSession, true);
		$this->setCookie($userSession->getIdentifier());
		return $userSession;
    }

    /**
     * Write cookie
     *
     * @param	string									$content		the cookie content
     * @return	void
     */
	protected function setCookie(string $content)
	{
		if(isset($_COOKIE[$this->cookieName]))
		{
			setcookie($this->cookieName, '', time() - 3600, '/');
		    setcookie($this->cookieName, $content, time() + (86400 * 30), '/');
		}
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
    /**
     * Prepare object for serialization
	 * Clear unnecessary objects (or recursive objects, given from base model and general trait)
     *
     * @param	object									$object		the object to prepare
     * @return	void
     */
	private function clearUnnecessaryMembers(object $object)
	{
		// initialization
		$methods = array('setConfigurationManager', 'setCheckout', 'setTsConfigService', 'setDivHelper');
		$reflectionClass = new \ReflectionClass($object);
		$members = $reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED);
		// clear members
		foreach ($methods as $method)
		{
			if (method_exists($object, $method))
			{
				$object->$method(null);
			}
		}

		// get sub-objects and call function recursively
		foreach ($members as $member)
		{
			$member->setAccessible(true);
			$subObject = $member->getValue($object);
			if (is_object($subObject))
			{
				if (get_class($subObject) == 'TYPO3\CMS\Extbase\Persistence\ObjectStorage')
				{
					foreach ($subObject as $subObjectItem)
					{
						$this->clearUnnecessaryMembers($subObjectItem);							
					}
				}
				elseif (get_class($subObject) == 'Maagit\Maagitproduct\Helper\ObjectStorage')
				{
					foreach ($subObject as $subObjectItem)
					{
						$this->clearUnnecessaryMembers($subObjectItem);							
					}
				}
				else
				{
					$this->clearUnnecessaryMembers($subObject);	
				}
			}
		}
	}
}