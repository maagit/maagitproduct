<?php
namespace Maagit\Maagitproduct\Service\Requirements;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				RequirementsService

	description:		Various methods to check requirements.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-12-23	Urs Maag		Check of existing key "jqueryVersion"

------------------------------------------------------------------------------------- */


class RequirementsService extends \Maagit\Maagitproduct\Service\BaseService
{
	/* ======================================================================================= */
	/* C O N S T A N T S                                                                       */
	/* ======================================================================================= */
	const MIN_JQUERY_VERSION = '1.11.3';
	const COOKIES = 2;
	const JAVASCRIPT = 4;
	const JQUERY = 8;
	const JQUERY_UNKNOWN = 16;
	const JQUERY_VERSION = 32;

	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */

	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Check, if requirements are given
	 *
	 * - if all is ok, do nothing further
	 * - if not all requirements are given, show message
     *
     * @return boolean
     */
	public function check()
	{
		$cookies = $this->cookiesAllowed();
		$javascript = $this->javascriptEnabled();
		$jquery = ($javascript==true) ? $this->jqueryAvailable() : false;

		if (!$javascript)
		{
			$jqueryState = RequirementsService::JQUERY_UNKNOWN;
		}
		elseif (!isset($_GET['jqueryVersion']) || $_GET['jqueryVersion'] == 'false')
		{
			$jqueryState = RequirementsService::JQUERY;
		}
		elseif (version_compare($_GET['jqueryVersion'], RequirementsService::MIN_JQUERY_VERSION) == -1)
		{
			$jqueryState = RequirementsService::JQUERY_VERSION;
		}
		else
		{
			$jqueryState = 0;
		}

		if (!$cookies && !$javascript && !$jquery)
		{
			return (RequirementsService::COOKIES | RequirementsService::JAVASCRIPT | $jqueryState);
		}
		if ($cookies && !$javascript && !$jquery)
		{
			return (RequirementsService::JAVASCRIPT | $jqueryState);
		}
		if (!$cookies && $javascript && !$jquery)
		{
			return (RequirementsService::COOKIES | $jqueryState);
		}
		if ($cookies && $javascript && !$jquery)
		{
			return ($jqueryState);
		}
		if (!$cookies && $javascript && $jquery)
		{
			return (RequirementsService::COOKIES);
		}

		return 0;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Check, if cookies are allowed
     *
     * @return boolean		are cookies allowed?
     */
	protected function cookiesAllowed()
	{
		session_start(); $firstCookie = session_id(); session_destroy();
		session_start(); $secondCookie = session_id(); session_destroy();
		if ($firstCookie == $secondCookie && isset($_COOKIE['maagitproduct']))
		{
			return true;
		}
		return false;
	}
	
	/**
     * Check, if javascript is enabled
     *
     * @return boolean		is javascript enabled?
     */
	protected function javascriptEnabled()
	{
		if (isset($_GET['js']) && $_GET['js']=='false')
		{
			return false;
		}
		return true;
	}
	
	/**
     * Check, if jquery is available
     *
     * @return boolean		is jquery available?
     */
	protected function jqueryAvailable()
	{
		if (!isset($_GET['jqueryVersion']))
		{
			return false;
		}
		if ($_GET['jqueryVersion']=='false')
		{
			return false;
		}
		if (version_compare($_GET['jqueryVersion'], RequirementsService::MIN_JQUERY_VERSION) == -1)
		{
			return false;
		}
		return true;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}