<?php
namespace Maagit\Maagitproduct\Service\Payment;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				PaymentConditionDeliveryService

	description:		Methods for evaluation conditions of available payment methods.
						Check condition based on delivery settings.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class PaymentConditionDeliveryService extends \Maagit\Maagitproduct\Service\Payment\PaymentConditionService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * get payment methods, based on given check
     *
     * @return	array			$paymentMethods		the available payment methods
	 * @return	array								the validated payment methods
     */
    public function check(array $paymentMethods)
	{
		$array = array();
		foreach ($paymentMethods as $method)
		{
			$m = ($method['id'] != 'saferpayCredit' && $method['id'] != 'saferpayWallet') ? $method['id'] : 'saferpay';
			if ($this->settings['checkout']['payment']['method'][$m]['condDelivery'] != '')
			{
				$ids = explode(',', $this->settings['checkout']['payment']['method'][$m]['condDelivery']);
				if (in_array($this->checkout->getDelivery()->getDeliveryMethod(), $ids))
				{
					$array[] = $method;
				}
			}
			else
			{
				$array[] = $method;
			}
		}
		return $array;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}