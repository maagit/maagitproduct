<?php
namespace Maagit\Maagitproduct\Service\Payment;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				PaymentService

	description:		Various methods, used by checkout process step "payment".
						Get available payment methods.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class PaymentService extends \Maagit\Maagitproduct\Service\BaseService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Domain\Model\Checkout
     */
	protected $checkout;
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(\Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		$this->checkout = $checkout;
	}
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * get payment methods, based on settings and items in basket
     *
     * @return array				the available payment methods
     */
    public function getPaymentMethods()
	{
		$paymentMethodsFromSettings = $this->getPaymentMethodsFromSettings();
		$paymentMethods = $this->addTarifToPaymentMethod($paymentMethodsFromSettings);
		$paymentMethods = $this->checkConditions($paymentMethods);
		return $paymentMethods;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Get selected payment methods from settings
     *
	 * @return 	array							payment methods
     */
	protected function getPaymentMethodsFromSettings()
	{
		$paymentMethods = array();
		$array = explode(',', $this->settings['checkout']['payment']['methods']);
		foreach ($array as $item)
		{
			$temp = array('id' => $item, 'text' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.payment.'.$item, 'maagitproduct'));
			$paymentMethods[] = $temp;
		}
		return $paymentMethods;
	}

	/**
     * Add tarif to payment methods
     *
	 * @param	array		$paymentMethods		the payment methods
	 * @return 	array							the payment methods with tarif
     */
	protected function addTarifToPaymentMethod(array $paymentMethods)
	{
		$array = array();
		foreach ($paymentMethods as $method)
		{
			$method['tarif'] = $this->checkout->getPayment()->getExpenses($method['id']);
			$array[] = $method;
		}
		return $array;
	}
	
	/**
     * Get the available payment methods based on defined conditions in the settings
     *
	 * @param	array					$paymentMethods			the available payment methods
	 * @return 	array											the validated payment methods
     */
	protected function checkConditions(array $paymentMethods)
	{
		// initialization
		$array = array();
		// check delivery condition
		$conditionServiceDelivery = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Payment\\PaymentConditionDeliveryService', $this->checkout);
		$array = $conditionServiceDelivery->check($paymentMethods);
		// check min condition
		$conditionServiceMin = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Payment\\PaymentConditionMinService', $this->checkout);
		$array = $conditionServiceMin->check($array);
		// check max condition
		$conditionServiceMax = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Payment\\PaymentConditionMaxService', $this->checkout);
		$array = $conditionServiceMax->check($array);
		// return paymentMethods
		return $array;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}