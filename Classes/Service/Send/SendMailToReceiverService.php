<?php
namespace Maagit\Maagitproduct\Service\Send;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				SendMailToReceiverService

	description:		Send order mail to receiver.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class SendMailToReceiverService extends \Maagit\Maagitproduct\Service\Send\SendService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */

	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
     * process action
     *
     * @return	boolean		process successfully completed?
     */
    public function process()
	{
		// initialize
		$this->checkout->setMailToReceiverError('');
		
		// render email template
		if ($this->settings['checkout']['receiver']['custom'])
		{
			$bodyText = $this->renderService->renderCObject($this->settings['checkout']['receiver']['cobj']);
			$bodyText = $this->renderService->renderPlaceholder($bodyText, $this->checkout);
		}
		else
		{
			$bodyText = $this->renderService->renderTemplate('Email/Order', array('checkout' => $this->checkout, 'settings' => $this->settings));
		}
		
		// get subject
		if ($this->settings['checkout']['receiver']['custom'])
		{
			$subject = $this->settings['checkout']['receiver']['subject'];
		}
		else
		{
			$subject = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.order', 'maagitproduct');
		}
		$subject .= ' ('.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.orderId', 'maagitproduct');
		$subject .= ' '.$this->checkout->getOrderId().')';
		
		// send mail
		try
		{
			$sent = $this->emailService->sendMail(
				array(($this->checkout->getAddress()->getEmail() != '') ? $this->checkout->getAddress()->getEmail() : $this->settings['checkout']['receiver']['email']),
				array($this->settings['checkout']['receiver']['email']),
				$subject,
				$bodyText,
				$bodyText
			);
		}
		catch (\Exception $ex)
		{
			$details = $ex->getMessage();
			$error = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.error.receiver', 'maagitproduct').'<br /><br />';
			$error .= \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.error.details', 'maagitproduct').':<br />';
			$error .= $details;
			$this->checkout->setMailToReceiverError($error);
			return false;
		}
		return true;
	}
	
	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}