<?php
namespace Maagit\Maagitproduct\Service\Send;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				SendLogService

	description:		Save order to database.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-12-25	Urs Maag		PHP 8, fix non existing array keys
						2022-01-01	Urs Maag		Set transactionId on stripe payment
						2022-04-18	Urs Maag		Added billing address fields
						2022-05-30	Urs Maag		Added userid and coupon fields
						2022-06-13	Urs Maag		Set "download mail" date

------------------------------------------------------------------------------------- */


class SendLogService extends \Maagit\Maagitproduct\Service\Send\SendService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */

	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
     * process action
     *
     * @return	boolean		process successfully completed?
     */
    public function process()
	{
		// get order object
		$orderRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\OrderRepository');
		$order = $orderRepository->create();

		// set properties
		$order->setOrderId($this->checkout->getOrderId());
		if ($this->settings['checkout']['login']['step'] && !$this->checkout->getLogin()->getGuest())
		{
			$order->setUserid($this->checkout->getLogin()->getUser()->getUid());
		} else {
			$order->setUserid('');
		}
		$order->setSalutation($this->checkout->getAddress()->getSalutation());
		$order->setFirma($this->checkout->getAddress()->getFirma());
		$order->setPrename($this->checkout->getAddress()->getPrename());
		$order->setLastname($this->checkout->getAddress()->getLastname());
		$order->setStreet($this->checkout->getAddress()->getStreet());
		$order->setHouseNumber($this->checkout->getAddress()->getHouseNumber());
		$order->setHouseAdd($this->checkout->getAddress()->getHouseAdd());
		$order->setAddress($this->checkout->getAddress()->getAddress());
		$order->setPo($this->checkout->getAddress()->getPo());
		$order->setCity($this->checkout->getAddress()->getCity());
		$order->setPocity($this->checkout->getAddress()->getPoCity());
		$order->setCountry($this->checkout->getAddress()->getCountry());
		$order->setTelephone($this->checkout->getAddress()->getTelephone());
		$order->setMobile($this->checkout->getAddress()->getMobile());
		$order->setEmail($this->checkout->getAddress()->getEmail());
		$order->setSex($this->checkout->getAddress()->getSex());
		$order->setBirthdate($this->checkout->getAddress()->getBirthdate());
		$order->setMessage($this->checkout->getAddress()->getMessage());
		$order->setBill($this->checkout->getAddress()->getBill());
		if ($this->checkout->getAddress()->getBill())
		{
			$order->setBillingsalutation($this->checkout->getAddress()->getBillingAddress()->getSalutation());
			$order->setBillingfirma($this->checkout->getAddress()->getBillingAddress()->getFirma());
			$order->setBillingprename($this->checkout->getAddress()->getBillingAddress()->getPrename());
			$order->setBillinglastname($this->checkout->getAddress()->getBillingAddress()->getLastname());
			$order->setBillingstreet($this->checkout->getAddress()->getBillingAddress()->getStreet());
			$order->setBillinghouseNumber($this->checkout->getAddress()->getBillingAddress()->getHouseNumber());
			$order->setBillinghouseAdd($this->checkout->getAddress()->getBillingAddress()->getHouseAdd());
			$order->setBillingaddress($this->checkout->getAddress()->getBillingAddress()->getAddress());
			$order->setBillingpo($this->checkout->getAddress()->getBillingAddress()->getPo());
			$order->setBillingcity($this->checkout->getAddress()->getBillingAddress()->getCity());
			$order->setBillingpocity($this->checkout->getAddress()->getBillingAddress()->getPoCity());
			$order->setBillingcountry($this->checkout->getAddress()->getBillingAddress()->getCountry());
			$order->setBillingtelephone($this->checkout->getAddress()->getBillingAddress()->getTelephone());
			$order->setBillingmobile($this->checkout->getAddress()->getBillingAddress()->getMobile());
			$order->setBillingemail($this->checkout->getAddress()->getBillingAddress()->getEmail());
			$order->setBillingsex($this->checkout->getAddress()->getBillingAddress()->getSex());
			$order->setBillingbirthdate($this->checkout->getAddress()->getBillingAddress()->getBirthdate());
			$order->setBillingmessage($this->checkout->getAddress()->getBillingAddress()->getMessage());
		}
		if ($this->settings['checkout']['tax'] > 0) {
			$order->setTaxpercent($this->settings['checkout']['tax'].'%');	
		} else {
			$order->setTaxpercent('0%');	
		}
		if ($this->checkout->getBasket()->getHasDownloads()) {
			$order->setDelivery(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.download.link', 'maagitproduct'));
		}
		if ($this->checkout->getBasket()->getHasDownloads() && !$this->checkout->getBasket()->getHasOnlyDownloads()) {
			$order->setDelivery($order->getDelivery().' / ');
		}
		$deliveryText = (isset($this->checkout->getDelivery()->getDeliveryMethodInfo()['text'])) ? $this->checkout->getDelivery()->getDeliveryMethodInfo()['text'] : '';
		$order->setDelivery($order->getDelivery().$deliveryText);
		$order->setPayment($this->checkout->getPayment()->getPaymentMethodInfo()['text']);
		if ($this->checkout->getTotal() == 0) {
			$order->setPayment(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.payment.free', 'maagitproduct'));
		}
		if ($this->checkout->getPayment()->getPaymentMethod() == 'paypal') {
			$token = substr(strip_tags($this->checkout->getPayment()->getPaymentMessage()), strpos(strip_tags($this->checkout->getPayment()->getPaymentMessage()), 'ID: ') + 4);
			$token = substr($token, 0, strlen($token) - 1);
			$order->setPaymenttransactionid($token);
		} elseif ($this->checkout->getPayment()->getPaymentMethod() == 'saferpayCredit' || $this->checkout->getPayment()->getPaymentMethod() == 'saferpayWallet') {
			$token = substr(strip_tags($this->checkout->getPayment()->getPaymentMessage()), strpos(strip_tags($this->checkout->getPayment()->getPaymentMessage()), 'ID: ') + 4);
			$token = substr($token, 0, strlen($token) - 1);
			$order->setPaymenttransactionid($token);
		} elseif ($this->checkout->getPayment()->getPaymentMethod() == 'stripe') {
			$token = substr(strip_tags($this->checkout->getPayment()->getPaymentMessage()), strpos(strip_tags($this->checkout->getPayment()->getPaymentMessage()), 'ID: ') + 4);
			$token = substr($token, 0, strlen($token) - 1);
			$order->setPaymenttransactionid($token);
		} elseif ($this->checkout->getPayment()->getPaymentMethod() == 'twint') {
			$order->setPaymenttransactionid($this->checkout->getOrderId());
		} else {
			$order->setPaymenttransactionid('');
		}
		if ($this->checkout->getPayment()->getPaymentMethod() == 'cash') {
			$order->setPaymenttext(strip_tags($this->checkout->getPayment()->getPaymentMethodCashInfo()));
		} elseif ($this->checkout->getPayment()->getPaymentMethod() == 'bill') {
			$order->setPaymenttext(strip_tags($this->checkout->getPayment()->getPaymentMethodBillInfo()));
		} elseif ($this->checkout->getPayment()->getPaymentMethod() == 'advance') {
			$order->setPaymenttext(strip_tags($this->checkout->getPayment()->getPaymentMethodAdvanceInfo()));
		} elseif ($this->checkout->getPayment()->getPaymentMethod() == 'twint') {
			$order->setPaymenttext(strip_tags($this->checkout->getPayment()->getPaymentMethodTwintInfo()));
		} else  {
			$order->setPaymenttext(strip_tags($this->checkout->getPayment()->getPaymentMessage()));
		}
		$order->setBasketamount($this->renderService->renderTemplate('Div/Currency', array('amount' => $this->checkout->getBasket()->getAmount(), 'settings' => $this->settings)));
		$order->setTaxamount($this->renderService->renderTemplate('Div/Currency', array('amount' => $this->checkout->getBasket()->getTax(), 'settings' => $this->settings)));
		$order->setShipping($this->renderService->renderTemplate('Div/Currency', array('amount' => $this->checkout->getDelivery()->getShipping(), 'settings' => $this->settings)));
		$order->setExpenses($this->renderService->renderTemplate('Div/Currency', array('amount' => $this->checkout->getPayment()->getExpenses(), 'settings' => $this->settings)));
		$order->setTotalamount($this->renderService->renderTemplate('Div/Currency', array('amount' => $this->checkout->getTotal(), 'settings' => $this->settings)));
		if ($this->settings['checkout']['coupon']['step'] && !empty($this->checkout->getCoupon()->getCode()))
		{
			$order->setCouponcode($this->checkout->getCoupon()->getCode());
			$order->setCoupondiscount($this->renderService->renderTemplate('Div/Currency', array('amount' => $this->checkout->getCoupon()->getAmount(), 'settings' => $this->settings)));
			$order->setCoupontext($this->checkout->getCoupon()->getText());
		} else {
			$order->setCouponcode('');
			$order->setCoupondiscount('');
			$order->setCoupontext('');
		}
		$order->setTerms(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.agb.'.$this->settings['checkout']['sending']['condition'], 'maagitproduct'));
		$order->setReceiver($this->settings['checkout']['receiver']['email']);
		if ($this->settings['checkout']['sender']['confirmation']) {
			$order->setSenderconfirmation(1);
			$order->setSenderconfirmationfrom($this->settings['checkout']['sender']['email']);
		} else {
			$order->setSenderconfirmation(0);
			$order->setSenderconfirmationfrom('');
		}
		if ($this->checkout->getBasket()->getHasDownloads() && $this->settings['checkout']['download']['secure']) {
			$order->setDownloadsecure(1);
		} else {
			$order->setDownloadsecure(0);
		}
		if ($this->checkout->getBasket()->getHasDownloads()) {
			$order->setDownloadfrom($this->settings['checkout']['download']['email']);
		} else {
			$order->setDownloadfrom('');
		}
		foreach ($this->checkout->getBasket()->getBasketItems() as $item)
		{
			$article = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Model\\Article');
			$article->setOrder($order);
			$header = $item->getProduct()->getHeader();
			if (!empty($item->getUnitDescription()))
			{
				$header .= ' ('.$item->getUnitDescription().')';
			}
			$article->setProductuid($item->getProduct()->getUid());
			$article->setProduct($header);
			$article->setUnit($item->getUnitDescription());
			$article->setDescription(strip_tags($item->getProduct()->getBodytext()));
			$assets = $item->getProduct()->getAssets();
			if (isset($assets[0])) {
				$path = $assets[0]->getPathAndFilename();
				$host = $_SERVER['HTTP_HOST'];
				$protocol = (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS'])) ? 'https://' : 'http://';
				$image = $protocol.$host.'/'.$path;
				$article->setImage($image);
			} else {
				$article->setImage('');
			}
			if ($item->getProduct()->getSecureLink() != '') {
				$article->setLink($item->getProduct()->getSecureLink());
			} else {
				$conf = array(
				    'parameter' => $item->getProduct()->getLink(),
				    'useCashHash' => false,
				    'returnLast' => 'url',
					'forceAbsoluteUrl' => true
				);
				// @extensionScannerIgnoreLine
				$linkUrl = $this->divHelper->getExtbaseRequest()->getAttribute('currentContentObject')->typolink_URL($conf); 
				$article->setLink($linkUrl);
			}
			$article->setQuantity($item->getQuantity());
			(!empty($item->getUnitStock())) ? $article->setStock($item->getUnitStock()) : $article->setStock($item->getProduct()->getStock());
			(!empty($item->getUnitWeight())) ? $article->setWeight($item->getUnitWeight()) : $article->setWeight($item->getProduct()->getWeight().'g');
			(!empty($item->getUnitPrice())) ? $article->setPrice($this->renderService->renderTemplate('Div/Currency', array('amount' => $item->getUnitPrice(), 'settings' => $this->settings))) : $article->setPrice($this->renderService->renderTemplate('Div/Currency', array('amount' => $item->getProduct()->getPrice(), 'settings' => $this->settings)));
			$article->setTotalamount($this->renderService->renderTemplate('Div/Currency', array('amount' => $item->getTotal(), 'settings' => $this->settings)));
			$pm = array('paypal', 'saferpayCredit', 'saferpayWallet', 'stripe');
			if ($item->getProduct()->getIsDownload() && ($item->getTotal()==0 || (in_array($this->checkout->getPayment()->getPaymentMethod(), $pm) && $this->checkout->getPayment()->getPaymentOK()==true)))
			{
				$article->setDownloadmail(time());
			}
			$order->addArticle($article);
		}
		$orderRepository->add($order);

		// return true, if log successfully written
		return true;
	}
	
	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}