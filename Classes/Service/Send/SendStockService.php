<?php
namespace Maagit\Maagitproduct\Service\Send;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				SendStockService

	description:		Recalculate stock of products and update database.

	created:			2022-06-05
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-06-05	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class SendStockService extends \Maagit\Maagitproduct\Service\Send\SendService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */

	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
     * process action
     *
     * @return	boolean		process successfully completed?
     */
    public function process()
	{
		// get product repository and services
		$productRepository = $this->makeInstance('Maagit\\Maagitproduct\\Domain\\Repository\\ProductRepository');
		$transformService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Content\\TransformService');

		// loop basket items and recalculate stock
		foreach ($this->checkout->getBasket()->getBasketItems() as $basketItem)
		{
			if (!empty($basketItem->getUnit()))
			{
				$productObj = $productRepository->findByUid($basketItem->getProduct()->getUid());
				$stock = ($basketItem->getUnitStock() - $basketItem->getQuantity());
				$newUnitTable = '';
				foreach ($productObj->getUnit() as $unit)
				{
					if ($unit[0] == $basketItem->getUnit())
					{
						$unit[4] = $stock;
					}
					$newColumn = $unit[0].'|'.$unit[1].'|'.$unit[2].'|'.$unit[3].'|'.$unit[4];
					if (!empty($newUnitTable)) {$newUnitTable = $newUnitTable."\n";}
					$newUnitTable = $newUnitTable.$newColumn;
				}
				$stock = $newUnitTable;
				$field = 'tx_maagitproduct_unit';
			}
			else
			{
				$stock = ($basketItem->getProduct()->getStock() - $basketItem->getQuantity());
				$field = 'tx_maagitproduct_stock';
			}
			$productRepository->updateByUid($basketItem->getProduct()->getUid(), $field, $stock);
		}

		// clear cache of pages, which inherits products
		$this->clearRelevantCaches();

		// return true, if update successfully
		return true;
	}
	
	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
    /**
     * clear relevant page caches
     *
     * @param	-
	 * @return	boolean		process successfully completed?
     */
	protected function clearRelevantCaches()
	{
		$cacheService = $this->makeInstance('Maagit\Maagitproduct\Service\Network\CacheService');
		$pageRepository = $this->makeInstance('Maagit\Maagitproduct\Domain\Repository\PageRepository');
		$pids = $pageRepository->findByTypes(array('maagitproduct_content'), array('maagitproduct_list'));
		$cacheService->clear($pids);
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}