<?php
namespace Maagit\Maagitproduct\Service\Send;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				SendDownloadsToCustomerService

	description:		Send mail with download informations to customer.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class SendDownloadsToCustomerService extends \Maagit\Maagitproduct\Service\Send\SendService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */

	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
     * process action
     *
     * @return	boolean		process successfully completed?
     */
    public function process()
	{
		// initialize
		$this->checkout->setMailToSenderError('');

		// loop downloads
		foreach ($this->checkout->getBasket()->getBasketItems() as $basketItem)
		{
			// only free downloads or payment has done and is ok
			$pm = array('paypal', 'saferpayCredit', 'saferpayWallet', 'stripe');
			if ($basketItem->getProduct()->getIsDownload() && ($basketItem->getTotal()==0 || (in_array($this->checkout->getPayment()->getPaymentMethod(), $pm) && $this->checkout->getPayment()->getPaymentOK()==true)))
			{
				// render email template
				if ($this->settings['checkout']['download']['custom'])
				{
					$bodyText = $this->renderService->renderCObject($this->settings['checkout']['download']['cobj']);
					$bodyText = $this->renderService->renderPlaceholder($bodyText, $basketItem);
				}
				else
				{
					$bodyText = $this->renderService->renderTemplate('Email/Download', array('basketItem' => $basketItem, 'settings' => $this->settings));
				}
				
				// get subject
				if ($this->settings['checkout']['download']['custom'])
				{
					$subject = $this->settings['checkout']['download']['subject'];
				}
				else
				{
					$subject = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.download', 'maagitproduct');
				}
				$subject .= ' ('.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.orderId', 'maagitproduct');
				$subject .= ' '.$this->checkout->getOrderId().')';
				
				// send mail
				try
				{
					$sent = $this->emailService->sendMail(
						array($this->settings['checkout']['download']['email']),
						array($this->checkout->getAddress()->getEmail()),
						$subject,
						$bodyText,
						$bodyText
					);
				}
				catch (\Exception $ex)
				{
					$details = $ex->getMessage();
					$error = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.error.sender', 'maagitproduct').'<br /><br />';
					$error .= \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('checkout.mail.error.details', 'maagitproduct').':<br />';
					$error .= $details;
					$this->checkout->setMailToSenderError($error);
					return false;
				}
			}
		}
		return true;
	}
	
	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}