<?php
namespace Maagit\Maagitproduct\Service\Send;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Service
	class:				RedirectService

	description:		Methods for redirecting to confirmation page, based on the
						flexform settings of the plugin.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class RedirectService extends \Maagit\Maagitproduct\Service\BaseService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitproduct\Controller\BaseController
     */
	protected $controller;

	/**
	 * @var \Maagit\Maagitproduct\Domain\Repository\CheckoutRepository
     */
	protected $checkoutRepository;

	/**
	 * @var \Maagit\Maagitproduct\Domain\Model\Checkout
     */
	protected $checkout;
	
	
	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function initializeObject(\Maagit\Maagitproduct\Controller\BaseController $controller, \Maagit\Maagitproduct\Domain\Repository\CheckoutRepository $checkoutRepository, \Maagit\Maagitproduct\Domain\Model\Checkout $checkout)
	{
		$this->controller = $controller;
		$this->checkoutRepository = $checkoutRepository;
		$this->checkout = $checkout;
	}
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
     * process action
     *
     * @return	mixed					the redirect object | false
     */
    public function process()
	{
		// no "redirect"-flag is set, continue
		if (!$this->settings['checkout']['redirect'])
		{
			return false;
		}

		// initialization and clearing
		$sessionService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Network\\SessionService');
		$renderService = $this->makeInstance('Maagit\\Maagitproduct\\Service\\Content\\RenderService');
		$sessionService->clear('settings');

		// redirect mode: "object" --> redirect and send params as object stored in session
		if ($this->settings['checkout']['redirectMode'] == 'object')
		{
			$object = $this->makeInstance('Maagit\Maagitproduct\Domain\Model\BaseModel');
			$sessionService->write($object, 'settings');
			$result = $this->divHelper->callByReflection($this->controller, 'redirect', array('show', 'Confirmation', 'Maagitproduct', array(), $this->settings['checkout']['redirect'], $delay = 0, $statusCode = 303));
			return $result;
		}
		
		// redirect mode: "parameter" --> redirect and send params as GET parameters
		if ($this->settings['checkout']['redirectMode'] == 'parameter')
		{
			$params = array('tx_maagitproduct_checkout' => array('checkout' => $this->checkout->toArray()));
			$params['tx_maagitproduct_checkout']['checkout']['tsBasket'] = $renderService->renderTemplate('Redirect/Basket', array('checkout' => $this->checkout, 'settings' => $this->settings));
			$this->checkoutRepository->clear();
			$uri = $this->divHelper->getUri($this->settings['checkout']['redirect'], $params);
			$result = $this->divHelper->callByReflection($this->controller, 'redirectToURI', array($uri, $delay = 0, $statusCode = 303));
			return $result;
		}
		
		// redirect mode: "no" --> redirect without sending parameters
		if ($this->settings['checkout']['redirectMode'] == 'no')
		{
			$uri = $this->divHelper->getUri($this->settings['checkout']['redirect'], array());
			$this->checkoutRepository->clear();
			$result = $this->divHelper->callByReflection($this->controller, 'redirectToURI', array($uri, $delay = 0, $statusCode = 303));
			return $result;
		}
		
		// end redirection, don't continue on caller-object
		return false;
	}
	
	
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}