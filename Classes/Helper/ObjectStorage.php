<?php
namespace Maagit\Maagitproduct\Helper;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			Helper
	class:				ObjectStorage

	description:		Extended Extbase ObjectStorage to avoid problems with
						"spl_object_hash" within some php versions - use "md5" instead.

	created:			2022-05-25
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-05-25	Urs Maag		Initial version
						2022-05-31	Urs Maag		create own hash code

------------------------------------------------------------------------------------- */


class ObjectStorage extends \TYPO3\CMS\Extbase\Persistence\ObjectStorage
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
     * Associates data to an object in the storage. offsetSet() is an alias of attach().
     *
     * @param TEntity $object The object to add.
     * @param mixed $information The data to associate with the object.
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($object, $information): void
    {
        $this->isModified = true;
        $this->storage[$this->getHash($object)] = ['obj' => $object, 'inf' => $information];

        $this->positionCounter++;
        $this->addedObjectsPositions[$this->getHash($object)] = $this->positionCounter;
    }

    /**
     * Checks whether an object exists in the storage.
     *
     * @param TEntity|int $value The object to look for, or the key in the storage.
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($value): bool
    {
        return (is_object($value) && isset($this->storage[$this->getHash($value)]))
            || (\TYPO3\CMS\Core\Utility\MathUtility::canBeInterpretedAsInteger($value) && isset(array_values($this->storage)[$value]));
    }

    /**
     * Removes an object from the storage. offsetUnset() is an alias of detach().
     *
     * @param TEntity|int $value The object to remove, or its key in the storage.
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($value): void
    {
        $this->isModified = true;

        $object = $value;

        if (\TYPO3\CMS\Core\Utility\MathUtility::canBeInterpretedAsInteger($value)) {
            $object = $this->offsetGet($value);
        }

        unset($this->storage[$this->getHash($object)]);

        if (empty($this->storage)) {
            $this->positionCounter = 0;
        }

        $this->removedObjectsPositions[$this->getHash($object)] = $this->addedObjectsPositions[$this->getHash($object)] ?? null;
        unset($this->addedObjectsPositions[$this->getHash($object)]);
    }

    /**
     * Returns the data associated with an object, or the object itself if an
     * integer is passed.
     *
     * @param TEntity|int $value The object to look for, or its key in the storage.
     * @return mixed The data associated with an object in the storage, or the object itself if an integer is passed.
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($value): mixed
    {
        if (\TYPO3\CMS\Core\Utility\MathUtility::canBeInterpretedAsInteger($value)) {
            return array_values($this->storage)[$value]['obj'] ?? null;
        }

        /** @var DomainObjectInterface $value */
        return $this->storage[$this->getHash($value)]['inf'] ?? null;
    }

    /**
     * Returns TRUE if an object is added, then removed and added at a different position
     *
     * @param mixed $object
     * @return bool
     */
    public function isRelationDirty($object): bool
    {
        return isset($this->addedObjectsPositions[$this->getHash($object)])
                && isset($this->removedObjectsPositions[$this->getHash($object)])
                && ($this->addedObjectsPositions[$this->getHash($object)] !== $this->removedObjectsPositions[$this->getHash($object)]);
    }

    /**
     * @param mixed $object
     * @return int|null
     */
    public function getPosition($object): ?int
    {
        if (!isset($this->addedObjectsPositions[$this->getHash($object)])) {
            return null;
        }

        return $this->addedObjectsPositions[$this->getHash($object)];
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
    /**
     * @param	object			$object			the object
     * @return	string							the hash string
     */
	private function getHash($object): string
	{
		$hash = md5($object);
		if (get_class($object) == 'Maagit\Maagitproduct\Domain\Model\BasketItem')
		{
			$hash = 'bi'.$object->getUid();
			$hash .= (!empty($object->getUnit())) ? '_'.$object->getUnit() : '';
		}
		return $hash;
	}
}