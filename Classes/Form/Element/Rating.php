<?php
namespace Maagit\Maagitproduct\Form\Element;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			FormElement
	class:				Rating

	description:		Form element "Rating" for displaying link to send rating emails.

	created:			2020-08-31
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-08-31	Urs Maag		Initial version
						2021-12-24	Urs Maag		Remove $parameters from render() method
						2022-06-10	Urs Maag		Validate of found records with "empty",
													not "$records===FALSE" (see row 122)

------------------------------------------------------------------------------------- */


class Rating extends \TYPO3\CMS\Backend\Form\Element\AbstractFormElement
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function render(): array
	{
		// initialization
		$value = '';
		$result = $this->initializeResultArray();

		// get order uid
		$order = $this->data['databaseRow']['uid'];

		// create result value
		$pid = $this->findPageWithRatingPlugin();
		if ($pid > -1)
		{
			$href = 'https://'.$_SERVER['HTTP_HOST'].'/index.php?id='.$pid.'&tx_maagitproduct_rating[action]=send&tx_maagitproduct_rating[order]='.$order;
			$value = '<u>Send rating emails</u><br />&raquo;&raquo;&nbsp;<a href="'.$href.'" target="_blank">'.$href.'</a>';
		}
		else
		{
			$value = '<i>Error: no accesseable and visible page with plugin "maagitproduct_rating" found. Please create a page which inherits the frontend plugin "maagitproduct_rating".</i>';
		}

		// return result
		$html = '<p>'.$value.'</p>';
		$result['html'] = $html;
		return $result;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * search "maagitproduct_rating" pluing in tt_contents and return its pid
     *
	 * @param	-
	 * @return 	int											the first found pid with the plugin | -1 (not found)
     */
	protected function findPageWithRatingPlugin()
	{
		$queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool')->getQueryBuilderForTable('tt_content');	
		$queryBuilder 
			->select('tt_content.*')
			->from('tt_content')
			->join(
				'tt_content',
				'pages',
				'p',
				$queryBuilder->expr()->eq('p.uid', $queryBuilder->quoteIdentifier('tt_content.pid'))
			)
			->where(
				$queryBuilder->expr()->eq(
					'tt_content.CType', 
					$queryBuilder->createNamedParameter('maagitproduct_rating', \TYPO3\CMS\Core\Database\Connection::PARAM_STR)
			)
		);
		$records = $queryBuilder->executeQuery()->fetchAllAssociative();
		return (empty($records))?-1:$records[0]['pid'];
	}
		
		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}