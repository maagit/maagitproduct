<?php
namespace Maagit\Maagitproduct\Form\Element;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitproduct
	Package:			FormElement
	class:				Image

	description:		Form element "image" for displaying images in backend.

	created:			2020-08-31
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-08-31	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Image extends \TYPO3\CMS\Backend\Form\Element\AbstractFormElement
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function render(): array
	{
		// initialization
		$value = '';
		$fieldValue = htmlentities($this->data['parameterArray']['itemFormElValue']);
		$parameters = $this->data['parameterArray']['fieldConf']['config']['parameters'];
		$result = $this->initializeResultArray();

		// create result value
		if (!empty($fieldValue))
		{
			$height = (empty($parameters['height'])) ? htmlentities('40px') : htmlentities($parameters['height']);
			$value = '<img src="'.$fieldValue.'" height="'.$height.'" alt="" />';
			if ($parameters['showUrl'])
			{
				$value .= '<br /><a href="'.$fieldValue.'" target="_blank">'.$fieldValue.'</a>';
			}
		}
		else
		{
			$value = (empty($parameters['emptyMessage'])) ? htmlentities('<there is no image available>') : htmlentities($parameters['emptyMessage']);
		}

		// return result
		$html = '<p>'.$value.'</p>';
		$result['html'] = $html;
		return $result;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}