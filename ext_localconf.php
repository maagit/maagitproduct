<?php
defined('TYPO3') || die('Access denied.');

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagitproduct',
	'Content',
	[
		\Maagit\Maagitproduct\Controller\ContentController::class => 'index'
	],
	[
		
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagitproduct',
	'List',
	[
		\Maagit\Maagitproduct\Controller\ProductController::class => 'list,detail,filter,rating'
	],
	[
		
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagitproduct',
	'FilterCategory',
	[
		\Maagit\Maagitproduct\Controller\FilterCategoryController::class => 'show'
	],
	[
		
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagitproduct',
	'FilterType',
	[
		\Maagit\Maagitproduct\Controller\FilterTypeController::class => 'show'
	],
	[
		
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagitproduct',
	'FilterSearch',
	[
		\Maagit\Maagitproduct\Controller\FilterSearchController::class => 'show'
	],
	[
		
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagitproduct',
	'Basket',
	[
		\Maagit\Maagitproduct\Controller\BasketController::class => 'show,ajax,add,remove,change,requirements,cookie,jquery'
	],
	[
		\Maagit\Maagitproduct\Controller\BasketController::class => 'show,ajax,add,remove,change,requirements,cookie,jquery'
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagitproduct',
	'Checkout',
	[
		\Maagit\Maagitproduct\Controller\CheckoutController::class => 'login,address,delivery,payment,coupon,terms,summary,send,ajax,quantity,remove,paypal,paypalanswer,saferpay,saferpayanswer,stripe,stripeanswer,requirements,cookie,jquery,experience'
	],
	[
		\Maagit\Maagitproduct\Controller\CheckoutController::class => 'login,address,delivery,payment,coupon,terms,summary,send,ajax,quantity,remove,paypal,paypalanswer,saferpay,saferpayanswer,stripe,stripeanswer,requirements,cookie,jquery,experience'
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagitproduct',
	'Confirmation',
	[
		\Maagit\Maagitproduct\Controller\ConfirmationController::class => 'show,requirements,cookie,jquery,experience'
	],
	[
		\Maagit\Maagitproduct\Controller\ConfirmationController::class => 'show,requirements,cookie,jquery,experience'
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagitproduct',
	'Rating',
	[
		\Maagit\Maagitproduct\Controller\RatingController::class => 'show,create,confirm,accept,send,sendmail'
	],
	[
		\Maagit\Maagitproduct\Controller\RatingController::class => 'show,create,confirm,accept,send,sendmail'
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagitproduct',
	'Download',
	[
		\Maagit\Maagitproduct\Controller\DownloadController::class => 'send,sendmail'
	],
	[
		\Maagit\Maagitproduct\Controller\DownloadController::class => 'send,sendmail'
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagitproduct',
	'Experience',
	[
		\Maagit\Maagitproduct\Controller\ExperienceController::class => 'show'
	],
	[
		\Maagit\Maagitproduct\Controller\ExperienceController::class => 'show'
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// register user-defined TCA type "image"
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1598886389] = [
	'nodeName' => 'image',
	'priority' => 91,
	'class' => \Maagit\Maagitproduct\Form\Element\Image::class,
];

// register user-defined TCA type "rating"
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1598889994] = [
	'nodeName' => 'rating',
	'priority' => 92,
	'class' => \Maagit\Maagitproduct\Form\Element\Rating::class,
];

// register user-defined TCA type "download"
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1654845220] = [
	'nodeName' => 'download',
	'priority' => 93,
	'class' => \Maagit\Maagitproduct\Form\Element\Download::class,
];

// register user-defined TCA type "terms"
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1702774576] = [
	'nodeName' => 'terms',
	'priority' => 94,
	'class' => \Maagit\Maagitproduct\Form\Element\Terms::class,
];

// make select fields compatible with type "float"
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][TYPO3\CMS\Backend\Form\Element\SelectSingleElement::class] = [
    'className' => \Maagit\Maagitproduct\Backend\Form\Element\SelectSingleElement::class,
];
