.. _support:

Need Support?
=============

There are various ways to get support for this extension!

.. _support-stackoverflow:

Stackoverflow
-------------

Please use `Stackoverflow <https://stackoverflow.com/questions/tagged/maagitproduct>`_
to get the best support and tag your question with `typo3` and `maagitproduct`.

.. _support-slack:

Slack
-----

The dedicated channel `#ext-maagitproduct <https://typo3.slack.com/messages/ext-maagitproduct/>`_
of the TYPO3 Slack Workspace can be used to get in direct contact with other
users!

If you are not registered yet, follow this
`guide <https://typo3.org/community/meet/chat-slack/>`_.

.. _support-personal:

Personal support
----------------

If you need private or personal support, ask one of the developers for it.

**Be aware that this support may not be free of charge!**

.. seealso::

   Take a look at the ":ref:`sponsoring`" page for professional, customized
   support for your business case.