.. _whatDoesItDo:

What does it do?
================

This extension implements a versatile shop system based on Extbase & Fluid and using latest technologies provided by TYPO3 CMS.

**Best practise from the scratch**

-  Based on Extbase & Fluid
-  Easy to use & understand for editors
-  Using as many elements from the Core as possible, e.g. FAL and sys categories
-  Built in support for content elements
-  Integrated payment interfaces to PayPal, Stripe and Saferpay
-  Define vouchers and discounts
-  Easy definiton of delivery costs
-  Get rating for articles
-  Getting rating of shopping experience
-  and many more ...

.. pull-quote::

   It is a highly customizable framework for various shopping carts aspects -
   more than just a shopping basket! See fully configurated example shop at
   `maagIT Shop <https://shop.maagit.ch>`__.

Further links
-------------

- `Open Hub <https://www.openhub.net/p/typo3-maagitproduct>`_