..  include:: /Includes.rst.txt

.. _start:

==============
MaagIT Product
==============

:Extension key:
    maagitproduct

:Package name:
    maagit/maagitproduct

:Version:
    main

:Language:
    en

:Author:
    Urs Maag

:License:
    This document is published under the
    `Creative Commons BY 4.0 <https://creativecommons.org/licenses/by/4.0/>`__
    license.

:Rendered:
    |today|

----

This extension implements a versatile shop system based on Extbase & Fluid and using latest technologies provided by TYPO3 CMS.

----

.. seealso::

   See fully configurated example shop at `maagIT Shop <https://shop.maagit.ch>`__.

..  card-grid::
    :columns: 1
    :columns-md: 2
    :gap: 4
    :class: pb-4
    :card-height: 100

    ..  card:: :ref:`Introduction <Introduction>`

        Introduction to the extension maagitproduct, general information.

    ..  card:: :ref:`Quick start <QuickStart>`

        A quick introduction in how to use this extension.

    ..  card:: Installation

        Provide which steps should be done to install the <extension-key>
        properly. We recommend to give enough hints to kindly guide the user
        through this process.

        ..  card-footer:: :ref:`View the installation steps <installation>`
            :button-style: btn btn-secondary stretched-link

    ..  card:: Configuration

        In this section you should cover all the needed configurations to set up
        the <extension-key> correctly. Additionally you should present an
        overview about all available configurations one can set up in <extension-key>.

        ..  card-footer:: :ref:`Learn how to configure the extension <configuration>`
            :button-style: btn btn-secondary stretched-link

    ..  card:: Templates

        In this section you can describe how a user can use its custom
        templates. Additionally you can present and describe what your custom
        viewhelpers are doing. Last but not least you can provide some example
        templates.

        ..  card-footer:: :ref:`Inspect the templating <templates>`
            :button-style: btn btn-secondary stretched-link

    ..  card:: The editor section

        This section is specialised for editors. Editors find everything that is
        useful for them using the <extension-key> in this section.

        ..  card-footer:: :ref:`Discover editor specific tools <for-editors>`
            :button-style: btn btn-secondary stretched-link

    ..  card:: The developers corner

        Use this section to provide examples of code or any information
        that would be deemed relevant to a developer. For example explain how
        a certain feature was implemented.

        ..  card-footer:: :ref:`Get to know the developers corner <developer>`
            :button-style: btn btn-secondary stretched-link

    ..  card:: Troubleshooting

        Use this section for informing about troubleshooting. If the users of
        your extension encountered any problems they should be able to find
        information to solve their problems in this section.

        ..  card-footer:: :ref:`Learn how to troubleshoot <known-problems>`
            :button-style: btn btn-secondary stretched-link

..  toctree::
    :hidden:
    :titlesonly:

    Introduction/Index
    QuickStart/Index
    Installation/Index
    Configuration/Index
    Editor/Index
    Templates/Index
    Developer/Index
    KnownProblems/Index

..  Meta Menu

..  toctree::
    :hidden:

    Sitemap

