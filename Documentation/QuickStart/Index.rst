.. _quickStart:

===========
Quick start
===========

.. rst-class:: bignums-tip

#. Install this extension:

   .. code-block:: bash

      composer require maagit/maagitproduct

   .. rst-class:: horizbuttons-attention-m

   -  :ref:`Quick installation <quickInstallation>`

#. Configuration:

   -  Include the basic TypoScript
   -  Further configuration

   .. rst-class:: horizbuttons-attention-m

   -  :ref:`Quick configuration <quickConfiguration>`

#. Create initial content:

   -  Recommended page structure
   -  Create product records
   -  Insert plugins

   .. rst-class:: horizbuttons-attention-m

   -  :ref:`Recommended page structure <quickPageStructure>`
   -  :ref:`Create product records <quickProductRecords>`
   -  :ref:`Insert plugins <quickPlugins>`



.. toctree::
   :maxdepth: 5
   :titlesonly:
   :hidden:

   Installation/Index
   Configuration/Index
   CreateRecords/Index
   Templating/Index