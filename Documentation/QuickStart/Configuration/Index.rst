.. _quickConfiguration:

===================
Quick configuration
===================

Include TypoScript template
===========================

It is necessary to include at least the basic TypoScript provided by this
extension.

Go module :guilabel:`Site Management > TypoScript` and chose your root page. It should
already contain a TypoScript template record. Switch to view
:guilabel:`Edit TypoScript Record` and click on :guilabel:`Edit the whole template record`.

.. figure:: /Images/QuickStart/Configuration/IncludeTypoScript.png
   :class: with-shadow

Switch to tab :guilabel:`Advanced Options` and add the following templates from the list
to the right: :guilabel:`Maagitproduct (maagitproduct)`. It is possible to include additional
templates provided by the maagitproduct extension depending on your use case. For example
you can additionally chose :guilabel:`Maagitproduct basket on top CSS`
to use basket in a header.

Read more about possible configurations via TypoScript in the
:ref:`Reference <typoscript>` section.

Further reading
===============

*  :ref:`Global extension configuration <extensionConfiguration>`
*  :ref:`TypoScript <typoscript>`, mainly configuration for the frontend
*  :ref:`TsConfig <tsconfig>`, configuration for the backend
*  :ref:`Templating <quickTemplating>` customize the templates