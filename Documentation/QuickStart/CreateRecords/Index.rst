.. _quickContent:
.. _howToStart:

===========================
Create some initial content
===========================

.. _quickPageStructure:

Recommended page structure
==========================

Create at least the following pages:

*  "Home": Root page of the site, containing the root TypoScript template record and
   the start page content: Normal page.
*  "Article Storage": A folder to store the articles in: Folder

Usually you will also need

*  "Products": A page to display all articles on: Normal page
*  "Checkout": A single page to display the checkout process on: Normal page

Your page tree could, for example look like that:

.. code-block:: none

   Home
   ├── Some page
   ├── ...
   ├── Products
   │   └── Checkout
   ├── ...
   └── Storage
       ├── Other storage
       ├── ...
       └── Article Storage


.. _quickProductRecords:
.. _howToStartCreateRecords:

Create articles
===============

Before any articles can be shown in the frontend those need to be
created.

.. figure:: /Images/QuickStart/CreateRecords/CreateRecord.png
   :class: with-shadow

#. Go to the module :guilabel:`Web > Page`

#. Go to the "Article Storage" Folder that you created in the first step.

#. Use the button :guilabel:`Create new record` and select the
   entry :guilabel:`Product`.

#. Fill out all desired fields on all desired tabs (e.g. see :guilabel:`Product` and click :guilabel:`Save`.

   .. figure:: /Images/QuickStart/CreateRecords/TabProduct.png
      :class: with-shadow
   
More information about the records can be found here:
:ref:`news record <recordProduct>`, :ref:`category record <recordCategory>`.

.. tip::

   Articles are normal content objects and uses it's fields :guilabel:`Title`, :guilabel:`Description`, :guilabel:`Media`, and so on.

.. _quickPlugins:
.. _howToStartAddPlugin:

Add plugins: display the articles in the frontend
=================================================

A plugin is used to render a defined selection of records in the frontend.
Follow these steps to add a plugin respectively for product list and view to
the basket:

Product page
------------

#. Go to module :guilabel:`Web > Page` and to the previously created page
   "Products".

#. Add a new content element and select the entry
   :guilabel:`MaagIT Product > Maagit Product List`.

#. Switch to the tab :guilabel:`Configuration` where you can define the plugins settings.
   The most important settings are :guilabel:`Storage Pid` and :guilabel:`Recursive`.

#. Fill the field :guilabel:`Storage Pid` by selecting the :guilabel:`sysfolder` you created
   in the beginning of the tutorial.

#. Save the plugin.

Basket
------

#. Go to module :guilabel:`Web > Page` and to the previously created page
   "Products".

#. Add a new content element and select the entry
   :guilabel:`MaagIT Product > Maagit Product Basket`.

#. Switch to the tab :guilabel:`Configuration` where you can define the plugins settings.
   The most important settings are :guilabel:`Checkout page`.

#. Fill the field :guilabel:`Checkout page` by selecting the :guilabel:`Checkout page` you created
   in the beginning of the tutorial.

#. Save the plugin.

Read more about the plugin configuration in chapter :ref:`Plugin <plugin>`.

Have a look at the frontend
===========================

Load the "Products" page in the frontend and you should now see the article records
as output. A click on the "add to basket" link should add the article to the basket.
You want to change the way the articles are displayed? Have a look
at the chapter :ref:`Templating <quickTemplating>`