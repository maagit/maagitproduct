// VARIABLES ----------------------------------------------------------------------- //
var ajaxFormId = 'frmAjax';


// EVENTS ------------------------------------------------------------------------- //
function registerDownloadSendEvent() {
	if ($('.js-tx-maagitproduct-download-send').length > 0) {
		$('.js-tx-maagitproduct-download-send')[0].addEventListener('click', downloadSendClick);	
	}
}


// MAIN ---------------------------------------------------------------------------- //
function downloadSendClick() {
	var articles = $('.js-tx-maagitproduct-download-article');
	for (var i=0; i<articles.length; i++) {
		row = $(articles[i]);
		state = $(row).data('state');
		if (state == 0) {
			var success = false;
			success = downloadSendProcessStart(row);
			if (success) {
				downloadSendProcessAjax(row, function(success, row) {
					if (success) {success = downloadSendProcessEnd(row);}
				});
			}
		}
	}
}

function downloadSendProcessStart(row) {
	orderId = $(row).data('order');
	productId = $(row).data('product');
	setTableRowClasses(row, 'text-primary');
	setTableColHtml(row, 0, '<i class="fa-play-circle far"></i>');
	setTableColHtml(row, 2, 'sending mail ...');
	return true;
}

function downloadSendProcessAjax(row, callback) {
	orderId = $(row).data('order');
	productId = $(row).data('product');
	
	elements = Array();
	elements.push($('<input type="hidden" value="sendmail" />').attr('name', 'tx_maagitproduct_download[action]'));
	elements.push($('<input type="hidden" value="'+orderId+'" />').attr('name', 'tx_maagitproduct_download[orderUid]'));
	elements.push($('<input type="hidden" value="'+productId+'" />').attr('name', 'tx_maagitproduct_download[productUid]'));
	submitAjax(elements, function(success, ajaxResult) {
		setTableColHtml(row, 2, ajaxResult);
		if (!success) {
			$(row).data('state', 0)
			removeTableRowClasses(row, 'text-primary');
			setTableRowClasses(row, 'text-danger');
			setTableColHtml(row, 0, '<i class="fa-stop-circle far"></i>');
		} else {
			userError = ajaxResult.replace(/[\n\r]/g, '').trim();
			if (userError != '') {
				success = false;
				$(row).data('state', 0);
				removeTableRowClasses(row, 'text-primary');
				setTableRowClasses(row, 'text-danger');
				setTableColHtml(row, 0, '<i class="fa-stop-circle far"></i>');
				setTableColHtml(row, 2, userError);
			} else {
				$(row).data('state', 4);	
			}
		}
		callback(success, row);
	});
}

function downloadSendProcessEnd(row) {
	removeTableRowClasses(row, 'text-primary');
	setTableRowClasses(row, 'text-success');
	setTableColHtml(row, 0, '<i class="fa-check-circle far"></i>');
	setTableColHtml(row, 2, 'successfully done');
	return true;
}


// AJAX --------------------------------------------------------------------------- //
function submitAjax(elements, callback) {
	// initalize
	if (typeof(ShowWaitMe) == 'function') {ShowWaitMe();}
	clearForm(ajaxFormId);

	// add elements
	for (var i=0; i<elements.length; i++) {
		elements[i].appendTo('#'+ajaxFormId);
	}

	// add ajax specific fields
	$('<input type="hidden" value="true" />').attr('id', 'ajax').attr('name', 'ajax').appendTo('#'+ajaxFormId);
	$('<input type="hidden" value="10260" />').attr('id', 'type').attr('name', 'type').appendTo('#'+ajaxFormId);

	// execute ajax call
	ajaxCall($('#'+ajaxFormId), callback);
}

function ajaxCall(data, callback) {
	$.ajax({
		url: '',
		cache: false,
		data: data.serialize(),
		success: function (result) {
			if (typeof(HideWaitMe) == 'function') {HideWaitMe();}
			callback(true, result);			
		},
		error: function (jqXHR, textStatus, errorThrow) {
			if (typeof(HideWaitMe) == 'function') {HideWaitMe();}
			callback(false, jqXHR.responseText);
		}
	});
}

function clearForm(formId) {
	$('#'+formId).find('input,select,textarea').remove();
}


// HELPER ------------------------------------------------------------------------- //
function setTableRowClasses(row, classes) {
	var tds = $(row).find('td');
	for (var i=0; i<tds.length; i++) {
		$(tds[i]).addClass(classes);
	}
}

function removeTableRowClasses(row, classes) {
	var tds = $(row).find('td');
	for (var i=0; i<tds.length; i++) {
		$(tds[i]).removeClass(classes);
	}
}

function setTableColHtml(row, colnr, html) {
	var tds = $(row).find('td');
	if ($(tds[colnr]).length > 0) {
		$(tds[colnr]).html(html);
	}
}


// JQUERY INIT -------------------------------------------------------------------- //
jQuery(document).ready(function ($) {
	// initialization: register detail link events
	init();
	function init() {
		registerDownloadSendEvent();
	}
});