// VARIABLES ----------------------------------------------------------------------- //
var radioElements = ['rating1', 'rating2', 'rating3', 'rating4', 'rating5', 'rating6', 'rating7', 'rating8', 'rating9', 'rating10'];
var ratingInputId = 'rating';
var ajaxFormId = 'frmAjax';


// EVENTS ------------------------------------------------------------------------- //
function registerRatingStarEvents() {
	var ratingStars = $('.js-tx-maagitproduct-star');
	for (var i=0; i<ratingStars.length; i++) {(
		function(i) {
			ratingStars[i].addEventListener('mouseover', ratingStarOn);
			ratingStars[i].addEventListener('mouseout', ratingStarOff);
			ratingStars[i].addEventListener('click', ratingStarClick);
		})(i);
	}
}

function registerRatingSendEvent() {
	if ($('.js-tx-maagitproduct-rating-send').length > 0) {
		$('.js-tx-maagitproduct-rating-send')[0].addEventListener('click', ratingSendClick);	
	}
}


// MAIN ---------------------------------------------------------------------------- //
function ratingSendClick() {
	var articles = $('.js-tx-maagitproduct-rating-article');
	for (var i=0; i<articles.length; i++) {
		row = $(articles[i]);
		state = $(row).data('state');
		if (state == 0) {
			var success = false;
			success = ratingSendProcessStart(row);
			if (success) {
				ratingSendProcessAjax(row, function(success, row) {
					if (success) {success = ratingSendProcessEnd(row);}
				});
			}
		}
	}
}

function ratingSendProcessStart(row) {
	orderId = $(row).data('order');
	productId = $(row).data('product');
	setTableRowClasses(row, 'text-primary');
	setTableColHtml(row, 0, '<i class="fa-play-circle far"></i>');
	setTableColHtml(row, 2, 'sending mail ...');
	return true;
}

function ratingSendProcessAjax(row, callback) {
	orderId = $(row).data('order');
	productId = $(row).data('product');
	
	elements = Array();
	elements.push($('<input type="hidden" value="sendmail" />').attr('name', 'tx_maagitproduct_rating[action]'));
	elements.push($('<input type="hidden" value="'+orderId+'" />').attr('name', 'tx_maagitproduct_rating[orderUid]'));
	elements.push($('<input type="hidden" value="'+productId+'" />').attr('name', 'tx_maagitproduct_rating[productUid]'));
	submitAjax(elements, function(success, ajaxResult) {
		setTableColHtml(row, 2, ajaxResult);
		if (!success) {
			$(row).data('state', 0)
			removeTableRowClasses(row, 'text-primary');
			setTableRowClasses(row, 'text-danger');
			setTableColHtml(row, 0, '<i class="fa-stop-circle far"></i>');
		} else {
			userError = ajaxResult.replace(/[\n\r]/g, '').trim();
			if (userError != '') {
				success = false;
				$(row).data('state', 0);
				removeTableRowClasses(row, 'text-primary');
				setTableRowClasses(row, 'text-danger');
				setTableColHtml(row, 0, '<i class="fa-stop-circle far"></i>');
				setTableColHtml(row, 2, userError);
			} else {
				$(row).data('state', 4);	
			}
		}
		callback(success, row);
	});
}

function ratingSendProcessEnd(row) {
	removeTableRowClasses(row, 'text-primary');
	setTableRowClasses(row, 'text-success');
	setTableColHtml(row, 0, '<i class="fa-check-circle far"></i>');
	setTableColHtml(row, 2, 'successfully done');
	return true;
}

function ratingStarOn(event) {
	var radioElementName = event.target.attributes['for'].value;
	var rating = $('#'+radioElementName)[0].value;
	setStars(rating);
}

function ratingStarOff(event) {
	setStars($('#'+ratingInputId).val());
}

function ratingStarClick(event) {
	var radioElementName = event.target.attributes['for'].value;
	var rating = $('#'+radioElementName)[0].value;
	$('#'+ratingInputId).val(rating);
	setStars(rating);
}


// AJAX --------------------------------------------------------------------------- //
function submitAjax(elements, callback) {
	// initalize
	if (typeof(ShowWaitMe) == 'function') {ShowWaitMe();}
	clearForm(ajaxFormId);

	// add elements
	for (var i=0; i<elements.length; i++) {
		elements[i].appendTo('#'+ajaxFormId);
	}

	// add ajax specific fields
	$('<input type="hidden" value="true" />').attr('id', 'ajax').attr('name', 'ajax').appendTo('#'+ajaxFormId);
	$('<input type="hidden" value="10250" />').attr('id', 'type').attr('name', 'type').appendTo('#'+ajaxFormId);

	// execute ajax call
	ajaxCall($('#'+ajaxFormId), callback);
}

function ajaxCall(data, callback) {
	$.ajax({
		url: '',
		cache: false,
		data: data.serialize(),
		success: function (result) {
			if (typeof(HideWaitMe) == 'function') {HideWaitMe();}
			callback(true, result);			
		},
		error: function (jqXHR, textStatus, errorThrow) {
			if (typeof(HideWaitMe) == 'function') {HideWaitMe();}
			callback(false, jqXHR.responseText);
		}
	});
}

function clearForm(formId) {
	$('#'+formId).find('input,select,textarea').remove();
}


// HELPER ------------------------------------------------------------------------- //
function setTableRowClasses(row, classes) {
	var tds = $(row).find('td');
	for (var i=0; i<tds.length; i++) {
		$(tds[i]).addClass(classes);
	}
}

function removeTableRowClasses(row, classes) {
	var tds = $(row).find('td');
	for (var i=0; i<tds.length; i++) {
		$(tds[i]).removeClass(classes);
	}
}

function setTableColHtml(row, colnr, html) {
	var tds = $(row).find('td');
	if ($(tds[colnr]).length > 0) {
		$(tds[colnr]).html(html);
	}
}

function setStars(rating) {
	removeStars();
	var rangeTo = (rating*2);
	for (var i=0; i<rangeTo; i++) {
		$('label[for="'+radioElements[i]+'"]').addClass('active');
	}	
}

function removeStars() {
	for (var i=0; i<10; i++) {
		$('label[for="'+radioElements[i]+'"]').removeClass('active');
	}	
}


// JQUERY INIT -------------------------------------------------------------------- //
jQuery(document).ready(function ($) {
	// initialization: register detail link events
	init();
	function init() {
		registerRatingSendEvent();
		registerRatingStarEvents();
	}
});