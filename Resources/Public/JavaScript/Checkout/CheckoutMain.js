jQuery(document).ready(function ($) {
	var checkoutForm = $('#checkoutForm');
	var checkoutFormAction = $('#formAction');
	var productUidInput = $('#productUid');
	var productUnitInput = $('#productUnit');
	var productQuantityInput = $('#productQuantity');
	var basketFormQuantity = $('#basketQuantity');
	var actionInput = $('#checkoutAction');
	var ajaxActionInput = $('#checkoutAjaxAction');
	var typeNumInput = $('#checkoutTypeNum');
	var links = $('.tx-maagitproduct-checkout_link');
	var headers = document.getElementsByClassName('tx-maagitproduct-checkout_header_link');
	var ajaxTypeNum = $('#checkoutAjaxTypeNum').val();
	var basketRemoveBtns = document.getElementsByClassName('js-tx-maagitproduct-checkout-remove-from-basket');
	var basketSelectItems = document.getElementsByClassName('js-tx-maagitproduct-checkout-change-from-basket');
	var requiredBillingAddressFields = '';

	initBasketEvents();
		
	function initBasketEvents() {
		// register "continue" links
		for (var i=0; i<links.length; i++) {(
			function(i) {
				links[i].addEventListener('click', doNext);
			})(i);
		}
		
		// register "step header" links
		registerHeaderClickEvents();
		
		// register "change delivery" events
		$('input:radio[name="tx_maagitproduct_checkout[checkout][delivery][deliveryMethod]"]').change(function() {
			submitAjax();
		});
		
		// register "change payment" events
		$('input:radio[name="tx_maagitproduct_checkout[checkout][payment][paymentMethod]"]').change(function() {
			document.getElementById('paymentError').value = '';
			submitAjax();
		});
		
		// register "basket remove" events
		registerBasketRemoveEvents();
		
		// register "basket change quantity" events
		registerBasketChangeEvents();
		
		// handle "key enter" events
		$('body').keypress(function(event) {
			if (event.which == 13) {
				if (event.target.type != 'textarea')
				{
					event.preventDefault();
					if ($('.tx-maagitproduct-checkout_link').attr('data-action')!='undefined' && $('.tx-maagitproduct-checkout_link').attr('data-action')!='send') {
						$('.tx-maagitproduct-checkout_link')[0].click();	
					}
				}
			}
		});

		// register "login" selection events
		$('#checkout-login-user, #checkout-login-password').keyup(function(event) {
			if (event.target.value != '') {
				$('#checkout-login-guest').prop('checked', false);
			}
		});

		// register "guest" selection event
		$('#checkout-login-guest').change(function(event) {
			checkoutChangeLogin();
		});

		// register "logout button" event
		$('#checkout-login-logout').click(function(event) {
			$('#checkout-login-logintype').val('logout');
		});

		// handle logout settings
		checkoutChangeLogin();

		// register "billing address" selection event
		$('#checkout-address-bill').change(function(event) {
			checkoutChangeBillingAddress();
		});
		
		// get required billing address fields
		$('#checkout-address-bill-section .form-control').each(function(index, obj) {
	    	if ($('#'+obj.id).prop('required')) {
	    		requiredBillingAddressFields = requiredBillingAddressFields+'x;x'+obj.id+'x;x';
	    	}
		});

		// handle billing address settings
		checkoutChangeBillingAddress();

		// register "coupon" selection events
		$('#checkout-coupon-code').keyup(function(event) {
			if (event.target.value != '') {
				$('#couponChanged').val('true');
			}
		});
		
		// register "terms" selection events
		$('#checkout-terms-accepted').click(function(event) {
			$('#termsChanged').val('true');
		});

		// handle "sale experience" dialog on summary
		showExperienceRating();
		
		// recalculate min order value
		recalculateMinorder('true');

		// add scrollItem Icons, if needed
		showScrollItem();
	};
	
	// register "header link click events"
	function registerHeaderClickEvents() {
		for (var i=0; i<headers.length; i++) {
			headers[i].addEventListener('click', gotoStep);
		}
	}
	
	// register "basket remove events"
	function registerBasketRemoveEvents() {
		for (var i=0; i<basketRemoveBtns.length; i++) {
			basketRemoveBtns[i].addEventListener('click', removeFromBasket);
		}	
	}
	
	// register "basket change quantity events"
	function registerBasketChangeEvents() {
		for (var i=0; i<basketSelectItems.length; i++) {
			basketSelectItems[i].addEventListener('change', checkoutChangeQuantity);
		}
	}	
	
	// remove product from basket
	function removeFromBasket(event) {
		event.preventDefault();
		if ($.fn.button.noConflict) {
			$.fn.button.noConflict();
		}
		$('<div></div>').appendTo('body')
	    	.html('<div>Sie wollen das Produkt «' + event.target.attributes['data-header'].value.trim() + '» aus dem Warenkorb entfernen. Sind Sie sicher?</div>')
	    	.dialog({
				modal: true,
				title: 'Artikel löschen',
				zIndex: 10000,
				autoOpen: true,
				width: 'auto',
				resizable: false,
				buttons: {
					Ja: function() {
						$(this).remove();
						if (typeof(ShowWaitMe) == 'function') {ShowWaitMe();}
						checkoutFormAction.val('remove');
						typeNumInput.val('');
						productUidInput.val(event.target.attributes['data-product'].value);
						productUnitInput.val(event.target.attributes['data-unit'].value);
						checkoutForm.submit();
					},
					Nein: function() {
						$(this).remove();
					}
				},
				Schliessen: function(event, ui) {
					$(this).remove();
				}
			});
	}
	
	// change quantity in basket
	function checkoutChangeQuantity(event) {
		event.preventDefault();
		if (typeof(ShowWaitMe) == 'function') {ShowWaitMe();}
		checkoutFormAction.val('quantity');
		typeNumInput.val('');
		productUidInput.val(event.target.attributes['data-product'].value);
		productUnitInput.val(event.target.attributes['data-unit'].value);
		productQuantityInput.val(event.target.value);
		checkoutForm.submit();
	};
	
	// make client validation (html5) and go to next step, if data are valid
	function doNext(event) {
		event.preventDefault();
		if (!checkoutForm[0].checkValidity()) {                
			checkoutForm[0].reportValidity();
			return false;
   		}
		submitForm(event.target.attributes['data-current'].value, event.target.attributes['data-action'].value, true, true);
	}
	
	// go to given step (without data validation)
	function gotoStep(event) {
		event.preventDefault();
		submitForm(event.target.attributes['data-current'].value, event.target.attributes['data-action'].value, true, checkoutForm[0].checkValidity());
	};
	
	// change properties of login step, based on given settings
	function checkoutChangeLogin() {
		if ($('#checkout-login-guest').prop('checked')) {
			$('#checkout-login-logintype').val('logout');
			$('#checkout-login-user').val('');
			$('#checkout-login-user').prop('required', false);
			$('#checkout-login-password').prop('required', false);
			$('#checkout-login-password').val('');
		} else {
			if ($('#checkout-login-password').length) {
				$('#checkout-login-logintype').val('login');
			} else {
				$('#checkout-login-logintype').val('');	
			}
			$('#checkout-login-user').prop('required', true);
			$('#checkout-login-password').prop('required', true);
		}
	}

	// change layout of address step, based on given settings
	function checkoutChangeBillingAddress() {
		if ($('#checkout-address-bill') == null) {
			return;
		}
		$('#checkout-address-bill-section .form-control').each(function(index, obj) {
			if (requiredBillingAddressFields.indexOf('x;x'+obj.id+'x;x') > -1) {
				$('#'+obj.id).prop('required', $('#checkout-address-bill').prop('checked'));
			}
		});
		if ($('#checkout-address-bill').prop('checked')) {
			$('#checkout-address-bill-section').css('display', 'block');
			$('#checkout-address-delivery').css('display', 'block');
		} else {
			$('#checkout-address-bill-section').css('display', 'none');
			$('#checkout-address-delivery').css('display', 'none');
		}
	}

	// submit form
	function submitForm(dataCurrent, dataAction, isVisited, isValid) {
		checkoutForm.attr('action', checkoutForm.attr('action') + '#' + dataAction);
		actionInput.val(dataAction);
		checkoutFormAction.val(dataAction);
		typeNumInput.val('');
		$('#'+dataCurrent+'Visited').val((isVisited)?1:0);
		$('#'+dataCurrent+'Valid').val((isValid)?1:0);
		if (typeof(ShowWaitMe) == 'function') {ShowWaitMe();}
		checkoutForm.submit();
	}
	
	// submit form per ajax
	function submitAjax(event) {
		if (event != null) {event.preventDefault()};
		if (typeof(ShowWaitMe) == 'function') {ShowWaitMe();}
		ajaxActionInput.val('ajax');
		checkoutFormAction.val('ajax');
		typeNumInput.val(ajaxTypeNum);
		checkoutAjaxCall(checkoutForm);
	}
	
	function checkoutAjaxCall(data) {
		// clear referrer arguments (perhaps is too long for the ajax request)
		$('input[name="tx_maagitproduct_checkout\[__referrer\]\[arguments\]"]')[0].value = '';
		// make ajax request
		$.ajax({
			url: 'index.php?ajax',
			cache: false,
			data: data.serialize(),
			success: function (result) {
				typeNumInput.val('');
				ajaxActionInput.val('');
				if (typeof(HideWaitMe) == 'function') {HideWaitMe();}
				var res = result.split('/xxxoooxxx/');
				var properties = JSON.parse(res[1].replace(/&quot;/g,'"'))
				changeProperties(properties);
				$('#checkoutSummary')[0].innerHTML = res[2];
				registerHeaderClickEvents();
				registerBasketRemoveEvents();
				registerBasketChangeEvents();
				showScrollItem();
			},
			error: function (jqXHR, textStatus, errorThrow) {
				typeNumInput.val('');
				ajaxActionInput.val('');
				if (typeof(HideWaitMe) == 'function') {HideWaitMe();}
				$('#checkoutSummary')[0].innerHTML = jqXHR.responseText;
				registerHeaderClickEvents();
				registerBasketRemoveEvents();
				registerBasketChangeEvents();
				showScrollItem();
			}
		});
	}
	
	function changeProperties(properties) {
		if ($('#paymentVisited').val() && properties.paymentMethod == null) {
			$('#paymentValid').val(0);
			$('input[name="tx_maagitproduct_checkout\\[checkout\\]\\[payment\\]\\[paymentMethod\\]"]').val('');
			if ($('span[data-action="payment"]')[0] != null) {
				$('span[data-action="payment"]')[0].innerHTML = '&#10008;';
			}
		}
		if ($('#addressValid').val()==0 || $('#deliveryValid').val()==0 || $('#paymentValid').val()==0) {
			if ($('#summaryVisited').val()!=0) {
				if ($('span[data-action="summary"]')[0] != null) {
					$('span[data-action="summary"]')[0].innerHTML = '&#10008;';	
				}	
			}
		}
	}
	
	function showExperienceRating() {
		if ($('#checkoutExperience').length) {
			$('#checkoutExperience').modal('show');
		}
	}

	function recalculateMinorder(init) {
		if (typeof(eval(basketMinOrderValueFunction)) == 'function') {
			if ($('.tx-maagitproduct-basket__minordersign').length > 0) {
				if ($('#paramCheckoutAmount').length > 0) {
					if (init == null) {init = 'false';}
					if ($('#paramCheckoutAmount').length > 0) {amount = $('#paramCheckoutAmount').text();} else {amount = 0;}
					if ($('#paramCheckoutMinOrderValue').length > 0) {minvalue = $('#paramCheckoutMinOrderValue').text();} else {minvalue = 0;}
					if ($('#paramCheckoutMinOrderValueReached').length > 0) {reached = $('#paramCheckoutMinOrderValueReached').text();} else {reached = 0;}
					eval(basketMinOrderValueFunction+'('+amount+', '+minvalue+', '+reached+', '+init+')');
				}
			}
		}
	}

	function showScrollItem() {
		containers = document.getElementsByClassName('scrollItem');
		for (var i=0; i<containers.length; i++) {
			element = $(containers[i]);
			frame = element.parent();
			content = frame.find('>:first-child');
			if (content.get(0).scrollHeight > frame.height()) {
				element.css('display', 'block');
				element.on('click', {content: content}, function(event) {
					event.data.content.stop().animate({
						scrollTop: event.data.content.offset().top
			        }, 1000);
				});
			} else {
				element.css('display', 'none');
			}
		}
	}
});