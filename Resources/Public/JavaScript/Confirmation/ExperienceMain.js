// VARIABLES ----------------------------------------------------------------------- //
var radioElements = ['rating1', 'rating2', 'rating3', 'rating4', 'rating5', 'rating6', 'rating7', 'rating8', 'rating9', 'rating10'];
var ratingInputId = 'rating';
var formId = 'frmExperience';


// EVENTS ------------------------------------------------------------------------- //
function registerRatingStarEvents() {
	var ratingStars = $('.js-tx-maagitproduct-star');
	for (var i=0; i<ratingStars.length; i++) {(
		function(i) {
			ratingStars[i].addEventListener('mouseover', ratingStarOn);
			ratingStars[i].addEventListener('mouseout', ratingStarOff);
			ratingStars[i].addEventListener('click', ratingStarClick);
		})(i);
	}
}

function registerExperienceSendEvent() {
	if ($('.js-tx-maagitproduct-experience-send').length > 0) {
		$('.js-tx-maagitproduct-experience-send')[0].addEventListener('click', experienceSendClick);	
	}
}


// MAIN ---------------------------------------------------------------------------- //
function experienceSendClick(event) {
	event.preventDefault();
	if (!$('#'+formId)[0].checkValidity()) {                
		$('#'+formId)[0].reportValidity();
		return false;
	}
	if (typeof(ShowWaitMe) == 'function') {ShowWaitMe();}
	$('<input type="hidden" value="true" />').attr('id', 'ajax').attr('name', 'ajax').appendTo('#'+formId);
	$('<input type="hidden" value="experience" />').attr('id', 'action').attr('name', 'tx_maagitproduct_confirmation[action]').appendTo('#'+formId);
	$('<input type="hidden" value="427592" />').attr('id', 'type').attr('name', 'type').appendTo('#'+formId);
	ajaxCall($('#'+formId), function(success, result) {
		$('#experience-detail')[0].outerHTML = result;
	});
}

function ratingStarOn(event) {
	var radioElementName = event.target.attributes['for'].value;
	var rating = $('#'+radioElementName)[0].value;
	setStars(rating);
}

function ratingStarOff(event) {
	setStars($('#'+ratingInputId).val());
}

function ratingStarClick(event) {
	var radioElementName = event.target.attributes['for'].value;
	var rating = $('#'+radioElementName)[0].value;
	$('#'+ratingInputId).val(rating);
	setStars(rating);
}


// AJAX --------------------------------------------------------------------------- //
function ajaxCall(data, callback) {
	$.ajax({
		url: '',
		cache: false,
		data: data.serialize(),
		success: function (result) {
			if (typeof(HideWaitMe) == 'function') {HideWaitMe();}
			callback(true, result);			
		},
		error: function (jqXHR, textStatus, errorThrow) {
			if (typeof(HideWaitMe) == 'function') {HideWaitMe();}
			callback(false, jqXHR.responseText);
		}
	});
}


// HELPER ------------------------------------------------------------------------- //
function setStars(rating) {
	removeStars();
	var rangeTo = (rating*2);
	for (var i=0; i<rangeTo; i++) {
		$('label[for="'+radioElements[i]+'"]').addClass('active');
	}	
}

function removeStars() {
	for (var i=0; i<10; i++) {
		$('label[for="'+radioElements[i]+'"]').removeClass('active');
	}	
}


// JQUERY INIT -------------------------------------------------------------------- //
jQuery(document).ready(function ($) {
	// initialization: register detail link events
	init();
	function init() {
		registerExperienceSendEvent();
		registerRatingStarEvents();
	}
});