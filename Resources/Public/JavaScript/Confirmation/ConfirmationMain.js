jQuery(document).ready(function ($) {
	// handle "sale experience" dialog on summary
	showExperienceRating();

	// add scrollItem Icons, if needed
	showScrollItem();

	function showExperienceRating() {
		if ($('#checkoutExperience').length) {
			$('#checkoutExperience').modal('show');
		}
	}

	function showScrollItem() {
		containers = document.getElementsByClassName('scrollItem');
		for (var i=0; i<containers.length; i++) {
			element = $(containers[i]);
			frame = element.parent();
			content = frame.find('>:first-child');
			if (content.get(0).scrollHeight > frame.height()) {
				element.css('display', 'block');
				element.on('click', {content: content}, function(event) {
					event.data.content.stop().animate({
						scrollTop: event.data.content.offset().top
			        }, 1000);
				});
			} else {
				element.css('display', 'none');
			}
		}
	}
});