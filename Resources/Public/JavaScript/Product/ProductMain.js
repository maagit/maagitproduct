// ====================================================================================================
//
// FOLLOWING VARIABLES CAN BE USED IN GLOBAL JAVASCRIPT AREA
//
//
// Variable name						type		default		description
// ----------------------------------------------------------------------------------------------------
// productAddBasketDelay				int			0			delay in miliseconds to add product
//																to basket (ajax call)
//																the basket symbol
// doProductAnimation					bool		false		enable product flying to basket
// productAnimationBasketElement		string		''			Jquery path to basket item (use
//																"$(target)" to select current event
//																element)
// productAnimationImagePath			string		''			Jquery path to product image (use
//																"$(target)" to select current event
//																element)
// productAnimationImageOpacity			string		'0.5'		CSS opacity value for flying image
// productAnimationImageZIndex			string		'9000'		CSS z-index for flying image
// productAnimationImageFromOffsetTop	int			0			Flying image starting offset top
// productAnimationImageFromOffsetLeft	int			0			Flying image starting offset left
// productAnimationImageToOffsetTop		int			0			Flying image ending offset top
// productAnimationImageToOffsetLeft	int			0			Flying image ending offset left
// productAnimationImageToScale			int			40			Flying image ending scale in percent
// productAnimationImageHideOffsetTop	int			0			Flying image hiding offset top
// productAnimationImageHideOffsetLeft	int			0			Flying image hiding offset left
// productAnimationDuration				int			1000		Flying image duration in miliseconds
// productAnimationBasketShake			bool		false		Shake basket item
// productAnimationBasketShakeTimes		int			2			Shake basket item x times
// productAnimationBasketShakeDuration	int			200			Shaking duration in miliseconds
// productAnimationBasketShakeDelay		int			1500		Shake basket delay in miliseconds
//
// ====================================================================================================


// VARIABLES ----------------------------------------------------------------------- //
// get global javascript settings or set initial value
if (typeof productAddBasketDelay == 'undefined') {productAddBasketDelay = 0;}
if (typeof doProductAnimation == 'undefined') {doProductAnimation = false;}
if (typeof productAnimationBasketElement == 'undefined') {productAnimationBasketElement = '';}
if (typeof productAnimationImagePath == 'undefined') {productAnimationImagePath = '';}
if (typeof productAnimationImageOpacity == 'undefined') {productAnimationImageOpacity = '0.5';}
if (typeof productAnimationImageZIndex == 'undefined') {productAnimationImageZIndex = '9000';}
if (typeof productAnimationImageFromOffsetTop == 'undefined') {productAnimationImageFromOffsetTop = 0;}
if (typeof productAnimationImageFromOffsetLeft == 'undefined') {productAnimationImageFromOffsetLeft = 0;}
if (typeof productAnimationImageToOffsetTop == 'undefined') {productAnimationImageToOffsetTop = 0;}
if (typeof productAnimationImageToOffsetLeft == 'undefined') {productAnimationImageToOffsetLeft = 0;}
if (typeof productAnimationImageToScale == 'undefined') {productAnimationImageToScale = 40;}
if (typeof productAnimationImageHideOffsetTop == 'undefined') {productAnimationImageHideOffsetTop = 0;}
if (typeof productAnimationImageHideOffsetLeft == 'undefined') {productAnimationImageHideOffsetLeft = 0;}
if (typeof productAnimationDuration == 'undefined') {productAnimationDuration = 1000;}
if (typeof productAnimationBasketShake == 'undefined') {productAnimationBasketShake = false;}
if (typeof productAnimationBasketShakeTimes == 'undefined') {productAnimationBasketShakeTimes = 2;}
if (typeof productAnimationBasketShakeDuration == 'undefined') {productAnimationBasketShakeDuration = 200;}
if (typeof productAnimationBasketShakeDelay == 'undefined') {productAnimationBasketShakeDelay = 1500;}

// local variables
var productActionParameter = 'tx_maagitproduct_list[action]';
var productIdParameter = 'tx_maagitproduct_list[uid]';
var searchParameter = 'tx_maagitproduct_list[search]';
var categorySelectionParameter = 'tx_maagitproduct_list[categories]';
var typeSelectionParameter = 'tx_maagitproduct_list[types]';
var sortParameter = 'tx_maagitproduct_list[order]';
var ajaxFormId = 'frmAjax';


// EVENTS ------------------------------------------------------------------------- //
function registerProductDetailLinkEvents() {
	var productLinkItems = $('.js-tx-maagitproduct-show-detail');
	for (var i=0; i<productLinkItems.length; i++) {(
		function(i) {
			productLinkItems[i].addEventListener('click', productDetailClick);
		})(i);
	}
}

function registerProductAddClickEvents() {
	var basketAddBtns = $('.js-tx-maagitproduct-add-to-basket');
	for (var i=0; i<basketAddBtns.length; i++) {(
		function(i) {
			basketAddBtns[i].addEventListener('click', addProduct);
		})(i);
	}
}

function registerChangeUnitRadios() {
	var basketUnitRadios = $('.js-tx-maagitproduct-unit');
	for (var i=0; i<basketUnitRadios.length; i++) {(
		function(i) {
			basketUnitRadios[i].addEventListener('click', changeProductUnit);
		})(i);
	}
}

function registerProductRatingLinkEvents() {
	var ratingLinkItems = $('.js-tx-maagitproduct-rating');
	for (var i=0; i<ratingLinkItems.length; i++) {(
		function(i) {
			ratingLinkItems[i].addEventListener('click', ratingDetailClick);
		})(i);
	}
}

function registerSearchClickEvents() {
	if ($('#frmMaagitproductSearch').length > 0) {
		$('#frmMaagitproductSearch')[0].addEventListener('submit', searchSubmit);	
	}
}

function registerSortChangeEvents() {
	if ($('#maagitproduct-sort').length > 0) {
		$('#maagitproduct-sort')[0].addEventListener('change', sortChange);	
	}
}

function registerCategoryClickEvents() {
	var categoryItemAll = document.getElementsByClassName('category-all');
	var categoryItems = document.getElementsByClassName('category-item');
	if (categoryItemAll.length > 0) {
		categoryItemAll[0].addEventListener('click', filterCategoryAllClick);	
	}
	for (var i=0; i<categoryItems.length; i++) {(
		function(i) {
			if (location.href.indexOf('tx_maagitproduct_list[categories]') == -1) {
				categoryItems[i].checked = true;	
			}
			categoryItems[i].addEventListener('click', filterSubmit);
		})(i);
	}
}

function registerTypeClickEvents() {
	var typeItemAll = document.getElementsByClassName('type-all');
	var typeItems = document.getElementsByClassName('type-item');
	if (typeItemAll.length > 0) {
		typeItemAll[0].addEventListener('click', filterTypeAllClick);	
	}
	for (var i=0; i<typeItems.length; i++) {(
		function(i) {
			if (location.href.indexOf('tx_maagitproduct_list[types]') == -1) {
				typeItems[i].checked = true;
			}
			typeItems[i].addEventListener('click', filterSubmit);
		})(i);
	}
}


// INITIALIZATION ------------------------------------------------------------------ //
function initUnitRadios() {
	var basketUnitRadios = $('.js-tx-maagitproduct-unit');
	for (var i=0; i<basketUnitRadios.length; i++) {
		if (basketUnitRadios[i].checked) {
			productId = basketUnitRadios[i].attributes['data-product'].value;
			unitPrice = basketUnitRadios[i].attributes['data-price'].value.trim();
			if (basketUnitRadios[i].attributes['data-stock'] != null && basketUnitRadios[i].attributes['data-stock'] != 'undefined') {
				unitStock = basketUnitRadios[i].attributes['data-stock'].value.trim();	
			}
			$('.tx-maagitproduct-checkout_unitPrice_'+productId)[0].innerText = unitPrice;
			if ($('.tx-maagitproduct-checkout_unitStock_'+productId).length > 0) {
				$('.tx-maagitproduct-checkout_unitStock_'+productId)[0].innerText = unitStock;
				setStockClasses($('.tx-maagitproduct-checkout_unitStock_'+productId));
			}
			if ($('.tx-maagitproduct-checkout_detailUnitPrice_'+productId).length > 0) {
				$('.tx-maagitproduct-checkout_detailUnitPrice_'+productId)[0].innerText = unitPrice;
			}
			if ($('.tx-maagitproduct-checkout_detailUnitStock_'+productId).length > 0) {
				$('.tx-maagitproduct-checkout_detailUnitStock_'+productId)[0].innerText = unitStock;
				setStockClasses($('.tx-maagitproduct-checkout_detailUnitStock_'+productId));
			}
			if (basketUnitRadios[i].id.substr(0, 5) == 'unit_') {
				setUnitSelection(productId);
			}
		}
	}
}


// SEARCH -------------------------------------------------------------------------- //
function searchSubmit(event) {
	event.preventDefault();
	$('#maagitproduct-search')[0].blur();
	filterSubmit();
}


// FILTER CATEGORY ----------------------------------------------------------------- //
function filterCategoryAllClick(event) {
	var categoryItemAll = document.getElementsByClassName('category-all');
	categoryItemAll[0].checked = false;
	$('.category-item').each(function(index, checkbox) {
		checkbox.checked = true;
	});
	filterSubmit();
}

function getCategorySelection() {
	selection = '';
	$('.category-item').each(function(index, checkbox) {
		if (checkbox.checked) {
			if (selection != '') {selection = selection + ',';}
			selection = selection + checkbox.value;
		}
	});
	return escapeHtml(selection);
}


// FILTER TYPE --------------------------------------------------------------------- //
function filterTypeAllClick(event) {
	var typeItemAll = document.getElementsByClassName('type-all');
	typeItemAll[0].checked = false;
	$('.type-item').each(function(index, checkbox) {
		checkbox.checked = true;
	});
	filterSubmit();
}

function getTypeSelection() {
	selection = '';
	$('.type-item').each(function(index, checkbox) {
		if (checkbox.checked) {
			if (selection != '') {selection = selection + ',';}
			selection = selection + checkbox.value;
		}
	});
	return escapeHtml(selection);
}


// SORT ---------------------------------------------------------------------------- //
function sortChange(event) {
	event.preventDefault();
	filterSubmit();
}


// MAIN --------------------------------------------------------------------------- //
function productDetailClick(event) {
	event.preventDefault();
	productUid = event.target.attributes['data-product'].value;
	elements = Array();
	elements.push($('<input type="hidden" value="detail" />').attr('name', productActionParameter));
	elements.push($('<input type="hidden" value="'+productUid+'" />').attr('name', productIdParameter));
	if (typeof maagitproduct_showDetail === 'function') {
		submitAjax(elements, function(ajaxResult) {
			unitId = getSelectedUnit(productUid);
			linkArguments = $('#'+ajaxFormId).serialize().substr(0, $('#'+ajaxFormId).serialize().indexOf('&ajax'));
			ajaxArguments = $('#'+ajaxFormId).serialize();
			maagitproduct_showDetail(ajaxResult, productUid, unitId, linkArguments, ajaxArguments);
		});
	}
	else
	{
		alert ('Please implement a function "maagitproduct_showDetail(view, productUid, unitId, linkArguments, ajaxArguments) { ... }" to process the detail view.');				
	}
}

function addProduct(event) {
	event.preventDefault();
	if ($('#productDetail').length > 0) {
		$('#productDetail').modal('hide');
	}
	flyToBasket(event.target);
	setTimeout(function () {
		$('#basketForm').attr('action', 'add');
		$('#basketAction').val('add');
		$('#basketUid').val(event.target.attributes['data-product'].value);
		$('#basketUnit').val(getSelectedUnit(event.target.attributes['data-product'].value));
		$('#basketForm').submit();
	}, productAddBasketDelay);
}

function changeProductUnit(event) {
	selectedValue = event.target.value;
	productId = event.target.attributes['data-product'].value;
	unitPrice = event.target.attributes['data-price'].value.trim();
	if (event.target.attributes['data-stock'] != null && event.target.attributes['data-stock'] != 'undefined') {
		unitStock = event.target.attributes['data-stock'].value.trim();
	}
	$('.tx-maagitproduct-checkout_unitPrice_'+productId)[0].innerText = unitPrice;
	if ($('.tx-maagitproduct-checkout_unitStock_'+productId).length > 0) {
		$('.tx-maagitproduct-checkout_unitStock_'+productId)[0].innerText = unitStock;
		setStockClasses($('.tx-maagitproduct-checkout_unitStock_'+productId));
	}
	if ($('.tx-maagitproduct-checkout_detailUnitPrice_'+productId).length > 0) {
		$('.tx-maagitproduct-checkout_detailUnitPrice_'+productId)[0].innerText = unitPrice;
	}
	if ($('.tx-maagitproduct-checkout_detailUnitStock_'+productId).length > 0) {
		$('.tx-maagitproduct-checkout_detailUnitStock_'+productId)[0].innerText = unitStock;
		setStockClasses($('.tx-maagitproduct-checkout_detailUnitStock_'+productId));
	}
	elementId = 'unit_'+productId+'_'+selectedValue;
	if ($('#'+elementId).length > 0) {
		$('input[name="unit_'+productId+'"]').each(function() {
			$('#'+elementId).prop('checked', false);
			$(this).removeAttr('checked');
		});
		$('#'+elementId).prop('checked', true);
		$('#'+elementId).attr('checked', 'checked');
	}
}

function ratingDetailClick(event) {
	event.preventDefault();
	productUid = event.target.attributes['data-product'].value;
	elements = Array();
	elements.push($('<input type="hidden" value="rating" />').attr('name', productActionParameter));
	elements.push($('<input type="hidden" value="'+productUid+'" />').attr('name', productIdParameter));
	if (typeof maagitproduct_showRating === 'function') {
		submitAjax(elements, function(ajaxResult) {
			linkArguments = $('#'+ajaxFormId).serialize().substr(0, $('#'+ajaxFormId).serialize().indexOf('&ajax'));
			ajaxArguments = $('#'+ajaxFormId).serialize();
			maagitproduct_showRating(ajaxResult, productUid, linkArguments, ajaxArguments);
		});
	}
	else
	{
		alert ('Please implement a function "maagitproduct_showRating(view, productUid, linkArguments, ajaxArguments) { ... }" to process the rating view.');				
	}
}

function filterSubmit() {
	elements = Array();
	elements.push($('<input type="hidden" value="filter" />').attr('name', productActionParameter));
	if ($('.category-item').length > 0) {
		elements.push($('<input type="hidden" value="'+getCategorySelection()+'" />').attr('name', categorySelectionParameter));	
	}
	if ($('.type-item').length > 0) {
		elements.push($('<input type="hidden" value="'+getTypeSelection()+'" />').attr('name', typeSelectionParameter));
	}
	if ($('#maagitproduct-search').length > 0) {
		elements.push($('<input type="hidden" value="'+escapeHtml($('#maagitproduct-search')[0].value)+'" />').attr('name', searchParameter));
	}
	if ($('#maagitproduct-sort').length > 0) {
		if ($('#maagitproduct-sort')[0].value != '') {
			elements.push($('<input type="hidden" value="'+$('#maagitproduct-sort')[0].value+'" />').attr('name', sortParameter));	
		}
	}

	submitAjax(elements, function(ajaxResult) {
		$('#products')[0].innerHTML = ajaxResult;
		registerProductDetailLinkEvents();
		registerProductAddClickEvents();
		registerChangeUnitRadios();
		initUnitRadios();
	});
}

function flyToBasket(target) {
	if (doProductAnimation) {
		var cart = eval(productAnimationBasketElement);
		var imgtodrag = eval(productAnimationImagePath);
		if (imgtodrag) {
			var imgclone = imgtodrag.clone()
				.offset({
					top: imgtodrag.offset().top + productAnimationImageFromOffsetTop,
					left: imgtodrag.offset().left + productAnimationImageFromOffsetLeft
				})
				.css({
					'opacity': productAnimationImageOpacity,
					'position': 'absolute',
					'height': imgtodrag.height()+'px',
					'width': imgtodrag.width()+'px',
					'z-index': productAnimationImageZIndex
				})
				.appendTo($('body'))
				.animate({
					'top': cart.offset().top + productAnimationImageToOffsetTop,
					'left': cart.offset().left + productAnimationImageToOffsetLeft,
					'width': (imgtodrag.width() / 100 * productAnimationImageToScale)+'px',
					'height': (imgtodrag.height() / 100 * productAnimationImageToScale)+'px'
				}, productAnimationDuration, 'easeInOutExpo');
				if (productAnimationBasketShake) {
					setTimeout(function () {
						cart.effect("shake", {
							times: productAnimationBasketShakeTimes
						}, productAnimationBasketShakeDuration);
					}, productAnimationBasketShakeDelay);
				}
				imgclone.animate({
					'top': cart.offset().top + productAnimationImageHideOffsetTop,
					'left': cart.offset().left + productAnimationImageHideOffsetLeft,
					'width': 0,
					'height': 0
				}, function () {
					$(this).detach()
				});
		}
	}
}

function setStockClasses(obj) {
	if (obj != null) {
		if (!isNaN(obj.text())) {
			stockAmount = obj.text();
			obj.removeClass('minus');
			obj.removeClass('plus');
			if (stockAmount > 0) {
				obj.addClass('plus');
			}
			if (stockAmount <=0) {
				obj.addClass('minus');
			}
		}
	}
}


// AJAX --------------------------------------------------------------------------- //
function submitAjax(elements, callback) {
	// initalize
	if (typeof(ShowWaitMe) == 'function') {ShowWaitMe();}
	clearForm(ajaxFormId);

	// add elements
	for (var i=0; i<elements.length; i++) {
		elements[i].appendTo('#'+ajaxFormId);
	}

	// add ajax specific fields
	$('<input type="hidden" value="true" />').attr('id', 'ajax').attr('name', 'ajax').appendTo('#'+ajaxFormId);
	$('<input type="hidden" value="10240" />').attr('id', 'type').attr('name', 'type').appendTo('#'+ajaxFormId);

	// execute ajax call
	ajaxCall($('#'+ajaxFormId), callback);
}

function ajaxCall(data, callback) {
	$.ajax({
		url: '',
		cache: false,
		data: data.serialize(),
		success: function (result) {
			if (typeof(HideWaitMe) == 'function') {HideWaitMe();}
			callback(result);			
		},
		error: function (jqXHR, textStatus, errorThrow) {
			if (typeof(HideWaitMe) == 'function') {HideWaitMe();}
			callback(jqXHR.responseText);
		}
	});
}


// HELPER ------------------------------------------------------------------------- //
function collapseElement(elementObj) {
	if ($(window).width() > 992) {
		try {
			elementObj.collapse('show');
		}
		catch (e) { }
	}
	if ($(window).width() < 992) {
		try {
			elementObj.collapse('hide');
		}
		catch (e) { }
	}
}

function clearForm(formId) {
	$('#'+formId).find('input,select,textarea').remove();
}

function setUnitSelection(productId) {
	$('.js-tx-maagitproduct-unit').each(function() {
		if ($(this)[0].id.substr(0, 6+productId.length) == 'unit_'+productId+'_' && $(this)[0].attributes['data-product'].value == productId) {
			if ($(this)[0].checked) {
				id = $(this)[0].value;
				elementId = 'detail_unit_'+productId+'_'+id;
				if ($('#'+elementId).length > 0) {
					$('#'+elementId).prop('checked', true);
				}
			}
		}
	});
}

function getSelectedUnit(productId) {
	var id='';
	$('.js-tx-maagitproduct-unit').each(function() {
		if ($(this)[0].attributes['data-product'].value == productId) {
			if ($(this).attr('id').substr(0, 5) == 'unit_') {
				if ($(this)[0].checked) {
					id = $(this)[0].value
					return id;
				}
			}
		}
	});
	return id;
}

function escapeHtml(str) {
	var map = {
		'&': '&amp;',
		'<': '&lt;',
		'>': '&gt;',
		'"': '&quot;',
		"'": '&#039;'
	};
	return str.replace(/[&<>"']/g, function(m) {
		return map[m];
	});
}


// JQUERY INIT -------------------------------------------------------------------- //
var userInteractionOccured = false;

jQuery(document).ready(function ($) {
	// events on resizing the window
	$(window).resize(function() {
		if (!userInteractionOccured) {
			if ($('#collapseSearch').length > 0) {
				collapseElement($('#collapseSearch'));
			}
			if ($('#collapseCategories').length > 0) {
				collapseElement($('#collapseCategories'));
			}
			if ($('#collapseTypes').length > 0) {
				collapseElement($('#collapseTypes'));
			}
		}
	});

	// initialization: register detail link events
	init();
	function init() {
		registerProductDetailLinkEvents();
		registerProductAddClickEvents();
		registerChangeUnitRadios();
		registerProductRatingLinkEvents();
		initUnitRadios();
		registerSearchClickEvents();
		registerCategoryClickEvents();
		registerTypeClickEvents();
		registerSortChangeEvents();
		$(window).trigger('resize');
	}

	// events to check, if user has set its preferences of filter boxes
	if ($('#searchFilter').length > 0) {
		$('#searchFilter')[0].addEventListener('click', function() {userInteractionOccured = true;});	
	}
	if ($('#categoryFilter').length > 0) {
		$('#categoryFilter')[0].addEventListener('click', function() {userInteractionOccured = true;});	
	}
	if ($('#typeFilter').length > 0) {
		$('#typeFilter')[0].addEventListener('click', function() {userInteractionOccured = true;});	
	}
});