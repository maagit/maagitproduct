// ========================================================================================
//
// FOLLOWING VARIABLES CAN BE USED IN GLOBAL JAVASCRIPT AREA
//
//
// Variable name			type		default		description
// ----------------------------------------------------------------------------------------
// basketBadgeTimeout		int			500			the timeout to show the badge of
//													the basket symbol
// basketNotShowByElements	array		[]			element references as string, if a
//													given element is available on page,
//													the badge is set to hidden and click
//													event of basket is disabled
// ========================================================================================


jQuery(document).ready(function ($) {
	// get global javascript settings or set initial value
	if (typeof basketBadgeTimeout == 'undefined') {basketBadgeTimeout = 500;}
	if (typeof basketNotShowByElements == 'undefined') {basketNotShowByElements = [];}
	if (typeof basketToggleFunction == 'undefined') {basketToggleFunction = '';}
	if (typeof basketMinOrderValueFunction == 'undefined') {basketMinOrderValueFunction = '';}

	// local variables
	var basket = document.getElementsByClassName('js-tx-maagitproduct-basket');
	var ajaxForm = $('#basketForm');
	var ajaxFormAction = $('#basketAction');
	var ajaxFormUid = $('#basketUid');
	var ajaxFormQuantity = $('#basketQuantity');
	var ajaxFormUnit = $('#basketUnit');
	if (basket.length > 0) {
		var	basketRemoveBtns = document.getElementsByClassName('js-tx-maagitproduct-remove-from-basket'),
			basketContent = document.getElementsByClassName('tx-maagitproduct-basket__content')[0],
			basketBody = basket[0].getElementsByClassName('tx-maagitproduct-basket__body')[0],
  			basketList = basketBody.getElementsByTagName('ul')[0],
			basketListItems = basketList.getElementsByClassName('tx-maagitproduct-basket__product'),
  			basketSelectItems = document.getElementsByName('quantity'),
			basketCount = basket[0].getElementsByClassName('tx-maagitproduct-basket__count')[0],
			basketCountItems = basketCount.getElementsByTagName('li');
		initQuantityBadge();
		initBasketEvents();
		
		function ajaxCall(data) {
			$.ajax({
				url: 'index.php?ajax',
				cache: false,
				data: data.serialize(),
				success: function (result) {
					if (typeof(HideWaitMe) == 'function') {HideWaitMe();}
					ajaxFormUid.val('');
					ajaxFormQuantity.val('');
					ajaxFormUnit.val('');
					basketContent.innerHTML = result;
					updateBasketCount();
					if (typeof(registerChangeUnitRadios) == 'function') {registerChangeUnitRadios();}
					callMinOrderValueFunction(false);
					registerRemoveLinks();
					registerChangeQuantityEvents();
				},
				error: function (jqXHR, textStatus, errorThrow) {
					if (typeof(HideWaitMe) == 'function') {HideWaitMe();}
					ajaxFormUid.val('');
					ajaxFormQuantity.val('');
					ajaxFormUnit.val('');
					basketList.innerHTML = jqXHR.responseText;
					basketCountItems[0].innerHTML = '!';
					BasketUtil.removeClass(basket[0], 'tx-maagitproduct-basket--empty');
				}
			});
		}
		
		function initBasketEvents() {
			// register form submit event
			ajaxForm.submit(function (event) {
				event.preventDefault();
				if (typeof(ShowWaitMe) == 'function') {ShowWaitMe();}
				ajaxCall($(this));
			});
			
			// show basket, if there are already items in it
			if (!isAvailable(basketNotShowByElements)) {
				if (Number(basketCountItems[0].innerText) > 0) {
					setTimeout(function() {
						BasketUtil.removeClass(basket[0], 'tx-maagitproduct-basket--empty');
					}, basketBadgeTimeout);
				}
			}
			
			// register "remove item" links
			registerRemoveLinks();

			// register "change quantity" events
			registerChangeQuantityEvents();

			// register "toggle" basket event (open/close the basket)
			basket[0].getElementsByClassName('tx-maagitproduct-basket__trigger')[0].addEventListener('click', function(event) {
				event.preventDefault();
				toggleBasket();
			});
			
			// register general "click" event, if basket is open
			basket[0].addEventListener('click', function(event) {
				if (event.target == basket[0]) {
					// close basket when clicking on bg layer
					toggleBasket(true);
				} else if (event.target.closest('.tx-maagitproduct-basket__delete-item')) {
					// remove product from basket
					event.preventDefault();
				}
			});

			// call user defined functions
			callMinOrderValueFunction(true);
			callBasketToggleFunction(true);
		};
		
		function toggleBasket(bool) {
			// return, if no items in basket or checkout page is displaying
			if (Number(basketCountItems[0].innerText) == 0) {
				callBasketToggleFunction(false);
				return;
			}
			// return, if a "basketNotShowByElements" element is available on page
			if (isAvailable(basketNotShowByElements)) {
				callBasketToggleFunction(false);
				return;
			}
			// toggle basket visibility
			var basketIsOpen = (typeof bool === 'undefined') ? BasketUtil.hasClass(basket[0], 'tx-maagitproduct-basket--open') : bool;
			if (basketIsOpen) {
				BasketUtil.removeClass(basket[0], 'tx-maagitproduct-basket--open');
				setTimeout(function() {
					basketBody.scrollTop = 0;
					if (Number(basketCountItems[0].innerText) == 0) {
						BasketUtil.addClass(basket[0], 'tx-maagitproduct-basket--empty');
					}
				}, 500);
			} else {
				BasketUtil.addClass(basket[0], 'tx-maagitproduct-basket--open');
			}
			
			// call user defined toggle function
			callBasketToggleFunction();
		};

		function removeFromBasket(event) {
			event.preventDefault();
			ajaxForm.attr('action', 'remove');
			ajaxFormAction.val('remove');
			ajaxFormUid.val(event.target.attributes['data-product'].value);
			ajaxFormUnit.val(event.target.attributes['data-unit'].value);
			ajaxForm.submit();
		};
		
		function changeQuantity(event) {
			event.preventDefault();
			ajaxForm.attr('action', 'change');
			ajaxFormAction.val('change');
			ajaxFormUid.val(event.target.attributes['data-product'].value);
			ajaxFormUnit.val(event.target.attributes['data-unit'].value);
			ajaxFormQuantity.val(event.target.value);
			ajaxForm.submit();
		};
		
		function registerRemoveLinks() {
			for (var i=0; i<basketRemoveBtns.length; i++) {
				basketRemoveBtns[i].addEventListener('click', removeFromBasket);
			}	
		}

		function registerChangeQuantityEvents() {
			for (var i=0; i<basketSelectItems.length; i++) {
				basketSelectItems[i].addEventListener('change', changeQuantity);
			}	
		}
		
		function initQuantityBadge() {
			var quantity = calculateQuantityBadge();
			basketCountItems[0].innerHTML = quantity;
			basketCountItems[1].innerHTML = quantity;
		}
		
		function calculateQuantityBadge() {
			var quantity = 0;
			for (var i=0; i<basketSelectItems.length; i++) {
				quantity = Number(quantity) + Number(basketSelectItems[i].value);
			}
			return quantity;
		}
				
		function updateBasketCount() {
			var quantity = calculateQuantityBadge();
			// return, if quantity is greater than 0 and it didn't changed
			if (quantity == basketCountItems[1].innerHTML && quantity != 0) {
				return;
			}
			// refresh quantity-badge
			if (quantity != basketCountItems[1].innerHTML) {
				basketCountItems[1].innerHTML = quantity;
				BasketUtil.addClass(basketCount, 'tx-maagitproduct-basket__count--update');
				setTimeout(function() {basketCountItems[0].innerText = quantity}, 150);
				setTimeout(function() {BasketUtil.removeClass(basketCount, 'tx-maagitproduct-basket__count--update');}, 200);
			}
			// show or hide basket
			if (quantity == 0) {
				var basketIsOpen = (typeof bool === 'undefined') ? BasketUtil.hasClass(basket[0], 'tx-maagitproduct-basket--open') : bool;
				if (basketIsOpen) {
					BasketUtil.removeClass(basket[0], 'tx-maagitproduct-basket--open');
				}
				setTimeout(function() {
					BasketUtil.addClass(basket[0], 'tx-maagitproduct-basket--empty');
				}, 500);
			} else {
				BasketUtil.removeClass(basket[0], 'tx-maagitproduct-basket--empty');
			}
		};
		
		function callMinOrderValueFunction(init) {
			if (typeof(eval(basketMinOrderValueFunction)) == 'function') {
				if ($('#paramBasketAmount').length > 0) {amount = $('#paramBasketAmount').text();} else {amount = 0;}
				if ($('#paramBasketMinOrderValue').length > 0) {minvalue = $('#paramBasketMinOrderValue').text();} else {minvalue = 0;}
				if ($('#paramBasketMinOrderValueReached').length > 0) {reached = $('#paramBasketMinOrderValueReached').text();} else {reached = 0;}
				eval(basketMinOrderValueFunction+'('+amount+', '+minvalue+', '+reached+', '+init+')');
			;}
		}

		function callBasketToggleFunction(init) {
			if (typeof(eval(basketToggleFunction)) == 'function') {
				if (BasketUtil.hasClass(basket[0], 'tx-maagitproduct-basket--open')) {isOpen = 'true';} else {isOpen = 'false';}
				if (isAvailable(basketNotShowByElements)) {dontShow = true;} else {dontShow = false;}
				eval(basketToggleFunction+'('+isOpen+', '+dontShow+', '+init+')');
			;}
		}

		function isAvailable(elements) {
			blnIsAvailable = false;
			for (var i=0; i<elements.length; i++) {
				if (eval(elements[i]).length > 0) {
					blnIsAvailable = true;
					break;
				}
			}
			return blnIsAvailable;
		}
	}
});