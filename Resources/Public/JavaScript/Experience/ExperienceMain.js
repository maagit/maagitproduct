// VARIABLES ----------------------------------------------------------------------- //
var generalScrollTop = 50;


// EVENTS ------------------------------------------------------------------------- //


// MAIN ---------------------------------------------------------------------------- //
function init() {
	fadeStickyNotes($(window));
}

function fadeStickyNotes(scrollElement) {
	$('.stickynotes').each(function() {
		var dataScrollTop = $(this).data('scrolltop');
		if (dataScrollTop == null || dataScrollTop == 'undefined') {dataScrollTop = generalScrollTop};
		fadeOnScroll($(this), scrollElement, dataScrollTop);
	});
}

function fadeOnScroll(fadeElement, scrollElement, scrollTop) {
	if (scrollElement.scrollTop() > scrollTop) {
		$(fadeElement).fadeOut();
	} else {
		$(fadeElement).fadeIn();
	}
}


// AJAX --------------------------------------------------------------------------- //


// HELPER ------------------------------------------------------------------------- //


// JQUERY INIT -------------------------------------------------------------------- //
jQuery(document).ready(function ($) {
	init();
	$(window).scroll(function () {
		fadeStickyNotes($(this));
	});
});