<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_article',
        'label' => 'product',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden'
        ],
        'iconfile' => 'EXT:maagitproduct/Resources/Public/Icons/product.png'
    ],
    'interface' => [
        'maxDBListItems' => 100,
        'maxSingleDBListItems' => 500
    ],
    'types' => [
        '1' => [
			'showitem' => '
				--div--;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_article.tabs.article,
					order, ratingmail, downloadmail, productuid, product, unit, description, image, link, quantity, stock, weight, price, totalamount'
        ]
    ],
	'palettes' => [

	],
    'columns' => [
		'order' => [
            'exclude' => true,
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.prename',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_maagitproduct_domain_model_order',
                'foreign_table_where' => 'AND tx_maagitproduct_domain_model_order.hidden = 0',
				'maxitems' => 1,
				'required' => true,
				'readOnly' => 1
            ]
        ],
		'ratingmail' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_article.ratingmail',
            'config' => [
                'type' => 'datetime'
            ]
        ],
		'downloadmail' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_article.downloadmail',
            'config' => [
                'type' => 'datetime'
            ]
        ],
		'productuid' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_article.productuid',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
				'required' => true,
                'max' => 255,
				'readOnly' => 1
            ]
        ],
		'product' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_article.product',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
				'required' => true,
                'max' => 255,
				'readOnly' => 1
            ]
        ],
		'unit' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_article.unit',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
		'description' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_article.description',
            'config' => [
                'type' => 'text',
                'rows' => 15,
                'cols' => 80,
				'readOnly' => 1
            ]
        ],
		'image' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_article.image',
            'config' => [
				'type' => 'user',
				'renderType' => 'image',
				'parameters' => [
					'showUrl' => true,
					'height' => '80px',
					'emptyMessage' => 'Es ist kein Bild vorhanden'
				]
            ]
        ],
		'link' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_article.link',
            'config' => [
				'type' => 'link',
				'size' => 20,
				'allowedTypes' => ['file', 'url']
            ]
        ],
		'quantity' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_article.quantity',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
		'stock' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_article.stock',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
		'weight' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_article.weight',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
		'price' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_article.price',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
		'totalamount' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_article.totalAmount',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ]
	]
];
