<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount',
        'label' => 'couponcode',
		'label_alt' => 'coupontext',
		'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
		'default_sortby' => 'ORDER BY hidden ASC, couponcode ASC',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime'
        ],
        'iconfile' => 'EXT:maagitproduct/Resources/Public/Icons/discount.png'
    ],
    'interface' => [
        
    ],
    'types' => [
        '1' => [
			'showitem' => '
				--div--;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.tabs.coupon,
					couponcode, coupontext, coupontype, discountamount, discountpercent, discountperarticle, discountperarticlemode, discountperarticleall,
				--div--;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.tabs.condition,
					minorderamount, useonce,
				--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
					hidden, --palette--;;access, fe_groups, fe_users'
        ]
    ],
	'palettes' => [
		'access' => [
			'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access',
			'showitem' => '
				starttime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel,
				endtime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel,
				--linebreak--'
		]
	],
    'columns' => [
		'hidden' => [
			'exclude' => true,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
			'config' => [
				'type' => 'check',
				'renderType' => 'checkboxToggle',
			 	'items' => [
					[
						'label' => 'Visible',
						'labelChecked' => 'Enabled',
						'labelUnchecked' => 'Disabled',
						'invertStateDisplay' => true
					]
				]
			]
		],
		'starttime' => [
			'exclude' => true,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
			'config' => [
				'type' => 'datetime'
			],
			'l10n_mode' => 'exclude',
			'l10n_display' => 'defaultAsReadonly'
		],
		'endtime' => [
			'exclude' => true,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
			'config' => [
				'type' => 'datetime'
			],
			'l10n_mode' => 'exclude',
			'l10n_display' => 'defaultAsReadonly'
		],
		'fe_groups' => [
			'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.fe_groups',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'size' => 5,
				'maxitems' => 20,
				'foreign_table' => 'fe_groups'
			]
		],
		'fe_users' => [
			'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.fe_users',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'size' => 5,
				'maxitems' => 20,
				'foreign_table' => 'fe_users'
			]
		],
		'couponcode' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.couponcode',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
				'required' => true,
                'max' => 255
            ]
        ],
		'coupontext' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.coupontext',
            'config' => [
                'type' => 'text',
                'rows' => 5,
                'cols' => 30
            ]
        ],
		'coupontype' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.coupontype',
			'onChange' => 'reload',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.coupontype.none',
						'value' => null
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.coupontype.amount',
						'value' => 1
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.coupontype.percent',
						'value' => 2
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.coupontype.article',
						'value' => 3
					]
				],
				'required' => true
			]
        ],
		'discountamount' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.discountamount',
			'displayCond' => 'FIELD:coupontype:=:1',
			'config' => [
				'type' => 'number',
				'format' => 'decimal',
				'size' => 10,
				'nullable' => false
			]
        ],
		'discountpercent' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.discountpercent',
			'displayCond' => 'FIELD:coupontype:=:2',
			'config' => [
				'type' => 'number',
				'format' => 'decimal',
				'size' => 10,
				'nullable' => false
			]
        ],
		'discountperarticle' => [
			'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitproduct_domain_model_discount.discountperarticle',
			'displayCond' => 'FIELD:coupontype:=:3',
			'description' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitproduct_domain_model_discount.discountperarticle.description',
			'config' => [
				'type' => 'text',
				'rows' => 6,
				'cols' => 30,
				'size' => 10,
				'eval' => 'trim',
				'required' => true
			]
		],
		'discountperarticlemode' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.discountperarticlemode',
			'onChange' => 'reload',
			'displayCond' => 'FIELD:coupontype:=:3',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.discountperarticlemode.none',
						'value' => null
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.discountperarticlemode.and',
						'value' => 1
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.discountperarticlemode.or',
						'value' => 2
					]
				],
				'required' => true
			]
        ],
		'discountperarticleall' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.discountperarticleall',
			'displayCond' => [
				'AND' => [
					'FIELD:coupontype:=:3',
					'FIELD:discountperarticlemode:=:2'
				]
			],
			'config' => [
				'type' => 'number',
				'format' => 'decimal',
				'size' => 10
			]
        ],
		'minorderamount' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_discount.minorderamount',
			'config' => [
				'type' => 'number',
				'format' => 'decimal',
				'size' => 10
			]
        ],
		'useonce' => [
			'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitproduct_domain_model_discount.useonce',
			'description' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitproduct_domain_model_discount.useonce.description',
			'config' => [
				'type' => 'check',
				'renderType' => 'checkboxToggle',
			 	'items' => [
					[
						'label' => 'Use once',
						'labelChecked' => 'Enabled',
						'labelUnchecked' => 'Disabled'
					]
				]
			]
		]
	]
];
