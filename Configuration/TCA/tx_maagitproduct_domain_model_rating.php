<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitproduct_domain_model_rating',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden'
        ],
        'iconfile' => 'EXT:maagitproduct/Resources/Public/Icons/product.png'
    ],
    'interface' => [
        'maxDBListItems' => 100,
        'maxSingleDBListItems' => 500
    ],
    'types' => [
        '1' => [
			'showitem' => '
				--div--;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitproduct_domain_model_rating.tabs.rating,
					tt_content, name, email, comment, rating'
        ]
    ],
	'palettes' => [

	],
    'columns' => [
		'crdate' => [
			'config' => [
				'type' => 'passthrough'
			]
		],
		'hidden' => [
			'config' => [
				'type' => 'passthrough'
			]
		],
		'tt_content' => [
            'exclude' => true,
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitproduct_ratings',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tt_content',
                'foreign_table_where' => 'AND tt_content.hidden = 0',
				'maxitems' => 1,
				'required' => true,
				'readOnly' => 1
            ]
        ],
		'name' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitproduct_domain_model_rating.name',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'required' => true
            ]
        ],
		'email' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitproduct_domain_model_rating.email',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255
            ]
        ],
		'comment' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitproduct_domain_model_rating.comment',
            'config' => [
                'type' => 'text',
                'rows' => 15,
                'cols' => 80
            ]
        ],
		'rating' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitproduct_domain_model_rating.rating',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_rating.rating.stars.1',
						'value' => '1'
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_rating.rating.stars.1.5',
						'value' => '1.5'
					],
			        [
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_rating.rating.stars.2',
						'value' => '2'
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_rating.rating.stars.2.5',
						'value' => '2.5'
					],
			        [
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_rating.rating.stars.3',
						'value' => '3'
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_rating.rating.stars.3.5',
						'value' => '3.5'
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_rating.rating.stars.4',
						'value' => '4'
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_rating.rating.stars.4.5',
						'value' => '4.5'
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_rating.rating.stars.5',
						'value' => '5'
					]
				],
				'eval' => 'trim',
				'required' => true
            ]
        ]
	]
];