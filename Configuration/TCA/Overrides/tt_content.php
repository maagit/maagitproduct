<?php
defined('TYPO3') || die('Access denied.');

// -------------------------------------------------------------------------------------------------------------------------

// Define backend form
$GLOBALS['TCA']['tt_content']['types']['maagitproduct_content']['showitem'] = '
	'.$GLOBALS['TCA']['tt_content']['types']['textmedia']['showitem'].',
	--div--;LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.title,
	tx_maagitproduct_type;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_type,
	tx_maagitproduct_nr;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_nr,
	tx_maagpitroduct_price;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_price,
	tx_maagpitroduct_weight;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_weight,
	tx_maagpitroduct_shipping;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_shipping,
	tx_maagpitroduct_orderquantity;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_orderquantity,
	tx_maagpitroduct_unit;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_unit,
	tx_maagpitroduct_stock;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_stock,
	tx_maagpitroduct_link;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_link,
	tx_maagpitroduct_basketimage;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_basketimage,
   	tx_maagpitroduct_verkauft;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_verkauft,
	tx_maagpitroduct_ratings;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_ratings,
';

// -------------------------------------------------------------------------------------------------------------------------

// Add type icon class
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['maagitproduct_content'] = 'extensions-maagitproduct_content';

// -------------------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagitproduct',																						// extension name
    'Content',																								// plugin name
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.title',							// plugin title
    'extensions-maagitproduct_content',																		// icon identifier
    'default', 		 																						// group
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.description'					// plugin description
);

// -------------------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagitproduct',																						// extension name
    'List',																									// plugin name
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.list.title',					// plugin title
    'extensions-maagitproduct',																				// icon identifier
    'maagitproduct',  																						// group
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.list.description'				// plugin description
);

// add flexform configuration field
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;Configuration,pi_flexform,',
	$pluginSignature,
	'after:subheader'
);

// add flexform definition file
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	'*',
	'FILE:EXT:maagitproduct/Configuration/FlexForms/List.xml',
	$pluginSignature
);

// -------------------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagitproduct',																						// extension name
    'FilterCategory',																						// plugin name
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.filterCategory.title',			// plugin title
    'extensions-maagitproduct',																				// icon identifier
    'maagitproduct',  																						// group
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.filterCategory.description'		// plugin description
);

// hide standard elements of plugin content
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['maagitproduct_filtercategory'] = 'recursive,pages';

// -------------------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagitproduct',																						// extension name
    'FilterType',																							// plugin name
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.filterType.title',				// plugin title
    'extensions-maagitproduct',																				// icon identifier
    'maagitproduct',  																						// group
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.filterType.description'			// plugin description
);

// hide standard elements of plugin content
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['maagitproduct_filtertype'] = 'recursive,pages';

// -------------------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagitproduct',																						// extension name
    'FilterSearch',																							// plugin name
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.filterSearch.title',			// plugin title
    'extensions-maagitproduct',																				// icon identifier
    'maagitproduct',  																						// group
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.filterSearch.description'		// plugin description
);

// hide standard elements of plugin content
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['maagitproduct_filtersearch'] = 'recursive,pages';

// -------------------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagitproduct',																						// extension name
    'Basket',																								// plugin name
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.basket.title',					// plugin title
    'extensions-maagitproduct_basket',																		// icon identifier
    'maagitproduct',  																						// group
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.basket.description'				// plugin description
);

// add flexform configuration field
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;Configuration,pi_flexform,',
	$pluginSignature,
	'after:subheader'
);

// add flexform definition file
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	'*',
	'FILE:EXT:maagitproduct/Configuration/FlexForms/Basket.xml',
	$pluginSignature
);

// -------------------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagitproduct',																						// extension name
    'Checkout',																								// plugin name
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.checkout.title',				// plugin title
    'extensions-maagitproduct_basket',																		// icon identifier
    'maagitproduct',  																						// group
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.checkout.description'			// plugin description
);

// add flexform configuration field
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;Configuration,pi_flexform,',
	$pluginSignature,
	'after:subheader'
);

// add flexform definition file
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	'*',
	'FILE:EXT:maagitproduct/Configuration/FlexForms/Checkout.xml',
	$pluginSignature
);

// -------------------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagitproduct',																						// extension name
    'Confirmation',																							// plugin name
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.confirmation.title',			// plugin title
    'extensions-maagitproduct_basket',																		// icon identifier
    'maagitproduct',  																						// group
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.confirmation.description'		// plugin description
);

// hide standard elements of plugin content
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['maagitproduct_confirmation'] = 'recursive,pages';

// -------------------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagitproduct',																						// extension name
    'Rating',																								// plugin name
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.rating.title',					// plugin title
    'extensions-maagitproduct_basket',																		// icon identifier
    'maagitproduct',  																						// group
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.rating.description'				// plugin description
);

// add flexform configuration field
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;Configuration,pi_flexform,',
	$pluginSignature,
	'after:subheader'
);

// add flexform definition file
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	'*',
	'FILE:EXT:maagitproduct/Configuration/FlexForms/Rating.xml',
	$pluginSignature
);

// -------------------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagitproduct',																						// extension name
    'Download',																								// plugin name
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.download.title',				// plugin title
    'extensions-maagitproduct_download',																	// icon identifier
    'maagitproduct',  																						// group
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.download.description'			// plugin description
);

// add flexform configuration field
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;Configuration,pi_flexform,',
	$pluginSignature,
	'after:subheader'
);

// add flexform definition file
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	'*',
	'FILE:EXT:maagitproduct/Configuration/FlexForms/Download.xml',
	$pluginSignature
);

// -------------------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagitproduct',																						// extension name
    'Experience',																							// plugin name
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.experience.title',				// plugin title
    'extensions-maagitproduct_rating',																		// icon identifier
    'maagitproduct',  																						// group
    'LLL:EXT:maagitproduct/Resources/Private/Language/locallang.xlf:plugins.experience.description'			// plugin description
);

// add flexform configuration field
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;Configuration,pi_flexform,',
	$pluginSignature,
	'after:subheader'
);

// add flexform definition file
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	'*',
	'FILE:EXT:maagitproduct/Configuration/FlexForms/Experience.xml',
	$pluginSignature
);

// -------------------------------------------------------------------------------------------------------------------------

// Define new columns
$temporaryColumns = [
	'bodytext' => [
		'config' => [
			'type' => 'text',
			'enableRichtext' => true
		]
	],
	'tx_maagitproduct_type' => [
		'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_type',
		'config' => [
			'type' => 'select',
			'renderType' => 'selectSingle',
			'items' => [

			]
		]
	],
	'tx_maagitproduct_nr' => [
		'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_nr',
		'config' => [
			'type' => 'input',
			'size' => 30,
			'max' => 30,
			'eval' => 'trim'
		]
	],
	'tx_maagitproduct_price' => [
		'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_price',
		'config' => [
			'type' => 'number',
			'format' => 'decimal',
			'size' => 10
		]
	],
	'tx_maagitproduct_orderquantity' => [
		'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_orderquantity',
		'description' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_orderquantity.description',
		'config' => [
			'type' => 'input',
			'size' => 30,
			'max' => 100,
			'eval' => 'trim'
		]
	],
	'tx_maagitproduct_unit' => [
		'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitproduct_unit',
		'description' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitproduct_unit.description',
		'config' => [
			'type' => 'text',
			'rows' => 6,
			'cols' => 30,
			'size' => 10,
			'eval' => 'trim'
		]
	],
	'tx_maagitproduct_stock' => [
		'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitproduct_stock',
		'config' => [
			'type' => 'number',
			'size' => 10
		]
	],
	'tx_maagitproduct_weight' => [
		'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_weight',
		'config' => [
			'type' => 'number',
			'format' => 'decimal',
			'size' => 10
		]
	],
	'tx_maagitproduct_shipping' => [
		'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_shipping',
		'config' => [
			'type' => 'number',
			'format' => 'decimal',
			'size' => 10
		]
	],
	'tx_maagitproduct_link' => [
		'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_link',
		'config' => [
			'type' => 'link',
			'size' => 20,
			'allowedTypes' => ['file', 'url']
		]
	],
	'tx_maagitproduct_basketimage' => [
		'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_basketimage',
		'config' => [
			'type' => 'file',
			'maxitems' => 1,
			'allowed' => 'common-image-types',
			'apperance' => [
				'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference',
				'enabledControls' => [
					'info' => false,
					'new' => false,
					'dragdrop' => false,
					'sort' => false,
					'hide' => false,
					'delete' => true,
					'localize' => false
				],
				'collapseAll' => 1
			]
		]
	],
	'tx_maagitproduct_verkauft' => [
		'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitprodct_verkauft',
		'config' => [
			'type' => 'check',
			'default' => '0'
		]
	],
    'tx_maagitproduct_ratings' => [
        'exclude' => true,
        'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_ratings',
        'config' => [
            'type' => 'inline',
            'foreign_table' => 'tx_maagitproduct_domain_model_rating',
			'foreign_table_where' => 'AND tx_maagitproduct_domain_model_rating.hidden = 0',
            'foreign_field' => 'tt_content',
            'appearance' => [
                'collapseAll' => 1,
                'expandSingle' => 1
			]
        ]
    ]
];

// Add new columns to tt_content
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
        'tt_content',
        $temporaryColumns
);

// Add new columns to tca types
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tt_content',
        'tx_maagitproduct_type,
		 tx_maagitproduct_nr, 
		 tx_maagitproduct_price, 
		 tx_maagitproduct_orderquantity,
		 tx_maagitproduct_unit,
		 tx_maagitproduct_stock,
		 tx_maagitproduct_weight,
		 tx_maagitproduct_shipping,
		 tx_maagitproduct_link,
		 tx_maagitproduct_basketimage,
		 tx_maagitproduct_verkauft,
		 --div--;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xlf:tx_maagitproduct_domain_model_rating.tabs.rating,
		 tx_maagitproduct_ratings',
		 'maagitproduct_content'
);

// -------------------------------------------------------------------------------------------------------------------------
?>