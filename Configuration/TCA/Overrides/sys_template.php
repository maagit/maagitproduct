<?php
call_user_func(function () {
	// add general typoscript code
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('maagitproduct', 'Configuration/TypoScript', 'Maagitproduct');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('maagitproduct', 'Configuration/TypoScript/DefaultStyles', 'Maagitproduct basket on top CSS (optional)');
});
?>