<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order',
        'label' => 'crdate',
		'label_userFunc' => \Maagit\Maagitproduct\Userfuncs\Tca::class.'->order',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
		'default_sortby' => 'ORDER BY crdate DESC',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden'
        ],
        'iconfile' => 'EXT:maagitproduct/Resources/Public/Icons/basket.png'
    ],
    'interface' => [
        
    ],
    'types' => [
        '1' => [
			'showitem' => '
				--div--;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.tabs.process,
					paymentdone, shippingdone, rating, download,
				--div--;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.tabs.address,
					salutation, firma, prename, lastname, street, housenumber, houseadd, address, po, city, pocity, country, telephone, mobile, email, sex, birthdate, message,
				--div--;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.tabs.address.billing,
					bill, billingsalutation, billingfirma, billingprename, billinglastname, billingstreet, billinghousenumber, billinghouseadd, billingaddress, billingpo, billingcity, billingpocity, billingcountry, billingtelephone, billingmobile, billingemail, billingsex, billingbirthdate, billingmessage,
				--div--;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.tabs.delivery_payment,
					taxpercent, delivery, payment, paymenttransactionid, paymenttext,
				--div--;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.tabs.user_coupon,
					userid, couponcode, coupontext,
				--div--;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.tabs.amount,
					basketamount, taxamount, shipping, expenses, coupondiscount, totalamount,
				--div--;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.tabs.order,
					orderid, terms, receiver, senderconfirmation, senderconfirmationfrom, downloadsecure, downloadfrom,
				--div--;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.tabs.article,
					articles'
        ]
    ],
	'palettes' => [

	],
    'columns' => [
        'crdate' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.crdate',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim,date',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
		'paymentdone' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.paymentdone',
            'config' => [
                'type' => 'datetime'
            ]
        ],
		'shippingdone' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.shippingdone',
            'config' => [
                'type' => 'datetime'
            ]
        ],
		'rating' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.rating',
            'config' => [
                'type' => 'user',
				'renderType' => 'rating'
            ]
        ],
		'download' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.download',
            'config' => [
                'type' => 'user',
				'renderType' => 'download'
            ]
        ],
		'orderid' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.orderId',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
		'salutation' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.salutation',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'firma' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.firma',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'prename' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.prename',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'lastname' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.lastname',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'street' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.street',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'housenumber' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.houseNumber',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'houseadd' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.houseAdd',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'address' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.address',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'po' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.po',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'city' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.city',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'pocity' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.poCity',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'country' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.country',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'telephone' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.telephone',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'mobile' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.mobile',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'email' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.email',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'message' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.message',
            'config' => [
                'type' => 'text',
                'rows' => 15,
                'cols' => 80,
				'readOnly' => 1
            ]
        ],
        'sex' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.sex',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'birthdate' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.birthdate',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'bill' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.bill',
			'config' => [
                'type' => 'check',
				'readOnly' => 1
            ]
        ],
		'billingsalutation' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.salutation',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billingfirma' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.firma',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billingprename' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.prename',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billinglastname' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.lastname',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billingstreet' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.street',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billinghousenumber' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.houseNumber',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billinghouseadd' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.houseAdd',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billingaddress' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.address',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billingpo' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.po',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billingcity' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.city',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billingpocity' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.poCity',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billingcountry' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.country',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billingtelephone' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.telephone',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billingmobile' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.mobile',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billingemail' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.email',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billingmessage' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.message',
            'config' => [
                'type' => 'text',
                'rows' => 15,
                'cols' => 80,
				'readOnly' => 1
            ]
        ],
        'billingsex' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.sex',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'billingbirthdate' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.birthdate',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'taxpercent' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.taxPercent',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'basketamount' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.basketAmount',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'taxamount' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.taxAmount',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'delivery' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.delivery',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'shipping' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.shipping',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'payment' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.payment',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'paymenttransactionid' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.paymentTransactionId',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'paymenttext' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.paymentText',
            'config' => [
                'type' => 'text',
                'rows' => 15,
                'cols' => 80,
				'readOnly' => 1
            ]
        ],
        'expenses' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.expenses',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'coupondiscount' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.coupondiscount',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'userid' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.userid',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'couponcode' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.couponcode',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'coupontext' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.coupontext',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'terms' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.terms',
            'config' => [
                'type' => 'user',
				'renderType' => 'terms'
            ]
        ],
        'receiver' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.receiver',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'senderconfirmation' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.senderConfirmation',
			'config' => [
                'type' => 'check',
				'readOnly' => 1
            ]
        ],
        'senderconfirmationfrom' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.senderConfirmationFrom',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'downloadsecure' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.downloadSecure',
			'config' => [
                'type' => 'check',
				'readOnly' => 1
            ]
        ],
        'downloadfrom' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.downloadFrom',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'totalamount' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.totalAmount',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'readOnly' => 1
            ]
        ],
        'articles' => [
            'exclude' => true,
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_order.articles',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_maagitproduct_domain_model_article',
				'foreign_table_where' => 'AND tx_maagitproduct_domain_model_article.hidden = 0',
                'foreign_field' => 'order',
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 1,
				]
			]
        ]
	]
];
