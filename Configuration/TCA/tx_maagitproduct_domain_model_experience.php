<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience',
        'label' => 'crdate',
		'label_userFunc' => \Maagit\Maagitproduct\Userfuncs\Tca::class.'->experienceRating',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
		'default_sortby' => 'ORDER BY crdate DESC',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden'
        ],
        'iconfile' => 'EXT:maagitproduct/Resources/Public/Icons/rating.png'
    ],
    'interface' => [

    ],
    'types' => [
        '1' => [
			'showitem' => '
				--div--;LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.tabs.rating,
					name, email, comment, rating'
        ]
    ],
	'palettes' => [

	],
    'columns' => [
		'crdate' => [
			//'config' => [
			//	'type' => 'passthrough'
			//]
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.crdate',
			'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim,date',
                'max' => 255,
				'readOnly' => 1
            ]
		],
		'hidden' => [
			'config' => [
				'type' => 'passthrough'
			]
		],
		'name' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.name',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
				'required' => true
            ]
        ],
		'email' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.email',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255
            ]
        ],
		'comment' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.comment',
            'config' => [
                'type' => 'text',
                'rows' => 15,
                'cols' => 80
            ]
        ],
		'rating' => [
            'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.rating',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.rating.stars.1',
						'value' => '1'
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.rating.stars.1.5',
						'value' => '1.5'
					],
			        [
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.rating.stars.2',
						'value' => '2'
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.rating.stars.2.5',
						'value' => '2.5'
					],
			        [
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.rating.stars.3',
						'value' => '3'
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.rating.stars.3.5',
						'value' => '3.5'
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.rating.stars.4',
						'value' => '4'
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.rating.stars.4.5',
						'value' => '4.5'
					],
					[
						'label' => 'LLL:EXT:maagitproduct/Resources/Private/Language/locallang_db.xml:tx_maagitproduct_domain_model_experience.rating.stars.5',
						'value' => '5'
					]
				],
				'eval' => 'trim',
				'required' => true
            ]
        ]
	]
];