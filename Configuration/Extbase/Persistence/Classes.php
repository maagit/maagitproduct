<?php
declare(strict_types = 1);

return [
	// Content property mapping
	\Maagit\Maagitproduct\Domain\Model\Product::class => [
        'tableName' => 'tt_content',
        'recordType' => \Maagit\Maagitproduct\Domain\Model\Product::class,
		'properties' => [
			'type' => [
				'fieldName' => 'tx_maagitproduct_type',
			]
		]
    ],

	// Category property mapping
	\Maagit\Maagitproduct\Domain\Model\Category::class => [
        'tableName' => 'sys_category',
        'recordType' => \Maagit\Maagitproduct\Domain\Model\Category::class
	],
			
	// User property mapping
	\Maagit\Maagitproduct\Domain\Model\User::class => [
		'tableName' => 'fe_users'
	]
];
