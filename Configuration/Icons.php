<?php
	return [
		'extensions-maagitproduct_content' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
			'source' => 'EXT:maagitproduct/Resources/Public/Icons/tt_content_product.png',
		],
		'extensions-maagitproduct' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
			'source' => 'EXT:maagitproduct/Resources/Public/Icons/product.png',
		],
		'extensions-maagitproduct_basket' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
			'source' => 'EXT:maagitproduct/Resources/Public/Icons/basket.png',
		],
		'extensions-maagitproduct_rating' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
			'source' => 'EXT:maagitproduct/Resources/Public/Icons/rating.png',
		],
		'extensions-maagitproduct_download' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
			'source' => 'EXT:maagitproduct/Resources/Public/Icons/download.png',
		]
	];
?>